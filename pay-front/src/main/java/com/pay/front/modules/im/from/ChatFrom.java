package com.pay.front.modules.im.from;

import lombok.Data;

import java.io.Serializable;


@Data
public class ChatFrom implements Serializable {

    private String data;

}
