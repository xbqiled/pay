package com.pay.front.modules.im.from;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class SearchUserFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;

    @ApiModelProperty("组织机构编码")
    private Long orgId;

    @ApiModelProperty("搜索关键字")
    private String keyStr;

}
