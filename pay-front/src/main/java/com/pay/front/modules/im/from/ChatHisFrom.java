package com.pay.front.modules.im.from;

import lombok.Data;

import java.io.Serializable;


@Data
public class ChatHisFrom implements Serializable {

    private String userId;

    private Long orgId;

    private String singIn;

}
