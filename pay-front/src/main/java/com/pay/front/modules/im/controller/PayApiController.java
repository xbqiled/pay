package com.pay.front.modules.im.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.*;
import com.pay.api.modules.im.vo.ImConfigVo;
import com.pay.api.modules.im.vo.ImUserInfoVo;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.api.modules.tools.service.OssStsTokenService;
import com.pay.common.core.constant.EnumConstant;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.R;
import com.pay.common.core.utils.Result;
import com.pay.front.modules.im.from.*;
import com.pay.front.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pay.common.core.constant.PayConstant.*;


@Api(tags = "信博银商接口")
@RestController
@RequestMapping("/imApi")
public class PayApiController {

    //用户信息远程服务
    @Reference(version = "${api.service.version}")
    private ImUserInfoService imUserInfoService;

    //离线信息服务
    @Reference(version = "${api.service.version}")
    private ImOfflineMessageService imOfflineMessageService;

    //离线通知服务
    @Reference(version = "${api.service.version}")
    private ImOfflineNoticeService imOfflineNoticeService;

    @Reference(version = "${api.service.version}")
    private ImMessageService imMessageService;

    //oss配置信息
    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;

    //ossToken配置信息
    @Reference(version = "${api.service.version}")
    private OssStsTokenService ossStsTokenService;

    //系统配置信息
    @Reference(version = "${api.service.version}")
    private ImConfigService imConfigService;

    //黑名单列表
    @Reference(version = "${api.service.version}")
    private ImBlackService imBlackService;

    //获取redis服务
    @Autowired
    private TokenService tokenService;


    @ApiOperation(value = "注册用户信息", notes = "注册用户信息")
    @RequestMapping(value = "/registerUser", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R registerUser(@RequestBody RegisterFrom registerFrom) {
        //判断请求参数
        if (ObjectUtil.isEmpty(registerFrom.getPasswd()) || StrUtil.isEmpty(registerFrom.getAuthCode())
                || StrUtil.isEmpty(registerFrom.getNickName())) {
            return R.parmError("请求参数错误!");
        }

        Result result = imUserInfoService.createUser(registerFrom.getOrgId(), registerFrom.getChanneld(), registerFrom.getAuthCode(),
                registerFrom.getPlatformId(), registerFrom.getNickName(), registerFrom.getPasswd());
        if (result.getCode() == EnumConstant.RESULT_CODE.CODE_OK.getValue()) {
            return R.ok("创建用户成功!").put("data", result.getData());
        } else if (result.getCode() == EnumConstant.RESULT_CODE.CODE_REEOR.getValue()) {
            return R.error(ERROR_CODE_ONE, "组织机构代码错误!");
        } else if (result.getCode() == EnumConstant.RESULT_CODE.CODE_FAIL.getValue()) {
            return R.error(ERROR_CODE_TWO, "鉴权码错误!");
        } else if (result.getCode() == EnumConstant.RESULT_CODE.CODE_VERIFYERROR.getValue()) {
            return R.error(ERROR_CODE_THREE, "注册ID已经被使用!");
        } else if (result.getCode() == EnumConstant.RESULT_CODE.CODE_VERIFYFAIL.getValue()) {
            return R.error(ERROR_CODE_FOUR, "昵称已经被使用!");
        } else {
            return R.error(ERROR_CODE_FIVE, "未知错误!");
        }
    }


    /**
     * <B>获取用户信息</B>
     *
     * @param userInfoFrom
     * @return
     */
    @ApiOperation(value = "获取用户详细信息", notes = "根据用户ID和用户ORGCODE查询用户详细信息")
    @RequestMapping(value = "/getUserInfo", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getUserInfo(@RequestBody UserInfoFrom userInfoFrom) {
        //判断请求参数
        if (ObjectUtil.isEmpty(userInfoFrom.getUserId()) || StrUtil.isEmpty(userInfoFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(userInfoFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        //得到组织对应的用户信息
        if (!userInfoFrom.getOrgId().equals(userInfo.getOrgId())) {
            return R.error(ERROR_CODE_ONE, "只能获取本组织机构下的信息!");
        }

        ImUserInfoVo resultUserInfo = imUserInfoService.getUserInfoById(userInfoFrom.getUserId(), userInfo.getId());
        if (resultUserInfo != null) {
            return R.ok("获取用户信息成功!").put("data", resultUserInfo);
        } else {
            return R.error(ERROR_CODE_TWO, "找不到对应的用户信息!");
        }
    }


    @ApiOperation(value = "获取个人信息", notes = "获取个人信息")
    @RequestMapping(value = "/getSelfInfo", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getSelfInfo(@RequestBody SelfFrom selfFrom) {
        //判断请求参数
        if (StrUtil.isEmpty(selfFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(selfFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        ImUserInfoEntity resultUserInfo = imUserInfoService.findUserById(userInfo.getId());
        if (resultUserInfo != null) {
            return R.ok("获取个人信息成功!").put("data", resultUserInfo);
        } else {
            return R.error(ERROR_CODE_ONE, "获取个人信息失败!");
        }
    }


    @ApiOperation(value = "搜索用户信息", notes = "根据用户账号，手机号，昵称搜索用户")
    @RequestMapping(value = "/searchUserInfo", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R searchUserInfo(@RequestBody SearchUserFrom searchUserFrom) {
        //判断请求参数
        if (StrUtil.isEmpty(searchUserFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(searchUserFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        if (!searchUserFrom.getOrgId().equals(userInfo.getOrgId())) {
            return R.error(ERROR_CODE_ONE, "只能获取本组织机构下的信息!");
        }

        List<ImUserInfoVo> imUserInfoList = imUserInfoService.findUserByKey(searchUserFrom.getKeyStr(), userInfo.getId().toString());
        if (CollUtil.isNotEmpty(imUserInfoList)) {
            Map<String, Object> map = new HashMap<>();
            map.put("content", imUserInfoList);
            return R.ok("获取搜索信息成功!").put("data", map);
        } else {
            return R.error(ERROR_CODE_ONE, "未找到对应的用户信息!");
        }
    }


    @ApiOperation(value = "修改个人密码信息", notes = "修改个人密码信息")
    @RequestMapping(value = "/modifySelfPw", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getSelfInfo(@RequestBody SelfUserPassWordFrom selfUserPassWordFrom) {
        //判断请求参数
        if (StrUtil.isEmpty(selfUserPassWordFrom.getToken()) || StrUtil.isEmpty(selfUserPassWordFrom.getOldPw()) || StrUtil.isEmpty(selfUserPassWordFrom.getNewPw())) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(selfUserPassWordFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        Integer result = imUserInfoService.modifyPw(userInfo.getId(), selfUserPassWordFrom.getOldPw(), selfUserPassWordFrom.getNewPw());
        if (result == EnumConstant.RESULT_CODE.CODE_OK.getValue()) {
            return R.ok("修改个人密码成功!");
        } else if (result == EnumConstant.RESULT_CODE.CODE_REEOR.getValue()) {
            return R.error(ERROR_CODE_ONE, "旧密码错误!");
        } else {
            return R.error(ERROR_CODE_TWO, "修改密码失败!");
        }
    }


    /**
     * <B>修改个人信息</B>
     *
     * @param selfInfoFrom
     * @return
     */
    @ApiOperation(value = "修改个人信息", notes = "修改用户自己的个人详细信息")
    @RequestMapping(value = "/modifySelfInfo", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R modifySelfInfo(@RequestBody SelfInfoFrom selfInfoFrom) {
        //判断请求参数
        if (StrUtil.isEmpty(selfInfoFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        //判断至少有一个参数修改
        if (StrUtil.isNotEmpty(selfInfoFrom.getNickName()) || ObjectUtil.isNotEmpty(selfInfoFrom.getSex())
                || StrUtil.isNotEmpty(selfInfoFrom.getSign()) || StrUtil.isNotEmpty(selfInfoFrom.getPhone()) || StrUtil.isNotEmpty(selfInfoFrom.getAddress())
                || StrUtil.isNotEmpty(selfInfoFrom.getEmail()) || StrUtil.isNotEmpty(selfInfoFrom.getBirthDay()) || StrUtil.isNotEmpty(selfInfoFrom.getCardNumber())
                || StrUtil.isNotEmpty(selfInfoFrom.getSchool()) || StrUtil.isNotEmpty(selfInfoFrom.getUserAvatar()) || ObjectUtil.isNotEmpty(selfInfoFrom.getEducation())) {
        } else {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(selfInfoFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        ImUserInfoEntity imUserInfo = new ImUserInfoEntity();
        imUserInfo.setId(userInfo.getId());
        BeanUtil.copyProperties(selfInfoFrom, imUserInfo);
        //修改用户信息
        ImUserInfoEntity imUserInfoEntity = imUserInfoService.modifyUserInfo(imUserInfo);
        if (imUserInfoEntity != null) {
            return R.ok("修改用户信息成功!").put("data", imUserInfoEntity);
        } else {
            return R.error(ERROR_CODE_ONE, "昵称已经被占用!");
        }
    }



    /**
     * <B>获取自己的离线信息列表</B>
     *
     * @param offlineMessageListFrom
     * @return
     */
    @ApiOperation(value = "获取个人离线信息列表", notes = "获取个人离线信息列表")
    @RequestMapping(value = "/getOfflineMessagePage", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getOfflineMessageList(@RequestBody OfflineMessageListFrom offlineMessageListFrom) {
        //获取自己的离线信息列表
        if (StrUtil.isEmpty(offlineMessageListFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        if (offlineMessageListFrom.getPage() == null) {
            return R.parmError("请求参数错误!");
        }

        //获取tonken信息对应的用户
        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(offlineMessageListFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        PageUtils pageObj = imOfflineMessageService.queryOfflineMessagePage(userInfo.getId(), offlineMessageListFrom.getPage().getPageSize(),
                offlineMessageListFrom.getPage().getPageIndex(), offlineMessageListFrom.getPage().getTimeOrder());
        return R.ok("获取用户离线消息列表信息成功!").put("data", pageObj);
    }



    @ApiOperation(value = "获取对应群组或用户发来的信息", notes = "获取对应群组或用户发来的信息")
    @RequestMapping(value = "/getDesignatOfflineMessagePage", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getDesignatOfflineMessagePage(@RequestBody DesignatMessageFrom designatMessageFrom) {
        //判断请求参数
        if (StrUtil.isEmpty(designatMessageFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        if (designatMessageFrom.getPage() == null) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(designatMessageFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }
        PageUtils pageObj = null;

        //获取离线消息分组信息
//        if (StrUtil.isNotBlank(designatMessageFrom.getLastMessageId())) {
//            pageObj = imMessageService.getMessagePage(designatMessageFrom.getLastMessageId(), userInfo.getId(), designatMessageFrom.getChatType(), designatMessageFrom.getFromId(), designatMessageFrom.getPage().getPageSize(), designatMessageFrom.getPage().getPageIndex(), designatMessageFrom.getPage().getTimeOrder());
//        } else {
//            pageObj = imOfflineMessageService.getOfflineMessagePage(userInfo.getId(), designatMessageFrom.getChatType(), designatMessageFrom.getFromId(), designatMessageFrom.getPage().getPageSize(), designatMessageFrom.getPage().getPageIndex(), designatMessageFrom.getPage().getTimeOrder());
//        }

        return R.ok("获取用户离线消息列表成功!").put("data", pageObj);
    }


    @ApiOperation(value = "获取对应群组或用户发来的信息", notes = "获取对应群组或用户发来的信息")
        @RequestMapping(value = "/getDesignatMessagePage", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
        private R getSelfMessagePage(@RequestBody DesignatMessageFrom designatMessageFrom) {
            //判断请求参数
            if (StrUtil.isEmpty(designatMessageFrom.getToken())) {
                return R.parmError("请求参数错误!");
            }

        if (designatMessageFrom.getPage() == null) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(designatMessageFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        //获取离线消息分组信息
//        PageUtils pageObj = imMessageService.getMessagePage(null, userInfo.getId(), designatMessageFrom.getChatType(), designatMessageFrom.getFromId(), designatMessageFrom.getPage().getPageSize(), designatMessageFrom.getPage().getPageIndex(), designatMessageFrom.getPage().getTimeOrder());
//        return R.ok("获取用户消息列表成功!").put("data", pageObj);
        return null;
    }


    /**
     * <B>获取自己的离线通知</B>
     *
     * @param offlineNoticeListFrom
     * @return
     */
    @ApiOperation(value = "获取个人离线通知列表", notes = "获取个人离线通知列表")
    @RequestMapping(value = "/getOfflineNoticePage", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getOfflineNoticeList(@RequestBody OfflineNoticeListFrom offlineNoticeListFrom) {
        //获取自己的离线通知
        if (StrUtil.isEmpty(offlineNoticeListFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        if (offlineNoticeListFrom.getPage() == null) {
            return R.parmError("请求参数错误!");
        }

        //获取tonken信息对应的用户
        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(offlineNoticeListFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        PageUtils pageObj = imOfflineNoticeService.queryOfflineNoticePage(userInfo.getId(), offlineNoticeListFrom.getPage().getPageSize(), offlineNoticeListFrom.getPage().getPageIndex(), offlineNoticeListFrom.getPage().getTimeOrder());
        return R.ok("获取用户离线消息列表信息成功!").put("data", pageObj);
    }


    @ApiOperation(value = "获取系统配置信息", notes = "获取系统配置信息")
    @RequestMapping(value = "/getSystemConfig", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getSystemConfig(@RequestBody SystemConfigFrom systemConfigFrom) {
        //获取自己的离线通知
        if (StrUtil.isEmpty(systemConfigFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        //获取tonken信息对应的用户
        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(systemConfigFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        ImConfigVo imConfigVo = imConfigService.getConfigByOrgId(systemConfigFrom.getOrgId());
        if (imConfigVo != null) {
            return R.ok("获取系统配置信息成功!").put("data", imConfigVo);
        } else {
            return R.error(ERROR_CODE_TWO, "找不到对应的系统配置信息!");
        }
    }


    @ApiOperation(value = "批量删除离线信息", notes = "批量删除离线信息")
    @RequestMapping(value = "/delOffLineMessage", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R delOffLineMessage(@RequestBody DelOfflineMessageFrom delOfflineMessageFrom) {
        if (StrUtil.isEmpty(delOfflineMessageFrom.getToken()) || ObjectUtil.isEmpty(delOfflineMessageFrom.getType())) {
            return R.parmError("请求参数错误!");
        }

        //判断是否存在信息
        if (delOfflineMessageFrom.getType() == 1 && (delOfflineMessageFrom.getIdArray() == null || delOfflineMessageFrom.getIdArray().size() == 0)) {
            return R.parmError("ID不能为空!");
        }

        //判断是否存在信息
        if (delOfflineMessageFrom.getType() > 1 && ObjectUtil.isEmpty(delOfflineMessageFrom.getSourceId())) {
            return R.parmError("来源ID不能为空!");
        }

        //获取tonken信息对应的用户
        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(delOfflineMessageFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

//        Integer result = imOfflineMessageService.batchDelOfflineMessage(delOfflineMessageFrom.getIdArray(), delOfflineMessageFrom.getType(), userInfo.getId(), delOfflineMessageFrom.getSourceId());
        return R.ok("批量删除离线信息成功!");
    }




    @ApiOperation(value = "获取用户黑名单列表", notes = "获取用户黑名单列表")
    @RequestMapping(value = "/getUserBlackList", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getUserBlackList(@RequestBody UserBlackFrom userBlackFrom) {
        if (StrUtil.isEmpty(userBlackFrom.getToken())) {
            return R.parmError("请求参数错误!");
        }

        //获取tonken信息对应的用户
        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(userBlackFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

//        List<ImBlackVo> resultList = imBlackService.getBlackListByUserId(userInfo.getId());
//        return R.ok("获取您的黑名单列表成功!").put("data", resultList);
        return null;
    }


}
