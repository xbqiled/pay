package com.pay.front.modules.im.from;

import com.pay.common.core.utils.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class DesignatMessageFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;

    @ApiModelProperty("最后一条消息的id")
    private String lastMessageId;

    @ApiModelProperty("查询类型:2为用户,1位群组")
    private Integer chatType;

    @ApiModelProperty("用户或群组ID")
    private Integer fromId;

    @ApiModelProperty("分页查询信息")
    private Page page;

}
