package com.pay.front.modules.im.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class InitModel implements Serializable {

    private Integer platformType;

    private String userId;

    private String channelId;

    private String nickName;

}
