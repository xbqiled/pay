package com.pay.front.modules.im.from;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SystemConfigFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;


    @ApiModelProperty("组织机构编码")
    private Long orgId;

}
