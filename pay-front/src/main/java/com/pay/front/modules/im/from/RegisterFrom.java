package com.pay.front.modules.im.from;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class RegisterFrom implements Serializable {

    @ApiModelProperty("平台ID,如果不是来源平台就为空")
    private String platformId;

    @ApiModelProperty("组织机构编码")
    private Long orgId;

    @ApiModelProperty("渠道编码")
    private String channeld;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("鉴权码")
    private String authCode;

    @ApiModelProperty("密码")
    private String passwd;

}
