package com.pay.front.modules.im.from;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class UserInfoFrom  implements Serializable {
    @ApiModelProperty("用户ID")
    private Integer userId;
    @ApiModelProperty("组织机构编码")
    private Long orgId;
    @ApiModelProperty("用户token")
    private String token;
}
