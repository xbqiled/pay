package com.pay.front.modules.im.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class CsModel implements Serializable {
    private Long orgId;
    private Integer isOnLine;
}

