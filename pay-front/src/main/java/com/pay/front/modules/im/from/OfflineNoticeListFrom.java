package com.pay.front.modules.im.from;

import com.pay.common.core.utils.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class OfflineNoticeListFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;


    @ApiModelProperty("分页查询信息")
    private Page page;

}
