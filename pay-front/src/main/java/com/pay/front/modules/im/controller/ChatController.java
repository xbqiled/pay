package com.pay.front.modules.im.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.pay.api.json.PayConfig;
import com.pay.api.json.TraderReplyConfig;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.ImChannelService;
import com.pay.api.modules.im.service.ImMessageService;
import com.pay.api.modules.im.service.ImUserInfoService;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import com.pay.api.modules.trader.service.TraderReplyConfigService;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.exception.AppException;
import com.pay.common.core.oss.CloudStorageConfig;
import com.pay.common.core.utils.OSSFactory;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.R;
import com.pay.common.core.utils.SignUtil;
import com.pay.front.modules.im.from.CsFrom;
import com.pay.front.modules.im.from.InItUserFrom;
import com.pay.front.modules.im.from.UserChatHisFrom;
import com.pay.front.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Api(tags = "聊天接口")
@RestController
@RequestMapping("/chatApi")
public class ChatController {

    @Value("${ift.websocket.url}")
    private String websocketurl;

    @Value("${ift.http.url}")
    private String httpurl;
    //UserDetails
    //用户信息远程服务
    @Reference(version = "${api.service.version}")
    private ImUserInfoService imUserInfoService;

    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;

    @Reference(version = "${api.service.version}")
    private ImMessageService imMessageService;

    @Reference(version = "${api.service.version}")
    private ImChannelService imChannelService;

    @Reference(version = "${api.service.version}")
    private TraderReplyConfigService traderReplyConfigService;

    //获取redis服务
    @Autowired
    private TokenService tokenService;


    @CrossOrigin(origins = "*")
    @ApiOperation(value = "显示支付信息列表", notes = "显示支付信息列表")
    @RequestMapping(value = "/showPay", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private ModelAndView showPay(Long orgId, Integer type, Model model, HttpServletRequest request) {
        //List<TraderReplyConfig> configList = new ArrayList<>();

        //判断请求参数
        if (ObjectUtil.isEmpty(orgId)) {
            return null;
        }

        /*List<TraderReplyConfigEntity> list = imMessageService.selectConfigList(orgId, type);
        if (CollUtil.isNotEmpty(list)) {
            for (TraderReplyConfigEntity traderReplyConfigEntity : list) {

                if (traderReplyConfigEntity != null && StrUtil.isNotEmpty(traderReplyConfigEntity.getConfig())) {
                    TraderReplyConfig config = new Gson().fromJson(traderReplyConfigEntity.getConfig(), TraderReplyConfig.class);
                    config.setType(traderReplyConfigEntity.getType());
                    config.setPayName(traderReplyConfigEntity.getNote());
                    configList.add(config);
                }
            }
        }*/
        TraderReplyConfigEntity ret = imMessageService.getPayConfig(orgId, type);
        if (ret != null) {
            TraderReplyConfig config = new Gson().fromJson(ret.getConfig(), TraderReplyConfig.class);
            config.setType(ret.getType());
            config.setPayName(ret.getNote());
            model.addAttribute("payConfig", config);
        }
        //model.addAttribute("payChannelList", configList);

        return new ModelAndView("/payChannel");
    }


    @CrossOrigin(origins = "*")
    @ApiOperation(value = "获取聊天用户历史信息", notes = "获取聊天用户历史信息")
    @RequestMapping(value = "/getUserChatHis", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getUserChatHis(@RequestBody UserChatHisFrom userChatHisFrom) {
        //判断请求参数
        if (userChatHisFrom.getUserId() == null || ObjectUtil.isEmpty(userChatHisFrom.getToken()) || userChatHisFrom.getPage() == null) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = tokenService.findUserInfoByToken(userChatHisFrom.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        PageUtils page = imMessageService.getUserMessageHisPage(Integer.valueOf(userChatHisFrom.getUserId()), userInfo.getId(),
                userChatHisFrom.getPage().getPageSize(),
                userChatHisFrom.getPage().getPageIndex(), userChatHisFrom.getPage().getTimeOrder());
        return R.ok("获取用户消息列表信息成功!").put("data", page);
    }

    @GetMapping(value = "/getCsPayMethods/{csId}", produces = "application/json;charset=utf-8")
    public R getPayMethods(@PathVariable("csId") Integer csId) {
        List<PayConfig> payConfigList = traderReplyConfigService.getReplyConfigList(csId);
        return R.ok().put("data", payConfigList.stream().map(PayConfig::getType).collect(Collectors.toList()));
    }


    @ApiOperation(value = "初始化用户信息", notes = "初始化用户信息")
    @RequestMapping(value = "/initUser", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R initUser(@RequestBody InItUserFrom inItUserFrom) {
        //判断请求参数
        if (inItUserFrom.getUserId() == null || StrUtil.isEmpty(inItUserFrom.getChannelId()) ||
                StrUtil.isEmpty(inItUserFrom.getNickName()) || StrUtil.isEmpty(inItUserFrom.getSgin())
                || ObjectUtil.isEmpty(inItUserFrom.getPlatformType())) {
            return R.parmError("请求参数错误!");
        }

        ImChannelEntity imChannelEntity = imChannelService.findByChannelId(inItUserFrom.getChannelId());
        if (imChannelEntity == null || StrUtil.isEmpty(imChannelEntity.getAuthCode())) {
            return R.parmError("您没有相关权限!");
        }

        String signStr = imChannelEntity.getAuthCode();
        StringBuilder sb = new StringBuilder();
        sb.append("channelId=" + inItUserFrom.getChannelId());
        sb.append("&nickName=" + inItUserFrom.getNickName());
        sb.append("&platformType=" + inItUserFrom.getPlatformType());
        sb.append("&userId=" + inItUserFrom.getUserId());
        sb.append("&key=" + signStr);
        String md5 = SignUtil.encryption(sb.toString());

        if (StrUtil.equals(md5, inItUserFrom.getSgin())) {
            ImUserInfoEntity userInfo = imUserInfoService.initCacUser(inItUserFrom.getUserId(),
                    inItUserFrom.getPlatformType(), inItUserFrom.getChannelId(), inItUserFrom.getNickName());
            return R.ok("获取用户消息列表信息成功!").put("data", userInfo);
        } else {
            return R.parmError("您没有相关权限!");
        }
    }


    @ApiOperation(value = "获取客服列表", notes = "获取客服列表")
    @RequestMapping(value = "/getCsList", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getCsList(@RequestBody CsFrom csFrom) {
        //判断请求参数
        if (csFrom.getOrgId() == null || ObjectUtil.isEmpty(csFrom.getOrgId()) || StrUtil.isEmpty(csFrom.getSgin()) || StrUtil.isEmpty(csFrom.getChannelId())) {
            return R.parmError("请求参数错误!");
        }

        ImChannelEntity imChannelEntity = imChannelService.findByChannelId(csFrom.getChannelId());
        if (imChannelEntity == null || StrUtil.isEmpty(imChannelEntity.getAuthCode())) {
            return R.parmError("您没有相关权限!");
        }

        String signStr = imChannelEntity.getAuthCode();
        StringBuilder sb = new StringBuilder();
        sb.append("channelId=" + csFrom.getChannelId());
        sb.append("&isOnLine=" + csFrom.getIsOnLine());
        sb.append("&orgId=" + csFrom.getOrgId());
        sb.append("&key=" + signStr);
        String md5 = SignUtil.encryption(sb.toString());

        if (StrUtil.equals(md5, csFrom.getSgin())) {
            List<Map<String, Object>> resultList = imUserInfoService.getCsList(csFrom.getOrgId(), csFrom.getIsOnLine());
            return R.ok("获取客服列表信息成功!").put("data", resultList);
        } else {
            return R.parmError("您没有相关权限!");
        }
    }


    @ApiOperation(value = "获取客服列表", notes = "获取客服列表")
    @RequestMapping(value = "/getChannelCsList", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getCsListByChannelId(@RequestBody CsFrom csFrom) {
        //判断请求参数
        if (csFrom.getIsOnLine() == null || StrUtil.isEmpty(csFrom.getSgin()) || StrUtil.isEmpty(csFrom.getChannelId())) {
            return R.parmError("请求参数错误!");
        }

        ImChannelEntity imChannelEntity = imChannelService.findByChannelId(csFrom.getChannelId());
        if (imChannelEntity == null || StrUtil.isEmpty(imChannelEntity.getAuthCode())) {
            return R.parmError("您没有相关权限!");
        }

        String signStr = imChannelEntity.getAuthCode();
        StringBuilder sb = new StringBuilder();
        sb.append("channelId=" + csFrom.getChannelId());
        sb.append("&isOnLine=" + csFrom.getIsOnLine());
        sb.append("&key=" + signStr);
        String md5 = SignUtil.encryption(sb.toString());

        if (StrUtil.equals(md5, csFrom.getSgin())) {
            List<Map<String, Object>> resultList = imUserInfoService.getChannelCsList(csFrom.getOrgId(), csFrom.getChannelId(), csFrom.getIsOnLine());
            return R.ok("获取客服列表信息成功!").put("data", resultList);
        } else {
            return R.parmError("您没有相关权限!");
        }
    }


    @CrossOrigin(origins = "*")
    @ApiOperation(value = "跳转到用户聊天界面", notes = "跳转到用户聊天界面")
    @RequestMapping(value = "/chat", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public ModelAndView chat(Integer fromUserId, Integer toUserId, String sgin, String channelId, Model model, HttpServletRequest request) {
        if (fromUserId == null || fromUserId == 0 || toUserId == null || toUserId == 0 || StrUtil.isEmpty(sgin) || StrUtil.isEmpty(channelId)) {
            return null;
        }

        ImChannelEntity imChannelEntity = imChannelService.findByChannelId(channelId);
        if (imChannelEntity == null || StrUtil.isEmpty(imChannelEntity.getAuthCode())) {
            return null;
        }

        String signStr = imChannelEntity.getAuthCode();
        StringBuilder sb = new StringBuilder();
        sb.append("channelId=" + channelId);
        sb.append("&fromUserId=" + fromUserId);
        sb.append("&toUserId=" + toUserId);
        sb.append("&key=" + signStr);
        String md5 = SignUtil.encryption(sb.toString());

        if (!StrUtil.equals(md5, sgin)) {
            return null;
        }
        List<PayConfig> payConfigList = traderReplyConfigService.getReplyConfigList(toUserId);
        //return R.ok().put("data", payConfigList.stream().map(PayConfig::getType).collect(Collectors.toList()));
        ImUserInfoEntity formUserInfo = imUserInfoService.findUserById(fromUserId);
        ImUserInfoEntity toUserInfo = imUserInfoService.findUserById(toUserId);

        model.addAttribute("websocketurl", websocketurl);
        model.addAttribute("httpurl", httpurl);

        model.addAttribute("userName", formUserInfo.getUserName());
        model.addAttribute("passWord", "123456");
        model.addAttribute("orgId", formUserInfo.getOrgId());

        model.addAttribute("toUserId", toUserInfo.getId());
        model.addAttribute("toUserName", toUserInfo.getUserName());
        model.addAttribute("toUserNickName", toUserInfo.getNickName());
        model.addAttribute("toAvatar", toUserInfo.getUserAvatar());
        model.addAttribute("payMethods",
                payConfigList.stream().map(PayConfig::getType).collect(Collectors.toList()));

        model.addAttribute("toOrgId", toUserInfo.getOrgId());
        //将服务器地址传过去
        return new ModelAndView("/chat");
    }


    @CrossOrigin(origins = "*")
    @ApiOperation(value = "跳转到用户聊天界面", notes = "跳转到用户聊天界面")
    @RequestMapping(value = "/tochat", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public ModelAndView tochat(Integer fromUserId, String nickName, Integer toUserId, String channelId, Model model) {
        if (fromUserId == null || fromUserId == 0 || StrUtil.isEmpty(nickName)
                || toUserId == null || toUserId == 0 || StrUtil.isEmpty(channelId)) {
            return null;
        }

        ImUserInfoEntity formUserInfo = imUserInfoService.initCacUser(fromUserId.toString(),
                1, channelId, nickName);
        ImUserInfoEntity toUserInfo = imUserInfoService.findUserById(toUserId);

        model.addAttribute("websocketurl", websocketurl);
        model.addAttribute("httpurl", httpurl);

        model.addAttribute("userName", formUserInfo.getUserName());
        model.addAttribute("passWord", "123456");
        model.addAttribute("orgId", formUserInfo.getOrgId());

        model.addAttribute("toUserId", toUserInfo.getId());
        model.addAttribute("toUserName", toUserInfo.getUserName());
        model.addAttribute("toUserNickName", toUserInfo.getNickName());
        model.addAttribute("toAvatar", toUserInfo.getUserAvatar());
        //将服务器地址传过去
        return new ModelAndView("/tochat");
    }


    @CrossOrigin(origins = "*")
    @ApiOperation(value = "图片上传接口", notes = "图片上传接口")
    @RequestMapping(value = "/imgUpload", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public R imgUpload(@RequestParam("im_img") MultipartFile file, HttpServletRequest request) {
        if (file.isEmpty()) {
            throw new AppException("上传文件不能为空");
        }
        //获取oss配置信息
        CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
        String url = "";
        try {
            //上传文件
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //返回文件的URL
            url = OSSFactory.build(config).uploadSuffix(file.getBytes(), suffix);
            return R.ok().put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @CrossOrigin(origins = "*")
    @ApiOperation(value = "文件上传接口", notes = "文件上传接口")
    @RequestMapping(value = "/fileUpload", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public R fileUpload(@RequestParam("im_file") MultipartFile file, HttpServletRequest request) {
        if (file.isEmpty()) {
            throw new AppException("上传文件不能为空");
        }
        //获取oss配置信息
        CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
        String url = "";
        try {
            //上传文件
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //返回文件的URL
            url = OSSFactory.build(config).uploadSuffix(file.getBytes(), suffix);
            return R.ok().put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
