package com.pay.front.modules.im.from;

import lombok.Data;

import java.io.Serializable;


@Data
public class CsFrom implements Serializable {
    private Long orgId;
    private Integer isOnLine;
    private String channelId;
    private String sgin;
}
