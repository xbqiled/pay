package com.pay.front.modules.im.from;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DelOfflineMessageFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;

    @ApiModelProperty("通过id数组来删除")
    private List<String> idArray;

    @ApiModelProperty("1通过id数组批量删除, 2通过用户id批量删除, 3通过群组id批量删除 ")
    private Integer type;

    @ApiModelProperty("来源ID")
    private Integer sourceId;

}
