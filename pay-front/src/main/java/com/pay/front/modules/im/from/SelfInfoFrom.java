package com.pay.front.modules.im.from;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SelfInfoFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;

    // 用户昵称
    @ApiModelProperty("用户昵称")
    private String nickName;

    // 性别  0, 男  1,女  2,未知
    @ApiModelProperty("性别")
    private Integer sex;

    //个性签名
    @ApiModelProperty("个性签名")
    private String sign;

    // 手机号码
    @ApiModelProperty("手机号码")
    private String phone;

    // 地址
    @ApiModelProperty("地址")
    private String address;

    // 邮箱
    @ApiModelProperty("邮箱")
    private String email;

    // 生日
    @ApiModelProperty("生日")
    private String birthDay;

    // 身份证号码
    @ApiModelProperty("身份证号码")
    private String cardNumber;

    // 毕业院校
    @ApiModelProperty("毕业院校")
    private String school;

    // 学历  0，初中 1，高中  2，大专 3，本科  4，研究生  5，博士  6，博士后
    @ApiModelProperty("学历 0，初中 1，高中  2，大专 3，本科  4，研究生  5，博士  6，博士后")
    private Integer education;

    // 头像
    @ApiModelProperty("头像")
    private String userAvatar;

}
