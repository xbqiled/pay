package com.pay.front.modules.im.from;

import lombok.Data;

import java.io.Serializable;


@Data
public class InItUserFrom implements Serializable {

    private Integer platformType;

    private String userId;

    private String channelId;

    private String nickName;

    private String sgin;

}
