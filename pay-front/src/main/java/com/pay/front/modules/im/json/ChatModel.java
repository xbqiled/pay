package com.pay.front.modules.im.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ChatModel implements Serializable {
    private Integer fromUserId;
    private Integer toUserId;
}

