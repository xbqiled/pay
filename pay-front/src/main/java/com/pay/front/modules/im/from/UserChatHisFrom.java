package com.pay.front.modules.im.from;


import com.pay.common.core.utils.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class UserChatHisFrom implements Serializable {
    @ApiModelProperty("指定用户")
    private String userId;

    @ApiModelProperty("token信息")
    private String token;

    @ApiModelProperty("分页查询信息")
    private Page page;
}
