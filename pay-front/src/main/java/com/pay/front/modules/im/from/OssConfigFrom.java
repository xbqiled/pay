package com.pay.front.modules.im.from;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class OssConfigFrom implements Serializable {

    //用户token
    @ApiModelProperty("用户token")
    private String token;



}
