package com.pay.front.service;


import com.pay.api.modules.im.entity.ImUserInfoEntity;


public interface TokenService {
     ImUserInfoEntity findUserInfoByToken(String token);
}
