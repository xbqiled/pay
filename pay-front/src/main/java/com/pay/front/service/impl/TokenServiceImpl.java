package com.pay.front.service.impl;


import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.front.service.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final RedisTemplate redisTemplate;

    private static final String TOKEN = "token:";

    public final String SUBFIX = ":";

    public final String KEY = "key";

    @Override
    public ImUserInfoEntity findUserInfoByToken(String token) {
        ImUserInfoEntity userInfo  = (ImUserInfoEntity)redisTemplate.opsForValue().get("token_" + token);
        return userInfo;
    }
}
