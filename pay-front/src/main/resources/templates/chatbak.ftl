<html>
<head>
    <meta charset="utf-8">
    <title>聊天中....</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="../css/common_bak.css"/>
    <link rel="stylesheet" type="text/css" href="../css/style_bak.css"/>
    <link rel="stylesheet" href="../layer/theme/layer.css"/>
    <script type="text/javascript" src="../chatjs/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../chatjs/seach.js"></script>
    <script type="text/javascript" src="../chatjs/scrolls.js"></script>
    <script type="text/javascript" src="../layer/layer.js"></script>
    <script src="http://cdn.bootcss.com/jquery/1.12.3/jquery.min.js"></script>

    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/User.js"></script>
    <script type="text/javascript" src="../js/Content.js"></script>
    <script type="text/javascript" src="../js/LoginReq.js"></script>
    <script type="text/javascript" src="../js/LoginRes.js"></script>
    <script type="text/javascript" src="../js/MessageReq.js"></script>
    <script type="text/javascript" src="../js/MessageRes.js"></script>
    <script type="text/javascript" src="../js/Message.js"></script>
</head>
<style>
    ::-webkit-scrollbar{width:0px;}
    .bri_bottom textarea{
        width: 100%!important;
        margin-left: 0!important;
        padding: 0 25px!important;
        box-sizing: border-box!important;
    }
</style>
<script>
    $(function (){
        res();
        scrolls();
        chatscr();
        $(window).resize(function(){
            res();
            scrolls();
            chatscr();
        })

        function res(){
            var winds = $(window).height();
            var windw = $(window).width();
            //最左边高度
            $(".buju_left").height(winds);
            //中间高度
            $(".buju_center").height(winds);
            //右边高度
            $(".buju_right").height(winds);
            $(".yinyings").height(winds);
            $(".buju_install").height(winds - 61);
            $(".bcen_box").height(winds - 62);
            $(".buju_right").width(windw - 310);
            $(".bri_bottom").width(windw - 310);
            $(".brce_block_box").width(windw - 310);
            $(".buju_right textarea").width(windw - 360);
            $(".bri_center").height(winds - 205);
            $(".brce_block_box").height(winds - 205);
        }

        $(".bcen_block").mouseenter(function (){
            $(this).not(".bcen_background").css("background-color","#ddd");
        })
        $(".bcen_block").mouseleave(function (){
            $(this).not(".bcen_background").css("background-color","#e7e8e9");
        })

        $(".bcen_block").click(function (){
            $(this).removeAttr("style");
            $(this).addClass("bcen_background");
            $(".bcen_block").not(this).removeClass("bcen_background");
            $(this).find(".bcen_block_dian").css("display","none");
            $(this).find(".bcen_block_nums").css("display","none");
        })

        $(".bri_bottom textarea").focus(function (){
            $(".bri_bottom").css("background-color","#fff");
        })
        $(".bri_bottom textarea").blur(function (){
            $(".bri_bottom").css("background-color","#f5f5f5");
        })

        $(".bri_bottom_face").click(function (){
            $(".bri_bottom_look").css("display","block");
        })

        $(document).bind("click",function(e){
            //id为menu的是菜单，id为open的是打开菜单的按钮
            if($(e.target).closest(".bri_bottom_look").length == 0 && $(e.target).closest(".bri_bottom_face").length == 0){
                //点击id为menu之外且id不是不是open，则触发
                $(".bri_bottom_look").css("display","none");
            }
        })

        $(".bri_bottom_img").click(function (){
            $(".bri_bottom_imgs").click();
        })


        $(".bri_top_more").click(function (){
            var bl = $(".buju_install").css("display");
            if(bl == "block"){
                $(".buju_install").css("display","none");
            }else if(bl == "none"){
                $(".buju_install").css("display","block");
            }
        })

        $(".bin_two_but").click(function (){
            var imgs = $(this).find("img").attr("src");
            imgs = imgs.replace("../images/","");
            imgs = imgs.replace(".png","");
            if(imgs == "butclose"){
                $(this).find("img").attr("src","../images/butopen.png");
            }else if(imgs == "butopen"){
                $(this).find("img").attr("src","../images/butclose.png");
            }
        })

        $(".bin_three input").focus(function (){
            $(".bin_three").css("background-color","#fff");
        })

        $(".bin_three input").keyup(function (){
            var val = $(this).val();
            if(val == ""){
                $(".bin_three_close").css("display","none");
                $(".bin_three_seach").css("display","block");
            }else if(val != ""){
                $(".bin_three_close").css("display","block");
                $(".bin_three_seach").css("display","none");
            }
        })

        $(".bin_three_close").click(function (){
            $(".bin_three input").val("");
            $(".bin_three_close").css("display","none");
            $(".bin_three_seach").css("display","block");
            $(".bin_three").css("background-color","#f5f5f5");
        })

        $(".bin_four_more").click(function (){
            var hts = $(this).find("label").html();
            if(hts == "查看更多群成员"){
                $(this).find("label").html("收起群成员");
                $(this).find("img").attr("src","../images/up001.png");
            }else if(hts == "收起群成员"){
                $(this).find("label").html("查看更多群成员");
                $(this).find("img").attr("src","../images/down001.png");
            }
        })

        $(".bin_five_text").mouseenter(function (){
            $(this).next("img").css("display","block");
        })

        $(".bin_five_text").mouseleave(function (){
            $(this).next("img").css("display","none");
        })

        $(".bin_five_text").click(function (){
            $(this).next("input").attr("type","text");
            $(this).css("display","none");
            $(this).next("input").focus();
        })

        $(".bin_five_text").next("input").blur(function (){
            var bin = $(this).val();
            if(bin != ""){
                $(this).prev(".bin_five_text").html(bin);
            }
            $(this).prev(".bin_five_text").css("display","block");
            $(this).attr("type","hidden");
        })

        $(".bin_five_text").eq(0).next("input").blur(function (){
            var bin = $(this).val();
            $(".bri_top_font").html(bin);
        })

        $(".bin_five_butname").click(function (){
            var imgs = $(this).attr("src");
            imgs = imgs.replace("../images/","");
            imgs = imgs.replace(".png","");
            if(imgs == "butclose"){
                $(this).attr("src","../images/butopen.png");
                $(".brce_lt_name").css("display","block");
            }else if(imgs == "butopen"){
                $(this).attr("src","../images/butclose.png");
                $(".brce_lt_name").css("display","none");
            }
        })

        $(".bin_five_but").click(function (){
            var imgs = $(this).attr("src");
            imgs = imgs.replace("../images/","");
            imgs = imgs.replace(".png","");
            if(imgs == "butclose"){
                $(this).attr("src","../images/butopen.png");
            }else if(imgs == "butopen"){
                $(this).attr("src","../images/butclose.png");
            }
        })

        $(".bin_five_note").click(function (){
            $(".bin_note").css("display","block");
        })

        $(".bin_note_close").click(function (){
            $(".bin_note").css("display","none");
        })
    })
</script>
<body>
<div class="buju" style="display: flex">
    <!-- 最左侧功能图标 -->
    <!--
    <div class="buju_left fl"  style="width: 60px">
        <div class="bleft_face fl">
            <img id ="toUserAvatar" src="../images/face001.jpg"/>
        </div>
        <div class="bleft_iconone fl">
            <a href="#"><img src="../images/chat011.png" width="100%"/></a>
        </div>
    </div> -->
    <script>
        $(function (){
            var image_width=$(".bleft_face").width();
            var image_height=$(".bleft_face").height();
            var image_bi=image_width/image_height;
            $(".bleft_face img").each(function (){
                var images_width=$(this).width();
                var images_height=$(this).height();
                var images_bi=images_width/images_height;
                if(image_bi<images_bi){
                    $(this).css("height",image_height);
                    var imgwidth=image_height * images_bi;
                    var widths= 0 - (imgwidth - image_width) / 2;
                    $(this).css("margin-left",widths);
                }else{
                    $(this).css("width","100%");
                    var imgheight=image_width / images_bi;
                    var heights=0 - (imgheight - image_height) / 2;
                    $(this).css("margin-top",heights);
                }
            })
        })
    </script>
    <!-- 右边对话框 -->
    <style>
        .buju_right{
            width: 100%!important;
        }
        .brce_block_box{
            width: 100%!important;
        }
        .bri_bottom{
            width: 100%!important;
        }
        .bri_bottom_but{
            position: absolute;
            bottom: 5px;
            right: 5px;
        }
    </style>
    <div class="buju_right fl" style="width: 100%">
        <div class="bri_top">
            <div class="bleft_face fl">
                <!-- <img id ="toUserFace" src="../images/face001.jpg"/> -->
                <img id ="toUserFace" src=  ${toAvatar} />
            </div>
            <div class="bri_top_font fl">
                ${toUserNickName}
            </div>
            <!--
            <div class="bri_top_more fr"></div> -->
            <div class="clear"></div>
        </div>
        <div class="bri_center">
            <div class="bri_center_scroll" style="display:none;"></div>
            <div class="bri_center_block">
                <div class="brce_block_box" style="width: 100%"><div>
                </div>
            </div>
        </div>
        <script>
            $(function (){
                img();
                $.each(emojiList,function(index,item){
                    htmlEmojiList += '<div class="fl">'
                    htmlEmojiList += item
                    htmlEmojiList += '</div>'
                })
                $(".bri_blook_center").html(htmlEmojiList)
                $(".bri_blook_center div").click(function(){
                    var text = $("#sendtxt").val() + $(this).html()
                    $("#sendtxt").val(text)
                })
            })
            function img(){
                var img_width = $(".brce_left_face").width();
                var img_height = $(".brce_left_face").height();
                var img_bi=img_width/img_height;
                $(".brce_left_face img").each(function (){
                    var imgs_width=$(this).width();
                    var imgs_height=$(this).height();
                    var imgs_bi=imgs_width/imgs_height;
                    if(img_bi < imgs_bi){
                        $(this).css("height",img_height);
                        var imgswidth = img_height * imgs_bi;
                        var wid= 0 - (imgswidth - img_width) / 2;
                        $(this).css("margin-left",wid);
                    }else{
                        $(this).css("width","100%");
                        var imgsheight=img_width / img_bi;
                        var hei=0 - (imgsheight - img_height) / 2;
                        $(this).css("margin-top",hei);
                    }
                })
            }
            var emojiList =  [
                '😀', '😁', '😂', '😄', '😅', '😆', '😇', '😉', '😊', '😋', '😌', '😍', '😘', '😙', '😜', '😝', '😎', '😏', '😶', '😑', '😒', '😳', '😞', '😟', '😠', '😡', '😔', '😕', '😣', '😖', '😫', '😤', '😮', '😱', '😨', '😰'
            ]
            var htmlEmojiList = ''
        </script>
        <div class="bri_bottom" style="width: 100%">
            <div class="bri_bottom_face fl" title="表情"></div>
            <div class="bri_bottom_look" style="display:none;width: 95%;max-width: 470px;left: 0;top: -155px;height: auto;padding-bottom: 20px;">
                <div class="bri_blook_center" style="width: auto;height: auto">
                </div>
            </div>
            <div class="bri_bottom_img fl" title="发送图片"></div>
            <input type="file" class="bri_bottom_imgs" style="display:none;" onchange="uploadImg()" accept="image/gif,image/jpeg,image/jpg,image/png" id="uploadImgs"  name="uploadImgs"  multiple/>
            <!--<div class="bri_bottom_file fl" title="发送文件"></div>-->
            <input type="file" class="bri_bottom_files" style="display:none;" name="img" multiple/>
            <div class="bri_bottom_recharge fl" title="充值详情" onclick="sendRechrgeMsg()"></div>
            <div class="clear"></div>
            <textarea id = "sendtxt"></textarea>
            <div class="bri_bottom_but fr" onclick="sendMsg()">发送</div>
            <div class="bri_bottom_empty" style="display:none;">不能发送空白信息</div>
        </div>
    </div>
    <!-- 最右设置框 -->
    <div class="buju_install fr" style="display:none;">
        <!-- 单聊设置 -->
        <div class="bin_one">
                  <div class="bin_one_face">
                        <img  id="selfUserAvatar" src="../images/face003.jpg"/>
                   </div>
                        <div  id="selfUserName" class="bin_one_name over">XXXX</div>
              </div>
              <!-- 单聊设置 -->
                <script>
                    $(function (){
                        var iw = $(".bin_one_face").width();
                        var ih = $(".bin_one_face").height();
                        var ib = iw/ih;
                        $(".bin_one_face img").each(function (){
                            var isw=$(this).width();
                            var ish=$(this).height();
                            var isb=isw/ish;
                            if(ib<isb){
                                $(this).css("height",ih);
                                var iwi=ih * isb;
                                var ws= 0 - (iwi - iw) / 2;
                                $(this).css("margin-left",ws);
                            }else{
                                $(this).css("width","100%");
                                var ihe=iw / isb;
                                var hs=0 - (ihe - ih) / 2;
                                $(this).css("margin-top",hs);
                            }
                        })
                    })
                </script>
                <div class="bin_five">
                    <p>信博娱乐</p>
                    <div  class="bin_five_note over3">第一包网平台</div>
                    <div  class="clear"></div>
                    <p>信博棋牌</p>
                    <div  class="bin_five_note over3">最好玩的棋牌游戏平台</div>
                    <div  class="clear"></div>
                </div>
                <a href=""><div class="bin_six">退出</div></a>
                <script>
                    $(function (){
                        var iw = $(".bin_nd_face").width();
                        var ih = $(".bin_nd_face").height();
                        var ib = iw/ih;
                        $(".bin_nd_face img").each(function (){
                            var isw=$(this).width();
                            var ish=$(this).height();
                            var isb=isw/ish;
                            if(ib<isb){
                                $(this).css("height",ih);
                                var iwi=ih * isb;
                                var ws= 0 - (iwi - iw) / 2;
                                $(this).css("margin-left",ws);
                            }else{
                                $(this).css("width","100%");
                                var ihe=iw / isb;
                                var hs=0 - (ihe - ih) / 2;
                                $(this).css("margin-top",hs);
                            }
                        })
                    })
                </script>
            </div>
        </div>
    </div>
    <script>
        function friface(){
            var image_width=$(".ba_lfb_face").width();
            var image_height=$(".ba_lfb_face").height();
            var image_bi=image_width/image_height;
            $(".ba_lfb_face img").each(function (){
                images_width=$(this).width();
                var images_height=$(this).height();
                var images_bi=images_width/images_height;
                if(image_bi<images_bi){
                    $(this).css("height",image_height);
                    var imgwidth=image_height * images_bi;
                    var widths= 0 - (imgwidth - image_width) / 2;
                    $(this).css("margin-left",widths);
                }else{
                    $(this).css("width","100%");
                    var imgheight=image_width / images_bi;
                    var heights=0 - (imgheight - image_height) / 2;
                    $(this).css("margin-top",heights);
                }
            })
        }
    </script>
  </div>
</div>
</body>
<script>
var userName;
var passWord;
var orgId;
var index;
var token;
var selfUserId;
var selfUserName;
var selfAvatar;
var toUserId;
var toUserName;
var toAvatar;
var toOrgId;
var socket;
var webSocketIsOpen = false;
var webSocketPingTimer = null;
var webSocketPongTimer = null;
var webSocketPingTime = 14000; // 心跳的间隔，当前为 10秒,
var webSocketPongTime = 10000;
var lastSendTime;
var websocketurl = "ws://" + window.location.hostname + ":11112/ws";
var httpurl = "http://127.0.0.1:8996/pay-front";
var reconnectflag = false;//避免重复连接
var webSocketReconnectMaxCount = 100;

//加载完成后执行
$(function () {
    //获取登录的用户名密码
    orgId = "${orgId?c}";
    userName = "${userName}";
    passWord = "${passWord}";

    toUserId = "${toUserId?c}";
    toUserName = "${toUserName}";
    toAvatar = "${toAvatar}";
    toUserNickName = "${toUserNickName}";
    toOrgId = "${toOrgId?c}";

    websocketurl = "${websocketurl}";
    httpurl = "${httpurl}";
    webSocetInit();
});
//初始化webSocet连接
function webSocetInit(){
  //先连接到
  initWebSocket(websocketurl, '连接服务器中....', initEventHandle);
}
//初始化连接
function initWebSocket(url, typeStr, callbak) {
    try {
        index = layer.msg(typeStr, {
            icon: 16,
            style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
            shade: 0.01,time:false
        });
        if (!window.WebSocket) {
            window.WebSocket = window.MozWebSocket;
        }
        if (window.WebSocket) {
            socket = new WebSocket(url);
            socket.binaryType = "arraybuffer";
            callbak();
        } else {
            alert("您当前的浏览器不支持websocket!!!");
        }
    } catch (e) {
        reconnect(url, callbak);
    }
}
//重新连接
function reconnect(url, callbak) {
    if (reconnectflag) return;
    reconnectflag = true;
    //没连接上会一直重连，设置延迟避免请求过多
    setTimeout(function () {
        initWebSocket(url, '重新连接到服务器...', callbak);
        reconnectflag = false;
    }, 5000);
}
//重连
function webSocketReconnect() {
    if (webSocketIsOpen) {
        return false
    }
    this.webSocketReconnectCount += 1
    // 判断是否到了最大重连次数
    if (
        this.webSocketReconnectCount >= this.webSocketReconnectMaxCount
    ) {
        this.webSocketWarningText = '重连次数超限'
        return false
    }
}
var initEventHandle = function () {
    //当连接上了以后
    socket.onopen = function (event) {
        webSocketIsOpen = true;
        console.log(webSocketIsOpen);
        layer.close(index);
        index = layer.msg('登录服务器中...', {
            icon: 16,
            style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
            shade: 0.01,time:false
        });
        //发送登录信息
        doLogin(orgId, userName, passWord);
        //webSocketPing();
        heartCheck();
    };
    //收到消息后
    socket.onmessage = function (event) {
        heartCheck();
        if (event.data instanceof ArrayBuffer) {
            //如果后端发送的是二进制帧（protobuf）会收到前面定义的类型
            var message = proto.im.Message.deserializeBinary(event.data);
            //判断是什么类型
            var cmd =  message.getCommand() || 0
            switch (cmd) {
                case 4: //登录响应
                    layer.close(index);
                    var loginres = message.getLoginres();
                    loginSuccess(loginres);
                    break
                case 5: //聊天请求
                    console.log("获取到聊天收到信息");
                    var messagereq = message.getMessagereq();
                    var msgType = messagereq.getMsgtype();

                    var sendText = messagereq.getContent().getText();
                    var receiveUserAvatar =  messagereq.getFromuser().getAvatar();

                    var receiveNickName =  messagereq.getFromuser().getNickname();
                    var sendTime = dateFormat(messagereq.getSendtime(), 'Y-m-d H:i:s');
                    showAcceptMsg(msgType, sendText, receiveUserAvatar, receiveNickName, sendTime );
                    break
                case 6: //聊天响应
                    console.log("获取到聊天响应信息");
                    var messageres =  message.getMessageres();
                    sendMsgSuccess(messageres);
                    break
                case 11:
                    console.log("强制下线");
                    reconnectflag = true;
                    socket.close();
                    heartReset();
                    layer.alert('您已在别处登录，被迫下线!!!', {
                        skin: 'layui-layer-lan'
                        ,closeBtn: 0,
                        anim:2
                    });
                    break
            }
        }
    };
    //连接关闭
    socket.onclose = function (event) {
        webSocketIsOpen = false;
        /*layer.confirm('您已下线，重新上线?', function (index) {
            reconnect(websocketurl, initEventHandle);
            layer.close(index);
        });*/
        reconnect(websocketurl, initEventHandle);
    };
    //报错以后
    socket.onerror = function () {
        webSocketIsOpen = false;
        layer.msg("服务器连接出错");
        reconnect(websocketurl, initEventHandle);
    };
}
//处理登录
function doLogin(orgId, userName, passWord) {

    if (!orgId){
        return;
    }

    var message = new proto.im.Message();
    message.setCommand(3);
    message.setVersion(1);
    //拼装登录信息包
    var loginreq = new proto.im.LoginReq();
    loginreq.setDeviceid("123456");
    loginreq.setDevicetype(1);

    loginreq.setOrgid(orgId);
    loginreq.setUsername(userName);
    loginreq.setPassword(passWord);

    message.setLoginreq(loginreq);
    socket.send(message.serializeBinary());
}

function sendMsgSuccess(messageres) {

}
//登录成功
function loginSuccess(loginres) {
    if (loginres.getToken() != null && loginres.getToken() != '') {
        token = loginres.getToken();
        selfUserId = loginres.getUser().getUserid();
        selfUserName = loginres.getUser().getNickname();
        selfAvatar = loginres.getUser().getAvatar();
        $("#toUserAvatar").attr("src", toAvatar);
        $("#selfUserAvatar").attr("src", selfAvatar);
        $("#selfUserName").text(selfUserName);
        //加载离线信息
        var data = {
            "token": token,
            "userId": toUserId,
            "page" : {
                "pageSize": 1000,
                "pageIndex": 0,
                "timeOrder": 1
            }
        };
        getUserOffLineMsg(data)
    }
}
//发送心跳
function heartStart() {
    webSocketPingTimer = setTimeout(function(){
        if (!webSocketIsOpen) {
            return false
        }
        console.log('发送心跳')
        var message = new proto.im.Message();
        message.setCommand(7);
        message.setVersion(1);
        this.socket.send(message.serializeBinary())
        // clearTimeout(this.webSocketPingTimer)
        // 重新执行
        // this.webSocketPing()
        this.webSocketPongTimer = setTimeout(() =>{
            socket.close();
        },this.webSocketPongTime);
    }, this.webSocketPingTime);
}

function heartReset(){
    clearTimeout(this.webSocketPingTimer);
    clearTimeout(this.webSocketPongTimer);
}

function heartCheck(){
    heartReset();
    heartStart();
}

//获取用户离线消息
function getUserOffLineMsg(data) {
    index = layer.msg('获取聊天历史记录...', {
        icon: 16,
        style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
        shade: 0.01,time:false
    });
    $.ajax({
        type: "post",
        url: httpurl + "/chatApi/getUserChatHis",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        async: false,
        crossDomain: true,
        dataType: "json",
        success: function(data) {
            addHisChatData(data)
        }
    });
}
function openURL() {
   var url = httpurl + "/chatApi/showPay?orgId=" + toOrgId
   window.open(url)
}
//添加一条消息信息
function showAcceptMsg(msgType, sendText, receiveUserAvatar, receiveNickName, sendTime) {
    var html = ''
    if (sendTime != null && sendTime != '' && sendTime.length == 19) {
        if (sendTime.substr(0, 16) != lastSendTime) {
            lastSendTime = sendTime.substr(0, 16);
            html += '<div class="brce_time">'+ lastSendTime +'</div>'
        }
    }

    //判断是图片还是文本
    if (msgType != null && msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
        //判断是否显示
        html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + receiveUserAvatar + '"/> </div>'
        html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_jiao fl"></div>'
        html += '<div class="brce_lt_block fl"> <div>' + sendText + '</div></div></div> <div class="clear"></div> </div>'
    } else if (msgType == 7){
        //判断是否显示
        html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + receiveUserAvatar + '"/> </div>'
        html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_jiao fl"></div>'
        html += '<div class="brce_lt_block fl"> <div onclick="openURL()"><img src="'+sendText+'"/></div></div></div> <div class="clear"></div> </div>'
    } else {
        html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + receiveUserAvatar + '"/> </div>'
        html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_jiao fl"></div>'
        html += '<div class="brce_lt_block fl"> <div><img src="'+sendText+'"/></div></div></div> <div class="clear"></div> </div>'
    }

    $('.brce_block_box').append(html)
    var scrollHeights = $('.brce_block_box').prop("scrollHeight");
    $('.brce_block_box').scrollTop(scrollHeights,0);
}
//批量添加消息
function addHisChatData(data){
    if (data != null && data){
        console.log(data)
        var html = ''

        layer.close(index);
        for (let i in data.data.list) {
            var sendUserAvatar =  data.data.list[i].sendUserAvatar;
            var sendNickName =  data.data.list[i].sendNickName;
            var msgType = data.data.list[i].messageType;
            const contont = JSON.parse(data.data.list[i].content);
            var sendText =  contont.text;

            var receiveUserAvatar =  data.data.list[i].receiveUserAvatar;
            var receiveNickName =  data.data.list[i].receiveNickName;
            if (data.data.list[i].sendTime != null && data.data.list[i].sendTime != '' && data.data.list[i].sendTime.length == 19 && i == 0) {
                lastSendTime = data.data.list[i].sendTime.substr(0, 16);
            }

            if (data.data.list[i].sendTime != null && data.data.list[i].sendTime != '' && data.data.list[i].sendTime.length == 19) {
                if (data.data.list[i].sendTime.substr(0, 16) != lastSendTime) {
                    lastSendTime = data.data.list[i].sendTime.substr(0, 16);
                    html += '<div class="brce_time">'+ lastSendTime +'</div>'
                }
            }

            if (selfUserId == data.data.list[i].sendUserId){
                //判断是图片还是文本
                if (msgType != null && msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
                    //判断是否显示
                    html += '<div class="brce_right"><div class="brce_left_face fr"> <img src="'+sendUserAvatar+'"/> </div>'
                    html +=       '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div>'+sendText+'</div> </div>'
                    html +=       '</div> <div class="clear"></div></div>'
                } else if (msgType == 7){
                    html += '<div class="brce_right"><div class="brce_left_face fr"> <img src="'+sendUserAvatar+'"/> </div>'
                    html +=  '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div></div>'
                    html +=  ' <div class="brce_rt_block fr"><img src="'+sendText+'"/></div></div>'
                    html +=  '</div> <div class="clear"></div></div>'
                } else {
                    //判断是否显示
                    html += '<div class="brce_right"><div class="brce_left_face fr"> <img src="'+sendUserAvatar+'"/> </div>'
                    html +=  '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div></div>'
                    html +=  ' <div class="brce_rt_block fr"><img src="'+sendText+'"/></div></div>'
                    html +=  '</div> <div class="clear"></div></div>'
                }
            }else {
                //判断是图片还是文本
                if (msgType != null && msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
                    //判断是否显示
                    html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + sendUserAvatar + '"/> </div>'
                    html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + sendNickName + '</div><div class="brce_lt_jiao fl"></div>'
                    html += '<div class="brce_lt_block fl"> <div>' + sendText + '</div></div></div> <div class="clear"></div> </div>'
                } else if (msgType == 7){
                    html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + sendUserAvatar + '"/> </div>'
                    html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + sendNickName + '</div><div class="brce_lt_jiao fl"></div>'
                    html += '<div class="brce_lt_block fl"> <div onclick="openURL()"><img src="'+sendText+'"/></div></div></div> <div class="clear"></div> </div>'
                } else {
                    //判断是否显示
                    html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + sendUserAvatar + '"/> </div>'
                    html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + sendNickName + '</div><div class="brce_lt_jiao fl"></div>'
                    html += '<div class="brce_lt_block fl"> <div><img src="'+sendText+'"/></div></div></div> <div class="clear"></div> </div>'
                }
            }
        }
        $('.brce_block_box').append(html)
        var scrollHeights = $('.brce_block_box').prop("scrollHeight");
        $('.brce_block_box').scrollTop(scrollHeights,0);
        img();
    }
}
//发送消息
function sendMsg() {
    //判断是什么消息
    var textArea = $('#sendtxt').val();
    if (textArea != null && textArea != '') {
        sendTxtMsg(textArea);
    }
}
//发送充值信息
function sendRechrgeMsg() {
    var text = "云商入款信息";
    var message = new proto.im.Message();
    message.setCommand(5);
    message.setVersion(1);
    //拼装消息包
    var messagereq = new proto.im.MessageReq();
    //拼装内容信息包
    var content  = new proto.im.Content();
    content.setText(text);
    //消息ID
    var nowTime =   new Date();
    var messageId = nowTime.getTime() + toUserId;
    var sendTime = nowTime.getTime();
    //发送用户
    var fromUser = new proto.im.User();
    fromUser.setAvatar(selfAvatar);
    fromUser.setNickname(selfUserName);
    fromUser.setUserid(selfUserId);
    fromUser.setOrgid(orgId);
    //接受用户
    var toUser = new proto.im.User();
    toUser.setUserid(toUserId);
    toUser.setNickname(toUserName);
    toUser.setAvatar(toAvatar);
    //瓶装消息包
    messagereq.setContent(content);
    messagereq.setFromuser(fromUser);
    messagereq.setTouser(toUser);
    messagereq.setMessageid(messageId);

    messagereq.setSendtime(sendTime);
    messagereq.setMsgtype(6);
    message.setMessagereq(messagereq);
    socket.send(message.serializeBinary());

    //添加记录
    showSendMsg(text, 0, dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
}
//显示发送信息
function showSendMsg(sendText, msgType, sendTime) {
    var html;
    if (sendTime != null && sendTime != '' && sendTime.length == 19) {
        if (sendTime.substr(0, 16) != lastSendTime) {
            lastSendTime = sendTime.substr(0, 16);
            html = '<div class="brce_time">'+ lastSendTime +'</div>'
        }
    }
    //发送文本
    if (msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
        var apps_one = "<div class='brce_right'><div class='brce_left_face fr'><img src='" + selfAvatar + "'/></div>" +
            "<div class='brce_left_talk fr'><div class='brce_rt_jiao fr'></div><div class='brce_rt_block fr'><div>";
        var apps_two = "</div></div></div><div class='clear'></div></div>";

        if (sendText == "") {
            $(".bri_bottom_empty").css("display", "block");
            setTimeout(function () {
                $(".bri_bottom_empty").hide();
            }, 2000)
        } else {
            if (html != null && html != '') {
                $(".bri_center .brce_block_box").append(html + apps_one + sendText + apps_two);
            } else {
                $(".bri_center .brce_block_box").append(apps_one + sendText + apps_two);
            }
            img();
            $(".bri_bottom textarea").val("");
            var scrollHeight = $('.brce_block_box').prop("scrollHeight");
            $('.brce_block_box').scrollTop(scrollHeight, 0);
        }
    } else {
        //发送图片
        var apps_one = '<div class="brce_right"><div class="brce_left_face fr"> <img src="' + selfAvatar + '"/> </div>' +
            '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div></div>' +
            ' <div class="brce_rt_block fr"><img src="' + sendText + '"/></div></div>';
        var apps_two = '</div> <div class="clear"></div></div>'
        if (html != null && html != '') {
            $(".bri_center .brce_block_box").append(html + apps_one + apps_two);
        } else {
            $(".bri_center .brce_block_box").append(apps_one + apps_two);
        }

        img();
        var scrollHeight = $('.brce_block_box').prop("scrollHeight");
        $('.brce_block_box').scrollTop(scrollHeight, 0);
    }
}
//发送文本消息
function sendTxtMsg(text) {
    var message = new proto.im.Message();
    message.setCommand(5);
    message.setVersion(1);
    //拼装消息包
    var messagereq = new proto.im.MessageReq();
    //拼装内容信息包
    var content  = new proto.im.Content();
    content.setText(text);
    //消息ID
    var nowTime =   new Date();
    var messageId = nowTime.getTime() + toUserId;
    var sendTime = nowTime.getTime();
    //发送用户
    var fromUser = new proto.im.User();
    fromUser.setAvatar(selfAvatar);
    fromUser.setNickname(selfUserName);
    fromUser.setUserid(selfUserId);
    fromUser.setOrgid(orgId);
    //接受用户
    var toUser = new proto.im.User();
    toUser.setAvatar(toAvatar);
    toUser.setUserid(toUserId);
    toUser.setNickname(toUserName);
    //瓶装消息包
    messagereq.setContent(content);
    messagereq.setFromuser(fromUser);
    messagereq.setTouser(toUser);
    messagereq.setMessageid(messageId);

    messagereq.setSendtime(sendTime);
    messagereq.setMsgtype(0);
    message.setMessagereq(messagereq);
    socket.send(message.serializeBinary());

    //添加记录
    showSendMsg(text, 0,  dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
}
//发送图片消息
function sendImgMsg(imgUrl, width, height) {
    var message = new proto.im.Message();
    message.setCommand(5);
    message.setVersion(1);
    //拼装消息包
    var messagereq = new proto.im.MessageReq();
    //拼装内容信息包
    var content  = new proto.im.Content();
    content.setText(imgUrl);
    content.setWidth(width);
    content.setHigth(height);
    //消息ID
    var nowTime =   new Date();
    var messageId = nowTime.getTime() + toUserId;
    var sendTime = nowTime.getTime();
    //发送用户
    var fromUser = new proto.im.User();
    fromUser.setAvatar(selfAvatar);
    fromUser.setNickname(selfUserName);
    fromUser.setUserid(selfUserId);
    fromUser.setOrgid(orgId);
    //接受用户
    var toUser = new proto.im.User();
    toUser.setUserid(toUserId);
    toUser.setNickname(toUserName);
    toUser.setAvatar(toAvatar);
    //瓶装消息包
    messagereq.setContent(content);
    messagereq.setFromuser(fromUser);
    messagereq.setTouser(toUser);
    messagereq.setMessageid(messageId);

    messagereq.setSendtime(sendTime);
    messagereq.setMsgtype(1);
    message.setMessagereq(messagereq);
    socket.send(message.serializeBinary());

    //添加记录
    showSendMsg(imgUrl, 1, dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
}
//上传图片
function uploadImg() {
    index = layer.msg('图片上传中...', {
        icon: 16,
        style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
        shade: 0.01,time:false
    });

    var data = new FormData();
    var file = $("#uploadImgs")[0].files[0];
    if (file != null) {
        data.append("im_img", file);
        $.ajax({
            type: "post",
            url: httpurl + "/chatApi/imgUpload",
            data: data,
            contentType: false,
            crossDomain: true,
            //默认文件类型application/x-www-form-urlencoded  设置之后multipart/form-data
            processData: false,
            // 默认情况下会对发送的数据转化为对象 不需要转化的信息
            success: function (data) {
                var url = data.url;
                sendImgMsg(url, 100, 100)
                layer.close(index);
            }
        });
    }
}
</script>
</html>
