<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
    <meta name="baidu-site-verification" content="2nXjznVkpe">
    <meta charset="UTF-8">
    <title>入款信息列表</title>
    <script src="http://cdn.bootcss.com/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div class="main-body">
    <#list payChannelList as item>
        <div class="content-list">
            <div class="title">
                ${item.payName}
            </div>
            <#if item.type == 1>
                <img src="${item.wxCodeUrl}" alt="">
                <table style="margin: 10px">
                    <tr>
                        <td  width='20%' align="left">微信账号:</td>
                        <td  width='70%' align="right">${item.wxAccount}</td>
                        <td  width='10%' align="center">复制</td>
                    </tr>
                    <tr>
                        <td width='20%' align="left">微信昵称:</td>
                        <td width='70%' align="right">${item.wxNickName}</td>
                        <td width='10%' align="center">复制</td>
                    </tr>
                </table>
            <#elseif item.type == 2>
                <img src="${item.aliPayCodeUrl}" alt="">
                <table style="padding: 10px">
                    <tr>
                        <td  width='20%' align="left">支付宝账号:</td>
                        <td  width='70%' align="right">${item.aliPayAccount}</td>
                        <td  width='10%' align="center">复制</td>
                    </tr>
                    <tr>
                        <td width='20%' align="left">支付宝昵称:</td>
                        <td width='70%' align="right">${item.aliPayNickName}</td>
                        <td width='10%' align="center">复制</td>
                    </tr>
                </table>
            <#elseif item.type == 3>
                <table style="padding: 10px">
                    <tr>
                        <td width='20%' align="left">银行卡账号:</td>
                        <td width='70%' align="right">${item.bankAccount}</td>
                        <td width='10%' align="center">复制</td>
                    </tr>
                    <tr>
                        <td width='20%' align="left">收款人:</td>
                        <td width='70%' align="right">${item.bankPayee}</td>
                        <td width='10%' align="center">复制</td>
                    </tr>
                    <tr>
                        <td width='20%' align="left">归属银行:</td>
                        <td width='70%' align="right">${item.ascriptionBank}</td>
                        <td width='10%' align="center">复制</td>
                    </tr>
                    <tr>
                        <td width='20%' align="left">开户银行:</td>
                        <td width='70%' align="right">${item.openAccountBank}</td>
                        <td width='10%' align="center">复制</td>
                    </tr>
                </table>
            </#if>
        </div>
    </#list>
</div>
<style>
    * {
        margin: 0;
        padding: 0;
    }

    .main-body {
        width: 100%;
        height: auto;
        border: 1px solid #eeeeee;
        margin: 20px;
        background: #00aadd;
    }

    .main-body .title {
        width: 100%;
        height: 40px;
        text-align: center;
        line-height: 40px;
        font-size: 20px;
        font-weight: 600;
    }

    .main-body .content-list {
        margin-top: 10px;
        width: 100%;
        text-align: center;
        border: 1px solid #8c2425;
    }

    .main-body .content-list img {
        width: 100%;
    }

    .main-body .content-list ul li {
        list-style: none;
    }
</style>
<script>
    function copyText(id){
        $('#on'+id).attr('data-clipboard-target', '#'+id);
        var clipboard = new ClipboardJS ("#on"+id);
        clipboard.on('success', function (e) {
            if(e.text){
                alert('复制成功');
            }else{
                alert('复制失败');
            }
        });
    }
</script>
</body>
</html>