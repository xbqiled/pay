<!DOCTYPE html>
<html lang="en" style="font-size: 55.2px;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>入款信息列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="icon">
    <link rel="stylesheet" href="../paycss/reset.e5d82d42.css">
    <link rel="stylesheet" href="../paycss/index.90acfd9a.css">
    <link rel="stylesheet" href="../paycss/webuploader.css">
    <script src="../payjs/rem.js"></script>
    <style type="text/css">.v-modal-enter {
            animation: v-modal-in .2s ease;
        }
        .v-modal-leave {
            animation: v-modal-out .2s ease forwards;
        }
        @keyframes v-modal-in {
            0% {
                opacity: 0;
            }
            100% {
            }
        }
        @keyframes v-modal-out {
            0% {
            }
            100% {
                opacity: 0;
            }
        }
        .v-modal {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            opacity: 0.5;
            background: #000;
        }
        .mint-popup {
            position: fixed;
            background: #fff;
            top: 50%;
            left: 50%;
            transform: translate3d(-50%, -50%, 0);
            backface-visibility: hidden;
            transition: .2s ease-out;
        }
        .mint-popup-top {
            top: 0;
            right: auto;
            bottom: auto;
            left: 50%;
            transform: translate3d(-50%, 0, 0);
        }
        .mint-popup-right {
            top: 50%;
            right: 0;
            bottom: auto;
            left: auto;
            transform: translate3d(0, -50%, 0);
        }
        .mint-popup-bottom {
            top: auto;
            right: auto;
            bottom: 0;
            left: 50%;
            transform: translate3d(-50%, 0, 0);
        }
        .mint-popup-left {
            top: 50%;
            right: auto;
            bottom: auto;
            left: 0;
            transform: translate3d(0, -50%, 0);
        }
        .popup-slide-top-enter, .popup-slide-top-leave-active {
            transform: translate3d(-50%, -100%, 0);
        }
        .popup-slide-right-enter, .popup-slide-right-leave-active {
            transform: translate3d(100%, -50%, 0);
        }
        .popup-slide-bottom-enter, .popup-slide-bottom-leave-active {
            transform: translate3d(-50%, 100%, 0);
        }
        .popup-slide-left-enter, .popup-slide-left-leave-active {
            transform: translate3d(-100%, -50%, 0);
        }
        .popup-fade-enter, .popup-fade-leave-active {
            opacity: 0;
        }
    </style>
    <style type="text/css">
        .mint-msgbox {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate3d(-50%, -50%, 0);
            background-color: #fff;
            width: 85%;
            border-radius: 3px;
            font-size: 16px;
            -webkit-user-select: none;
            overflow: hidden;
            backface-visibility: hidden;
            transition: .2s;
        }

        .mint-msgbox-header {
            padding: 15px 0 0;
        }

        .mint-msgbox-content {
            padding: 10px 20px 15px;
            border-bottom: 1px solid #ddd;
            min-height: 36px;
            position: relative;
        }

        .mint-msgbox-input {
            padding-top: 15px;
        }

        .mint-msgbox-input input {
            border: 1px solid #dedede;
            border-radius: 5px;
            padding: 4px 5px;
            width: 100%;
            appearance: none;
            outline: none;
        }

        .mint-msgbox-input input.invalid {
            border-color: #ff4949;
        }

        .mint-msgbox-input input.invalid:focus {
            border-color: #ff4949;
        }

        .mint-msgbox-errormsg {
            color: red;
            font-size: 12px;
            min-height: 18px;
            margin-top: 2px;
        }

        .mint-msgbox-title {
            text-align: center;
            padding-left: 0;
            margin-bottom: 0;
            font-size: 16px;
            font-weight: 700;
            color: #333;
        }

        .mint-msgbox-message {
            color: #999;
            margin: 0;
            text-align: center;
            line-height: 36px;
        }

        .mint-msgbox-btns {
            display: -ms-flexbox;
            display: flex;
            height: 40px;
            line-height: 40px;
        }

        .mint-msgbox-btn {
            line-height: 35px;
            display: block;
            background-color: #fff;
            -ms-flex: 1;
            flex: 1;
            margin: 0;
            border: 0;
        }

        .mint-msgbox-btn:focus {
            outline: none;
        }

        .mint-msgbox-btn:active {
            background-color: #fff;
        }

        .mint-msgbox-cancel {
            width: 50%;
            border-right: 1px solid #ddd;
        }

        .mint-msgbox-cancel:active {
            color: #000;
        }

        .mint-msgbox-confirm {
            color: #26a2ff;
            width: 50%;
        }

        .mint-msgbox-confirm:active {
            color: #26a2ff;
        }

        .msgbox-bounce-enter {
            opacity: 0;
            transform: translate3d(-50%, -50%, 0) scale(0.7);
        }

        .msgbox-bounce-leave-active {
            opacity: 0;
            transform: translate3d(-50%, -50%, 0) scale(0.9);
        }

        .v-modal-enter {
            animation: v-modal-in .2s ease;
        }

        .v-modal-leave {
            animation: v-modal-out .2s ease forwards;
        }

        @keyframes v-modal-in {
            0% {
                opacity: 0;
            }
            100% {
            }
        }

        @keyframes v-modal-out {
            0% {
            }
            100% {
                opacity: 0;
            }
        }

        .v-modal {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            opacity: 0.5;
            background: #000;
        }
    </style>
    <style type="text/css">
        .mint-indicator {
            transition: opacity .2s linear;
        }

        .mint-indicator-wrapper {
            top: 50%;
            left: 50%;
            position: fixed;
            transform: translate(-50%, -50%);
            border-radius: 5px;
            background: rgba(0, 0, 0, 0.7);
            color: white;
            box-sizing: border-box;
            text-align: center;
        }

        .mint-indicator-text {
            display: block;
            color: #fff;
            text-align: center;
            margin-top: 10px;
            font-size: 16px;
        }

        .mint-indicator-spin {
            display: inline-block;
            text-align: center;
        }

        .mint-indicator-mask {
            top: 0;
            left: 0;
            position: fixed;
            width: 100%;
            height: 100%;
            opacity: 0;
            background: transparent;
        }

        .mint-indicator-enter, .mint-indicator-leave-active {
            opacity: 0;
        }
    </style>
    <style type="text/css">
        .mint-spinner-snake {
            animation: mint-spinner-rotate 0.8s infinite linear;
            border: 4px solid transparent;
            border-radius: 50%;
        }

        @keyframes mint-spinner-rotate {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .mint-spinner-double-bounce {
            position: relative;
        }

        .mint-spinner-double-bounce-bounce1, .mint-spinner-double-bounce-bounce2 {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            opacity: 0.6;
            position: absolute;
            top: 0;
            left: 0;
            animation: mint-spinner-double-bounce 2.0s infinite ease-in-out;
        }

        .mint-spinner-double-bounce-bounce2 {
            animation-delay: -1.0s;
        }

        @keyframes mint-spinner-double-bounce {
            0%, 100% {
                transform: scale(0.0);
            }
            50% {
                transform: scale(1.0);
            }
        }

        .mint-spinner-triple-bounce {
        }

        .mint-spinner-triple-bounce-bounce1, .mint-spinner-triple-bounce-bounce2, .mint-spinner-triple-bounce-bounce3 {
            border-radius: 100%;
            display: inline-block;
            animation: mint-spinner-triple-bounce 1.4s infinite ease-in-out both;
        }

        .mint-spinner-triple-bounce-bounce1 {
            animation-delay: -0.32s;
        }

        .mint-spinner-triple-bounce-bounce2 {
            animation-delay: -0.16s;
        }

        @keyframes mint-spinner-triple-bounce {
            0%, 80%, 100% {
                transform: scale(0);
            }
            40% {
                transform: scale(1.0);
            }
        }

        .mint-spinner-fading-circle {
            position: relative
        }

        .mint-spinner-fading-circle-circle {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: absolute
        }

        .mint-spinner-fading-circle-circle::before {
            content: " ";
            display: block;
            margin: 0 auto;
            width: 15%;
            height: 15%;
            border-radius: 100%;
            animation: mint-fading-circle 1.2s infinite ease-in-out both
        }

        .mint-spinner-fading-circle-circle.is-circle2 {
            transform: rotate(30deg)
        }

        .mint-spinner-fading-circle-circle.is-circle2::before {
            animation-delay: -1.1s
        }

        .mint-spinner-fading-circle-circle.is-circle3 {
            transform: rotate(60deg)
        }

        .mint-spinner-fading-circle-circle.is-circle3::before {
            animation-delay: -1s
        }

        .mint-spinner-fading-circle-circle.is-circle4 {
            transform: rotate(90deg)
        }

        .mint-spinner-fading-circle-circle.is-circle4::before {
            animation-delay: -0.9s
        }

        .mint-spinner-fading-circle-circle.is-circle5 {
            transform: rotate(120deg)
        }

        .mint-spinner-fading-circle-circle.is-circle5::before {
            animation-delay: -0.8s
        }

        .mint-spinner-fading-circle-circle.is-circle6 {
            transform: rotate(150deg)
        }

        .mint-spinner-fading-circle-circle.is-circle6::before {
            animation-delay: -0.7s
        }

        .mint-spinner-fading-circle-circle.is-circle7 {
            transform: rotate(180deg)
        }

        .mint-spinner-fading-circle-circle.is-circle7::before {
            animation-delay: -0.6s
        }

        .mint-spinner-fading-circle-circle.is-circle8 {
            transform: rotate(210deg)
        }

        .mint-spinner-fading-circle-circle.is-circle8::before {
            animation-delay: -0.5s
        }

        .mint-spinner-fading-circle-circle.is-circle9 {
            transform: rotate(240deg)
        }

        .mint-spinner-fading-circle-circle.is-circle9::before {
            animation-delay: -0.4s
        }

        .mint-spinner-fading-circle-circle.is-circle10 {
            transform: rotate(270deg)
        }

        .mint-spinner-fading-circle-circle.is-circle10::before {
            animation-delay: -0.3s
        }

        .mint-spinner-fading-circle-circle.is-circle11 {
            transform: rotate(300deg)
        }

        .mint-spinner-fading-circle-circle.is-circle11::before {
            animation-delay: -0.2s
        }

        .mint-spinner-fading-circle-circle.is-circle12 {
            transform: rotate(330deg)
        }

        .mint-spinner-fading-circle-circle.is-circle12::before {
            animation-delay: -0.1s
        }

        @keyframes mint-fading-circle {
            0%, 39%, 100% {
                opacity: 0
            }
            40% {
                opacity: 1
            }
        }
    </style>
    <style type="text/css">
        .mint-toast {
            position: fixed;
            max-width: 80%;
            border-radius: 5px;
            background: rgba(0, 0, 0, 0.7);
            color: #fff;
            box-sizing: border-box;
            text-align: center;
            z-index: 1000;
            transition: opacity .3s linear
        }

        .mint-toast.is-placebottom {
            bottom: 50px;
            left: 50%;
            transform: translate(-50%, 0)
        }

        .mint-toast.is-placemiddle {
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .mint-toast.is-placetop {
            top: 50px;
            left: 50%;
            transform: translate(-50%, 0)
        }

        .mint-toast-icon {
            display: block;
            text-align: center;
            font-size: 56px
        }

        .mint-toast-text {
            font-size: 14px;
            display: block;
            text-align: center
        }

        .mint-toast-pop-enter, .mint-toast-pop-leave-active {
            opacity: 0
        }
    </style>
    <style type="text/css">.weichat-dialog {
            background: #00a6ff;
            width: 100%;
            height: 100%;
            padding-top: .3rem;
            box-sizing: border-box;
            overflow: auto
        }

        .weichat-dialog .close {
            width: .5rem;
            height: .5rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0 0;
            background-size: 80%;
            margin: 0 0 .3rem .3rem
        }

        .weichat-dialog .sec {
            background: #fff;
            box-shadow: 0 0 .1rem 0 rgba(0, 0, 0, .04);
            border-radius: .2rem;
            margin: 0 .3rem
        }

        .weichat-dialog .sec1 .dialog-title {
            width: 6.1rem;
            margin: 0 auto;
            position: relative;
            height: 1.24rem;
            padding-top: .5rem;
            box-sizing: border-box
        }

        .weichat-dialog .sec1 .dialog-title:after {
            content: "";
            width: 200%;
            height: 200%;
            border-bottom: .01rem solid #000;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%) scale(.5);
            border-radius: 0;
            box-sizing: border-box;
            opacity: .13
        }

        .weichat-dialog .sec1 .dialog-title h2 {
            height: .48rem;
            font-size: .34rem;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333;
            line-height: .48rem;
            padding-left: .59rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/alipay_ac58a40e.png) no-repeat 0;
            background-size: .39rem
        }

        .weichat-dialog .sec1 .dialog-title p {
            margin-top: .13rem;
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989
        }

        .weichat-dialog .sec1 .dialog-content {
            padding-top: .37rem;
            padding-bottom: .46rem
        }

        .weichat-dialog .sec1 .dialog-content .qr-img {
            display: block;
            width: 3.18rem;
            max-height: 7.95rem;
            margin: 0 auto
        }

        .weichat-dialog .sec1 .dialog-content .save-img {
            display: block;
            margin: .2rem auto;
            height: .45rem;
            font-size: .3rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #ffac00;
            line-height: .45rem;
            outline: none;
            text-align: center
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text {
            display: flex;
            font-size: .28rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 700;
            color: #666;
            width: 5rem;
            margin: 0 auto;
            justify-content: flex-start;
            margin-top: .07rem;
            height: .4rem;
            line-height: .4rem
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text .pre {
            white-space: nowrap
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text .next {
            width: 3rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text i {
            width: .3rem;
            height: .32rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/copy_8388f75b.png) no-repeat 0;
            background-size: 100%;
            margin-top: .03rem
        }

        .weichat-dialog .sec2 {
            margin-top: .2rem;
            margin-bottom: 2rem;
            padding: .2rem .4rem
        }

        .weichat-dialog .sec2 h2 {
            height: .8rem;
            font-size: .3rem;
            font-family: PingFang-SC-Heavy, PingFang-SC;
            font-weight: 800;
            color: #333;
            line-height: .8rem
        }

        .weichat-dialog .sec2 p {
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989;
            line-height: .48rem
        }

        .weichat-dialog .sec2 p .green {
            color: #00a6ff
        }

        .weichat-dialog .upload-img {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 1.77rem;
            background: #f04a5a;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3), 0 .1rem .1rem 0 #000;
            box-sizing: border-box
        }

        .weichat-dialog .upload-img .webuploader-pick {
            padding-top: .3rem
        }

        .weichat-dialog .upload-img .btn {
            width: 5.36rem;
            height: .8rem;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3);
            border-radius: .1rem;
            border: .03rem solid #fff;
            line-height: .77rem;
            font-size: .34rem;
            color: #fff;
            text-align: center;
            margin: 0 auto;
            display: flex;
            justify-content: center
        }

        .weichat-dialog .upload-img .btn i {
            width: .44rem;
            height: 100%;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0;
            background-size: 100%
        }

        .weichat-dialog .upload-img p {
            margin-top: .16rem;
            font-size: .22rem;
            color: #fff;
            text-align: center
        }</style>
    <style type="text/css">.weichat-dialog {
            background: #27c683;
            width: 100%;
            height: 100%;
            padding-top: .3rem;
            box-sizing: border-box;
            overflow: auto
        }

        .weichat-dialog .close {
            width: .5rem;
            height: .5rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0 0;
            background-size: 80%;
            margin: 0 0 .3rem .3rem
        }

        .weichat-dialog .sec {
            background: #fff;
            box-shadow: 0 0 .1rem 0 rgba(0, 0, 0, .04);
            border-radius: .2rem;
            margin: 0 .3rem
        }

        .weichat-dialog .sec1 .dialog-title {
            width: 6.1rem;
            margin: 0 auto;
            position: relative;
            height: 1.74rem;
            padding: .5rem 0 0 .37rem;
            box-sizing: border-box
        }

        .weichat-dialog .sec1 .dialog-title:after {
            content: "";
            width: 200%;
            height: 200%;
            border-bottom: .01rem solid #000;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%) scale(.5);
            border-radius: 0;
            box-sizing: border-box;
            opacity: .13
        }

        .weichat-dialog .sec1 .dialog-title h2 {
            height: .48rem;
            font-size: .34rem;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333;
            line-height: .48rem;
            padding-left: .59rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/weixin_efc4ee6c.png) no-repeat 0;
            background-size: .39rem
        }

        .weichat-dialog .sec1 .dialog-title p {
            margin-top: .13rem;
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989
        }

        .weichat-dialog .sec1 .dialog-content {
            padding-top: .37rem;
            padding-bottom: .46rem
        }

        .weichat-dialog .sec1 .dialog-content .qr-img {
            display: block;
            width: 3.18rem;
            max-height: 7.95rem;
            margin: 0 auto
        }

        .weichat-dialog .sec1 .dialog-content .save-img {
            display: block;
            margin: .2rem auto;
            height: .45rem;
            font-size: .3rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #ffac00;
            line-height: .45rem;
            outline: none;
            text-align: center
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text {
            display: flex;
            font-size: .28rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 700;
            color: #666;
            width: 5rem;
            margin: 0 auto;
            justify-content: flex-start;
            margin-top: .07rem;
            height: .4rem;
            line-height: .4rem
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text .pre {
            white-space: nowrap
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text .next {
            width: 3rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .weichat-dialog .sec1 .dialog-content .pay-info-text i {
            width: .3rem;
            height: .32rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/copy_8388f75b.png) no-repeat 0;
            background-size: 100%;
            margin-top: .03rem
        }

        .weichat-dialog .sec2 {
            margin-top: .2rem;
            margin-bottom: 2rem;
            padding: .2rem .4rem
        }

        .weichat-dialog .sec2 h2 {
            height: .8rem;
            font-size: .3rem;
            font-family: PingFang-SC-Heavy, PingFang-SC;
            font-weight: 800;
            color: #333;
            line-height: .8rem
        }

        .weichat-dialog .sec2 p {
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989;
            line-height: .48rem
        }

        .weichat-dialog .sec2 p .green {
            color: #00a6ff
        }

        .weichat-dialog .upload-img {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 1.77rem;
            background: #f04a5a;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3), 0 .1rem .1rem 0 #000;
            box-sizing: border-box
        }

        .weichat-dialog .upload-img .webuploader-pick {
            padding-top: .3rem
        }

        .weichat-dialog .upload-img .btn {
            width: 5.36rem;
            height: .8rem;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3);
            border-radius: .1rem;
            border: .03rem solid #fff;
            line-height: .77rem;
            font-size: .34rem;
            color: #fff;
            text-align: center;
            margin: 0 auto;
            display: flex;
            justify-content: center
        }

        .weichat-dialog .upload-img .btn i {
            width: .44rem;
            height: 100%;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0;
            background-size: 100%
        }

        .weichat-dialog .upload-img p {
            margin-top: .16rem;
            font-size: .22rem;
            color: #fff;
            text-align: center
        }</style>
    <style type="text/css">.huabei-dialog {
            background: #00a6ff;
            width: 100%;
            height: 100%;
            padding-top: .3rem;
            box-sizing: border-box;
            overflow: auto
        }

        .huabei-dialog .close {
            width: .5rem;
            height: .5rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0 0;
            background-size: 80%;
            margin: 0 0 .3rem .3rem
        }

        .huabei-dialog .sec {
            background: #fff;
            box-shadow: 0 0 .1rem 0 rgba(0, 0, 0, .04);
            border-radius: .2rem;
            margin: 0 .3rem
        }

        .huabei-dialog .sec1 .dialog-title {
            width: 6.1rem;
            margin: 0 auto;
            position: relative;
            height: 1.24rem;
            padding-top: .5rem;
            box-sizing: border-box
        }

        .huabei-dialog .sec1 .dialog-title:after {
            content: "";
            width: 200%;
            height: 200%;
            border-bottom: .01rem solid #000;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%) scale(.5);
            border-radius: 0;
            box-sizing: border-box;
            opacity: .13
        }

        .huabei-dialog .sec1 .dialog-title h2 {
            height: .48rem;
            font-size: .34rem;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333;
            line-height: .48rem;
            padding-left: .59rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/huabei-blue_5bf0bc81.png) no-repeat 0;
            background-size: .39rem
        }

        .huabei-dialog .sec1 .dialog-title p {
            margin-top: .13rem;
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989
        }

        .huabei-dialog .sec1 .dialog-content {
            padding-top: .37rem;
            padding-bottom: .46rem
        }

        .huabei-dialog .sec1 .dialog-content .qr-img {
            display: block;
            width: 3.18rem;
            max-height: 7.95rem;
            margin: 0 auto
        }

        .huabei-dialog .sec1 .dialog-content .save-img {
            display: block;
            margin: .2rem auto;
            height: .45rem;
            font-size: .3rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #ffac00;
            line-height: .45rem;
            outline: none;
            text-align: center
        }

        .huabei-dialog .sec1 .dialog-content .pay-info-text {
            display: flex;
            font-size: .28rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 700;
            color: #666;
            width: 5rem;
            margin: 0 auto;
            justify-content: flex-start;
            margin-top: .07rem;
            height: .4rem;
            line-height: .4rem
        }

        .huabei-dialog .sec1 .dialog-content .pay-info-text .pre {
            white-space: nowrap
        }

        .huabei-dialog .sec1 .dialog-content .pay-info-text .next {
            width: 3rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .huabei-dialog .sec1 .dialog-content .pay-info-text i {
            width: .3rem;
            height: .32rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/copy_8388f75b.png) no-repeat 0;
            background-size: 100%;
            margin-top: .03rem
        }

        .huabei-dialog .sec2 {
            margin-top: .2rem;
            margin-bottom: 2rem;
            padding: .2rem .4rem
        }

        .huabei-dialog .sec2 h2 {
            height: .8rem;
            font-size: .3rem;
            font-family: PingFang-SC-Heavy, PingFang-SC;
            font-weight: 800;
            color: #333;
            line-height: .8rem
        }

        .huabei-dialog .sec2 p {
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989;
            line-height: .48rem
        }

        .huabei-dialog .sec2 p .green {
            color: #00a6ff
        }

        .huabei-dialog .upload-img {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 1.77rem;
            background: #f04a5a;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3), 0 .1rem .1rem 0 #000;
            box-sizing: border-box
        }

        .huabei-dialog .upload-img .webuploader-pick {
            padding-top: .3rem
        }

        .huabei-dialog .upload-img .btn {
            width: 5.36rem;
            height: .8rem;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3);
            border-radius: .1rem;
            border: .03rem solid #fff;
            line-height: .77rem;
            font-size: .34rem;
            color: #fff;
            text-align: center;
            margin: 0 auto;
            display: flex;
            justify-content: center
        }

        .huabei-dialog .upload-img .btn i {
            width: .44rem;
            height: 100%;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0;
            background-size: 100%
        }

        .huabei-dialog .upload-img p {
            margin-top: .16rem;
            font-size: .22rem;
            color: #fff;
            text-align: center
        }</style>
    <style type="text/css">.mint-toast {
            z-index: 10000
        }</style>
    <style type="text/css">.cart-dialog {
            background: #27c0c6;
            width: 100%;
            height: 100%;
            padding-top: .3rem;
            box-sizing: border-box;
            overflow: auto
        }

        .cart-dialog .close {
            width: .5rem;
            height: .5rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0 0;
            background-size: 80%;
            margin: 0 0 .3rem .3rem
        }

        .cart-dialog .sec {
            background: #fff;
            box-shadow: 0 0 .1rem 0 rgba(0, 0, 0, .04);
            border-radius: .2rem;
            margin: 0 .3rem
        }

        .cart-dialog .sec1 .dialog-title {
            width: 6.1rem;
            margin: 0 auto;
            position: relative;
            height: 1.24rem;
            padding-top: .5rem;
            box-sizing: border-box
        }

        .cart-dialog .sec1 .dialog-title:after {
            content: "";
            width: 200%;
            height: 200%;
            border-bottom: .01rem solid #000;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%) scale(.5);
            border-radius: 0;
            box-sizing: border-box;
            opacity: .13
        }

        .cart-dialog .sec1 .dialog-title h2 {
            height: .48rem;
            font-size: .34rem;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333;
            line-height: .48rem;
            padding-left: .59rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/credit-cart_c434b435.png) no-repeat 0;
            background-size: .39rem
        }

        .cart-dialog .sec1 .dialog-title p {
            margin-top: .13rem;
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989
        }

        .cart-dialog .sec1 .dialog-content {
            padding-top: .37rem;
            padding-bottom: .46rem
        }

        .cart-dialog .sec1 .dialog-content .qr-img {
            display: block;
            width: 3.18rem;
            max-height: 7.95rem;
            margin: 0 auto
        }

        .cart-dialog .sec1 .dialog-content .save-img {
            display: block;
            margin: .2rem auto;
            height: .45rem;
            font-size: .3rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #ffac00;
            line-height: .45rem;
            outline: none;
            text-align: center
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text {
            display: flex;
            font-size: .28rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 700;
            color: #666;
            justify-content: space-between;
            padding: 0 .4rem;
            box-sizing: border-box;
            width: 100%;
            height: .5rem;
            line-height: .5rem
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text .pre {
            white-space: nowrap
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text .next {
            width: 4rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            text-align: right
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text i {
            width: .3rem;
            height: .32rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/copy_8388f75b.png) no-repeat 0;
            background-size: 100%;
            margin-top: .08rem
        }

        .cart-dialog .sec2 {
            margin-top: .2rem;
            margin-bottom: 2rem;
            padding: .2rem .4rem
        }

        .cart-dialog .sec2 h2 {
            height: .8rem;
            font-size: .3rem;
            font-family: PingFang-SC-Heavy, PingFang-SC;
            font-weight: 800;
            color: #333;
            line-height: .8rem
        }

        .cart-dialog .sec2 p {
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989;
            line-height: .48rem
        }

        .cart-dialog .sec2 p .green {
            color: #00a6ff
        }

        .cart-dialog .upload-img {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 1.77rem;
            background: #f04a5a;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3), 0 .1rem .1rem 0 #000;
            box-sizing: border-box
        }

        .cart-dialog .upload-img .webuploader-pick {
            padding-top: .3rem
        }

        .cart-dialog .upload-img .btn {
            width: 5.36rem;
            height: .8rem;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3);
            border-radius: .1rem;
            border: .03rem solid #fff;
            line-height: .77rem;
            font-size: .34rem;
            color: #fff;
            text-align: center;
            margin: 0 auto;
            display: flex;
            justify-content: center
        }

        .cart-dialog .upload-img .btn i {
            width: .44rem;
            height: 100%;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0;
            background-size: 100%
        }

        .cart-dialog .upload-img p {
            margin-top: .16rem;
            font-size: .22rem;
            color: #fff;
            text-align: center
        }</style>
    <style type="text/css">.mint-toast {
            z-index: 10000
        }</style>
    <style type="text/css">.cart-dialog {
            background: #f7b500;
            width: 100%;
            height: 100%;
            padding-top: .3rem;
            box-sizing: border-box;
            overflow: auto
        }

        .cart-dialog .close {
            width: .5rem;
            height: .5rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0 0;
            background-size: 80%;
            margin: 0 0 .3rem .3rem
        }

        .cart-dialog .sec {
            background: #fff;
            box-shadow: 0 0 .1rem 0 rgba(0, 0, 0, .04);
            border-radius: .2rem;
            margin: 0 .3rem
        }

        .cart-dialog .sec1 .dialog-title {
            width: 6.1rem;
            position: relative;
            height: 1.74rem;
            padding: .5rem .4rem 0;
            box-sizing: border-box
        }

        .cart-dialog .sec1 .dialog-title:after {
            content: "";
            width: 200%;
            height: 200%;
            border-bottom: .01rem solid #dde0e5;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%) scale(.5);
            border-radius: 0;
            box-sizing: border-box;
            opacity: .13
        }

        .cart-dialog .sec1 .dialog-title h2 {
            height: .48rem;
            font-size: .34rem;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333;
            line-height: .48rem;
            padding-left: .59rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/cart_883b14f8.png) no-repeat 0;
            background-size: .39rem
        }

        .cart-dialog .sec1 .dialog-title p {
            margin-top: .13rem;
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989
        }

        .cart-dialog .sec1 .dialog-content {
            padding-top: .37rem;
            padding-bottom: .46rem
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text {
            display: flex;
            font-size: .28rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 700;
            color: #666;
            justify-content: space-between;
            padding: 0 .4rem;
            box-sizing: border-box;
            width: 100%;
            height: .5rem;
            line-height: .5rem
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text .pre {
            white-space: nowrap
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text .next {
            width: 4rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            text-align: right
        }

        .cart-dialog .sec1 .dialog-content .pay-info-text i {
            width: .3rem;
            height: .32rem;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/copy_8388f75b.png) no-repeat 0;
            background-size: 100%;
            margin-top: .08rem
        }

        .cart-dialog .sec2 {
            margin-top: .2rem;
            margin-bottom: 2rem;
            padding: .2rem .4rem
        }

        .cart-dialog .sec2 h2 {
            height: .8rem;
            font-size: .3rem;
            font-family: PingFang-SC-Heavy, PingFang-SC;
            font-weight: 800;
            color: #333;
            line-height: .8rem
        }

        .cart-dialog .sec2 p {
            font-size: .26rem;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #898989;
            line-height: .48rem
        }

        .cart-dialog .sec2 p .green {
            color: #00a6ff
        }

        .cart-dialog .upload-img {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 1.77rem;
            background: #f04a5a;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3), 0 .1rem .1rem 0 #000;
            box-sizing: border-box
        }

        .cart-dialog .upload-img .webuploader-pick {
            padding-top: .3rem
        }

        .cart-dialog .upload-img .btn {
            width: 5.36rem;
            height: .8rem;
            box-shadow: 0 0 .08rem 0 rgba(0, 0, 0, .3);
            border-radius: .1rem;
            border: .03rem solid #fff;
            line-height: .77rem;
            font-size: .34rem;
            color: #fff;
            text-align: center;
            margin: 0 auto;
            display: flex;
            justify-content: center
        }

        .cart-dialog .upload-img .btn i {
            width: .44rem;
            height: 100%;
            background: url(https://xinbotrader.oss-cn-hongkong.aliyuncs.com/payimg/upload-icon_9eb2ea54.png) no-repeat 0;
            background-size: 100%
        }

        .cart-dialog .upload-img p {
            margin-top: .16rem;
            font-size: .22rem;
            color: #fff;
            text-align: center
        }</style>
    <style type="text/css">.mint-popup {
            background: #49494e
        }
        .mint-popup .preview-img{
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            max-width: 6rem
        }</style>
    <style type="text/css" title="fading circle style">.circle-color-23 > div::before {
            background-color: #ccc;
        }</style>
</head>
<body>
<div id="main">
    <div>
        <div class="mint-popup mint-popup-bottom">
            <div class="cart-dialog">
                <#if payConfig??>
                    <#switch payConfig.type>
                        <#case 1>
                            <div class="sec sec1">
                                <div class="dialog-title"><h2>${payConfig.payName}</h2>
                                    <p>保存二维码到相册，使用微信 [扫一扫] 付款</p></div>
                                <div class="dialog-content"><img alt="" src="${payConfig.wxCodeUrl}" class="qr-img"><span class="save-img">长按图片另存为</span>
                                    <p class="pay-info-text account"><span class="pre">微信账号：</span> <span class="next">${payConfig.wxAccount}</span>
                                        <i onclick="copytxt('${payConfig.wxAccount}')"></i></p>
                                    <p class="pay-info-text name"><span class="pre">微信姓名：</span> <span
                                                class="next">${payConfig.wxNickName}</span> <i onclick="copytxt('${payConfig.wxNickName}')"></i></p></div>
                            </div>
                            <#break>
                        <#case 2>
                            <div class="sec sec1">
                                <div class="dialog-title"><h2>${payConfig.payName}</h2>
                                    <p>保存二维码到相册，使用支付宝 [扫一扫] 付款</p></div>
                                <div class="dialog-content"><img alt=""  src="${payConfig.aliPayCodeUrl}"  class="qr-img">
                                    <span class="save-img">长按图片另存为</span>
                                    <p class="pay-info-text account"><span class="pre">支付宝账号：</span>
                                        <span class="next">${payConfig.aliPayAccount}</span>
                                        <i onclick="copytxt('${payConfig.aliPayAccount}')"></i>
                                    </p>
                                    <p class="pay-info-text name"><span class="pre">支付宝姓名：</span>
                                        <span class="next">${payConfig.aliPayNickName}</span>
                                        <i onclick="copytxt('${payConfig.aliPayNickName}')"></i>
                                    </p>
                                </div>
                            </div>
                        <#break>
                        <#case 3>
                            <div class="sec sec1" style="margin-top: 20px">
                                <div class="dialog-title"><h2>${payConfig.payName}</h2>
                                    <p>可使用支付宝，微信 [转账到银行卡] 功能付款</p></div>
                                <div class="dialog-content"><p class="pay-info-text name">
                                        <span class="pre">收款人：</span> <span class="next">${payConfig.bankPayee}</span>
                                        <i onclick="copytxt('${payConfig.bankPayee}')"></i></p>
                                    <p class="pay-info-text account"><span class="pre">银行卡号：</span> <span class="next">${payConfig.bankAccount}</span>
                                        <i onclick="copytxt('${payConfig.bankAccount}')"></i>
                                    </p>
                                    <p class="pay-info-text name"><span class="pre">归属银行：</span> <span
                                                class="next">${payConfig.ascriptionBank}</span>
                                        <i onclick="copytxt('${payConfig.ascriptionBank}')"></i></p>
                                    <p class="pay-info-text name"><span class="pre">开户支行：</span> <span
                                                class="next">${payConfig.openAccountBank}</span>
                                        <i onclick="copytxt('${payConfig.openAccountBank}')"></i></p></div>
                            </div>
                            <div class="sec sec2" style="margin-top: 20px"><h2>温馨提示</h2>
                                <p>* 支持各种银行，支付宝，微信的银行卡转账</p>
                                <p>* 支付宝转到银行卡功能路径：<span class="green">打开支付宝</span> &gt; <span class="green">转账</span> &gt;<span
                                            class="green">转到银行卡</span></p>
                                <p>* 微信转到银行卡功能路径：<span class="green">打开微信</span> &gt; <span class="green">右上角+号</span> &gt;<span
                                            class="green">转到银行卡</span></p>
                                <input type="text" id="copyTxt" style="position: absolute; top: 0; left: 0; opacity: 0; z-index: -10;" >
                            </div>
                        <#break>
                        <#case 4>
                            <div class="sec sec1" style="margin-top: 20px">
                                <div class="dialog-title"><h2>${payConfig.payName}</h2>
                                    <p>使用微信 [转账到银行卡] 功能付款</p></div>
                                <div class="dialog-content"><p class="pay-info-text name">
                                        <span class="pre">收款人：</span> <span class="next">${payConfig.bankPayee}</span>
                                        <i onclick="copytxt('${payConfig.bankPayee}')"></i></p>
                                    <p class="pay-info-text account"><span class="pre">银行卡号：</span> <span class="next">${payConfig.bankAccount}</span>
                                        <i onclick="copytxt('${payConfig.bankAccount}')"></i>
                                    </p>
                                    <p class="pay-info-text name"><span class="pre">归属银行：</span> <span
                                                class="next">${payConfig.ascriptionBank}</span>
                                        <i onclick="copytxt('${payConfig.ascriptionBank}')"></i></p>
                                    <p class="pay-info-text name"><span class="pre">开户支行：</span> <span
                                                class="next">${payConfig.openAccountBank}</span>
                                        <i onclick="copytxt('${payConfig.openAccountBank}')"></i></p></div>
                            </div>
                            <div class="sec sec2" style="margin-top: 20px"><h2>温馨提示</h2>
                                <p>* 微信转到银行卡功能路径：<span class="green">打开微信</span> &gt; <span class="green">右上角+号</span> &gt;<span
                                            class="green">转到银行卡</span></p>
                                <input type="text" id="copyTxt" style="position: absolute; top: 0; left: 0; opacity: 0; z-index: -10;" >
                            </div>
                        <#break>
                        <#case 5>
                            <div class="sec sec1" style="margin-top: 20px">
                                <div class="dialog-title"><h2>${payConfig.payName}</h2>
                                    <p>使用支付宝 [转账到银行卡] 功能付款</p></div>
                                <div class="dialog-content"><p class="pay-info-text name">
                                        <span class="pre">收款人：</span> <span class="next">${payConfig.bankPayee}</span>
                                        <i onclick="copytxt('${payConfig.bankPayee}')"></i></p>
                                    <p class="pay-info-text account"><span class="pre">银行卡号：</span> <span class="next">${payConfig.bankAccount}</span>
                                        <i onclick="copytxt('${payConfig.bankAccount}')"></i>
                                    </p>
                                    <p class="pay-info-text name"><span class="pre">归属银行：</span> <span
                                                class="next">${payConfig.ascriptionBank}</span>
                                        <i onclick="copytxt('${payConfig.ascriptionBank}')"></i></p>
                                    <p class="pay-info-text name"><span class="pre">开户支行：</span> <span
                                                class="next">${payConfig.openAccountBank}</span>
                                        <i onclick="copytxt('${payConfig.openAccountBank}')"></i></p></div>
                            </div>
                            <div class="sec sec2" style="margin-top: 20px"><h2>温馨提示</h2>
                                <p>* 支付宝转到银行卡功能路径：<span class="green">打开支付宝</span> &gt; <span class="green">转账</span> &gt;<span
                                            class="green">转到银行卡</span></p>
                                <input type="text" id="copyTxt" style="position: absolute; top: 0; left: 0; opacity: 0; z-index: -10;" >
                            </div>
                        <#break>
                        <#default>
                    </#switch>
                </#if>
            </div>
        </div>
    </div>
</div>

<script src="../payjs/mvvm.js"></script>
<script src="../payjs/dependent.js"></script>
<script src="../payjs/chunk.e513dd65.js"></script>
<script src="../payjs/index.1c6cd877.js"></script>

<script>
    $.fn.scrollUnique = function () {
        return $(this).each(function () {
            var eventType = 'mousewheel';
            // 火狐是DOMMouseScroll事件
            if (document.mozHidden !== undefined) {
                eventType = 'DOMMouseScroll';
            }
            $(this).on(eventType, function (event) {
                // 一些数据
                var scrollTop = this.scrollTop,
                    scrollHeight = this.scrollHeight,
                    height = this.clientHeight;

                var delta = (event.originalEvent.wheelDelta) ? event.originalEvent.wheelDelta : -(event.originalEvent.detail || 0);

                if ((delta > 0 && scrollTop <= delta) || (delta < 0 && scrollHeight - height - scrollTop <= -1 * delta)) {
                    // IE浏览器下滚动会跨越边界直接影响父级滚动，因此，临界时候手动边界滚动定位
                    this.scrollTop = delta > 0 ? 0 : scrollHeight;
                    // 向上滚 || 向下滚
                    event.preventDefault();
                }
            });
        });
    };
    function copytxt(txt) {
        //获取要赋值的input的元素
        var inputElement =  document.getElementById("copyTxt");
        //给input框赋值
        inputElement.value = txt;
        //选中input框的内容
        inputElement.select();
        // 执行浏览器复制命令
        document.execCommand("Copy");
        //提示已复制
        alert("已复制好，可贴粘。");
    }
</script>
<script src="../payjs/index.0e9939a5.js"></script>
<script src="../payjs/webuploader.min.js"></script>
<div class="mint-indicator" style="display: none;">
    <div class="mint-indicator-wrapper" style="padding: 15px;"><span class="mint-indicator-spin"><div
                    class="mint-spinner-fading-circle circle-color-23" style="width: 32px; height: 32px;"><div
                        class="mint-spinner-fading-circle-circle is-circle2"></div><div
                        class="mint-spinner-fading-circle-circle is-circle3"></div><div
                        class="mint-spinner-fading-circle-circle is-circle4"></div><div
                        class="mint-spinner-fading-circle-circle is-circle5"></div><div
                        class="mint-spinner-fading-circle-circle is-circle6"></div><div
                        class="mint-spinner-fading-circle-circle is-circle7"></div><div
                        class="mint-spinner-fading-circle-circle is-circle8"></div><div
                        class="mint-spinner-fading-circle-circle is-circle9"></div><div
                        class="mint-spinner-fading-circle-circle is-circle10"></div><div
                        class="mint-spinner-fading-circle-circle is-circle11"></div><div
                        class="mint-spinner-fading-circle-circle is-circle12"></div><div
                        class="mint-spinner-fading-circle-circle is-circle13"></div></div></span> <span
                class="mint-indicator-text" style="display: none;"></span></div>
    <div class="mint-indicator-mask"></div>
</div>
</body>
</html>