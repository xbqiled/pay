<html>
<head>
    <meta charset="utf-8">
    <title>聊天中....</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" href="../css/common.css"/>
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    <link rel="stylesheet" href="../layer/theme/layer.css"/>
    <script type="text/javascript" src="../chatjs/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../chatjs/seach.js"></script>
    <script type="text/javascript" src="../chatjs/scrolls.js"></script>
    <script type="text/javascript" src="../layer/layer.js"></script>
    <script src="http://cdn.bootcss.com/jquery/1.12.3/jquery.min.js"></script>

    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/User.js"></script>
    <script type="text/javascript" src="../js/Content.js"></script>
    <script type="text/javascript" src="../js/LoginReq.js"></script>
    <script type="text/javascript" src="../js/LoginRes.js"></script>
    <script type="text/javascript" src="../js/MessageReq.js"></script>
    <script type="text/javascript" src="../js/MessageRes.js"></script>
    <script type="text/javascript" src="../js/Message.js"></script>
</head>
<style>
    ::-webkit-scrollbar {
        width: 0px;
    }

    .bri_bottom textarea {
        width: 72% !important;
        margin-left: 10px !important;
        margin-top: 8px !important;
        padding: 0 10px !important;
        box-sizing: border-box !important;
    }
</style>
<script>
    $(function () {
        res();
        scrolls();
        chatscr();
        $(window).resize(function () {
            res();
            scrolls();
            chatscr();
        })

        function res() {
            var winds = $(window).height();
            var windw = $(window).width();
            //最左边高度
            $(".buju_left").height(winds);
            //中间高度
            $(".buju_center").height(winds);
            //右边高度
            $(".buju_right").height(winds);
            $(".yinyings").height(winds);
            $(".buju_install").height(winds - 61);
            $(".bcen_box").height(winds - 62);
            $(".buju_right").width(windw - 310);
            $(".bri_bottom").width(windw - 310);
            $(".brce_block_box").width(windw - 310);
            $(".buju_right textarea").width(windw - 360);
            // $(".bri_center").height(winds - 205);
            $(".bri_center").height(winds - 110);
            $(".brce_block_box").height(winds - 205);
        }

        /*$(".bcen_block").mouseenter(function (){
            $(this).not(".bcen_background").css("background-color","#ddd");
        })
        $(".bcen_block").mouseleave(function (){
            $(this).not(".bcen_background").css("background-color","#e7e8e9");
        })

        $(".bcen_block").click(function (){
            $(this).removeAttr("style");
            $(this).addClass("bcen_background");
            $(".bcen_block").not(this).removeClass("bcen_background");
            $(this).find(".bcen_block_dian").css("display","none");
            $(this).find(".bcen_block_nums").css("display","none");
        })*/

        $(".bri_bottom textarea").focus(function () {
            $(".bri_bottom").css("background-color", "#fff");
        })
        $(".bri_bottom textarea").blur(function () {
            $(".bri_bottom").css("background-color", "#f5f5f5");
        })

        $(".bri_bottom_face").click(function () {
            $(".bri_bottom_look").css("display", "block");
        })

        $(document).bind("click", function (e) {
            //id为menu的是菜单，id为open的是打开菜单的按钮
            if ($(e.target).closest(".bri_bottom_look").length == 0 && $(e.target).closest(".bri_bottom_face").length == 0) {
                //点击id为menu之外且id不是不是open，则触发
                $(".bri_bottom_look").css("display", "none");
            }
            if ($(e.target).closest(".payImage").length == 0 && $(e.target).closest(".payType").length == 0) {
                $(".payTypeModule").hide();
            }

        })

        $(".bri_bottom_img").click(function () {
            $(".bri_bottom_imgs").click();
        })


        /*$(".bri_top_more").click(function (){
            alert('cnbm');
            var bl = $(".buju_install").css("display");
            if(bl == "block"){
                $(".buju_install").css("display","none");
            }else if(bl == "none"){
                $(".buju_install").css("display","block");
            }
        })*/

        /*$(".bin_two_but").click(function (){
            var imgs = $(this).find("img").attr("src");
            imgs = imgs.replace("../images/","");
            imgs = imgs.replace(".png","");
            if(imgs == "butclose"){
                $(this).find("img").attr("src","../images/butopen.png");
            }else if(imgs == "butopen"){
                $(this).find("img").attr("src","../images/butclose.png");
            }
        })*/

        /*$(".bin_three input").focus(function (){
            $(".bin_three").css("background-color","#fff");
        })

        $(".bin_three input").keyup(function (){
            var val = $(this).val();
            if(val == ""){
                $(".bin_three_close").css("display","none");
                $(".bin_three_seach").css("display","block");
            }else if(val != ""){
                $(".bin_three_close").css("display","block");
                $(".bin_three_seach").css("display","none");
            }
        })

        $(".bin_three_close").click(function (){
            $(".bin_three input").val("");
            $(".bin_three_close").css("display","none");
            $(".bin_three_seach").css("display","block");
            $(".bin_three").css("background-color","#f5f5f5");
        })*/

        /*$(".bin_four_more").click(function (){
            var hts = $(this).find("label").html();
            if(hts == "查看更多群成员"){
                $(this).find("label").html("收起群成员");
                $(this).find("img").attr("src","../images/up001.png");
            }else if(hts == "收起群成员"){
                $(this).find("label").html("查看更多群成员");
                $(this).find("img").attr("src","../images/down001.png");
            }
        })*/

        /*$(".bin_five_text").mouseenter(function (){
            $(this).next("img").css("display","block");
        })

        $(".bin_five_text").mouseleave(function (){
            $(this).next("img").css("display","none");
        })

        $(".bin_five_text").click(function (){
            $(this).next("input").attr("type","text");
            $(this).css("display","none");
            $(this).next("input").focus();
        })

        $(".bin_five_text").next("input").blur(function (){
            var bin = $(this).val();
            if(bin != ""){
                $(this).prev(".bin_five_text").html(bin);
            }
            $(this).prev(".bin_five_text").css("display","block");
            $(this).attr("type","hidden");
        })

        $(".bin_five_text").eq(0).next("input").blur(function (){
            var bin = $(this).val();
            $(".bri_top_font").html(bin);
        })*/

        /*$(".bin_five_butname").click(function (){
            var imgs = $(this).attr("src");
            imgs = imgs.replace("../images/","");
            imgs = imgs.replace(".png","");
            if(imgs == "butclose"){
                $(this).attr("src","../images/butopen.png");
                $(".brce_lt_name").css("display","block");
            }else if(imgs == "butopen"){
                $(this).attr("src","../images/butclose.png");
                $(".brce_lt_name").css("display","none");
            }
        })*/

        /*$(".bin_five_but").click(function (){
            var imgs = $(this).attr("src");
            imgs = imgs.replace("../images/","");
            imgs = imgs.replace(".png","");
            if(imgs == "butclose"){
                $(this).attr("src","../images/butopen.png");
            }else if(imgs == "butopen"){
                $(this).attr("src","../images/butclose.png");
            }
        })

        $(".bin_five_note").click(function (){
            $(".bin_note").css("display","block");
        })

        $(".bin_note_close").click(function (){
            $(".bin_note").css("display","none");
        })*/

    })
</script>
<body>
<div class="buju" style="display: flex">
    <!-- 最左侧功能图标 -->
    <#--<div class="buju_left fl"  style="width: 60px">
        <div class="bleft_face fl">
            <img id ="toUserAvatar" src="../images/face001.jpg"/>
        </div>
        <div class="bleft_iconone fl">
            <a href="#"><img src="../images/chat011.png" width="100%"/></a>
        </div>
    </div>-->
    <script>
        $(function () {
            var image_width = $(".bleft_face").width();
            var image_height = $(".bleft_face").height();
            var image_bi = image_width / image_height;
            $(".bleft_face img").each(function () {
                var images_width = $(this).width();
                var images_height = $(this).height();
                var images_bi = images_width / images_height;
                if (image_bi < images_bi) {
                    $(this).css("height", image_height);
                    var imgwidth = image_height * images_bi;
                    var widths = 0 - (imgwidth - image_width) / 2;
                    $(this).css("margin-left", widths);
                } else {
                    $(this).css("width", "100%");
                    var imgheight = image_width / images_bi;
                    var heights = 0 - (imgheight - image_height) / 2;
                    $(this).css("margin-top", heights);
                }
            })
        })
    </script>
    <!-- 右边对话框 -->
    <style>
        .buju_right {
            width: 100% !important;
        }

        .brce_block_box {
            width: 100% !important;
        }

        .bri_bottom {
            width: 100% !important;
        }

        .bri_bottom_but {
            position: absolute;
            bottom: 15px;
            right: -24px;
            display: none;
        }

        .bri_top {
            background: url('../img/newHead.jpg') no-repeat;
            background-size: 100% 100%;
        }

        .bri_float {
            background-color: #e0d8f0;
            position: absolute;
            top: 50px;
            left: 0;
            text-align: center;
            line-height: 30px;
            width: 100%;
            color: #e05922;
            font-size: 14px;
            font-weight: 600;
        }

        .wxpayBack {
            background-image: linear-gradient(to right, #91e68f, #43af40);
        }

        .alipayBack {
            background-image: linear-gradient(to right, #d6a6fc, #8164d2);
        }

        .uninoPayBack {
            background-image: linear-gradient(to right, #e0c763, #cd9238);
        }

        .chatCard .cardImg {
            float: left;
            width: 60px;
            height: 60px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .chatCard .cardImg img {
            width: 55px;
        }

        .chatCard .cardFont {
            font-size: 12px;
        }

        .chatCard .cardFont span:nth-child(1) {
            font-size: 16px;
            font-weight: 600;
        }

        .chatCard .cardClick {
            height: 30px;
            background: #ffffff;
            margin: 0;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            line-height: 28px;
            font-weight: 600;
            color: #8571ee;
        }
    </style>
    <div class="buju_right fl" style="width: 100%">
        <#--        <div class="bri_float">-->
        <#--            官方一对一专职服务，3秒极速到账-->
        <#--        </div>-->
        <div class="bri_top">
            <!--            <div class="bleft_face fl">-->
            <!--                &lt;!&ndash; <img id ="toUserFace" src="../images/face001.jpg"/> &ndash;&gt;-->
            <!--                <img id ="toUserFace" src=  ${toAvatar} />-->
            <!--            </div>-->
            <div class="bri_top_font fl">
                ${toUserNickName}
            </div>

            <div class="bri_top_more fr" style="display: none"></div>
            <div class="clear"></div>
        </div>
        <div class="bri_center">
            <div class="bri_center_scroll" style="display:none;"></div>
            <div class="bri_center_block">
                <div class="brce_block_box" style="width: 100%;">
                </div>
            </div>
        </div>
        <script>
            $(function () {
                img();
                $.each(emojiList, function (index, item) {
                    htmlEmojiList += '<div class="fl">'
                    htmlEmojiList += item
                    htmlEmojiList += '</div>'
                })
                $(".bri_blook_center").html(htmlEmojiList)
                $(".bri_blook_center div").click(function () {
                    var text = $("#sendtxt").val() + $(this).html()
                    $("#sendtxt").val(text)
                })
            })

            function img() {
                var img_width = $(".brce_left_face").width();
                var img_height = $(".brce_left_face").height();
                var img_bi = img_width / img_height;
                $(".brce_left_face img").each(function () {
                    var imgs_width = $(this).width();
                    var imgs_height = $(this).height();
                    var imgs_bi = imgs_width / imgs_height;
                    if (img_bi < imgs_bi) {
                        $(this).css("height", img_height);
                        var imgswidth = img_height * imgs_bi;
                        var wid = 0 - (imgswidth - img_width) / 2;
                        $(this).css("margin-left", wid);
                    } else {
                        $(this).css("width", "100%");
                        var imgsheight = img_width / img_bi;
                        var hei = 0 - (imgsheight - img_height) / 2;
                        $(this).css("margin-top", hei);
                    }
                })
            }

            var emojiList = [
                '😀', '😁', '😂', '😄', '😅', '😆', '😇', '😉', '😊', '😋', '😌', '😍', '😘', '😙', '😜', '😝', '😎', '😏', '😶', '😑', '😒', '😳', '😞', '😟', '😠', '😡', '😔', '😕', '😣', '😖', '😫', '😤', '😮', '😱', '😨', '😰'
            ]
            var htmlEmojiList = ''
        </script>
        <style>
            .cardType {
                width: 100%;
                height: 50px;
                background: #c0b1da;
                position: absolute;
                bottom: 60px;
                left: 0;
                display: flex;
                align-items: center;
                padding-left: 10px;
                box-sizing: border-box;
            }

            .cardType div {
                height: 45px;
                width: 45px;
                margin-left: 10px;
            }

            .cardType img {
                width: 100%;
            }

            .payTypeModule {
                width: auto !important;
                height: 40px;
                position: absolute;
                top: -50px;
                align-items: center;
                justify-content: center;
                box-sizing: border-box;
                margin-left: 0 !important;
                display: none;
            }

            .payTypeModule div {
                width: auto !important;
                height: 30px;
                font-size: 12px;
                float: left;
                display: flex;
                align-items: center;
                justify-content: center;
                background: #ffffff;
                border: 1px solid #a2a5ae;
                padding: 0 10px;
                margin-left: 10px;
            }
        </style>
        <div class="cardType">
            <div class="payTypeModule"></div>
            <div id="pmwx" onclick="showPayType('wx', this)" class="payImage" tabindex="1"
                 style="display: none;border: 3px solid rgb(192, 177, 218);"><img src="../img/pay/1.png"></div>
            <div id="pmali" onclick="showPayType('ali', this)" class="payImage" tabindex="1"
                 style="display: none;border: 3px solid rgb(192, 177, 218);"><img src="../img/pay/2.png"></div>
            <div id="pmcard" onclick="showPayType('card', this)" class="payImage" tabindex="1"
                 style="display: none;border: 3px solid rgb(192, 177, 218);"><img src="../img/pay/3.png"></div>
            <#--<div>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACCCAYAAACHHWC6AAAgAElEQVR4Xt29CZwc1XUv/K+ll5numelZJI2kkTRIgxgjgYSNQQYSNY6d2ODdCTF+jp+x4/g5eQnEL/vywyTfS56z+JkvwTbxopfEzziyE9tJiE3smFGMhTDYSCDBSIzEACOptY1m6aW6a/t+59a9NaduV0tgwMbf8BM9091Vdevec/7nf5Z7ysCP6c+9O57Mo4CSk3HycIBaaOY90y8thkZ+zvNQg405AKc9iFf6VwEwJe43H9215wG23WEGPPF+ER6GYGMEwBA8lOg9GyjBwyDo1Ube9lAwLMewrTnbChxxBTfvoIa5a2+6QPz94/Jj/LgMVB/nHXc/OfyMl9taMbLDk7Cwx8gMI7C2wLSHYYRAAMCn/9GCm0AYLTACei+UpzMA04zeo1dffAEQsxKKwwCLJCc6h/ibzhUAlgVxHTqVQZ/7lSvh79toepU1vovVllfphrf3pusvIDn8sfl5yQvEvfc+mccplBzfyZOqNUIzXwuc0knDGp3xzK1Pwx5+BCaeCTLDMDJbYGWGxRqGtGAGYNiAQb/TwqmFpkWlBbeiV/UjfqUvkdDI74r3fCCk9+lVCRr97QE+SZELBH5lVeju22IHldHAw4jpTw/YmCjBm6Yz9FpZp7vW/ZJHjJe8QNxx98nhBcvb+oSfHX7Qt/ColxmGaW4BrFEEYQmw8whbpMl5hEYJYZCXKw8YtFh0i2b0IhZXqPTSKwkECU5SKqK/6H0hMOpzfqxEC/GZQBAHRjgHw3ai64bTgL8Llj+9FS1stYzKKPy9H75++UsaMV5yAhFxg3ppzm/l55BHxfNGTwVB+YkgP/otM4OakR+GaW+BKQRDIgDBPMECYTpbbC4QJAi0wIFEBJMWWwpIYuG5AKjTMRPDzU1kQyKEIAQhU6LO6XsVhN4+BK3KMtRxudeqrLewd72P6WLOm1tuZBziI7DyDpZh7tprXxpc4yUnEIQIUxa27g/zw99waa6tUZjGdoQCEWji8zAlEiS0m8wDKSYzAZGaL3EGgQRMy5WJiNebH5uGCiR0dEppUuKva8gRIY6DMJxDYDiwSBhDByZx23AaVmvfa22zsgktXBAElZU4tfeG6ze9JJDjRy4QChEc38xX4OApxx6dRFD+HrpGnzBzQKY4DMvaAiKNtLiKDygbrxY81nIlBMwKKHTgb9F7sUDIBSWSGJsHZV6U6ZAmQsyYYJ+SV6iTdjhWcA9CDuIfPuC6Ffi1fRcGbuUVaGHcDKfXZVsTw8hP58n5+RF7Jz9ygdix88nhg4XC1r3IDH/dJ/KXGYUXbodvjEaMPszDQAmGkY+1Xyw+W7AY/qUwCCCgRaAv0T8F7QwtOHIIQWPOh1rjGCQUyeRchJsbNRaJIIKUEiowYVpCEwcI5mAIzkHIMQ3b3/U605t+ue/iwrBZWW57e6//EXknP3SBOLDzQPYZoGjmrHyz6dvTNXP9ARvl3Z41+ohAhMIwMpktCOxhWJloYYW3wM2BtrBCs5PepDiANDJ2L/iip3ECDh/y/DHqKIFQC66bFoUOSvDk94VAEEDI76vzCbeY/nmA61TgN/ZdGjiVq8wAm0xveti2J0pW9khXzvXMZsHpw0J10w2biDm/6D8/dIH4+s4DAzNdXWOPGt3D/xZki0+EXRsRGhEi0LwFYR4WcQSBDNGMJmy1bhIkWeQEMRYOtvAdEUCZjrS51qeHCUKMUpymMLORxmfayKsQMAfw5mDAgU2kN5h+udnYdZURHLo8bFQHA6+ScRtTr7th0+yLLg2awXxRr0dcoV6olxp+MHI8xPhDrczo3yI7DLswBjO3BVY28hrEesuFEC+CSDIyKIepvxXzOk0bddPAvQRdg7l7yRdPLW4qT1ECqXiIQrLzkVw6Kd0vHe9HUdPABVrNSjGo7XuT5U9ts9zKatM60p0x9vcF/kwf8KIjxQ8NIXbc/eTwQS+39VvIjn036FoBM0Newzi8cASGQR6YjCczuYy1jAmI7kTEXoOCBZ0PcPPA3MjEgiu+oSCfCR03RUpYYzRSCJYiDAn3lJsUAYNLgh+fSwqHT0iBOZjGDEJn8pVwD223nMOX+cHUoIup192w5kVFihdNIIgrzAPFquIKzWD9Adjlf/UzG2eypYLwGjKZMYTmQMfAUBvj57Cgs3olSJoJSZyjjWjIg3wZhFKQwQVCIlRMMDWzEV9Wcz15PCQWDk46mQBzV9in8QfEL2bhNabW+I2pnzGbh18ZtCaHM8b+nG3OrHkRkeJFEwjiCgsZe+yYWRy+1+4qftUJN8IobEdorEcQ2DBsQoQigGzs1yutVfEFQQrpR4ae4xiDNmwO6WKez0calfehzqN5D+p4EexS39WQK8FTYmlpt8KcS7SZIXmSOFIqI6PRMS0YYRWGPQPD24/Amfy5jHf4Vb4zNeJi6oYXCSlecIHYufNAtpcSgqY1UnFbm6ca5vp7zNzw/XZ+DFaP5Ao0ERnGC5Q3wLVGBoDE4qgIJKc5ukbyz3QvhLuUErI5KVS2XKGJClcL70ZGIBMMSzNDCbOlj1GaFfG2bu/YOFWSLT6cPvOBwJhF6E6hOT+1PWwcvjpwJi/KGftX2ObMAlC94QX2Pl5wgSBkOJPpGps0MmM7s90bDjYzG4HMODxzBIZZAsh7IBjmC5rGEPWJ1LRQ5xfxRHKToaCfvZeam0gGNGMXVwlNm8fQidEygY7XmnkxiXiJuj/6osywCjCipBvPoVCixqgiNGZgevth+pNXWM7hsuFNXd7wXnCkeMEEQsUXmqY1Mut6m/e1MP4x2BuQ6R+D1UWexAAMSZzEBKibViuZRgY5mdQ5gyZQqZ6AnHRFDdSEd+QmSoD0pFaKP6a7wm33w64tOKRa6Jh0yNR6iklKJODk94NwFqE/Ba82NdyaP/zWjD+5zfRecKR4wQRCIcMjRmbsTqO4YS7MjcM3NyMwRmCiiDDMLpFHlQtQrF737zUO0Oa/K+3iQpJmQnTvhHsDXJtpPCQEzIYroRGnoIgpvVJyhRFP8SvFDrg5kILdxmW4IHTiHGlCHyMlBaaqCMIZmOF+WMHkOqN5+E1Wc+onXkCkeN4CESODF4ycCsLNe8Ps+F+HuQ3I9I7Byo/BsAeiueX+unK7qB5B4w3KziqiFX+uCwETGo7gqYSSLUBaLIFYvTidQjApGATd9BNILiEKYWQOI06U8ZyGSqWzsXVMtnGhYOYxfpvzDXmDdE0/mAWCKbhVQovD77G8yavN5v41LxCneN4CEXkTXWN7g9zYn5jFDTCy4wi8zQisEcAsAkZWTGKsaWRTVTiaT1wnDeJ1Ceo750IIfh72/QSJZHUwurAmeIMSZHm9BP/gY9GFWv9M+1tdMxFwk3Mhrq+Qk5Pt+BwtwK8CmIGB/TCtyRXBwuH/mjWmLm80njen+IEFQiFD6AUjFSPc/L1WdvyvjZ4NMHNjyFljMPIDS0kldTPyRkkglEK2RSL5xHCI1+FUr1XoJCya+RFrJ8+lOwtp3kJCYNJMkBSWBMllaJgwd3KMAohUBFYJk/wsIRCscishU8LEzSLwp+A6U/Cqh3/J8ievNuuCUzyfOMUPLBC7dx4YOJHpGpsKzLH/Y/ZsOIDsOMLsZnjBCEwjQoaORE/dnbK3jFNwdh5nIc/FFeIDksxPN9NUN0mkVtRUKo1XCyxNAiWchKupaiWVZ+xGx3L+oAQrnkFmt/R7aCOJ3MYpFNJyNh3T+TQXFDcxWgiDKhDOwAj3wwwmrzDqh98UOlMb3YWpG37A3MdzFggVgayb1sgp19t8wAnGP2H1bDhjdY8h0zMGWAMwNdePXyXmfgwJRL2AMgOabe3E3mON0Ykim+D4O8proAxjAHic1Mo0tThMCQgnuSZgUSSTFkEFqjgqcA3XeYG6J+07bQKjn0PnD8wkiWMV1xFjn4XnTiFwptY4s4d/Hs7ktpy7v2jnfyCkeM4CQchwhiKQgT32Fbu44WuuNQ6juBl+MALbipBBpXvVfCSIIYdGeaO0EJ1Gkpg8bYLP67oy4SBBIJj2HMCoSwGkhZbjEWbEjviNKcmjiJBS1XZ/JBCJ5Bt932Mhb3azMV/RTBMpivBmNCGmc4s5SjMRHE1ibZIgR58ZLSCsIiBO4e+H5U++zagdfm1Yn7rgB8iSPmeBuO+rk6tONHH5k3Xn0i9ZhYv2UPo6XxqDYQ5EE6bgV7pyCbjk8M4mMNZMpdLMRKQpna54iUXVtVcKBZXkWybQmwN6qQwaWEvKT8RdKqRYL4oOiHkGDtMtLAI4RQ5fU5oTKt1Xi0qCI7/M0Uj8rmVBJdIns7fqRpjGq2NTK3a4hmk2MUAU0fSqU9d6c4evQ2NyY8Z+zrmP5ywQO3ceXnt/Nn/N33mZq85YxSvhG+sjbwKRN9EWnmULFPvzGnuOCVk8q0sakIYuaZOlxyqUlqrQt+sDdh2vWJ3FLy2r46JCAQUzEtpAVE3TyCOBDoSJNlALTDxxtoUPHu0FThGyyOSUzeMnOr9JwOISr4nvUSoFH6+6x4SrTYdy0yWXqrMb20LoV2GYM0BzPwxn8g/ROnyJ706NoDF11bPkFM9aIFQ9w2wzGN8Fu/xpL78NXf1bYLI6hraF4jUBXJPoRnVvQhOGtjoG7lWwSRUryaGUo5DUZPqYBKIY4ue3FvDuIeDCbqBLWQvtVRTR0x4QAJOngZ+dBHCU/hKSApBAxBCfJhAahMXJK3XbKbyn7TSK2+hzyO61LbQuKrFm4den4FcP/apXPXiFZTyywrYe+ukbx4/pM5z297MXiJ0Hhg/bua3fCnPbPm/1bYdvjyOk3ITB6hi0hRFn5wUufKIUmmjQl1Cwc5CrcyaUFDmU5WsUWCIiWXDwu1tLeO9QiJEuA3mBEHwllsYUhgHOGja+f9LFa5/IAEdrQECbfshkKBvDrhMjGX/vXIvHCoHaUPB8JYMso5vgK2LyojiFlT0Cf+GBNxit3Tf4Z+579w2bnn5BBELFG3w/WD8dGNvu8TJX/XO2dDWMrrXI5FhVuy7iNHEqe5w26dyUpFmaTsfw22KTr5BCQXFsMgLAl0Sh4OC3Linhvcs8rCvYUiDSpykIfMwig4cqDbz+yTwwQ0RUCoQoq09ZNG4WdBMRX4bpYIJSpfEmjYe0KQs3P0r56FVkSStwz+57tdHa83PB3MTaXHby2ewcOy9CRPEGe+yplrnxI3bfRceN/KUALkeQWbW04hpEij958koOXJFHxTXi8C8fBodTTcjSzpnqnjBkIXNCpIAQotjEhy7pw3uHXIz2ZFCQ3CEZQIuOpcPOhBYeOOHiDU9mgGckQlDdo3KreVVUXMPBrq3IJwc6niNRAtEWbteFhgsGU6S4XoRVlce1HFSrGc7BDCe3umd2vT3j7Xm5V997/Q3n3v9xXoH497smV83Bv/yI61/6O0bvRbCLG2HLSCSRLBV1TK0Q4oPntl5KM7etMStXsJ3GKdhnMQFLzHZ7HpvGJ0xGKATilk19+MXlLkaLGRRUbiIu0186fywQlRbecCQDHKsDtE2APBXyRPUtgJzsxbZdjk28qNVnrwpB+S0nCLauKGpO1Puq0ouV/sXjkFXdgT892jq56522P/FK05l4642Xib2mnX7OKxD/svPA2m9ke6/5bDN7VTXbeyU8rIdh90QVLsxvbiM4dElV6SS9ipgu6PspmSmn74g4xjnscxt0coTityoXhEyG5wOFJm7ZXIoEQiCEGr86Rnk/IcLQxpnQwANCIGzgmBMJhEAIzns4b+ILRu+z8n1+7wkE5YJCv9M/VSnG74XbF33ZlFLI94WSqtiOMQ00d8FqTfyOszjxv2684AcTCNV/4XRzbvweM1/+dCu/Dd2DW2Co/RI02E6kTN6IQBBp09QkxNrAMoN6hDDBJfW6Cb74GunU7bYyMUIgIpPx65v78D5lMtSWPGXCmICQQJwODHz3hEQIQSozUXcAQgm1cHoQKhZWJhBiHjTPoo0/SAUSY1kSTPFnO3HUyLAmEEn1r8Cr7UNzfs8tQXXiqoI5uewcu9A7IsS9Ow8MH7dzWx/xM9v+l923HSIimZUVT2mHpUB3YgMNOya2tzKwI/IEMiQrEMKXwR9dE1WRqtIgtjBCI+Q2/0TFs0QcbjKW+RjttVIQIokup30LD5z08YYpCzhOaQMilRSxlC4hhZDFwqo6CdV8RCmKRMhEaF7GF4SAKARlisVJcSwMKsinFE1zXxMCIM8r3hMI5SD0ad/HJPyzu345n9lTbp7ae0MHLtFZIO56fHQ6cMsPtezyHd1D2xFkR2GT585i/4nkkxqsKjJhi8kjkTTe2M1SpkQtpPTzeXFtXOSqblRpHp2fR/hoXAqR1FY6+TdFKSVCkMl435CH0R4bReF2KsjiwmcgDEOcCYhUenjDQRs4uQiEmUhbycsQi6WuR4JN16edZnIhhKDwnVx8oVjxcAIRJBrEZPScms92s/Gx0wmVosh5EPtKg6fRPPOdd6G5+6ft2p7lVu5IWla0o0B8+a6HR/8lP1j+rGOXkRvYDj8YjaK06qaVBGqegPiStqUtZr5amZwifKRhNEcihqybEhagiedHXlOfLw7LsWlSCBEAPQ381paVeNcKFxf0ZVAUJoOPX6laiCAIMRva2HOyjjcesICZ+UggyFyonEZHU6fGrDRbcgNRYs/iGCQwooZSTlmWEFAinRg/323O0uV6BDOmMWkEW05EaBwDvIeA1iN/GJw4eIlhHOqF17YjrKNAfOnzD43/o9n3lruadhldy7fAMKNWPYJiKwOokzJ1OlY7GSdzWCBGBKsUolBCgT5T59UUXwl8jCwsy80FQpgaBqkcRknYBELU8fvjRbxz0MfaHuscAkFJ0QBnQhsPnnJw/ZE8cKK5FJ0XyQ4WO+GzSGOKC6/YDnOlP4o36scL5aBONJSEUyhLlel0vyo7q5ApLXC1JMypHg0wC5cimK1DvxmcOHhpxnpkBdojmB0F4kNfeHLbR43+m2FmyjBsyR1kf6WlFOESm+WmQEFeIl6v7YamBaItbHYWKM7jzQMWLqMUf7ymBjzDEDmFsM3fDuT76nOy4fQjJ1Bj6TTHtg+sygW4ZFU/Luw1UMrayMTemr6BhhQ3hBMaqMw7+NrJBs4sNOHBRkj/qa8LSaafJa8gGq+6Cw0puZCy3+k6Rx0fdy2WgEVafOIqSvB1c6B7GzE8MM9CCWuig06U6wCOwD/7wC/Z7u43+rX73qhFMNsEgryL6UK99FDNKN9hD9yMTNc26tqThFYmqTEfUIOQBSYJt1GHMgAtP7LFxQIwDPz2euAyycnUt8k5EAoiJy8VEJWysg8TNyWVjQRi0ABW54HVVoiC4IaRYLWZOFr0kPpZGaj6Jh4MgEZLlthS0JONi2/h4JaR5/hiIddqeNX3Wwbw5Bzwx+QQUryDCDD1v1JIlOrSc+lSQqIjB3N7BSJ5xCUqaJzYd6PRnHi73fjKz77zcsrUxD9tAnH3zgPDE3Zu6181c2Uns+zNCDEepXsVxsmLpxWudHSP5LLG4zaBJhExDygV8Z7VdfzSWDe2ZKJay0BqHC2YQFrDFAtnyDoLUfPKkoGhjAvon5uItFVpbAYhLOoeZ1Fekz499w/lM4hLOMrEBREieXKhbPKGyCrI5mWxUyXGE0ZIL+8nGm+EGGK8YRgnM+umjamzDbxqKgs80wC8TOTJiKwqi9no0VBxUtUmid0N1yBxjCrsoQGZDgJnDkZz4iP+sdt/+x1b9pxTIL684+HRb2Xz5b/ycmXkV2yHaY4KEpUQCG6vtDB1ml+u1FyhG60oZR9JYksF3DTi4L9t7MIrZJosjQpw03w+6pDGzWmOlro7RFfoLBBJoulKkqyoAfcROlEXdQb1ubIAyrLya1Me9eCch1cesoCnalIgJF1LjJJjpAo8cT6W2OCztGteCKxQrYhnBZSXqe/5I/fE7eP53ASPS7TNyf++6/HRv7Z6yoeNQhlG13YAo0k90hiUnkyKv8wnVf7O78dzI0gsdgOrgVvIZFBFhQEQhNKPihCT/CSXSFtN7VK+JO82s2x03mEfWGsDa8hSkfcYNyVTIrgUAiZUaRoWqj6w2wQWPMCTFIrGk8BYeX3lxYrPpTPBa2mXECQ6OiMlq2YCR+aBv3iC0uzVKCJKP7SQ4lra3QsA0gxoJ3ROC/FHcZ7JK/wzX32r0Zy4FM04x9EmEH+y45Hx/50pvOWU3VeG3bMFwsJrM87JYhvqpmyliwFFbuGLi11lFVO+is3dAV4Z1hAGIVpiXUJYEvKDtiQYJwxGmw55csKUQNDZ/CDARV0Wrlq3DFv6bQzl87CtyJwYEbYzLhENuO6HOHq2hrtmFvHUmQaaIZkuE4ERwDCj65I3on5UVjyQJiH624AvJYHuh6YykL2sctJtXTQNzLgh9rRKgENqkI3C98K9ZW53vA5yPRh1iHMrummPY0VsoUgg4E1vqlV2/ZzlTFyZ8SdeL3McbQLx8194bNs/ZFbfjIC8C6p3oL4NiSsn1SOW1k45fHWsBGl1xbg9oDRHiRbDGjRyoeP28byaIr9Mp2t5QMHF727qxi8MhlhTMFhgqk2q48DUg6c8XEe5jAqVLmajqVCBqQ4xDBGQ4rdNkpHQaM3eCztC36GmIZS6lscnkn+xVrXnedQ86FHOeBoZVIpfSSCsafjVXa+2WxPvb1UmbtQFgryLxwr10r1OWP5SbuXNQHabcAkVYsn15OyZBRPkaBWS6Cihv88WnH6lxJPecjiWQZ7L0A0HZws6c2DXFNlOqpjy8JtbB/FuilT22ecMTIlIJQWmjjfxxkkjKpARgSkLMGVNVSpfEiyTSRjNRdq4uWRLHkZ+sNgFrpFJrnQ6WnPblaqcfGuhWkyRAa7AW9x3DWoTv9w885V33hR5G7Go7iTvAt1bP+5ny8gORN5F3PImbc+ALikptk5dQUmuWrNE4YzsPy20RGqGijAJSJVD5Bqn5yp46px/T12PziMrpm65hLKdHtb12KKmMp6AOJ0dLR4JxOnQwu6TdbxlMi8rpjIR81e2IV5TzaQmEFVDH3GMjA8owTFkHMWUuRJdAHTBawNsHVF1AdTnVRBRB2jNwWxO/JZ76vY/e8fFwtuI5+Mu8i6sfPlTRncZXUPbEZqjS7u1UyDu3NAhZ0FfTAZd6qYSsCbZMJE9UdjCBVGHdf2mUwSSC4SoqWzilktL+MVlPtZRcou4QHxahihSIE4FkUC89ZCsmKISOiUQitfQCXiBeTwvujVmCBbnQqjsn7iCvF/lB8VbHbX2iwnTqTdjY5qgE8xEjkctDQVUqMajtufXnBO3X1bITYzWuufiUX/irodH/xqD5QNEJg17O2BH3oXQHC47GjS3aW6niZDn0AtLOATGmhFGTcXJ7qv8hiRkieRaIpcg0UTYajlmwfekYBFn6W3iQ1tX4l3DLi7syXTkEFHsguraM3jwWAvXPRYARxcBYqs2mQx5j/FUSGFKWDAmsPGUSLciR1yEaiukN6G+Gi8kE+54TuT8xfOdFOAlWUlTFE1ZhBASarpkBCavaJ366s9mmhOvhL83HuqdOx4Z/0im8JYjwrvo3oLQkrmLNGFQl+fSEONnUpUTN8kHxrbLxUxYkS+KNWeXnmXBZUxH59gsdchlSPkQO/mLDn7/ojxuGAqwoWCiENcd6OgTha5nYeHhUyFeO20AJ6RgqURUJ4uZNh5eGSViQyHg0IBUDIEEWA9CqZQR52MddsQJQUwTEN2UsPukOXUpPxNMX1I/sesdOW/i5aY5EU/1rZ/bt+223MjNQK4MIyO9i06zzwaWQAhdslNgXE1YomBEDkNVSgVNoNfA9flZrM7ZsA0P5EIaoSVuOzB8GbmMjhNuoIhkqr8jKTCljfbCQCzwui4TV6wZwKZeYDDXHdW5RGfQJCLKdlJz6uNzTfzT8UUcn3Ok20nBwWiiTZHUCOHJAIQdUPST3EwVuooim4p8u4aJsy7wD24xeqJLk4SeEEeZUl4aoAJ+amideAofepqJ5oRDrQezcb4xDffUrvdm/Yk3+nMTBnkXpwr10qO1ZvmPu0duRpjfBjvXrjJtEpjAR/l9jhL6fgLtxmIopF9kJIlsJ82l6QLriviD9cDanqhqzVJdd6Si0tnEcrC/Ve2roGx0ebnOgoqEwDKKsuWAtVaAYmguVcLFlV8cDamyxMSiC9xPG7jcyGLEIi4DT2KrqNquIbscxEl11WZC7jF2DeD4AnD7GWD2sAPMBUDWXgqhxrPOuFZC+/kcqt+1Dr1tgUKOEkrIWEWWjwqcE/vekwsm3h6c/YpBuYtpD1u/6VvlL3ePvBm+OZ7M958jMJJg+2lowiaY27420iM6uEbCQKtnOsBYHx4fa2K0L5dIuOuA1AZQTDQ5iLLpk5gQCm2m4ztlzdPOnfaeHZ9BL3xRokGXzAhQeHg2xJ9VDHz9iAfMukDOBiiHw8sEEkGMpJDGIpkIDurmQv7dkd8xRILhoHl2bpvlT7zPPX278bW7Hh59wkX5m2Gu/M/FtdsRkHfByI5y6XRykxqU4anJZ2PTWHk5qTClEcXqNIALS3jkgirW9heR8VWbZy2dHNcqLoWeO6XLo4nU09Ey2aRC4wFFLZdEJJnKpntbqhGNPotMQs4MYYqooh4C56YoegbYfz61gN+b9nHgpA00bCCXkQLBC2BSQtNtQsJNiib6bYlHLWioFFLQlhBozuEyOBPvbZ66zfjyXY+P7oJV/pifKaN72XZ4lMyS5fXKAsS1itwepemfrocKYJWU6w1CNVSh+VMmY6SIX1snLIeoW7AJjuXX1d5sVcGottwqkyH+JqCREK5yI1nZdUDoMTkMktORHIrKOPk5xYVontR+HN2ZEI4LfQ5gmQ1clQcGKUQRRtwm+RMdTec7YVjYOd3EzdM54FQDaIZLJiOViym469A7oi1/kWLpU4sH1BglkfVpC8fCxO+4Z28zvvT5R8a/5Vlv+ViSCf0AACAASURBVDiRye4VWxDSQ8yUJunFrNoCprlJPJTZ5mGwgg0+doU+wtjTH02RfXptYR4jWQsZ06fupmIrHRWo+HI/hQ1LkEVfpuctyktQ4FOSB1s8d4vO5omFygZESun4SCLE9ymdLTXbkm6sT8cbBixKd4vNwC2YQimiyC0V7ARhgLwNjPZ24/qVGazpKyCDTgJhiJzHTGjjC0/M47dp4888RWcJHYhU8kCG7lIpZdLNAOcZHUyGYjxxgkt5LupVrrPXoCc2TPxG88xtxuc+971td9kDN9/tZcvIlUowTJm7kCKbZqvUAnYK3erwxgUjcWwHt0jE/umLouZ9KXx2PsKQpiDnUpr4QShxCdSzOwPJdZNqNFu4ZjjER9e1sHlFH/IxQnBFssT+sLoX4HuOiTunAvzDEZLSVsSWFfvlMB/PF92wZMfxmnfy/BQaa8ReKCirCBOHa0STWmH6zsQ73BO3GX//9w+W/84o3fqNIFtGfkh2Skl7aJl+Qf635nFw2IyvneY26SZFfcePHrFIk64CU20BLXmsOCSlnaDSjgRksskSv6paRS4QbJzxNfkky2euuSHV5OH6sS786WoHF5byyMfeirrpKGhBArHgtPDtsyE+MeXgnuNd0f2JGj7mDokx82txzpOiDVwhU39X8i3XU2Q55XwpDigoUxNwaxM/2zx9m/H3Ox4ufzQ3eOvDyJeR7ZYuoJRcPrgEUmiQI26ZJoEndTQ+EZNSHXnY9xIURQapVLyC34Dq3pboDKtJPQ92tbliDDrb8iJqPHQ+JuhiHPRsT9lFLwhw5ao6PrSxH68eDDCYzwnjoos4/U0FNkfrwF+dBj46HQJHxXM+o44aNDZxmCw9FGPVTWvK3k5uSfRcTmTX5DnZ74nRqWuSQNBD5FoT15DJIIH4y0zp1r2Uw8gV5KIqyU1juxxVlWapIlcpEG1mhkl+qmDIadRNEA+bq5C0EFKpRTp/47ESFflLQC0TvgTipAiT+CpfGJnFpKwprWA2jzdvCPGrF1l4ZT7q4m5KhIgYwVKdBgnEk3Mu/vhIC5875gLzXRGKC0rCSuDiiGZ7wU4MHlxBYm6gj58ro07+OdLICRTbIBoT17inbzM+tePh8vsLK2+Fny9HxbQMUuILqplPIz9pBTFpA0wZSELredqY0rNy13Ys7fSLEjiFSHLjDJFMMVb5uSpdUuYknh85LoUwklwmn83JEUIuWIzcRhRypsBZr42/uKCBGzcOYRW5QCk/US2nhbMG8MCsj+toB9hME2hSxRKVhHFTpZsEJvSxEHBk41ikX5xB7Tl5lxQWUXwbTmxtHmcCQTWUGTIZnHQoeOd7NNM6mnCJ5BOasAFJ+8hhXNRCyAUVN09PmGEhyFjzuUDQdaTjqeoTKH1M5yF/Uaw94wMKWMR12SyJX+k9NQZ+nDyeSv2EgEqBoA1s67qxY02I1y83sCKxRzR5/0Fg4rjr4juVKn7+YD5qTUSKR3GL2MLqUMdMVTxUrumasLSRe62vRIzKncLh5PH4E5ubFSkQPStvhUsIIZvscBaa0DIdfqRpUWQqBpK0rnKaTRMmWvZtkNXLQhAED5OJHxHjp/fUDcpAgTAH8nnePgkBCSy90udyY634W+2UkuMRi0DHMi9AjZlPmvKSFEKKHIsMnNH2gcIsfmPzMN67rIkLegvIUwAjDSFgou4G+GbTxOeOAl96nJqX1YCc3KElq7OlFmq1k2y+YjBIS3Tpa8KUs5OcSeBbokiiYGZitPrMbcYnPvVg+YOlNbfCJYQggVD7KuRRKjcf37C6SgSHEaLIln8qLSw8A21/hsjsMY1V1aeipl5m+zI2kDWihy3RnhuysXK9o8QF43li659i/OxVbQls0U3Kf1FKjD3zU/WdZN5JvD1PaZFW5BA9RDYSuMEG/vwVPXhnH7DMpKEqgUhqMVVbVJs+vnSigTuf9PDd433E5oG8LMUTLqeW0OJkOJpcTdTYHIpPGJrEf+vn1eyGHh+i9fMxMVqbkQLRf8GtoDgEJbUSKK+z2w6mQYesxPXZDaheTxSKpgom2j2T84C8i9f0mLg4b2B9zsOybhuDNtCdNZE3MzApNCz2ZkQkl9bGD3z4FGTyTNSCFuZbBhZaAWZ9A2eawDMt4IkG8CBFA8lFdO0oO+URgkiEMWVgKKMmVe2tlDmI+L4yQIvC5x6QC3HDqjpu2TSAK7qjkDWPTlIdhQKWBmxUnBA3HTewa9KPklkkxRnlxT0bTtCBBHCTK+RCNymauYxNKIcHtthhMFGqkkDseLD8wb4Nt6JplZFRO7R0TEmTVCadaTZMD04JbSWYF/XvQD4TUfNeYHsv8Mo+YLwbuCAP9NuAeiqbrMxPOLUKKFTqyCE/n4I/IXCackZN4GgLmK4Dkw4wQWl/+hJtgKjLV6qgEpaDkEN1uZdpSxGplEhB5onq+sm7oI62K7rwoXXAu9cAW+jvNg2mc/oIEKIWWHj6bANvnDbw5CF6fmsxMn9cIGL976Dpic8ZWiRMHFdUfh6NS8TLqpmZSLgmSouxybjgVrhWGVlpMtKgiENbAnLkABLBKO2ChAaCJDYBq4GRko+3LbPxk/0WNvbk0FfIomgYyGRN5GBF2xFME5ZYMWWaOFQu/R5ppClCw2Sp6NWjcLZvohl4IMvRCnzUmj5OOT6mqgEO1jzsW/Dxn3UPrYZCD7JRJBD5aMHIZlHBhKqWpkCSWcVbL+zGb65x8bJSAaVsWtxlaWyPtkz802ngw4eod/0ikMlG51QcLbGRgyudfq/6gncgB3qsSK1jG5rohIeuZ06UFp9SHIJMRkYiBPtyLIU6bHEewX6Px8n6QxIHEeYhF6l+P/ALK4Cr+4DLu4A1dtQvUnaeUH6DtIwtseUuPeClxhlNnnDxZJZWUUZOMwgkCLGfbgFHmtG/Z1zg+w3g8ZrsWEuvhCS+BzjStRVch7Q6C3R7+MAlXfj1EWDEgqjJbN/zurTZ9/5ZF5894uAzR7PAQgvId0ela8orUftB4mLbFD7BeUHid51M8jXRzUKaAGjfN82J0rwSiL6RW+HlI7eTC31cBS2zlKpGUZGdBL9hJFN5HZRFovBzZgHoauJ3V+dw3YoMRgfzWJHNRYpIiyk1pWmYaNAjbgOgRp6e5EaK3pHQ5AMgbwJdtMdHVk4t3a5MZ6dkHUlgiOQFhB6hAc+PYL3peDhcc3FkoYXDVQdHFoGHai3sr9MWsizg5QCiDz09uGzwNH79klV413K5STgiNW1moxWawnzdfgr48OMucMYCWk4Uqo4Th2mwrcywunHFNbjgKcLOg4a6MOhcT/NO2sycSDFPjJPJ+NSOB8vvL4zcCr8rcjsF41c3qm2Tj+9B/qLMlXLv4sYIxFplf0hqMLLawo3rgTcMANuywIAF9LFwR0j0MAxR80PMNnwsOB7mvABN0lT5Y1kmeiwDfRkbPVkDvZaNXNaINu/qcQpWKxntzIomLJAbfGl61RQTQT0eAqfo1QdO+sChJvDIPPDvVMBAj00944lA1M+tAf7LOuDN3eluZjTUEHU3xKlqC392LIOPT54Bmn0RbxKcVeWJzhXQkwusut7HO8s0fqAUM9XT0FEhRUjit8jWYmKMSKUQiNzQrQj7y8hS6FptLFEMOMV2Kzcz7g2VcE0iYSBmb9QxMuThQyMerls9iAsKNjJ2RmQ+CMKPEYS7wFP0zweeqkdQfh99SP/o0kJ4VYcV4GU2MJgHLsgCy3LAahMYNIGVNjBkA8tpu6gJUIiNRhWFqs69gMltVsBiM8SxegvHqi08PV/Hk2drKBQKuGJ5HhcPFLCsW/WhWNLoJcEz8bgPfOUE8HtPEXdoRIU/ooCTYipaIk3YdxWzluOMuQCD/jbepvGIGCTS0CDWXCkC2uYd0XLIn9hKbqcIXef6bwVIILoj310VfcZCpkOjypqpwJSKEchyO9EMxAL6s/gvFwPvXAG83Kaaxqi8veEFWGg08cSCiwNnm9h/tol91RD3k932bJlAorpOWYkSAxaFqGmbPNmLEON5A5syNlblLWwoZrC2J4O13SYG6F8uC8s2kDWspSxz/FRAPpl8sqL3ndDEggHMA5gPgFmaEhMYNYCVUNyhXQNFbYZv4nvVJj792Dw+fSIPkOmxZAV5HNHUF403H9Nd0SRXai+cSifbyfiGTkLZNeijQPSNmNhaq9xm7Nhxf/mj9tCtjxrFMugRrNTaJ45UdgpwyBWKpVZj424V6MvhN1bP412j/biojzbWmsIUn/SAuxeAfz4L/PsZ6S82vAgRVCYxTkuz9LBKRongl+oVSa8UzAqAnISFbmC0APxMEXhZHnhZV+TKEnKQiEXxy0Cgd7uPoCZXGZSoAIeE2DSj35MVUTw9TU9SMPFYDfjCSeBjj9aBOdp7QRzKlns5dDdVIZdcMDWfcQsh7mbqm3a0aLDyhhKBJC5cWpxCnFoKJu3P8OoTZcp27tjxYPljdv+t+0wSCLLsyi3StUhHCS6ZqmqaIkZUW+YCFxRx54XAz/REsO4HIeaaLRw5XcPnZ1z89UnK+lE5MiWo6JhuwFK9pFX+niWrhEDI8mWV+VTuLAUXKPxNIWQiqPkQm4sWtvUYuKw3g/E+CyNdJvryGXTZJuwcNT63JaPQ/f+IbUQ/cfqxQ7xBCUSEmGcJHU4t4rNPW7jrcB1wCpGgqk4wMpq+BLwqoiJ5UOy6s+uL4Slex4ikHm3kKfD4AmkEVFkNtr4+CURjokz1ECQQXzAKt96DrjIKy6NJ4GFcjoyxG6psm/pQbkUT4eIM0HcGv33hMnxoxEWh2C2owMkA+D+nVNscwmWCBDnnSiMEIKjNvUtuZMTV+MLJhVKCET/BR+1hlHkS6upGzJ4emNIP/GI/8KpeYKwIrDcBYkz0z5KIkTQCulvH7HkbS48QZT6wsa9Sx78cb+CzJxzMzlElUrd8pBOFq8l0ZKOwfNz7gTSdlcXTucUThgSWS3rDxqKQUsyTMt0yfxMjjLwTniqILQ97oKzS8cABmgsT73BO32Z8bgeV0BVvvjvMl9G1LAoQqqrrc7JX5upQrIH+tUIgXwTWhfjLUQPv7w+RtQzMNj1MnTqLv3oK+OIzHtAoRoybfHvaK0kTIti3WnSVrVQaymMdioTRGrAcAk2GQnryTlwaE2XxQiDnAnkPW4oBfrIbGO/NYHOPjWW9WazIWSgULOREFzypjeI1zaVsdzGjY6IFrcLGVA34dg24rwHcWwNOUWxDRUdJByhvRwG6WIhlW0IVBBOCoJBHcjKFEmJYrHuKIIPSc4nHzJ0BprhCvhhiKOWm81GRrVebeBeZjM9/fvf4fc3sW/4mKJS93lVbENIDUWQXND7hSzjHpFZdUDxgFHDrQF8P/p8N83jjcA829mYF2HyxBnxmGriXWPfZxaX9kXFDDA7N+mIwZFBr1Ob7KzeYE0T5nui3oDYRE4nIRT5vL3D9AHBVP3BxP3CBFW3kITChZFUGhix40dEwVjXNjAjnGYFnoun7cGFg3nFRqfp4qtrEoZqLyUUX31708EydBDYfoWnYFc0HuefEj1SCUOiAdLtlD6ukN8SURAkKl2dhQljZQloUUykgPYeseXriN5pnbzPuuuv+0QeCQvnvvHx5tjC8HYE9Ckr6iMlXZfNpdlZdXZI8EXdwgOE+3HkJ8NoSsMoEXNfHZ08a+M39FbTOUHdDShDRzZNtZdwjoZVq0tOakLAFSXPP9POI/R7EPchjkXUWmVa0Gcj28YZigC0DBi7tM3FhqYRhaiSSs9BtZ2LTv2RK0hFCkU0KdZmw4yQsBT3PUmzDA2aki33QAR6uA9+lZ3lRnINcGUqpUxO2uOaRQEI9DZDmQCXdWHKTTVFbYisGN/3hbnzNmDxT1bV/duJ3mnO3GdRk7EjglO8L8uUv92+QAiGTPAmJY38o8iMmWxWykHvlACNZseNqxbI+Ea07WQX+8Chw92NEsmiDBesVzW2xJvDR/aogTnqIeCn1K2eHawGHRIF0tJhWNOkUF6DkFv2Q2SoS7wFePwi8bgi4hngGPauNu4mpxbraBC9JjvZbJEgUGW01A5xotvBU1cNTZx1MVps4sNDEt2oU/6CMLD2UhuaIgoQq9y9RSikQFQRRQK6NI3ASyRcvzSFgyOecxuVebeJ97pnbjLt3fHd4CtbW7yBX3tkz+mYEtJVPRf/4faX4siJOoOpNDKDHwOi6DL65po6hvm7MucChmdP4neNFfP9p2gyST25uFafXtY4RKL2bvoLGRAFpGoTr52S1kaoOQ+TN5Pd8qjquAvkmfm19L960Dnj5UEmkXpLowCYxHnv0XrtL2i4dQiwDUzhXZ8IoxjEDgGjVY1Xg0bPAdwgxqL0o/WvQuGh+pfAK0yJ3jyuljJ8QeK7Yii4oDB3C0IEzO3eN0Zz4r/UTtxv37rg3P41C6QnHLf9J77qbYfXIzb6xwZYZ3pTmFYLIhZROjCqWBpt435oM/miti4GeIp5oAf/wVID/OWMCFdk8nDa3qvSyzpLjBVcaL02Veq4n45xLrhjjDdyEtLllaZFXsmnUh0J2Je2dxe+ND+LnVwXY0FtEIQ5IdjAVapjx2ifjEnzXqJJDXnwr1IG63Xkeag5wst7E8YaHU46DQ9UQU9UQ32+Y2C8it1aEHsSHhDBQ8xIZ36AKMxF0S4trcG9JxTuU6RGtnCqoVfbdmHEn3u6e/UosVrd+as+223rW3QxqR2hlSzDkw9XofIrzcSqh3B8hEPSBDawI8Mtrs/iDlTWUigU80QR2TFbxsWNdwBz1RqSCFNWSh21A4ZXUMWKwnIr+aKbEg187uYcSfbibxsFEoYPwRkKguwvjq4Ff3whc1wssE4EszR3UEC3JrKRJEt9RC8NMlQiHRWiiB8R8GPBCQyT0qK6D/hFqPNkADlSB78wBjxJy0AfktVAgSaV06ZSiLoMlh9R8itXVkD02zTRekXOaRv3krl/INyfe5swt9Yf4qzvvHf/L7PBbpjOlMvJ9UcMQpbFxWZe8cFy1LKujBfySQPj49TV5/NbKBkrFbhxoAHceXMSnKl1R1QrZcNq6JtxMObJY+RiJ0PdbiJlndpCbRO6SyQlPwLxyR9vQntxkQgfKvZ/B29cW8KvrDLysv4DBLitqNMcyk5H8ULbURFNTNFXpl/5AmaTRoa2E6T106QKRiBE5DXyqx/RQc31Umy2cblqYXmji0UUPjyyG+DciaBQWpzaGoi8G1VrQBlPatCrJuOAcUjFixGRiTJVrbmP6ssbxXW+1mxOXwF8SiE/suH/0o9Zg+Qm7VEauJ+oxFR/LInJqgoVzIQWCElkkEMMB/sfaPH5jZQN9hS487gCfOlTHJ4/ngNP1yLVSCMGJaSxwbPA8BsKsQlq6uXP3FCZ0dD4laLS6wuOgkHcRGAF+fz3w7mXiV3SJRHkaBwjFQp1ueqhWF0UYu7u7G6WciS6b7IteWynvR54tiSj8/AlbKEtHl7wVsfMLwHEvqgB7vArsqQHfIp5BH9AruTRUhCRiL/QUYkJitVWQX4tRAYrXeA3RUuitdmPiUo+1FLprx/2ju9xs+ZN2Txm9q7cjNGTTMR495FkyKdHE4F3ypy1gpYdfWZPF7w/X0V/sFpnLLx+exx9QgQiVnxOppOihegY2z5kkIpEc23X3g//Nci2JGIUcc2Skl8rhRDEmVUhRTSdVxy7iJ4d83HJRHtuGerAsZ4l0etQhpp03UK7isAt8sQ48MhU5A2tXAT/dD2zMyoBoyJNf+jnSuUiK7Mm3lhSRbsMNojiHEzRRDYFFx8XR+RD7F5vYP+9hT9XHAUINqgIj80xxDkpYisouNm9KGV3agb6459eaT99+WSYzMYplS03Hdu747vB9yG39fymnUVj5ZsBYakuY5nKJ88uq5hZ1U6N0ZgPvH8njtrVNIRBPe8A9xwP82jMm8Aw17bKArGzsHds83odSc48SpiGFHHWqOOaxf9ERVs4vnaJJBIwIQkHAAVU/3bgcGLfpwb/J/hD6Qp1uBbh/Zh6fqNi4/+kTmIeJTf0DeFu/gy19JtaUujBazGKwiwqD08rrdFRI8wz4e0mSSsaGboEsNAEC/TvjAlNuVDZwsAl8twY8pJCDYh2O7Cmu1lC549QoxG/M0SbfDzWnb//oTduSbQnJ23gMhdK9rlH+Us/am2EWtoFgsC2LxhFDKpKAKlP48uvXZfEfq+sYLHWj4QOnTp3BbZUCvvhEDWhRfyAKuKi8f8IWsP5+56pV7LQJJUWYFELQ2AgZyH0jr6JrHtf1B3jf+gKuXFXEYA7IGYQMyXNEgSbK0po4QzUOTeAPDwKz9BiDRWpkSoH+HNAdVQVvHQJetwzY2gusJ8A0gSH6SupGnqQ5iURFMUU9s5qONJSFJc33qYdWYKEJX6DGzHwdexcs3LNo4B+p4qdJD1HkCiVSDRU05/dd6S5MvLdV+coHPnBtsnGpkt137Hi4/AWqoKJkl9oJFQt2GuTRRNO9GEC2AazO4dBYC8uGesXbzVaI/1kx8PFH6sCCtLP0QpOvqrNi4yoZv4o/6ORxSbqT+xnSCJNyQYW/J6OBdB+FbmAd8EfrgOt6gA0ZSnD58aNXOLRG3SaiXlOTx+Zw59EW7qTHFyz2R2n3uNiF2h7XAKuJK4oBruq3cPlQHpcsy2N9T0HEvaJTpSECf597JWkCk/5eNH2mCFkQalDricMhMFEDbjsQACdk8jHWPxE8mkZrcddPhIsT722dmbjppleJxze2cacPf+L+8mdyfbc+k+mhRyslbTCv/ed+vji/rKpe3Yu/3QT8VD8wLGsl//VEHZ88NI+vnywADQOi3F90EmOFNYnKD327oJ6wUVWWWnpaeS+CNFKhDeVYaDJod3MDKNbwqyvyeNNKC1uHBzCYU3sqeEWVEvpogs8EwD0OcNdTwD00ZQvNqAiXIpyqyFYohNxVRrmSQlQjODoIvHkI2NIFbMwBL6OdB/LWokquTmX87awiGfjqTE+VyHi+gQccA7/36CK+XaFm6uppSHK3d+BOj9ef2fVzXn3i4gwmbuwkEB/51Pe27cj33DxpFcvI9C/FI8QYecEMG7QybJTcGujBX6xfxPUr8xjriWzpbtfAP54APnZEPjGGIFzVGAr1ZQurex/iMhoyiRI+NR6e/ZPNhYhdu2QmpOtIu8H6LbxhFfC2NcA1XbLyiR5yotLM2nXC0ETNCzA9u4i/m3Hw58c84CxlaakgR/YAj5uaU65EZhOVYoRVwHaxqSfEawbzuHwwiysHshjqzogMeN6iSi7VtrBdANLfWZoH9VCY9hZGsvLQN7DHMfD7+6v4z0om2qQUIayDwJuDEUxurR/d9fZgcc9FwN4bbrqikooQn7/z3vH/ROEtnzR7yuhbuQXIDS+16VdunFawIUg5BahoF1YRGPXxsQssvL83Khai4tmTswv42+M+/mS6AczRDh2ZeRSPTtYiX21kkqV9VaBJzZiy+zQGtTOMAjfEFbItoNvFOyhHsczC5cu6sK6viKJcz07LQNv3zwL4l3ngi8eAe56hRBSlrGWOh7rGcW6lTJkwTzK3I/ZxkFmxIZ7nRIUXg8AvlICre4DLaJqIyC71yRHZjvReNp28E16gswT2lDMJfAt7HEQCcZw8Dkneqel5QA92PbvnjxeOTYwUc5OjqM1de9O1ZG3aTcaXyf20CuVPevmy07siel5n/GjjNJePkRXRnTULlE7hDzYO4ZaVAQoFStIAVQ/4Sh34p2PA1ygNTvsUiIySqotZUCaEkx9tiGoRFIooL0MJJAXMRFtDSlrRPhBgYDnwP5YB5YHIk4ieUZ0SL6BSORlKrno+pmcdfP6oj488UwUWqGQ3G7lwNBfWUjV4e90EnYWSaGK/YVSXQQUooQNkQ6zu8/H2wQyuHrKxsc8Wrm533kaXlRH5qkxq05VOosu9EIYeIv1hYE9TmgwSCMr4RrmnafjNXcD8xJ/OHZ343Q9G3CHWL/1SX9vx8OgzgVPe13LLd+RWbUd+cFTYfFU+LhCc2zAmvQTRNNe2C2wo4P+OAT9BD8wRibkACy0fRxcb+OaJFr5xrIF/m/WBRle0iLTRmMwIaZTI5LHeUnEvKFYRJR5nKIMxNOGUwhXFty30lmy8Z9DGa1YAG3vyGCjmUcpYyCy1rk3cdiQIUWEt4eY35oE7ngIOUuZpoRGV5VEOQRTzyOppxXkEl+JV0yzwo3I9dDUKgoltjNml+s8S8PahSFhf0RvVfZJXQhw06tThp0Q10xQmiSCEEFTsSxxCIMQxQgiFXsE0Fqd3vSeYn9jumxM3nU8gqJHpyaaz9dGWt+2j2RXbkRkYh02PeNYfpCLnVIGGuKAonoy6rPfb+KPVDbx9XR8u6suIzboUvD4bAt9rAg/MATtmgaOEzRRtE5E22v8okzQiiaPWjaqCZVmc8E4YfxBFtogqaKm6pQi8owRc2wds6wHWmtFHNMFL5fjtE0hp6eMNB4+d8fHVky18qtIAqv3RAKjdHC2TUEieg1GmTAmBJLuJWJpUnngbGX1H+gM5YKDo4W29Aa4oGbig2I3RnixKxQz6MlZUEpEaM+1EKqP7EnoiSWUkEEQqAwdhMAeEk6gf3fWBoLrnp+DH3KEjQlCr4zpOlWad2vjd2VL5C0F+G0orZSWVehKtgiqVk+d9h0O5dS8DDNi4+VID7xoENsrSRtp72RKl+LSZpYnJsy3sW3DwyLyLvVUPUw3ZIzOgmyAhYD0ayU0VQTcKLkWbZsfzJrZ2A5t7cxgv5XFRbx7LCkCvRYgQPYUvsjBanEJu3KFE7dMAHnSAb5wF/pb+OEGp54ZsXqnaBtI5lKmR7pH+CGsVeeV5GoGoarqliRR9MaSHQb930b7AyDN523LgqgHgZeQdUyzDALqNADmBFdH1CQHop9NzBQXH903sbgF/tHcR/yG8jKACzxEPhf/v9ZmJVwS5O4aeKgAAFJ9JREFUydHuJe7QUSDUBzvvvG/tv+RL1/x92HUVepZdCd9eD8sqIlTPGZIMXgilzGkI+JSd44gfWFVctSLEzSMBymuWYbnQtKgwjHSESg1nXOCwD0x7wFMUp28AFdq404oefCa+KAijXFM6RQ64Igv054HxHLAuA6zPAqusaM9lj6zIN+NAT5t3LU5KT/Waq3v4/ul5/Hulhb8/5eH4IjFy2qVNj3OUVV200GQmuVchFjolHCzukMM6i8TGgkLusDyneEYn3STdrCNqP9/a4+PykonN/d3Y1J/HUNFCMZuTFVxR1jSZRk8inkKI3S0DfywEgtLmfsQdvNrELa1nJj4m3UydMqTNlPjOVz/z/VVH/frlB1vGpbf3DF0Ee/lGZLvHgHBgaasf0zwhDHQ6+keZRLpJYkl5YA3w4Y3A9RQIor4PshfIkh8ePQyt7vqotXws+sQ5DTgtFw0/hENmiI4xDeRsA4WsiVI2g14zRCFro0t0ZFlSRRVQisAzyb6Jm1BEt2IAk03g/gXgI8QVCBXOUpW0J5uUqORQ2tOM1X2zyyZmUkKCitXEATVlUlQWUo6ZOBQl20TvNmqVQMVGESm+dgVQ7gMupliGHfExMn9qZ1o0Ah5Hid5phh52O1n86d55fOM4pRYwjTniDosTV/rOxAc/eG2CTJ4XIb6+88DATKM+dsTPbPyTrtJFQP+lsPOXIwxWLRWC6k91UUkUiRJ0o7TZN3ca160wcdOaENuGl6OUtUR7COoru+RHh+KrLizRxsGh3h6U0KFXOdkU8SbemaM5o0ctUjM4sUsu6TWkQSoJnEcdaH0LpxoNHFr0sPt4Df9acfHAPJmpYtQKQDzoVRJWQUJjvNeVSf4tlUC4w1IAeUQ1gRhLoemowEcxf3mDote3RAzxLAvaIOyg3BNg+3A3XjnQhfFSBn1dFnqtbMRxRSyjfYzN0MduJ0MC4XzjmDVHj2XEInEHZ882NPfeJOMOzxohdu48kMXCfHHRa67fHea2fcbqvQq9q6+GmVsrgjOJJl1KO2UPR0pzk6SLFJ3sG0VIMQK8ZTlwXR9wdRFYHpUyCttITYuXOq6qidPtficyxW+LTEE0wZSJIKVTZJbM0D5KGy8AX6VNvKcoJ+FF8RNaG7GdSwdNzhAV0VUhdp6Y46gh4zRKTklghWfGSxPleZlTEsmOzMiqkDuhbt6K6j4HgIuLwE+XgFcUgI1dUSyDXOmINEe8hK7kesC/eyY+/r3Tla89sbgPtrXnlurTExcG3uTF3YjjDs9aINQXKQs6AWvrF8zebbPdK7YjtMZh0sPhkY9ukkm6gi4hEMr2E8l0o1XJzgOFJj403I3XDJnYMNQlWgcR5FNyyTRtLXLYQSnjt7lJWPpdbPQPqDQNYh9pzfdxdMHFwYUWdp/28Ok5sklE7HKAlYs2zwiXl5WgJbRcnZuZRC43CdKohd3ps7hNokSERO5Flt6rxmYkzKoPAs0vuavUWJQQg3bEZVu4esDCtQM5bOm38LKijaFiFj2miVzGgClqIEy4bti6x7OqH//+qSNfn6o+gO7u3b/tzNz3kXdfTrS5409HDqGOEDWXdZSeCrzxv8kPl491DW1DrrgFhj0ct/dL9DygI6XpEGRMaoeSfCrxp93TPVF28LWDwBUlYDwvFEA2AI0bDrKk0xJ467hBRU8qBELmhlCBlH+qDjw8D3yzBjxI+0ip7J3cW+oTSVxBdLeTG4TEWqstdepJKGxTbqoA8BwL13iNQ8TaoVCERxh5FJauK6OhKupJCif0TjU9o4oocrWtqO9SCfjAAHB1CbikGxiRbrYLzH6zhqkH9jx9aGry6YP9XYVHXof6Qze+75pjz0sg1MF/uuP+0c+qiqpsYTtgjMYBmbYmGDwUzewqQbNIlZPbSO39WnhtL/Cqko9LejKi2diQZSGXyaA7YyCbMZG3acsMbbaNJlHF8Ol3348ehdQIPYEELdfHXMvDYrOJkw0Dj1dDPLwI3E3lZlUTaNF+E0pKUYGqDDSRRhGhE/ENiQDx1jp592J9ublKktW2CU6YAW7flVTp86OZmzhnxK7JQ+IezaMMzXe5eHkxwGv6bFzWC1zYl8dAQSDtsXtbhYce/f5Tjzz99MmDy7q7D726sTB1wy9eRcbyB0cIdeRdn7h39JnAKu82c+WvdK3YjlzfaNT5lnVUS2iCmkxZsSQknamZ2s9B0k7uKMX6qUVMEfipQtSArJ9cS/lYqm5KecjD6fkXVClGik7Vymc94OkmQH29Hhbl6/If/U5CKDRMPnCDzhG3O9DHrgRC9cNUcsDcSJ7l5fQi/l3dJ2+8ovGFWGA0kycur1odKwdJ2ByJEEwwBeJKpCFlyUhC1g+8Zwi4yMLTxbPOd9zJJ3a7pyt7LrCLR9DbV73hhk3qaTSpQnFek8G5xHHf23q/0bXtC4WV22F0jcPKRdnQWIP4DPHrxexqiWyKiKbMEJrkgxOEt6InpNsBtuQCDNkWBmwfGSsAPSc7I1l8y4xqGx2XuszYOBMY+L5Lbiudj4pOKfVN8XsKWshwM82daCWs4gly8vlC8scI6NOlkz9VlcX5Q1yDITVenFuLSXBh4MKlQII+F9NFx/FAmByQGq/IlagOu2QCad4oLe84Gwrh3OVWbXKN7+8anF/c09us7f2Vm64V2czz/TxrgSAucQqF0ny1Nv4dO1v+gt27zSkMb4HVPSxctcTN8WyoHIKAZhVdYpOktFcgBiWFZKCLzhc/T0IStXgvqGg3F4W51bO6BKTKiVQda6kPZpxzUDCtraxaRDFM+Vki0sggP/5uCl+Ij2darL4fH6e8DDUPXFG44Cheob0XryZbNiFAkqsRAXVRgVHfh8apPbcEJycuDDDZ3Y25m2Q28wUTCHWiv7vzvrX3ZwvXfNnovqqSH7oSRn49LJuyuOKhN1FRa0pzi4SmMJupPBUxN7SVjY6XpI5vPohzGHQNWfegAmFxQExCKHkM4vsEGVrlsUIzrs1KANT4ef4gfi9tkZRw6XrFbH/inGprohJUHpfQl4oRU1pwlebXv0bCL5QhbCEIq/BwBGg8UGid3v3fWyfv+8gHrjmnV5EGhOcTmsTnX/3MfavO+rh8xmleek+2dNG3zd6N6Bkag5UbEK4bFdvGSsg1lAlJQiv56VUTciUwUqvVLnRVQS1eVZNR7Rg1cWman1rCxhdVt+k88MbSIWkE85yCw9DnnMkqGedQPbz0DUqCX6imqlroPAxnETSn0Fg49NP1Ywd/wm89sgrWQ+/71XN7Fc9bICiCeaZRHzvhumP3ZEob7kFxHLnezbByIzAo3Ee1ZQo2laYwT4MhajIKyKA8Tq+rkHRaqVka61cxEXmRVMHT4gQJC5LiZsaIp3GBeCY7dJPTBVIXmLR5iGtJVeKrkzeTMDVU0FkFjBkEzn40q5NvDmYPb3ebUyNwz+tVPG+BoAhmL1Cszi+OnHTrm2ea3vjnu4c3TOeGxpDvGYNlDcT+PLfLiZpJOQydVLVhVZrLllZ1rWaXcYCEaVDX414DH4OGDGl8QK/5TKAEX12dfepmhSmHqlFVxcZxExFOzjlf0SYomp5ZeM4U3OrUJbXjh3/GW5i8yAr292W7Z56NV/G8BUKd4Ouf3j1wIpMZe8pxxr6YX7HhUatvHLniZsAYgWEVxSPs0ha8DVpTFismdcqOsqZmaiFSTQKHZvm7bhFi9OIL12HRdA9B3XzinMzWx0XI6ouMXGsym4hrCMuouuZwoVbH6yFyIVQthKgixAxMdz/c2uSrnFOHX+0vTG0BnjMy8Ftr08tn84bKdXihN1JxmpunYIx/PLt8A7oGxpDtHYOdGUg27WJnVVvYxZ5EtrOqzX1lE5tSz9C+u0ozTbHi6p4CP2/a3epark1X/DE3W+w8beSUo4IcFCfeivfEw+QkVSOoKsRNz6r3m1Nw61NDjZOH39xanLzEau5f9QMiw/MWCHWCnZ/ePXAGGDsMjP2NNbBhIVsaR7ZnM0KMwCSkoNCg9iPulz0xJ96fwex0m/3XVD31cx262d9p34+RWeMOegEtP20MQrpLyBaRC0QaEMXTwRGEBbRi4plyDUNsT67CD2cAdz/81uR65+Th6/3q1Ba4U794nkjk+ZRd95fO9/22zxVSNFreyBm/uflQYIz/U3Z4w8l83xi6i2MwcxGn6KQ1PAATP7qZX0bXFs2+Js7bgXOkhYITae1Onsa5EErnHc9mnBo6xDxH10/tPpQCkbcVhLPwW1NwF6dW104fvt6rTV7oV/evK/b9QJwhVVefsxSkHKCQ4hCssX+1+jc8ke0bRya3GUZ2BIYZxSnETwph4o88ir+TRvRYIihR/KJDN7frfLA6a+/E4rmLqcNDDBHJsjxdMBOxmBQiE4eopdfDuVU8ZMYnELYQhlWE4QxCfz/c6uR4a+7w6/3q1KYXABleMJMRmw5ZP+G1vJGTfnPzdGCMfyU7uGE6T5yiZwwZilOIyp0OJWZya1+qn95JZHXXUwpMYg3P5amkxRZSAlCpgTZ+H5rQtUU605it3C5Aw+PbC2IrwcdNyCA5gzM/tcmZPXytNzd5sd/cP/QCIcMLLhCxYHx698ACMHYEGLs7M7Bhn1ncCLs4DiszAssuIaSeADKYpHZIx/0TmWaL+dZtO4PceNE1IhkzfY3962Fnbsd15GrzYDSoF99Pu27KtHb0SNiGNRFskhFVpTD0GhXrtBAGVQThDILWfviNyVe15g7/hH92aiPwvDnDi2Yy2pAi9EbmHG/zcddfvzufH/4PuzSGrmVbkC0Oi04nYj61za2c5J3PdHCvQ2dCCeBgZiHVeehgNhSEtwkSj4PIu9bNQ5sXImBAJtpY49XE3k4ZVIsJLR0iHp84i+biFJoLU693Th++zG1MjuS9/UPZF4YzvOgCwZGiAYxVAmP425lM8V/Nno3ILtsOK7ceBuW8g3wU2TSykXDQLOpMXU24XsrONTGNa+iNTTTNTdjrtP2q54g+KuTidp7TojZuQx9KwVcdgtUTdOL3U0PktEllDoE/Q11e0Jw/9DZ/8fCVaEwNvgjI8KKZDI4UvQvzxaYR5Oe9hn2i7q0/Gtjlb2dLG79XGCwg1z8MOzMGy6ZCKe2H4yyrSkoEuhgMtHkaHTYlJxCigyC1BaM01y+NTyQQoZO3wfm0dk4uXCIqTc3InQoas/suacxPXePNVS4Ejgxmjf1dzzPO0D7XyXeet9t5vguoz+/Yce/wM3V763dy/WPf7iqtgN07CtMajyKb5hK3ECM617A42dKRQieEmq1P8At5Db6YCTKojlXj0QWog9DGpo5dO9XFZWOPTtUS7bL90IHpewjcI2ic3fWqoHboVe5i9UIzrBAynK/i6dmuR6fv/dAEQtRT1FGabbojZ0x//JCZH73fLg4fsileUdgCq3dYeCFENJXGi7Xn3oeuWdqidHRFdRPACCFn9Wl8QcUwEqYiYSM0u6YLmhSMNKKqTiMaoPmzCBan4FQrL2vN1650qoc2wJlYbtpHeotdnhWazg+Sm3iuAvJDE4jYlHx690AF3tjjRtfwd82u4veIW+S6tsMsjIqNviF14w9lVbesSFY1ExxaE3d6LtdSq8/Q8yttcJ/iyp4zsZXibahr6FxFF9ioK56D0J9DEMzAr07CrU9fHtZPXOHWp0abtb2/9SvPrtLpuS78jxwhOLeg/R5+o5pfCAx7NnDWPwG7/ESmNPptagRmDw4j17UFVn5YbKcTW+iYGWnzFDpxCQ75Cmn0bCfXZo4+Og9Qe1pZzCAhJKzUTbnKiSScZj7o9K4blde3Fipoze97Vas2dXFQq6x3venhrDXZY2Zmas+h0unHViD0gd9xx73DB3O5rY8bxeFviLYDPaPIEGLkor4UAjGwVLupimS4reZCkqrxeld9PopzkEC1qLHpShEyPfQudl5pFWPJPAo1iyDvgTq5AO7iNLzqrtfAObQpcKrrQrcyAvtF5wovGYTQB6K4Rc108nMObbG0RytA+aBdGL3fLgD50jByhS0w6TketLBUkcW71DFzobKo8Vvc1rOFV8EftVCKTHZMrWs0IQYl3bxoFpgeIS0yuvKx0mELaMxX0Krt2+YtVi5wm7gA3vQyOBPdZv5Ir9nlWV0/HK7wkhUIfWAf3vHd4Vnf20oc4xsmmZCuUdhd22HlZScbk2CkJDZiitJ6Vjwr1kPVFXAzoXkIqjWz8mbidUzjD8xcicGy7QQ8S8tRIDoNeQtzgiPQMR4cGN4cmgu0C3vfq4NGZYPvYG3gVUZytNfyh8sVfmwEYseOe/OFOkoKMeY9jFZNrzxlFkYfyxRxKFcYRq43qvYW1dWUSaUVIORgDxpRiycWXUVEVXt7vjlYX3A9XqA+ZxHK+HmlSoBEriEq/hUFwCQAfgV+ax9a85WNThWXeIuV1WFr73LXnc5bxtygZTv5fJ72FTvLzrHX8oXiBs/2PD90L+PZDkx978/uuHe4bmHrITM//JiVx16zdxi5bhKIUcEtxJZtIRB5GHLPadtdqepmEggVRtb4APc+1C7utvgRM08KEcR3RNVzFFkMfUds7/CoH4O7C/7i9Fanjs1BtbLSb+798x+y1/Bc5/slLxCcYzjUSso383WjWZoNzNHTdnbrMeSHj2YyeMTqGoad3wIrG8UzCC3ImtBeUrFZh/6mlSJEkQ+aVRtzEiVsMseiEm5CBuRGGFGPQBuKiBvIB8/RljrxELNWZZPf3LfWdSrDYQMrfW+6xzYnSoDow5DP5h38CLyG/98JRKcb+vAd9w7PW7mtM2ZuuJLJ4H6ra9gL7C3I5oajyBYtvmwNKPbFkCCQUNBuLk0ghFrQl2QjDyKDHEjoM9rBLs5LTxemz9kXghYKbrNyceDtG/Wdymq0sCLwKr2+t/dXXuKIoM/vSx4hOglEzDVa1BrBQcv38r5hlWqBn6/Bjp5SZNuiHaJ4FSyPXm1U7eg92gg+J571q35s2f3NQxEOSh5QpH+gv+Wr58G2vegzeGJLKu1wzZuWY2etOdiBk0ceVh7OMnTuw/BcNfeH9f3/D/Yy3DTIaZdJAAAAAElFTkSuQmCC" alt="">
            </div>
            <div>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACCCAYAAACHHWC6AAAgAElEQVR4Xuy9B3gc13ku/E7Z3lEXvS0KSZAAO1ixFCVRlCxLsmzZuq5y4iRObiLf/0/54zjRVRJf5zqO/8hOHJfYsmXJsmjZkqxeuRAp9k6QAEiURV/ULdi+M7P3OWdmgCUIEAAJynKuRw+FLbMzZ855z/vV8x0Gv6XH/v09ekRhRxJ6PYA0Dz2rh53jeb1ez4PnAZ7nIb/mwWd8xoN8mfHg5LUgvxcE+QX5I5D/yNu4/FeIxxEX5NfxOPmfADGNuCQiwAiIk99xWsQjowjseqiCvv9tO5jftgar7T22/4IT0DXqTZxTr+eg0zFOXqdr0Gh5J8MCHMeC4VlwHAOW5cBxPDgOYBiG/qODRz5QjnQ6DUmS6DvymvwjhyiKEEX5O1GUkE6LEIU0REGEJIpIxERfMi6cTcRTvlRcQjSZ8gkJnNl11yrfb2PffuABsf/xHj3yYNcnoYce0Oih12gMdr2RK9fquUajgXcaTHroDLxTq2cawMIJ9alu1tPJWIEkAqlE2peKp85GIylfPJJALJr0JaKpM7EE65WkVICJyswR1yKO3wLmuFldtmyTY//LF5w6I9doNvFOo0UPs03v1Ot0DZyGL+d42FmW0/M8S2a9nmEYO/kLyDMdYK+zHSIwjar5r6OwSFwU0wFRFOMym4hxQRACiYTojYbCZyOTKV88mkIizvgEiB945vjAAILqBGOwQxPXEyrQGHV6jYm1m4xcuc6sazToNU6NiYPeyDt5DdPAMKCiYe5DHdDrBYQKKPXqi78OkTTkXyop+RKx1FkhnPbFIinEoqI3HI174uG4F0QXSenjiHzwdI0PDiBe7nEC0UYtC6fexMJuNzqNWcYGq9VYzmk5O8/zRHcEy7J6MIwdgP76G3+jDLIw8UiSFE+n0wEiLQhzEMaIBGMtAX/EGw1EEE1KH0hd4/r7dOE+ueYZF/Zd0AYBc1jH6ZmEyOstxkq7Veu2ZVnKbTY9tDreyerTDRot6wSXnlYEqdKnXHmhxhNKVxXIKxtz8wGReT+qpIqML5VIn00SnSMhIByMekOBuCcciHvj8TiQkuKIGH/j1slCfXqDwz7/zw/tu5AlGNIujY53agxps8mqrzE7TM02u7HcaNRTJmAgM8EV4jzDKph7sG9akxd14StBeAXwiHIZSKfTcWKxxKOCN+ifagkFY95oII5EMuVLfACsk/cdENRqMMGu0UnFWhNfZ88yldtzjE6LWe9iDGjgNXCyrGweZlLB/LN9UeP0vp10DUBMt4GauCLjSws4G4tKvng4iYnxgDc8Gfck4lI3pJTAJsS4DQivemBV8n1r/CxV+n257/59F5y6LDRabSaX2aHNN9sM5Qajvk6n0xVznMII70tLfuM3oYxBrZKEgGgk6Z0KTrUEJ+OXIpFUOBoUfIYU07n1gVWT72dLbzpDyLqC1SzpInqwGl6nZyutOXp3To69xmDXmfR6xgkOLoZB1vv54B+oe0mAJMCXiotnY2Gpc2ws4AuMJ7zJRLI9BXbg/fRf3HRAEF0hpkm7dHrGac3Sm7NzzDWWbGOzTqer5DQsz3EcsR7MALQfqEF6/xsTlwQxIAqpgXg81R4KJr3jY4GRqYlEpyC+f57PmwYIwgz9gNmoMxQbLZp6q1VTacuxOO3ZBpfWwDSAgfO6/Ubv/2Dd9DtS3YNoz+n0JNJMZywKX2AyHJmaiF6KTiY9kXjyfdEtbhogVCvCaOVc9lx7VVauscZk1ddxHFfMcZxsPfzumKsHiBIZJn4MQRCEyFSie2Is2BIcDV+KB5JhSdL7+JRw03SLZQeE6l9gCTPk6uqz88x1ZruhymBiXRo95wL+L9YVrmMCCEn4ouHk2fBkvDM4OuULTES7haTUykviwM2wQpYdEDIzpFyWbKMrt9hWlZ1lrdMZdfUMwxT/Tle4DkQAxG8RiMfEgcmJYPvYsP/SVCDZxcQ0nXzK2Ln1gZJltUKWDRCZzGArstTnOI11JpumSqvnXSy1Ipj/e62I68LBzI+II0sS2clkUuyMTiY7/eORrvHhULsgMK18QjNgQ2jZ/BXLBojX9l3IsmanXQ6b2WV3Wqrs2ZY6vV5TD2AJzPD+upRvcJze758nJUkKC4IwMDEeaA2MRNoDE4kuISx28svor7hhQEwzg8VQbM7S1RcVZ9Xps7RVOh1LfAtL1BkIIEikYiZx5f3u9Q/6/SRJmkwm050xf7LTNzDVNTUWbk9Qpogti05xw4CgOoM17crNN7tyii1VZrOlTqfjf6cz3CRkpdNpaoUkk8mBYDDa6h8OtY/5Ql3pmG5ZdIrrBkQmMxizNPVFRVl1ljxdFctyLo5bKjPcpN77L3pZJcVvUhTYztBErNM3MNkVGRPaEzFifdyYTnHdgCA6g8aQcuXmWVy5pfYqu91cp9Xy9SzLFgOS4nlcfGLJf82xu6k6EdUpEonEQCCQbJ0cmmz3D4W70iJ7QzrFkgGR6YG052rqc53WOpvTUKXV8i6OYxSd4UYzlv6rwOOmAoJ2kiiKk8kE2xkYmbo0ORjsCExGz0WBE7ffUzd0Pb24ZECosQmbTevKq7RV5eRZ63Q6rcIMyxSTWGwGzPU88W/VbxYFqKQoiuF4NNUdDMSODveMHIqHpIPb717Vdz2PumhAqDqDwHLFOpu+vqDIVmcr1FeZzLoMZrieJszxG9IPpGWLbt0y3fcDdhmS8i8nAS0sekUBvtiUcHZ8MHAk4At7Av5o+/VkYC26y1VmMBg4V1axrsrpzK0z2fT1PM9TP0M6ndbecAYTYQbyb/bzL5kxFiuylHUYyg0X3RmzgLPk5t0o8Oa+Ic32ngrG2kcG/S3+4fCRRAhndj2wtPUhi+6DN15oL9Rx3AaHQ7cmv8xSm51nr2E4ycWy7PJ5IJcNEIv1Z8iAkBRALDwP5x7JDwgg6OIiQWD6/COR90Z6xw+FgvEj4YjYXbKEzKtFA+LNfRdKHfma7QUlWVsdTtNmg0FfueTYxPveczc6FWf/flEyfblvSiG7OIcdZcahZDhxYmIiem64z98RmIhcSqU0nXcsMvNqQUDIOZBRu96ircsptLqLy3KbNGY0cBzjXPKT/w4QS+4y+QeLFYH0vEkkmc54NH1psH+iwzc4eS6aXLzVsTAg9l1wwiQ0OvOsTTklluYsh6OO1bKLymeYTwJc3Ss3Z+bNe9XfemBeE1fUkykIQvfk5NTRyf7QoYnhyKKtjoUB8VxbucnKucsqnG5LnqbZoNeXp9n51jtc2dDF9/tiKXFpE2xeQC6+YUu74QfobEmS6CLkgC9+xNc36QmMLc7qWBAQh19tK7c69e7i8ly32WpoZlm2fN7nXu6OXvB612aW/yrW64LPMY/VQfIopkKJ9n7vSMvURPRIYnJhq2NeQKi6Q06BtS6r1OzOybM0afVcA4D5dYcFB3CJU2jB6xGZSY65o6ML/nyJzflNnb7gcygnpHE1c6eS8PrHoi0j/ROeCV/Es+u+Fd5rPcf8gNh3wclbhcbCkpymgorsZq1WV8dxWJTuMN8NySJYpTTD3LJFXbWd2QO0ToMy4IzCCLNlwfT5qughj8XfuGNrgZFQvyZ/yR1VeJLX6mfkQVVzNhO+5Ht15pPvZ7+XzWH5YGb124K0ntG7oghvKpFq8fX5PF7vhGfX3rXXCYjn2srtTr27qCjbnVVobuY4Zn5RcQNTh2YbS0BKSIGRBLlQh6h0FcfQ9xw4uqI6zcpdxEhyD6VpkjL5npw38z05J53mkAKLdFou8iF/NlMsZG5wqkMpPxCbZkBmHbmPfKhDxJGlhkgziicxzdIBJe2T64yw9HtJkqvRcMr3kgJoNs3KAJrnPZcmlU0IwOR2k9/T52bTcjUcVr7/oo40fEIKZwOjgSMDAwFPYDLajtH515DOCzaiOzirLO7sfIfbbDE0M8xNAASpuEEWug6OAwO9gLcbCAbkej4sI/+jI6k8OgGE3PPq3Jn5ki79i5NeA1grxtlcXNQVY0I0IER+liaXUwZU4qlLWAUKGIWBFFexChzVdSyRekNpQCPI54UMAkI2EeO5CcSMDFI6GQiCQtkcASwpS6S433mJrk2EQAaYAeh78r3yPDwBANIQSPvIbyQZMARgJH2OXE8DFlU6E1bn5aPG4UCB3rKotHWylpSsEAuHE+1jw4GWkf7Ja+oSVwEiU3corLG7bXZzE6dFQzqddt6wazoD0uRB08kEwmOjSJ7pgL/1DMSLFxCfnACSSVruRyT9SAUGWbFAAKLY45I8I5XpSGcxmTG8FKMfjcGC9qQF5/l8jAk6BJISREkEp+JIksUJAzKQhG04BXRkwAiwSAESGYn0e4WZ+CQDlmERtQLxPA1GCwSELBpMaiVI6TRtLwgYpgFMKWwGwHTAGUBSun1aBJIfkgYpQkUFPPmelDmS0nTF806LA801tdhRU4P64gpYtdoryiJdizHEFLxBf7Klr6PfE5hIzatLXA0IRXfIK7I3FZXnNxtN2jqGSSu6wyJpakEuk4BYEvB2AS0HgR89jfDxc2DhV2QpmQ8sRCQpENT+zbysLLMlql2Qf6RlCbYY/boc/Kx0E45FbBiUDJQJDKKgULwCgOkLKVeeo/KILDzk7zmR0L+EOC8gZpHQvToFlKWBLKU/BFYGwhUDrIoYRQeiF8ywiuj7GQDKTZrne0onEpEdgNmKz61oxCfXbsbGrBxY9MZrhL7UWA3BFeuNhuMtvt4xz+BgxLNr79zK5dWAeK6tPNupd+cXZrnt+eZmrY6YmdfvOJpr1XZaEpCcDGLyYAtCL70G/ofPAvDDCkCTMeqqEkblZ8bnqsKmAkWnfBeCCZdgx0+tG3BGyMWoxgqNRgOTJMkyXaFscjWiW9ACZPS3MuimNQhFwaBkQXSclHwDQQ+kc7VoXwegKAmYSGk6AUgqv6S6Cpnp01SksIXyngBG0THkmyn9Ss6nn2eondOAUWhSJLpVAkiK2Fa5Ap/fuBW3u2pQmJN3DUAouhO9GUvrU4wP+4+M+qKeQH9ozmjonIAoX2V35+bb3XqTppnjuMUrkwvaR0oHx6JARxfwza9j5Ml9IOqfOhgKeV7BMddy3BJ2MCpd+VL+bXhHzMJLjAtSWoIBDHhI4KmyR6hborOeIV04/eQqFDIVSlV3IbMeYJPygF2sjkIo54DVAilhBxCfoKjO8owmq9eejVz1lCt11wX5lOpStP1Et+KB7AL8Se1K/PGa9agtKllsSjKNhsbj8fbRoWhLX/v4EZB6FLOiodPdMh2zcGjrymvy3PlOa1OalxpYll18zOIagFCZgkbkRkcRPnkG3L98HcPvvINc2Uicnh+ze2g+fiK3I78zEXEB4Bf8FuzncvCmcSXVxgkg2LQAFgJYqkQqMlxhBTLKM2NDtTlZRNGBlC0cOreSLBiWgXeViCABRG1UbmJcsX8za1modJbZF5k26OzPM89XGzMbm9O/l2RAOPLwR646/MnaDagrLgM3b6UcuZmZfS9JkndsKNribfd54hHil7jSDJ0BhBKzyMoyNpXUZjc7HI464Mb8DnNBn8hitv0i0q+/ifRj/4be3m4qKsjAZnoRZmNL7pMMKlbEC3kABlb4weCb+XvwnpiHXskArVYLWyoBhogKUZRFBlUkFd1g+gbMtI7HTisr8gtWIK1KI8GmkDKx6F4bB6o4wKZUOU1MX/DqR51miUw00OGZsZ5UpKugoCJF0Tmm2UVRUglLEKuMZ8DnF+PP6xrxuVUNqHIWXlGDdWG6gXdyItQyeHnMMxFIXaVLzADi6bZyfR7ndhba3bklxmaTybR4UTFPK+bSH0gBruTpk4i+8CKM//FDjE8MU8qXDa8rHTpK903P4tmAIMNFFXuQAnYpfNN2N07wxehnTJQhCCCuuMa0GXulp0cinh8wVGfLPJ8VOKpMJvkUJKsWl9clgUoGMCfkgaO6wxzazXwiIZMB1D7L1JiplUE5acZSyexb4tfggbLKFXh4w1Z8tMyFQqt9sSJDvZIvEk6cHekLeUYGIs9v3VPRnnmL6ad59enT5dnlJnd5Wb7blq1v1mq1NwyI2Tihdn9KAPPGawg++xz0P3kOkwjRwhDyXJwZEPI605M3F+ZmlMlsnEMhvlW4FQMJI70iqWSrT8vaoKwjyqMxn9UiX/9KBuKTLK1kmzClEc4HhrZMATkmgEvJFxWvUsHky8yBEcVOvvLLTF1D/V0mocharfyPdIaYArGd71rViC9u2YFtBaWwG0mC+5KOeDIpBMKTUx5v18Rj67dXH5kTEPufPVeXV2m7t7yq0K0zMQ0cxy1ed1hkewggpEQS3PO/QveTP0fuy68gRpS+WVlzKjBm6w6zyZcAggzhJOw4jXw8lrMVI6IVEd5IAaCna1pIh8oeyszJON1kihZ1NBRAKJ5NPsXR2tcpC4NYIYu+LWEgy0CUB1mZzCTr2YM7cwPllXLCQgrl9PezFA/SGYQhOBYfW9+EP9nuxnp7Hsw6uarCUmpwkVLNYlQ40tU59thIZ8CTmXs5jeWDr1xsyqsyP1xWWuTW6NSKsIsc6cWeJgpAaAp4+kn4vvcj6M6dmTb35tKjZl92dp8TZZIc7SjHfkstfmRsQCpFFC95YDkqCuY/ZMe0elzZAtLB2oRc6zqSI6GrOg2sjwFGk1woncjzTIhlNo7lZL8EmdbXYoH5mkc9mATIatCOVEIliBAAkxkPr9+OP9y2E+VWGwxcpqG+uIGQU+2E9t7uiRd8XaMeIcJPWxvTgDj+5mV3fq3tkQJnrptf+j0W1RLimUyNTyD95E8w+L0fw9p9aVr+zZ44s9kgk4lVUaKSZStK8BbvwhP2dRCIFchxlBGI5+FKJriS4pVIhSKzFb1CZYw0KCAIQ8TyWVyuZYDGMGA0AEQSEUBQqyXj0acVQYVpiJY6LQeVF9PRvcz7ZVTcow+akR9CcUWCPSKgYYB8J76ydis+v2krivQGaOcv53vNMRFF0TvUF2zx9Yx6IqH4tLUx/Thtx3vc2WWWRxyOLDfPzyMbFzXs1zgpHAZ6+4AnfojRbzwJvTQ6LbXJcxOCz/SFqiqb2prZSrnKEC9a3Wjh8vCCpo4qgZZ0jJqJCpfOyO3MUOsc0S3Vo0zGkSUBNomhVe+HKpLw12iBFVHQzRiIl5UgTz3SxBWe4XpWRQmvRmd5ZcYrZm+mcnmFUnkl6Uxfn7BNPAlYdNhYUYf/3rgRe1c0IEennacwq/rL+R2KBBATE8GW8QG/Z7RvagYQcrX5qL2qIs9tLdQ9bLFYmhYbSFsqPtKBAMIX28A/+WOM/8fPoEdYjuUoQCB9Q/QJ9TPS5epcIUAh/9RtLsg5BBDknBf4bfBo8/CysZ7KUisTl8dn2v1MtHbVssjgnlmgICEEGrpIgwaUKCBEESNVIsZqdEDtlOyQisRkQKgKn1ovTbUSGLpZB6Ah9yRubRIrUUWIErSTw6JXluWdw0Kl5xCGiKcAhwl712zEF1avhbu0Gg5N5qYfc43G/PkioihSa2N8OOgZ6Rx/fuvda6i1wby874LTahIaK1eWubML9PdoNJq6RYdWl4oInw949yDwxBPoe/lFOtjkkUg4jkhLoiQSlY0c5FHI51RcK98T9YlIMzV2QWKCxEX0pO1uvKUpxnnqNgGsJAaizEKqbFGEz1LS6JkzTJgpslgS3CTFRRkJEs+ijTBDrR5wxmW0RAlDKN5OykTER0DNAMUdTQadBbQagIBjlvWS4Q3L8JdnNEdljWlqJLGfFGCz4f/ZshOfrl6NmpJiGBcExOwByvT0sPFEIhkYHw96vOeGH9t+ZwO1NhhiblqzOXddQ7Xblr1EV/USAZEeGEDo1ddhevIpDL67n85wK/Ig2XSQkiKk2CTIxjSqNc7DBr3WDJh5iEkBqXAAEiLTa3nIMEcAPKHbi3eMZWjXZNMWWdIkKJap8s019WaBQZ2xDANOlL2UNB9Bx+PCqgTg0gCOEGAzArmF2GzNQa5RD72O1GJnkEqlMJWMYzASQvvYODDpB+IEroQttApDqeF7ha0Wa3EQiyYhALm5+Optd+GBshoUZ2dBPx2+XexAqD0rO7+ItREKhD0dxwcf3bJ3hWcaEPYivXvlqnK3NUvfDGDZ/Q+UGclMPX8Okad/Bs0TP0dgaBDeT30UeVU1SOt0CAQDKDl+CvF3WgGMwoosXH7obuSVVyCt0WBychK5h45Af+g9yiqELQgYulGJ/3Rux7mkHeOsrFVwTBIa6paUg1cqHEgkI/MzQuOqOaq6qcn3vCCLCoGXINk06FgXBgrTgE4ASrLxTzXbsaKwADaDETqdjl6DKp+JBILBIDrGhvGm1wvPYDeQJFFKDqCzWbVMMkzQTLGhjqtKaLIPXY5hxAXk5Bfinz/8MXyopBwOo3kmnL9YPMxxXjAQ87Sd6X10yy4FEMT/YCs13VtTU+w22bTXzpm8zhtT+iWJHseO4tJ3vgPrkz+DxZEP/d9+CdzWHTK99niBF19C/5OvQ8Q4yvNqIX75D8FtaQJ4DcTWC4jt+wXiL79MB5hYGGT+taMQP8raiVYxG36NlYoKNp0ER9lRFRcKM1MdQdH8VQUznQbLcUiT/AxFqPMCSwdY1KTBOHRo2xAFCkTAwgIrK/HC1ntQV+AEK4iIRWM014IwBacliTcsRoUYnjt7Ft847AFGJmV2IICgVsfsPMKM9kyjN8NdTaKc1KEnoayoFF//yIO4o6AYJo0W3DLo/uGp+JHui77HJvui1B/BEP+DvdDwcJWr0K03aW8oZ3I+vBCHVDqZAvvWm+j7p28gcfAYyleshPexr6K6aTOQFDB++jxyvvUtXHzxAJJIodG9A91/82eo3LCBzrCRg0dg++evI75/P9UjCBcMw4G39avwfet6jMTIDDZAkkTEkxE6w1mOgZ7TwqgxUw+lSHKWBAGSRPbhYpWUPAa8YtrQHXEEAXxaRwERNaTgL+YQXT0O5JkBhx1rV9fiqaY7kJ+Xj47AKFrOnsG5qQAKnPl4wFmOirJKykonz5zEx4+9i1DnJSASAgxmQKtorap/QY1ikswa8no6QSaTKmYWPrvLqvH/ffhe7HQWQsfwi1gCvPAMjoQT7X1dwy9M+EIeIcSfYYj/IbvE/EhhSY5bZ+SX5PFa+HbyGSS3MBGOwPDqa2j/+jfAn2qFa/Mm9H/jUZSQAY/E4D1wGEVf/wbOHz4KoyELdXt2YeQvvoj8FStoBtVlz3sw/sM/gr9wjgbDiPLZDRZvogH/admI8SQL3mSmM1QCybgipqBEk6w4SUMZQEgL9C/1KRGG4Injj6OAIMyRTCTkHE1JS//GTSKCpRpMEUAQl3VBPm5ZuwrfWb0DJqMJ73acx4+e/xXeHh3Gxvp6/PWmnWja3AQdw+HEudP4vVOHMNB2AQj6ZYcWuRGlMNWWUhiDuMAJS047pBSGIE4n1S9iteD+qpX4s9v3oMmeAz4j1LfYcZjrvHhU9A72TbSMDU164qNxD3N8/2V3fpnlkTxntltnWMiMuc5bJ2LA8Djwq1/g4vefgDDkx5pdGzH4lb9E0Zo1QMCPkddfRv4Xv4buaDeQW4a8hx5E/OP3I6esDBgZweSzvwQe+QeMQ6RWBol/nEMe9qMCT9vWgGxCotNpkZeXj7JSwJmfj2RSwGD/GPr6kggE/JDSSWi1HPSsSGk+JRjAsTrwZMc+ngPLBKhoS4t0Dy9IuQxi1XoMlQYBPUuiSvhSwwY8tHozvd8vzhzBD5/7FfyRMG7f2YyHt+5CY0MjBJbHK8cP44ueF4GeQSVHlJUBQbx+Oq0MgFRcGXC6jaCcXEwSbshfYraS8SCvybkFJfi7VfX4xLotqLPnXuGKvxHJkUgkvCNDoZbxgQlPaDDuYU4f6HEXVNgeycqxuTW65UqRmwEOVdbiUcQue2H4xc9x9qlfwZAUUXPLNoz9v3+KXJcL8Plw6dVfo+hP/x6DCMJQWoOSP/gsIh+5B6asbCQvX8LwCy/A9o1/hw46GCoLAYMOPksJ3tNU4bnsJvh8PgwM9MPpdGLD+lysW7cO/skADh08iVOnBhAOh1FZVYaSkkLkWPWIxWIYHJqC1zsASZTgcDjgcmXTvzwj5yoms0SEioEfMeeBkX7kl1fh9xo34sGaBoRCITx/4ST2Hz2CtFaL3c3NeKBqFRx2B7qGBvBuayv+cegSoDHCZjIiGI0pqwlYIBIGiJNOSgJZWYDRQkGPSATQ6wC7AzAYgcAYMDpKQVGxoh4Pr2nEh2rWoMpkU3JBZSjcCCCSyaRvcjx2dqR3wuMfijzPnD/c486vcjxit5vdGs1NKgc4NQXx+HFwjz+Jcy+8DZ3LidoP343Qgx+FNS8PaG9H+OlngG9/CxNkYeLW7VjxuU8jeeutNK+B3f82Bg4ewlC3FytWrICwcysiZgcS6RRCaT0meTNaTw3jtRdPw2g0YsO2fNx6623o75vEr559BRcu9sBiseKuu1dj/cYa5OTaKEBOHx/CG68cgX8sgfLyctz5sWqsWFkBHVUORTBaDkkti692nsW+997DjuJi3LNuI+6vrEEkHMGpwT74hn3IM1mwYsVK6O0WeAMB/OzccYxPTmKNxYrNZTUostup9ZFIS4ikEvjXfi9O9fdjLc9iS80K1Gbl4eTp02ifmoQzNw93lpfDYDDgjzrOIdZ6AYgn8OGNm/GHjeuxsawGuUT8LNORSqXiQX80MDLg9wx7Q48xbcf73c6q7EcsVr2bbHp6Uw6/H6GDB2H9/o9x9q1jcGxwofT++5G49y7oDEbEjh/H+DP7oH/ypzTzSdqzF6Vf+DywfQeEZALp536JkQtt4FIiCogSumMrwBto0mmSt2BK78Cpo148/cR+TIWn4Fqpwy237EZP9yheeP4NmM152LBhA3a6y5CVS8zEFPR6PXq74njtpUPo7RpDXl4e7v/MGlTXliIRi0CjYaA16ZDS8fiHthP4tceDtT46VPoAACAASURBVCUluHvdRtyRX0wpuy8RRTwWg04gO+RI6PEN4mx/Pw6GxlDsdOKOuhUoN9hhYcmGsjxYvRb+yBT+9uQxvNfVhVuy7NjVuAENRaU4e+4chqUkKsvLsdluhz/gx4ffeR24eJGKmc+4b8Efr9+MGkc+HFo18H/jo0WU5/BUHCO9AU9PZ+BR5vLpYbfTZXvEYLg5gKAiw+eD9PwLYB/7D/R0nIN43z1wffwTEHfuoHTHvPQy+p99GYk3fk0tCOEP/wCuhz4PrFyJqfEJWL73XYyNjiJWV4PSnbuAChcCg6Mg6yWs1mxIzly0nunDT3/SQqm8opbH+vXr0XEhhHfeeRvbd67C7Xv2wGI1Ydg3jJHRcaxevQYavYSD+1vQ2xGEyWzCHfdvhNlkxuULozCbzSiq1MJmt+OrfWfwZsu7+HBVDW5taESBI4vqEJ3JMPxgMREOon1gEC9duoihsTF8rqAYzfVrsaKiAoODgxgOBuFyuVBvy6FM8YlDb+F8Wxu2VpTjU2s3Y09BKXy+YQqagoIC9EsJvHr5Ir769muyOZ6diy/d9mF8afUaOM0O6LTzMfliywbMAIlYVvGYgCHvqKerfeRRpqdtzO0stz2i1WrcNyOGQVPmBvox8uMnwD/yL/AjCO0DH0XpJz8FNG1GkqzBeOVVDDz7MtJvvohc5EP8yu/D+uB/A8orqAOL/Zd/ofa+pmkjsrbsQJjX4tVX3oCYFlBXW4/ijevQ2TGGp554F86CAuy4pQzFJSU4dWwYBw4cwM5dq7Fu7Vq0trXi4MEDGBoewac//RmsXF2FU0eOwdeXgMVswdbbatHX14vnntmPyspKbNtdi5LiYnzddxEHjx7Df2tcj9saGpGaiuDM2TN4veM8hhJJjMciOBecAmIhIC8P3/rQPdjoWoFEKIRXXnkFnUNDuOvOO7GzogaxWBQf2v8K+s6fx8qqSnxh8w58vE6Owagez9daz+Cnxw/hYF+3rFfkF+Cv996HL9U3wK41QKvayVcRxNIBQcYnlUhTQHRcGJQBUVRle4TnefdyLsSRvZMiGBIE6miD//s/RPrb35YTlT/5SRR97iGI69ZRrT7a2oq+o0cR7OmiM0RsbkYNMTd5DonzbUh87X9DZ9Bj+I8/i/LqBqB3GE89/gwkiwUNt9yKvE1rcOniAH7+kzfR0NCA3XevRCqVxNnDY2hra0fzHSvoAL/64hkcOnQIKXEKn//8Z7ByZRneO3QIsVgCxUXFqF9bh1OnTmHf9w9SpXTHx8sQcxjxeN8FnGm7iK+s3orVq1ejdWAATxx5F788e2wmyJVIyNZAzUq8u/fDKC4qQstQN1595U1M+P345Efux46KakyMj+P2t15E6Nw5aMvL8Ve7duOvatfDaNBhNJXCmbEhfPOIB2+cOAqEZSUzz1mG/7n7Tnx25SroiehRQ97LIOHl3AgJg92jnvbzCiCKXfZHWJZ1L39QS4IQi4NvPYehH/wI/A9+QJ23qU98AqWf/RxSGzZAQ/ILhn1Adzfgn6D+etStoB0hjY6g5/gppP/5X1FWWY7Y3zwMa3E10ucv4Uc/fBpZ5eXY9uF7kS7Lx6lj7Xj+mQPYtm0bmvfW4OLFizh/dBx+fwC33r0GlRUVePO1C+jp6YHFrsXuW3bCYJDw1ltvw+7Iwrq161BUlo/Dhw/jpadOwe12Y8v9RWgLjeInA+0YHh3FX6/egvLyMrx7vhU/PvIujly+oASxlFiFQQ/UrMKBHbuRk5ONAz4vLrTKKYt37NqNlRYHOju7sLvlFeB8K1BcjL+6dQ/+tn4zyNaUl8bG8E5HK7524gD6L3XQ7CjCOBtKq/Hwjlvx8coqaKgrW6GGZQAErbQvMRju8XkunB18lOknDFGb/QjDMG6VgZaSjnVNtSYtITURhObQIUw+8VOkf/kMzS0xbdwMy2c/CezZA5SUUddsIhaDmErBSBQmLQ8EAsCvnoX/dQ98Fy9jxdaNwFe+BJhykXzvHJ79xctwrGvEtnvug5/V4MihszjgOYkdO3agqbkeR44cwaH9g5iYmMB9n1yH3bduxeBQP8KRMFhooJH06LwQwtvvvI3GjcXYu3cvpe0D7x7BO/vPYvee2+H6SBE83RdwcniQMtkX6tfB7nDghyeO4wenjgF9nYBGA2iJm0xJxSstx89u+zAaSkogxGIIB0PUK0qYzy8k8W77RTz89otAexdQXILf3/shfH3NJpiNRjzb14nvtnjwbtd5YGxMDp0Xl+H3a1fjUxu2oJn01U04SKB2qHvM03qm91Gm/7KfioxMQCzbPdMSpgZHYHj7HfieeQb6V1+kDJGsqIbm7j0Q770HOTUrwJqN8hpF4jqOJxEJBxHq64PuiSfQ4zkKO8ehbsdWTP3Rp5BO6zFx6DzeefswLA2rsWbXbnQHIzh3+hI6Lw1h8+bNqGssxskTJ3Fo/wCGh32486MrsffOW2BzmOn6jIA/jLbTl3D0QA/Ot57H9lvqcPvtt2NifALHj59BR8c4tu3cidzbzDjQ24H2qSCsFgs+XuqiK8F+fPY0XiYuaf8woNXL/0ivJuNAdg7+Yd12bHS54DCbwacZ6gQj4rh3chyHOi/jm6cPAn3DFBC/d9eH8U/1Gyhoftx6Cn/72quIBn2AQBJ5Abhq8JeNm3D/qrXYlJO3bEMz+0IDnQogiA5RVpv9CIBl1yEgpRC73A3Dc79G6JnnED9zmPrfiSVh5qrAffoWYPNmoLQA0OkAkjY/Og60XQROnUfyjYM0K5uzVSF3Rz0NLEliGpc6k3inM4jLdeuRW1KPkckghoeHEY1MoaKyEs6CbCoa2lt9mJicgKu6BKsbalFSaQPLchj1BXDyeBu62kdoVuWK+gJUVVVh0hfB8PAQfJIfxo0lOLZZRNw/KaevWSzYZHfQTKyjJK+DzOBEWHYLkRxKMnipFKDRAUXF2JCbixKLRY6aCgK0Wg0uxZK4ODwE9PYAsRhQVICv7N6Dj9Wvx9jYGL5z/gh+9d571FVPPZpCClz1Cnx7wxbcWbUSZcSJRbhoVrB0OVAy0DXmaT3dKyuVJdUOyhA04LPAKqAl3VwSEGhtA/vzfUj86mXwHadp6JoolgI08Deuh1BdBakkn4aRpWQC4oQfbE83mAte5I73UrM0QfZgWZGDeJ6JigffiA7HR9I4WlAOkc9GOCFSM5Bl09Rc1GgZ6ngKB3gkEnE6gW0OHRx5cjQy6I+hv3cC0RALk8mMrFwGOp0eiSkgEo0gbEgitcKBkdVheaaSASeigcRH5G2H5c9FJfuahNpJcIq6nUk+DCcDnPwjuQzkc/J7EsNIJoCpIE122diwBp/evBVbbHk4dvw4vt12Au29vfLvietaSKGsYT2+tmUndhVVwmkkK1hkQBD8LZdfWRYZI57WM/03z8qg5qYoIHn4ELTf+R4Cz+2HkByhMQgSmSa6BMmGIgNO3CzEsiafK4nz0w87O6WO9Pcb7E7s1+SgxVBNB5yVJErlHC9AFCUkhCRdqKNnNdDwPGJJgZ4X51JguTR4TVIuHyAYwDJaJFMCncUcL1HfxGh+ArEqA1BBImMsQBfaqtlQHM18psAgUTKSSU56lOTfkZg7OZcwHfksc10FsbbIIKufVbrwvaadqKurw1gyhcfefBkHOtuoV5ICh+foGpbNGzfhsc23oK6gEDYCsGU+ZKUSGOgc9rS3Dj/KXD4/7CZWhkajcVM5vkyHHMOIYeKdtyF9/ZsQ3j0MAxJXlNdRsgynF+SooFBT5DJzLdVFwAQwL2IH9vPZOGCulRff0NoPHFiOhLbJGm85uZSEiAkwUmKaMog/GYLNboSz0EGtACnCoburF/5AkJ7nyDKjpDwfTL0ZnMuE8fIU9FotUokUguEwemJRRINheSkBiUKaTQDJXDIa4WQ4pFgJOgIIIYlQIoFxIQUtx9NtCH3EhJwYB/wBwG5DxdoN+M5WN/WYnujswtffeQ1jowOA3gDwuum1Is2bNuOxrbehOicXxmUcH3WYSX8JQhqDXT5PxwUCiGlPpcG9bPcjI02idAE/Yi+9DPzlIwiMeaeZQHWfqPkg5HT1X2a8VbWuCKsQkKRgwjj0+FluM94QcjDAZ9GZbiC1EwjDMEJGFvJM2pwoyCluCS6E+sYibN9Rjvz8fIRjcbSevQxvZ4CKirKiHKysL4NjQzYSDi3CWgEmoxE5aRIOlzAWD+P4YA+ePX2GMkRTaQV2Vq9Cqc0GMRqnDiOdVksryYwFA7gcnYLNaoOL1yEWj+P1ng60eXvQVFiCnes3Ym3tSurJ/IfW42gjMYtIANBblRA5S1P+v7hhM/5qczOK7dnLkiE1e74THSeVBAa7fZ7LbcOP0lhGUa39EYPB6Obn9YAtkTZIOD+eBDM+ivEXX4L2z/8e0dgwBURmGmFmettcd7gaEAYMg8PPbLfgrXQuBQSZ2UbC2CRLmlMrsMyseSBlAcgDE+vCUWzGnrs24SP3b4TNZsNUJIr+3jGcPt5DlVBXeQEa1rlgbrSha8qHy5MDMBqMWJlXRMPpk8kY3rxwEv/60iuA1Yrfa9qG3Q0bkKPRYmJ4BBaDDo6sLGj0OnT19+Fofy+KCguxMa+APt4b3W241NmJHXWrsLqyGuJUBAcOHsTfnz8mO6FIPgSJ0RD2Ien+xYX427Wb8Mer1iLXaFmWDKnZ/SwIQjwWTQUGvaOens6Jx2i0s8CV/YjNbrpuQMjULxf/Up0m6UgcTPt5JJ97AcL/+gZCSFBAXOtQ/SwqW5BziahQq8RE4EA77Phu4W5cTBgwkdbRaKiBLqsjNCMoMlpZzi8QL5wIIZ2kYmL7HTnY0bwFla4KDA0NYXgwgmSMweW2fpovsXVbJdZtqkKqRIvTg9043++j1sGuggoai7gUDeCN44fwr6+8jMbSUnx5+x6srq9HbyyC4ydOIM9uw4rqGhRlWdHZ2Ymfdl2iQHLnF0On12NobJzGMprKyqmo+GV3K7554gSmLl+WFUmt4uAieRJaI26pqcWD6zbjI64VcBiNiwhzL72wiyAIvqlQ/Oxwz7in+9LE88zpA5cpIBxZFreWOISu47jCcaa8SQWnkD51DFPP/xr8t79FU+qJuanmj851m0xAZH5PWkVAEYYNrbDgu7m70ZY0IsSbwfMaGBU7P00AMX0wkFISFRVE2VtZ78Knv7gRdStdCIVDOH36FPp6/GDSBowOhaj5ed99TVizrgzjlijODPfi0ugEzZXckVeCkpISHBnoxstHDuAXJ47DvWYN/vaWD6GysgqtI3LMxJmdhfq6Fcgx6+Dt8eIpbxdys7OxI6cARpMRIstTf0uV0QS/348fnTmEn1y8AExMAsQhR5RGouHFIoDZhgc2bsL9q9dhT2EZrCSZd8GxWTogksmkNzA51TLknfT4vJGZjKl8Z45ba1gmpTItQRwdA/fmawgTen1mHw1rKysYMh5LVRUziwdd/dREhyC/9aEU7zEl+JFzC0aiElKkg3kOHCMDgVUXzigFPIiPXhAFkDyPhrV1+MyfNNKYhn/Kj96+PkxORJBKcvB2jFCl886PrUXN6kqMcFEEQyFqeRQ4cpCTnUWB9Y8dp/CUpwUY7Mdtm7fiaztuR2FhIUaTJNlmCIVaEywWC7qmxtE+MowBf4CG1Ruyc+EgQSmSzJtOY1KI48zEAJ5qb8fJzm5gZFD2dhLfA1keGI0C9hx8xe3Gx2oaUZ2XB8O11lfegCubZEyNDodbxvrHPKMkY+rwm+fd+WX2RwqLnDSnkjLvjfoiSKLrkA/JXz6D0KtvwPr669TEJMeVKL9y+f18E4AAghxDKMS7KMaPc7dhUuCQYjg64xhGLk7GZhb1otV+6KJWKkoqq4tw5ydKsH7dOpjtFoyMjKDH68PYSAgjvSGYLWbc+bFGlFQVoisxhmAgQKVfaX4BjHodRsfG8JdnDuPEyZNUQH581634m3XbaB5EXziARDyO2hwnbDY7FTdnenvQOzGJvNxcbC4qQXVBMTQcj8nJCVwY6MGRoR48OTqKBFnDEQ7KYCA5lHSVVgzILcA/33EH7q2sR7HFQoNa8x43AIhYLNY+PBB8YaRv0uOfjJxhDrx63p1bZHuktDLfbTCpXT/3rdVFtgtSl5iC2NUF/Nu3Ef7lG8BQ57QVsRRRQc5V8yfJ6xZbE94VcvAcWbInkYKicuEwUkfqSnWVwhoCdR8ISKXjyMo2YcuOMtx+53qsWV+LgYEBnDjhxdkzHdAmGVRXV2PXJ1ZD58zCmdFBjI2PQZMSYLfbEeeANv8Evnn2KNDVDeQV4E+bb8PnV6xG/8AAjviGqUdyb7kLxcXFeG+4D2cuXoB3wIvsrCzsKa7BmoYGWtqst7cXL3VfxvO9XvTGJoAwcW4R5xfJula0p0QKKKrEL26/A82uOmRpdIsuP7igVJl1QjScPNLbPfLYyMCUB6OjAWb/r0822Zzmh6trS0hx82um4S8GiJRdknFE2tsR+fr/RvJnb8ICeb/yzMW6me2aS3dQjUaidxCYEv+DR7seLVIOfm1ZI1dyVUq+8JQZiEKrrsaWC20kRLkyriPXhsqqQqzfXICtO2tRu6oc4xPjOHqsC4cPnYY+xVFAbPhQBeIGDoe9l6kosmm0iEaj8AtxdIXDeKr9NEBmdM0KfHn7bny6egUm/ZM4PxWi/o8t9hzKNPu9negdHkJKTNIcz+1ZRTS4lUwmqDJ7NDiJ1/t78WbPBWA8KK+7ULOulaKnKKvDy3fcgW2lLlhZeTX7DTP3HGiZCsY8XZcHH127sVpeufX6syfqbE7TvatWVrjNDt0NLtSRK6+ykTDEU6cgffnL8B06SlPmr1c7IURJrJMYgH3Ze/CWWIgjTD54DQ9NOgEN8eOSmaWuwpouAAKEU3JafVV1PtZvLsGO3YWor69HTk4WFRmnj43j2LFjMPA8XNXlGNptx1Q4jM5uL2qKy7DFkYOBwUF0JxLoifix7+xxIBjEPY1NuK9pJ7ZVViIYCCKaiNGczXyTEdFIFP9/53lMTE6i2pqF7WWV2JZTCJ7jaM7kuREfvPEY3h3qx1ttp4EJAgiiAMnFkWjpQZ0J7oo6/Mutt6O+pBLaBSl5qbwwc/5UMO7pOD3w6MZdCiCee/xweXap3r16dY3bnmu8waV8MiCYyQmETxwH/3f/E6PHT9JVVmoNKbUps1dbzvdIKkOQVVpPGXdhP1eCo1wBtBoNtIy8QotRqXZWdY5wKkmth+JiK5p2VOG+j6/BypUrIUoCzp09i+OHRuD1elFdXoqVq2rwWv4gfCMjCIbC2Ll+E+6ocNHUvfZwGBdGB/D9wy3US/nFW/bijsbNqDIa0draiqQooL5+FUocDgq0/+F5DRe9XmzMceLBrTtwb90a+niH+rrw1vlzONjTgxY/CW8ngFTGyi1aG1sEcvLxido1+JuNTah1FlHr7GawA2mT3x/2XDjpfXTHbatlhiCAsOdp3WvW1rltebobrEtJq5gDvV7gnQMQHv0aRoa6KENcy9yUJf7c6eRyGQAeAQj4ft5H4EkXo0s0QcNrYEAUvDizJYC6TpMUR6fpaCKDFKl2a0phzfpSfOGPNmHlqlXwB0P49XOv4+Dbg1TpvG1PA5q2r8T/Sp7H5cEBFLMmfHTTdmwvLkZ7WxueH/PizMAQOkgtbqsVz7rvRv3qekyKSbz5lofGT27ZtQurs7PQ19eHe5//OXpHR3Fv7Up8apsbd1VX0ye8MNiPl1pb8U8n30PcNwwYdcpCYGU6CElA4pFVVoz/vroRD63agLKc3EWYm0tjCFX0i4KEqbGg5+zJ3kd33b1WBsTLjx9zcjahcdVqlzu7yHyPTqdbfDmAq5QKidaQira3QXjtbWi/+hgmpwZolbnrZT3VDvED+K79brQwpehlrNBqtDAy0WnnpFr3mj6UMulIRfwEREwlx1G7Khf/4y920xS74dExPPH4M3j7lW6a7v7Jz9yOO+/ehkfDJ9A9MIgygx13rdmIBpsNR48cxXfaT6Ctt5/O3vzVq/Hs7ntRVlaG3sAE3tl/gN7a7W7GKrudKo2feePXiKdS+Nj6TdhSXI4NZgt4jQYTYgpHfcP4xrtv4GJHu7y0j9XKi3TIRYhLFRrUrazFH67bgI+Vr6BV5mb33Y2yhaK6xmORWGB8aNxz+fzYY7fdv14uB7D/8f36qMlkd7mc7pwy68M2m5UWDLm+/EoJUiwB9tAhxF95Celv/jsmkKKAWIgh5mIJ1eaJIAsdyMN3nE1oT1oxzhjB8TysUvzKQmLqQlqqW/K0pgexMhKpKTQ0VuPPv7oLa9euw/iEH6+99hbefaMT42NjuPv+Ztz7ib3oTg+jfWQQE/EYqiorUe3IQY/Xi6+dOYKTp04DRj00q1fj6Q1bke/IRiQWQywYgkNvQEGBE4Ik0byGkz2XqCeysLAAQ0PD6IxEkZuTjY3ZZMUVi/88fQw/P30a8I/KsWzipiZPQvMgeHx09UZ8YWMTNhaVwGY0zxnmvlFQkIIhgUD47NiA39PXOfX8no8qBUNUsjl+oM1dUGF/JD8/77pd2ERbFqJxSK+/gbEXnoP1iZ8ipCzdz9Qh1Fi+SjDX0h8IkEKw4SKy8R85W9EpZWGCM1MzzCwSVZPiWvmrWBt05TdH64VTTyWTRO2KYnzmz1Zh1apVSCRFtHd04vR7I2jvaMeGLdW45c5tSBZx8AbHcXloCFlZDlSY7UgkEvj3njYcPXNWXopXVoa/yymCRW+gYMy1O5BnsdDwuz8QwNTUFKJ6FharlZrGJ06ewNEhH4qcTuwuLUdWVhZeGOjBsyRnkjikSHSUAkKpMqfV4qGNO/AHm7dhpc0Bi47U413+I5VKeX0+f8tY/7hntC/p2fugXNF2+l4HXjztLqjLfaSUVKGjLvXFbbR2RVMlEWJgCtwTP8HUj3+O1NkjNO9hdtnBxTweAZCSqYguUlTMVIMnzWswlWIgsaQyCyFXUQmWqXUgZDKkZYHIol8S60+Txb0iHYi6teUoKcuFwcgjEo1ipGMKHR0dsJTZkL2lBKNrU3gPcUyNTchKjc4gJ7aEggD9TFKW9SuFRclqbbMVsDiQbzBghKTMx2Ioz8qGSauFNxpBxNsDBEihERuQl01D3yQKjLFJ2UVN8yQ4JcFGBEwmfGXTLfjslm0oMhmX3UOpjitxWQ96Ay0DnUOeSAhXA+LNnx9scq7Ie9hVXeHW6Tk7wzAKbBczfKpSlEJsPADuhz/A6H/8FPrB9ml39VJRTgBBrBOid3fAibf4Wjxlb0BU4pDm5CIdHFkbSWGt1pecqWWt1rkn+1iQ0SXRTq1NpGOsN5J6EGlIfj0CgSBEexqoNqJ1bYpWq0GUlp+T6ZwAgmZIkWsrq7VJ7qRadpBmUWnlwBTNmBLlnAa6mDcJRCPy/hhk0DWkzJBWrjJHXbfyXhgyqpW/jiz8U9Ot+NTmJuRqNdBeKy9qMY6hOYaPgCKRSLT3tPle6Okc9iDCn7nroU2+KxjixcdP1BXU2u6tri10G63aBp7nl164lJTQGRoEHvs39H/rpzDSdU3yoVjZ86Ir08ogr9Wi5uT1O4YteIfLxrPG1XQg9crON0zmHlxXbOYlFy1XnTkEMLTeQzxJ/5JSxuQ7Y5KHRqPFYFEYobIU0JgEDAaq6VMXMgUBaQxJf1NS52ghVGUkCGOQfwmlABndBUjZO4OcQ6r5EYWR5keSoh/EK0lYRi9nT9HOUarVkntpNeCcTvx4SzPubtw07ZBawpRc1KkkxhOJJI50Xxh6bPjSpMeISGDXQ7tI/HFGZMj+CJO7vDLXneu0N+v1+iWXFpLCIUS7OqH/znfR//0nYUVs2v+QmQexUKtVQFiUIjxvspvwjjYHv7KsoYAw0foaDCQa3ZS3O5CHSIXVjHZCZgMVH6T+gyBRFzNxExP/hCklq63jFUn4KtPAqrDMCCkiEtS9LRSnEd26SQnvq4AgM5y8Jj5ypVyRvGOO8l4FCAEF+Yy4qAnQOMISmVtIEVAJVJysrq7G3zduxi01q2Bh5LIEy3zE4/FkYGzE7+m7PPLY9tvkYmPqMX03Aghrtt5dUJzlLqiwNtvt9gUBcZWeQZa0nzgB/OB76HvhRVrlRa1yv9iHUodSdUiROOi+rD14G0VoYfPpZWxEM8ksADrtByXbG1ABMVO6h1KxWsqHjDOZ9aTchlqNFmhdlQSqNUBxTJ7hpAo+zZOcVVdyds+phS2ne1NtvcKLV+wiqGzlRCOx8h5e07UTSV4mqTKXm4cvrGnEQ/WNWFtRTbdVWq4jY6x8fn/k7Ih3zNM3OPH8nrs3zF38nPgjyJbOdoehqWp1QXNubi7xR1w7tjFL8Uz19SGyfz+0P/0Jxt7eT3WApQAiU2yoQS3CAU8TDyVfioPaAtqJdpqkpspd0mkZNappoTGVMdTuVKMoyh5adJc/eT8r4lI+1yACVTxQEJYBkVABlWEHZVKcWgqI3FeNPVCAqrGUmU3daAumf6uUMpwVc6GJukRvKSvHX2zaggddtVhZULpgQtH1gIXslzE+HmrpbvV6AqNxz94Ht1yxbeM0BIk/Ygwmu8OsqatZ53QXFuXTzdcYJnPT9wUSMC60IvHrF8A/8XP42ltppTjZ3Lwy7yGjcO/0M2WCQQ30iuARAo/v5+zFQRTDK5kohZpJIclZNEd+n1mJdubCmcXN5U85migtQeAkiFYtLjVG5G0PLIouQNMz5M3Y5s35u1o6zT8+BEC0EkymeUw6Rrk+SekPJ1BRWYEv33o7bi+pQZEta94cykXrktMnzowb2dh1uH+ipeN8tycSED33PTQPINSnefXpw+VFddnuopJ8t81uoq7sGbpZABCnT2H8F/tg+tmv4O+9TKOUhPqlWYU75wJEZm8SEJGuIxsvBcHgO6a9OKwpxQBnkwGhJMRMK3eK+BCJskj4QdnXc64RItclgCDuZpEnLkxWswAAD2lJREFUG6kZcJEAoiINGNVtD1T2ISWJlassRQmaC63TYoUE45Td9sh0oTpIioqMNatW4u/uuhvu/DI4NPoZ/Mx6kOsERFwQpMDUVKx9sHeipa9t4EgkIZx54KFd1LqYbt7sTnvu6cPl2dkmd0lJrju/LKvZYFjCvhnHjmPoiSdge/xFhKK9FBCzg1pz3TzTQUVeE2Yh/U+yrIahx7ey7sYpODHKWqinzww5ijnzFBmyVvVWZhQxz9xOmGCAiHBaY0qbhpSjx+X1YaCUrNEk5iSJOM5KRskEg/o68y8VX3PAb7re5BzpGvR0ci9izZAYBoNPNKzFX+y5F2ty8mny8I0fV2zI4kskxLPD3uEjA96AJ+SLtWdaF/MCQtYlEo3OoqymqlWlzVaruY6KbXmcrn2cOoPws7+A7kfPYnTkEpWBMkNcnb6ikDK93uy+VFPmyG5ZPpjxWO6dOCnlwQcTzYOwMClqNch7YSjKmdqyDL2GgoZUxacWiaw/kOrxJP2AZDolNRJSDg26NoSBMi2gV/wadKecWdNGVlrkJ5krgWP6STK+n27TrA6g2yUpVgdxV5MiZJUuPLpmLR6s3wiXkQB/ORTKGUZPi/AGg1Mt3e1eT1/fhOe+B3fNueXzVXeVdQnYC3Lsda41he6cHHsTq+flzdgWSJlK9PcjePAQkvueQ/j5N+CAHxoFR3TLZKW0OdkUTe5emeLlQ+5s8hkHkS57J7FKMkTP8ptxWFuAgwYXdVmTXQJmfpMxPtPliTPxngEaBSBkAS6ti5AWadCpc5WEqUoeKCKFwMiMVZXKmZ126RWn60gqSqO6Ey+pO0mvnVlsnDSS6CTkh+o+5DPKLf2cmKEJAfW2bHy++RbcUbsCpbn5MJH1oTdwzBYpcslFyesfi7RcvtDtmfDFr9Id5mUI9QuiS5TX2d2FhXluc469mWXZ8rnqElxhegaDQMdl4PmXEH/ldQhnLyOFKF1+n4akbLtKbIJrJ9UySFLokO0bSXr/a+V7cUhyYj9ThmQqCZ74FqYLmsvV4mkZQaWBFGQZOCPFz0k71baSbWKpUqk4uE5VRIByBqhMKZnPitk5eyOT2VtNKzv+kq2Zp7dHoMQ1O1dUoX9yPq2yryzrs5hQk+vEPaUu3Fa3CuuysmHldXINiBs4FO5UNbe4JEkB/8RUO1EmB3tHjoSCiat0h4UB8fjhcpOTOKqc7vzyrGaNhiufn8VkWUU7ORZH+Nw5+E6fR2qgF+lwHBzdqDSNFC8PHKvsfS0qO6SyZL8JegWZIcjQEl2BZWW/QjCtQX9Mi3OiCeEUgwQjb4bOqusxFB2QbM5ODgpAurOSvOeW+p4shKE8JCmAItYGyRAnzkmrFuFSFqxOS7dspqWOr9BDyJUyN2JVGIDeQc0HU2S2sjybJO7Ie/bIDKK+J/c3cRoUZtvols0NpWUoduTCrFP1huVaxktjUr5USjw70Dd+pK/T54n6UnPqDgsCgugSgi7SWFld3FTsym+2WCx1HDefLiHbDXQGkugd2b97MgxEg3QnWhoToO6CDEqlna2aj2rFVsWNS+15tdQv8egR9cUOnzYHsbQGKeLpIwJoWq4r2xlQIJFxUzZ7p0v7iF9KXjJMVn3S3XxJgi5pjiSLjqROREyXxogjApElwbMZcXaF8nrVVsyySGEUQKibxxMIqsCWmyh7HEl9fRU+pDi7iWeQwxuQy2tgZkkG2NLXVSxEJGSjlEgk2tLdOejxXvLNqzssCAjVL1HgNNQ5y3PchcWOJqP5RnMuF2q++v38G3/IZ1y744h4khlnNnXL72dR6mIb9YE971qR6VhU9A70jtE9vyd8E577HppbmVwQEOoJL+47WJqdb93uqive6siybGZZtpJlWeKE1F5XiHxR3brQTLn295QJZI+Ecrcrz1+0Hb+otn4wT0qn03FRFKnucLmjvyUwGDqChHDmrll+h9mtX1B7eeGHBwu1Fm5DVV3ZmvwSe63RrK3hec6VTqezlsc0mqNDl2vElOssd4GNZYfAcj2v2jDi3hAEXywinB0anjzSfbHPMxXm2nMzoprzPcOCgNj3n4eyNAbBlZdvrSkoz6p1Fuat0eu1GxiGKVz2jlEvuFwdlAEIWYR8QI9Fr4C6sv3zMHQSUjqcTCS6BwfHjw72jx8aHogcfOAz2/sW8/QL9tG+fRe0CAXNOi1X6ay0N7mqi7ZaHIZtGg1bupgbLOmc5QLC7JverOsu8HDTusr7dX/5PpOiIHYGJpOXui8PdAx7R88hiRP3/N72ocWMxYKAUC+y7/H9TmuWrrGkPL+psDS32W63LP9m8dc5UxZ80PdrQGY15GYDYhZDkEUopAraQDgUbu31jrUP9I11hUbDnVYYOu/4/a3y8rkFjkUD4vHH9+vzYLIbczR1pS6nu7g8t4nXMrIH86oOX3qJ3YUaeq3vb3y839/2Xv0sV4b7rp4XCynZMjOkBbFTEJjOod7xru6uofap8URrIiUNwGoLP/AASfpY+Fg0INRLvfjEm6XZZfnbSyqyt2bnODYbDIZKpGnqg3ZGSC/qARZu3SLPuHFieX/bOzcgyKczZjF5NzM412wfGWjKDPFYvHVsNNA+3DvWNTQw1pmKGTofWCQzqG1aMiBkq0O3IT/ftqakNr/WZjPV8DrWxTCMXETxd8d0D9w4cy2qMyfT6XRnKpHuDAXCXQOdI+2+/snWqejSmOG6AUGsDiDmMll5V1lNQZWzIK/O6jDW8zxfzDCMzBS/O2gP3DhzXbMjKTNIkjQgimJrMBhpHxke6xpo93WGQtHOB37/jkXpDLPvsGSGUK0OVsMW22xMfUl5fl1ReXaV3mBw8VrOBdAqfL87bqgHFiXCyIB3plKpzlhE7BryjrR7+3ytU2PXxwzXzRDqDwlTpBByFRRYXaXVeVXOfGedwWyoZ1m2OJ1OmxmG+R1TXDcorqnkJtPpdJhhGMoMtALM8ERXf/dQ58RA6LqZ4cYBofonNEKxJcdSX1nlrMsryq7S6ngXp2FmMcVvWou/7pH5Df9wzmRDhRnQmYjFuyaGgu2d3sHWycHEkqyJ+R5sySJj9oX2/edrWRqDxpWTZ3PlF2VVFRTm1elNOqpTKIuvtAsFo37Dvf4Bvv0VomPamkilUq2JmNg+PDTU5eud7PSFJjofeOD6dIYb1iGuAoTCFKxGKM76P+1dS3PaSBBupJE06AHiZWSb7Kps1uVDqux/YN/37nuq9pbfs+fc957zJpc9myofqBQkrOM4AiwQ6MEghNgagR0/kgUDtsluDq4xMFLNNB/f9LSmv06pz7d21ncza9J2XOaLMTZWZBjmh0+xHMhFu4kgCCo9e1DtNrzyh9rpibEkZlh4yfgaUwDHFVOpeHFTz24/+2l9F0v4iimi44wL89FyLPud3eWKGajP4Lpu+fzvevW8alRMc/7dxIMtGVdO5iVTjIJCLq8+3/wps5supLZlJV5kUfiDKeZHYWs4HFbCIVuxHa9qnrbKp6eNk1ZzOT7D0peMu0zxVxq4XjEho2Lhl83tfD6zIyfwLs/zhWmZYPPb7D955VWcwff9E6dLyvV6s3pWNSrdB2CGpS8ZX2MKOYmea+vZrfSmqqXTiaKYQFEm2DiT6+bj6Ic7bPPdgiXaTTgOqXRMp2p8bJeb9daJbS0WZ5hmjQdb1aM4xcAsyqqk5TZS8vpGdieTVw4wjm8xHIdiEKPl1X9ENiff0LUfhD8agTMchmee5500m+1y45NZrZ+1KmZvUPltzgjkNCA8GEPcZgohFuI+ByilCFvZQvrwZz2/o6YkKQagAQvjeMWdoP+0ZL9Zp7f6/e4wYwgtGEKl03Yrpx+Mat2wyp2OdxIO0FLiDNMs8mAMcce3ePWnJiVhX3uWK+by2XxSSehxid9FiC3ERkCl1qLs97GBLhGyvOPo0wzxFJ/fAgPxfd8iveDMttrlhtF69/nTRdV1/AoMYOEI5KzzezRA0PMUEoDKsFBYy6V2N55penpN0SRFKDIotseyjPZ/3JZSUFCR9mEQM9yuW7LqTuX8k2mYjfb7wO+fhDHxUZjhwZeMbyGSRjZpvEJVBU1OS3JuTd3Jr2cPMBZ1jmowjXNIZ8slnRX2q9mPSvhYw+GQuC6BRv2idlE333Ya3jvX7jmkFxo0y3Dep5bzTvnRGOKmb/FRHvYYzCZFlEtJW1ohfZjJJHQhKdLK9xrCzB4DMW32wd1+OjjT08J5bTb1umk7pqgAuw/GoBeUPKdvWO02nJ9d1KyG98b2yXsOjQJ2JJP7nHSaOqgZO8xu8xlveN9uf7x6rSEB9pNJRUtv5EDTsrqiSgcch3Q0TtmbgTFWCxDUBl8BRZRjSRmBCp91O17t83nzbbPRqtktC/o+ayBPOD56eVOv4b72XLT/kwMi8i08UIciYFXFoMqqnkiLh8mkpMdVBWSF1ziOKtnADVW8ab/CRQ1z/fpFTz7RrMXhAAzbdkp2t2e4Vg/aplPrXnhvLOLUgBBgQ0xyIlypwS1z/Pe515MD4u5u5LUmCMJ+Kitpai4DqbSiS0r8QBAEnVYNpIzBsnPqaN7HMtf63hMQZDQaWWEY0swpqnoXqb55nl9rNcxS86JldE0Xeu7AQH04Pnr56w0FlzmHuLTLVg4Ql4zBiYCp5K+MQZflxKGaUXQlI0JKVTSM+T2GA+3yFOoXtnjicxdjsTsjDKBEyMCwW11om7bRtXrHdqdXI/2uFfSj8l60rANxRbBeTPQhl/aNLnijlQPE7fm8+v21JojsfjIhaGo6AWpK1kQlvhfHWGc4RuV5HtMiawgh2k4UeB8lfnHNJwjB930S+KFFNaQJ8Uqe2ze6pg2ddtfodrzjoxerxQTfws3qA+LSx2DGUjQcBhzjQE0qsq4kpH1FwVoikQRRETQscXssQhpLBUKiLPzBOP0/RuUAqCbEF2fv3/JSv4iLUGGRselCWgoykjOcaIaNRsYoDEuOSwzP9sCxekbbdo89u1cjZt9ygBYbX10m+G4B8a2BR7sTJOwLYkwTsQgcz2qCwu8JPNIQ4iPRLl5kxi2PJy0aF4FHY/2ISC97Ut+agoBuBy/BEEkgU3WhYAi+PwDfJxAOaK12AoEfwGDgG/2+X3J7gRGSAZC+b7gBHB8dPe0uYcEVY3XzX6dN7Gp3QpmDEBghBocsowpxhGVMi8BjQDKK6laM/xCg6H0EOFJ4o/+P5Z8Qra1BhcsnbQDB+DUJgARBtOgHJABn0tLXPglI3/etfhgSTJmAxyvpE0yz4+3P/wEzPwuTzGgtSAAAAABJRU5ErkJggg==" alt="">
            </div>-->
        </div>
        <div class="bri_bottom" style="width: 100%">
            <div class="bri_bottom_face fl" title="表情"></div>
            <div class="bri_bottom_look" style="display:none;width: 95%;max-width: 470px;left: 0;top: -155px;height: auto;padding-bottom: 20px;">
                <div class="bri_blook_center" style="width: auto;height: auto">
                </div>
            </div>
            <div class="bri_bottom_img fl" title="发送图片"></div>
            <input type="file" class="bri_bottom_imgs" style="display:none;" onchange="uploadImg()" accept="image/gif,image/jpeg,image/jpg,image/png"
                   id="uploadImgs" name="uploadImgs" multiple/>
            <!--<div class="bri_bottom_file fl" title="发送文件"></div>-->
            <input type="file" class="bri_bottom_files" style="display:none;" name="img" multiple/>
            <!--            <div class="bri_bottom_recharge fl" title="充值详情" onclick="sendRechrgeMsg()"></div>-->
            <div class="clear"></div>
            <textarea id="sendtxt" placeholder="请输入文字，最多不要超过100个"></textarea>
            <div class="bri_bottom_but" onclick="sendMsg()">发送</div>
            <div class="bri_bottom_empty" style="display:none;">不能发送空白信息</div>
        </div>
    </div>
    <script>
        /*$("#sendtxt").focus(function () {
            $(".bri_bottom_but").show();
            $(".bri_bottom_img").hide();
        });*/
        $("#sendtxt").bind("input propertychange",function(){
            if ($("#sendtxt").val().length > 0){
                $(".bri_bottom_but").show();
                $(".bri_bottom_img").hide();
            }else{
                $(".bri_bottom_but").hide();
                $(".bri_bottom_img").show();
            }
        });
        $("#sendtxt").focus(function(){
            $(".buju_right").height($(window).height()/2);
            $(".brce_block_box").height(($(window).height()/2) - 180);
        });
        $("#sendtxt").blur(function () {
            if ($("#sendtxt").val().length === 0) {
                $(".bri_bottom_but").hide();
                $(".bri_bottom_img").show();
                /*$(".bri_bottom_empty").css("display", "block");
                setTimeout(function () {
                    $(".bri_bottom_empty").hide();
                }, 1000);*/
            }
            $(".buju_right").height($(window).height());
            $(".brce_block_box").css('height','auto');
        });
    </script>
    <!-- 最右设置框 -->
    <#--<div class="buju_install fr" style="display:none;">
        <!-- 单聊设置 &ndash;&gt;
        <div class="bin_one">
                  <div class="bin_one_face">
                        <img  id="selfUserAvatar" src="../images/face003.jpg"/>
                   </div>
                        <div  id="selfUserName" class="bin_one_name over">XXXX</div>
              </div>
              <!-- 单聊设置 &ndash;&gt;
                <script>
                    $(function (){
                        var iw = $(".bin_one_face").width();
                        var ih = $(".bin_one_face").height();
                        var ib = iw/ih;
                        $(".bin_one_face img").each(function (){
                            var isw=$(this).width();
                            var ish=$(this).height();
                            var isb=isw/ish;
                            if(ib<isb){
                                $(this).css("height",ih);
                                var iwi=ih * isb;
                                var ws= 0 - (iwi - iw) / 2;
                                $(this).css("margin-left",ws);
                            }else{
                                $(this).css("width","100%");
                                var ihe=iw / isb;
                                var hs=0 - (ihe - ih) / 2;
                                $(this).css("margin-top",hs);
                            }
                        })
                    })
                </script>
                <div class="bin_five">
                    <p>信博娱乐</p>
                    <div  class="bin_five_note over3">第一包网平台</div>
                    <div  class="clear"></div>
                    <p>信博棋牌</p>
                    <div  class="bin_five_note over3">最好玩的棋牌游戏平台</div>
                    <div  class="clear"></div>
                </div>
                <a href=""><div class="bin_six">退出</div></a>
                <script>
                    $(function (){
                        var iw = $(".bin_nd_face").width();
                        var ih = $(".bin_nd_face").height();
                        var ib = iw/ih;
                        $(".bin_nd_face img").each(function (){
                            var isw=$(this).width();
                            var ish=$(this).height();
                            var isb=isw/ish;
                            if(ib<isb){
                                $(this).css("height",ih);
                                var iwi=ih * isb;
                                var ws= 0 - (iwi - iw) / 2;
                                $(this).css("margin-left",ws);
                            }else{
                                $(this).css("width","100%");
                                var ihe=iw / isb;
                                var hs=0 - (ihe - ih) / 2;
                                $(this).css("margin-top",hs);
                            }
                        })
                    })
                </script>
            </div>
        </div>
    </div>-->
    <script>
        function friface() {
            var image_width = $(".ba_lfb_face").width();
            var image_height = $(".ba_lfb_face").height();
            var image_bi = image_width / image_height;
            $(".ba_lfb_face img").each(function () {
                images_width = $(this).width();
                var images_height = $(this).height();
                var images_bi = images_width / images_height;
                if (image_bi < images_bi) {
                    $(this).css("height", image_height);
                    var imgwidth = image_height * images_bi;
                    var widths = 0 - (imgwidth - image_width) / 2;
                    $(this).css("margin-left", widths);
                } else {
                    $(this).css("width", "100%");
                    var imgheight = image_width / images_bi;
                    var heights = 0 - (imgheight - image_height) / 2;
                    $(this).css("margin-top", heights);
                }
            })
        }
    </script>
</div>
</div>
</body>
<script>
    var userName;
    var passWord;
    var orgId;
    var index;
    var token;
    var selfUserId;
    var selfUserName;
    var selfAvatar;
    var toUserId;
    var toUserName;
    var toAvatar;
    var toOrgId;
    var socket;
    var webSocketIsOpen = false;
    var webSocketPingTimer = null;
    var webSocketPongTimer = null;
    var webSocketPingTime = 14000; // 心跳的间隔，当前为 10秒,
    var webSocketPongTime = 10000;
    var lastSendTime;
    var websocketurl = "ws://" + window.location.hostname + ":11112/ws";
    var httpurl = "http://127.0.0.1:8996/pay-front";
    var reconnectflag = false;//避免重复连接
    var webSocketReconnectMaxCount = 100;
    var payMethods = [];
    //加载完成后执行
    $(function () {
        //获取登录的用户名密码
        orgId = "${orgId?c}";
        userName = "${userName}";
        passWord = "${passWord}";

        toUserId = "${toUserId?c}";
        toUserName = "${toUserName}";
        toAvatar = "${toAvatar}";
        toUserNickName = "${toUserNickName}";
        toOrgId = "${toOrgId?c}";

        websocketurl = "${websocketurl}";
        httpurl = "${httpurl}";

        <#list payMethods as p>
        payMethods.push(${p});
        </#list>
        webSocetInit();
        showPayMethods();
    });

    //初始化webSocet连接
    function webSocetInit() {
        //先连接到
        initWebSocket(websocketurl, '连接服务器中....', initEventHandle);
    }

    //初始化连接
    function initWebSocket(url, typeStr, callbak) {
        try {
            index = layer.msg(typeStr, {
                icon: 16,
                style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
                shade: 0.01, time: false
            });
            if (!window.WebSocket) {
                window.WebSocket = window.MozWebSocket;
            }
            if (window.WebSocket) {
                socket = new WebSocket(url);
                socket.binaryType = "arraybuffer";
                callbak();
            } else {
                alert("您当前的浏览器不支持websocket!!!");
            }
        } catch (e) {
            reconnect(url, callbak);
        }
    }

    //重新连接
    function reconnect(url, callbak) {
        if (reconnectflag) return;
        reconnectflag = true;
        //没连接上会一直重连，设置延迟避免请求过多
        setTimeout(function () {
            initWebSocket(url, '重新连接到服务器...', callbak);
            reconnectflag = false;
        }, 5000);
    }

    //重连
    function webSocketReconnect() {
        if (webSocketIsOpen) {
            return false
        }
        this.webSocketReconnectCount += 1
        // 判断是否到了最大重连次数
        if (
            this.webSocketReconnectCount >= this.webSocketReconnectMaxCount
        ) {
            this.webSocketWarningText = '重连次数超限'
            return false
        }
    }

    var initEventHandle = function () {
        //当连接上了以后
        socket.onopen = function (event) {
            webSocketIsOpen = true;
            console.log(webSocketIsOpen);
            layer.close(index);
            index = layer.msg('登录服务器中...', {
                icon: 16,
                style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
                shade: 0.01, time: false
            });
            //发送登录信息
            doLogin(orgId, userName, passWord);
            //webSocketPing();
            heartCheck();
        };
        //收到消息后
        socket.onmessage = function (event) {
            heartCheck();
            if (event.data instanceof ArrayBuffer) {
                //如果后端发送的是二进制帧（protobuf）会收到前面定义的类型
                var message = proto.im.Message.deserializeBinary(event.data);
                //判断是什么类型
                var cmd = message.getCommand() || 0
                switch (cmd) {
                    case 4: //登录响应
                        layer.close(index);
                        var loginres = message.getLoginres();
                        loginSuccess(loginres);
                        break
                    case 5: //聊天请求
                        console.log("获取到聊天收到信息");
                        var messagereq = message.getMessagereq();
                        var msgType = messagereq.getMsgtype();

                        var sendText = messagereq.getContent().getText();
                        var duration = messagereq.getContent().getDuration();
                        var receiveUserAvatar = messagereq.getFromuser().getAvatar();

                        var receiveNickName = messagereq.getFromuser().getNickname();
                        var sendTime = dateFormat(messagereq.getSendtime(), 'Y-m-d H:i:s');
                        showAcceptMsg(msgType, sendText, receiveUserAvatar, receiveNickName, sendTime, duration);
                        break
                    case 6: //聊天响应
                        console.log("获取到聊天响应信息");
                        var messageres = message.getMessageres();
                        sendMsgSuccess(messageres);
                        break
                    case 11:
                        console.log("强制下线");
                        reconnectflag = true;
                        socket.close();
                        heartReset();
                        layer.alert('您已在别处登录，被迫下线!!!', {
                            skin: 'layui-layer-lan'
                            , closeBtn: 0,
                            anim: 2
                        });
                        break
                }
            }
        };
        //连接关闭
        socket.onclose = function (event) {
            webSocketIsOpen = false;
            /*layer.confirm('您已下线，重新上线?', function (index) {
                reconnect(websocketurl, initEventHandle);
                layer.close(index);
            });*/
            reconnect(websocketurl, initEventHandle);
        };
        //报错以后
        socket.onerror = function () {
            webSocketIsOpen = false;
            layer.msg("服务器连接出错");
            reconnect(websocketurl, initEventHandle);
        };
    }

    //处理登录
    function doLogin(orgId, userName, passWord) {

        if (!orgId) {
            return;
        }

        var message = new proto.im.Message();
        message.setCommand(3);
        message.setVersion(1);
        //拼装登录信息包
        var loginreq = new proto.im.LoginReq();
        loginreq.setDeviceid("123456");
        loginreq.setDevicetype(1);

        loginreq.setOrgid(orgId);
        loginreq.setUsername(userName);
        loginreq.setPassword(passWord);

        message.setLoginreq(loginreq);
        socket.send(message.serializeBinary());
    }

    function sendMsgSuccess(messageres) {

    }

    //登录成功
    function loginSuccess(loginres) {
        if (loginres.getToken() != null && loginres.getToken() != '') {
            token = loginres.getToken();
            selfUserId = loginres.getUser().getUserid();
            selfUserName = loginres.getUser().getNickname();
            selfAvatar = loginres.getUser().getAvatar();
            $("#toUserAvatar").attr("src", toAvatar);
            $("#selfUserAvatar").attr("src", selfAvatar);
            $("#selfUserName").text(selfUserName);
            //加载离线信息
            var data = {
                "token": token,
                "userId": toUserId,
                "page": {
                    "pageSize": 1000,
                    "pageIndex": 0,
                    "timeOrder": 1
                }
            };
            getUserOffLineMsg(data)
        }
    }

    //发送心跳
    function heartStart() {
        webSocketPingTimer = setTimeout(function () {
            if (!webSocketIsOpen) {
                return false
            }
            console.log('发送心跳')
            var message = new proto.im.Message();
            message.setCommand(7);
            message.setVersion(1);
            this.socket.send(message.serializeBinary())
            // clearTimeout(this.webSocketPingTimer)
            // 重新执行
            // this.webSocketPing()
            this.webSocketPongTimer = setTimeout(() => {
                socket.close();
            }, this.webSocketPongTime);
        }, this.webSocketPingTime);
    }

    function heartReset() {
        clearTimeout(this.webSocketPingTimer);
        clearTimeout(this.webSocketPongTimer);
    }

    function heartCheck() {
        heartReset();
        heartStart();
    }

    //获取用户离线消息
    function getUserOffLineMsg(data) {
        index = layer.msg('获取聊天历史记录...', {
            icon: 16,
            style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
            shade: 0.01, time: false
        });
        $.ajax({
            type: "post",
            url: httpurl + "/chatApi/getUserChatHis",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            async: false,
            crossDomain: true,
            dataType: "json",
            success: function (data) {
                addHisChatData(data)
            }
        });
    }

    function showPayMethods() {
        if (payMethods.length == 0) {
            $(".cardType").hide();
            $(".brce_block_box").height($(window).height() - 145);
        }
        payMethods.forEach(e => {
            if (e === 1 || e === 4) {
                $("#pmwx").show();
            }
            if (e === 2 || e === 5) {
                $("#pmali").show();
            }
            if (e === 3) {
                $("#pmcard").show();
            }
        });
    }

    function openURL(type) {
        var url = httpurl + "/chatApi/showPay?orgId=" + toOrgId + "&type=" + type;
        window.open(url)
    }

    //添加一条消息信息
    function showAcceptMsg(msgType, sendText, receiveUserAvatar, receiveNickName, sendTime, duration) {
        var html = ''
        //alert("duration >>>>>>>>> " + duration);
        if (sendTime != null && sendTime != '' && sendTime.length == 19) {
            if (sendTime.substr(0, 16) != lastSendTime) {
                lastSendTime = sendTime.substr(0, 16);
                html += '<div class="brce_time">' + lastSendTime + '</div>'
            }
        }

        //判断是图片还是文本
        if (msgType != null && msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
            //判断是否显示
            html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + receiveUserAvatar + '"/> </div>'
            html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_jiao fl"></div>'
            html += '<div class="brce_lt_block fl"> <div>' + sendText + '</div></div></div> <div class="clear"></div> </div>'
        } else if (msgType == 7) {
            //判断是否显示
            //html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + receiveUserAvatar + '"/> </div>'
            //html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_jiao fl"></div>'
            //html += '<div class="brce_lt_block fl"> <div onclick="openURL()"><img src="' + sendText + '" style="width: 100%; height:
            //100%"/></div></div></div> ' +
            //    '<div class="clear"></div></div>'
            html += getPayCardHtml(duration, receiveUserAvatar, receiveNickName);
        } else {
            html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + receiveUserAvatar + '"/> </div>'
            html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_jiao fl"></div>'
            html += '<div class="brce_lt_block fl"> <div><img src="' + sendText + '" style="width: 100%; height: 100%"/></div></div></div> <div class="clear"></div> </div>'
        }

        $('.brce_block_box').append(html)
        var scrollHeights = $('.brce_block_box').prop("scrollHeight");
        $('.brce_block_box').scrollTop(scrollHeights, 0);
    }

    function getPayCardHtml(_type, receiveUserAvatar, receiveNickName) {
        var _html = '';
        switch (_type) {
            case 1:
            case 4:
                _html += '<div class="brce_left"><div class="brce_left_face fl"><img src="' + receiveUserAvatar + '"/> </div>';
                _html += '<div class="brce_left_talk fl"><div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_block fl wxpayBack chatCard">';
                _html += '<div class="cardImg"><img src="../img/wx.png"></div>';
                _html += '<div class="cardFont"><span>微信充值</span><br><span>支付完成后请发送截图进行核实，代理将为您进行充值</span></div>';
                _html += '<div class="cardClick" onclick="openURL(' + _type + ')">点击开始充值</div></div></div><div class="clear"></div></div>';
                break;
            case 2:
            case 5:
                _html += '<div class="brce_left"><div class="brce_left_face fl"><img src="' + receiveUserAvatar + '"/> </div>';
                _html += '<div class="brce_left_talk fl"><div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_block fl alipayBack chatCard">';
                _html += '<div class="cardImg"><img src="../img/alipay.png"></div>';
                _html += '<div class="cardFont"><span>支付宝充值</span><br><span>支付完成后请发送截图进行核实，代理将为您进行充值</span></div>';
                _html += '<div class="cardClick" onclick="openURL(' + _type + ')">点击开始充值</div></div></div><div class="clear"></div></div>';
                break;
            default:
                _html += '<div class="brce_left"><div class="brce_left_face fl"><img src="' + receiveUserAvatar + '"/> </div>';
                _html += '<div class="brce_left_talk fl"><div class="brce_lt_name">' + receiveNickName + '</div><div class="brce_lt_block fl uninoPayBack chatCard">';
                _html += '<div class="cardImg"><img src="../img/uninoPay.png"></div>';
                _html += '<div class="cardFont"><span>银行卡充值</span><br><span>支付完成后请发送截图进行核实，代理将为您进行充值</span></div>';
                _html += '<div class="cardClick" onclick="openURL(' + _type + ')">点击开始充值</div></div></div><div class="clear"></div></div>';
        }
        return _html;
    }

    //批量添加消息
    function addHisChatData(data) {
        if (data != null && data) {
            console.log(data)
            var html = ''

            layer.close(index);
            for (let i in data.data.list) {
                var sendUserAvatar = data.data.list[i].sendUserAvatar;
                var sendNickName = data.data.list[i].sendNickName;
                var msgType = data.data.list[i].messageType;
                const contont = JSON.parse(data.data.list[i].content);
                var sendText = contont.text;
                var duration = contont.duration;
                var receiveUserAvatar = data.data.list[i].receiveUserAvatar;
                var receiveNickName = data.data.list[i].receiveNickName;
                if (data.data.list[i].sendTime != null && data.data.list[i].sendTime != '' && data.data.list[i].sendTime.length == 19 && i == 0) {
                    lastSendTime = data.data.list[i].sendTime.substr(0, 16);
                }

                if (data.data.list[i].sendTime != null && data.data.list[i].sendTime != '' && data.data.list[i].sendTime.length == 19) {
                    if (data.data.list[i].sendTime.substr(0, 16) != lastSendTime) {
                        lastSendTime = data.data.list[i].sendTime.substr(0, 16);
                        html += '<div class="brce_time">' + lastSendTime + '</div>'
                    }
                }

                if (selfUserId == data.data.list[i].sendUserId) {
                    //判断是图片还是文本
                    if (msgType != null && msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
                        //判断是否显示
                        html += '<div class="brce_right"><div class="brce_left_face fr"> <img src="' + sendUserAvatar + '"/> </div>'
                        html += '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div>' + sendText + '</div> </div>'
                        html += '</div> <div class="clear"></div></div>'
                    } else if (msgType == 7) {
                        html += '<div class="brce_right"><div class="brce_left_face fr"> <img src="' + sendUserAvatar + '"/> </div>'
                        html += '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div></div>'
                        html += ' <div class="brce_rt_block fr"><img src="' + sendText + '" style="width: 100%; height: 100%"/></div></div>'
                        html += '</div> <div class="clear"></div></div>'
                    } else {
                        //判断是否显示
                        html += '<div class="brce_right"><div class="brce_left_face fr"> <img src="' + sendUserAvatar + '"/> </div>'
                        html += '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div></div>'
                        html += ' <div class="brce_rt_block fr"><img src="' + sendText + '" style="width: 100%; height: 100%"/></div></div>'
                        html += '</div> <div class="clear"></div></div>'
                    }
                } else {
                    //判断是图片还是文本
                    if (msgType != null && msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
                        //判断是否显示
                        html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + sendUserAvatar + '"/> </div>'
                        html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + sendNickName + '</div><div class="brce_lt_jiao fl"></div>'
                        html += '<div class="brce_lt_block fl"> <div>' + sendText + '</div></div></div> <div class="clear"></div> </div>'
                    } else if (msgType == 7) {
                        /*html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + sendUserAvatar + '"/> </div>'
                        html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + sendNickName + '</div><div class="brce_lt_jiao fl"></div>'
                        html += '<div class="brce_lt_block fl"> <div onclick="openURL()"><img src="' + sendText + '" style="width: 100%; height: 100%"/></div></div></div> <div ' +
                            'class="clear"></div> </div>'*/
                        html += getPayCardHtml(duration, sendUserAvatar, sendNickName);
                    } else {
                        //判断是否显示
                        html += '<div class="brce_left"> <div class="brce_left_face fl"> <img src="' + sendUserAvatar + '"/> </div>'
                        html += '<div class="brce_left_talk fl"> <div class="brce_lt_name">' + sendNickName + '</div><div class="brce_lt_jiao fl"></div>'
                        html += '<div class="brce_lt_block fl"> <div><img src="' + sendText + '" style="width: 100%; height: 100%"/></div></div></div> <div class="clear"></div> </div>'
                    }
                }
            }
            $('.brce_block_box').append(html)
            var scrollHeights = $('.brce_block_box').prop("scrollHeight");
            $('.brce_block_box').scrollTop(scrollHeights, 0);
            img();
        }
    }

    //发送消息
    function sendMsg() {
        //判断是什么消息
        var textArea = $('#sendtxt').val();
        sendTxtMsg(textArea);
        $(".bri_bottom_but").hide();
        $(".bri_bottom_img").show();

    }

    function showPayType(type, d) {
        $(".cardType .payImage").css('border', '3px solid #c0b1da');
        $(d).css('border', '3px solid #01AAED').css('border-radius', '50%');
        $(".payTypeModule").css('display', 'none');
        var html = '';
        if (type === 'wx') {
            $(".payTypeModule").css('display', 'flex');
            //$(".payTypeModule").html('<div>微信红包</div><div>微信转账</div>');
            if (payMethods.indexOf(1) != -1) {
                html += '<div onclick="sendRechrgeReq(1)" class="payType">微信扫码</div>'
            }
            if (payMethods.indexOf(4) != -1) {
                html += '<div onclick="sendRechrgeReq(4)" class="payType">微信转账</div>'
            }
            $(".payTypeModule").html(html);
        } else if (type === 'ali') {
            $(".payTypeModule").css('display', 'flex');
            //$(".payTypeModule").html('<div>支付宝红包</div><div>支付宝转账</div>');
            if (payMethods.indexOf(2) != -1) {
                html += '<div onclick="sendRechrgeReq(2)" class="payType">支付宝扫码</div>'
            }
            if (payMethods.indexOf(5) != -1) {
                html += '<div onclick="sendRechrgeReq(5)" class="payType">支付宝转账</div>'
            }
            $(".payTypeModule").html(html);
        } else {
            sendRechrgeReq(3);
        }
    }

    function sendRechrgeReq(d) {
        /*$(".payTypeModule").css('display', 'none')
        $(d).css('border', '3px solid #c0b1da')*/
        var text = "云商入款信息" + d;
        switch (d) {
            case 1:
                text = '微信扫码入款信息';
                break;
            case 2:
                text = '支付宝扫码入款信息';
                break;
            case 3:
                text = '银行转账入款信息';
                break;
            case 4:
                text = '微信转账入款信息';
                break;
            case 5:
                text = '支付宝转账入款信息';
                break;
            default:
                text = "云商入款信息" + d;
        }
        var message = new proto.im.Message();
        message.setCommand(5);
        message.setVersion(1);
        //拼装消息包
        var messagereq = new proto.im.MessageReq();
        //拼装内容信息包
        var content = new proto.im.Content();
        content.setText(text);
        content.setDuration(d);
        //消息ID
        var nowTime = new Date();
        var messageId = nowTime.getTime() + toUserId;
        var sendTime = nowTime.getTime();
        //发送用户
        var fromUser = new proto.im.User();
        fromUser.setAvatar(selfAvatar);
        fromUser.setNickname(selfUserName);
        fromUser.setUserid(selfUserId);
        fromUser.setOrgid(orgId);
        //接受用户
        var toUser = new proto.im.User();
        toUser.setUserid(toUserId);
        toUser.setNickname(toUserName);
        toUser.setAvatar(toAvatar);
        //瓶装消息包
        messagereq.setContent(content);
        messagereq.setFromuser(fromUser);
        messagereq.setTouser(toUser);
        messagereq.setMessageid(messageId);

        messagereq.setSendtime(sendTime);
        messagereq.setMsgtype(6);
        message.setMessagereq(messagereq);
        socket.send(message.serializeBinary());
        $(".payTypeModule").css('display', 'none');
        //添加记录
        showSendMsg(text, 0, dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
    }

    //发送充值信息
    function sendRechrgeMsg() {
        var text = "云商入款信息";
        var message = new proto.im.Message();
        message.setCommand(5);
        message.setVersion(1);
        //拼装消息包
        var messagereq = new proto.im.MessageReq();
        //拼装内容信息包
        var content = new proto.im.Content();
        content.setText(text);
        //消息ID
        var nowTime = new Date();
        var messageId = nowTime.getTime() + toUserId;
        var sendTime = nowTime.getTime();
        //发送用户
        var fromUser = new proto.im.User();
        fromUser.setAvatar(selfAvatar);
        fromUser.setNickname(selfUserName);
        fromUser.setUserid(selfUserId);
        fromUser.setOrgid(orgId);
        //接受用户
        var toUser = new proto.im.User();
        toUser.setUserid(toUserId);
        toUser.setNickname(toUserName);
        toUser.setAvatar(toAvatar);
        //瓶装消息包
        messagereq.setContent(content);
        messagereq.setFromuser(fromUser);
        messagereq.setTouser(toUser);
        messagereq.setMessageid(messageId);

        messagereq.setSendtime(sendTime);
        messagereq.setMsgtype(6);
        message.setMessagereq(messagereq);
        socket.send(message.serializeBinary());

        //添加记录
        showSendMsg(text, 0, dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
    }

    //显示发送信息
    function showSendMsg(sendText, msgType, sendTime) {
        var html;
        if (sendTime != null && sendTime != '' && sendTime.length == 19) {
            if (sendTime.substr(0, 16) != lastSendTime) {
                lastSendTime = sendTime.substr(0, 16);
                html = '<div class="brce_time">' + lastSendTime + '</div>'
            }
        }
        //发送文本
        if (msgType == 0 || msgType == 6 || msgType == '0' || msgType == '6') {
            var apps_one = "<div class='brce_right'><div class='brce_left_face fr'><img src='" + selfAvatar + "'/></div>" +
                "<div class='brce_left_talk fr'><div class='brce_rt_jiao fr'></div><div class='brce_rt_block fr'><div>";
            var apps_two = "</div></div></div><div class='clear'></div></div>";

            if (sendText == "") {
                $(".bri_bottom_empty").css("display", "block");
                setTimeout(function () {
                    $(".bri_bottom_empty").hide();
                }, 2000)
            } else {
                if (html != null && html != '') {
                    $(".bri_center .brce_block_box").append(html + apps_one + sendText + apps_two);
                } else {
                    $(".bri_center .brce_block_box").append(apps_one + sendText + apps_two);
                }
                img();
                $(".bri_bottom textarea").val("");
                var scrollHeight = $('.brce_block_box').prop("scrollHeight");
                $('.brce_block_box').scrollTop(scrollHeight, 0);
            }
        } else {
            //发送图片
            var apps_one = '<div class="brce_right"><div class="brce_left_face fr"> <img src="' + selfAvatar + '"/> </div>' +
                '<div class="brce_left_talk fr"> <div class="brce_rt_jiao fr"></div>' + ' <div class="brce_rt_block fr"> <div></div>' +
                ' <div class="brce_rt_block fr"><img src="' + sendText + '"/></div></div>';
            var apps_two = '</div> <div class="clear"></div></div>'
            if (html != null && html != '') {
                $(".bri_center .brce_block_box").append(html + apps_one + apps_two);
            } else {
                $(".bri_center .brce_block_box").append(apps_one + apps_two);
            }

            img();
            var scrollHeight = $('.brce_block_box').prop("scrollHeight");
            $('.brce_block_box').scrollTop(scrollHeight, 0);
        }
    }

    //发送文本消息
    function sendTxtMsg(text) {
        var message = new proto.im.Message();
        message.setCommand(5);
        message.setVersion(1);
        //拼装消息包
        var messagereq = new proto.im.MessageReq();
        //拼装内容信息包
        var content = new proto.im.Content();
        content.setText(text);
        //消息ID
        var nowTime = new Date();
        var messageId = nowTime.getTime() + toUserId;
        var sendTime = nowTime.getTime();
        //发送用户
        var fromUser = new proto.im.User();
        fromUser.setAvatar(selfAvatar);
        fromUser.setNickname(selfUserName);
        fromUser.setUserid(selfUserId);
        fromUser.setOrgid(orgId);
        //接受用户
        var toUser = new proto.im.User();
        toUser.setAvatar(toAvatar);
        toUser.setUserid(toUserId);
        toUser.setNickname(toUserName);
        //瓶装消息包
        messagereq.setContent(content);
        messagereq.setFromuser(fromUser);
        messagereq.setTouser(toUser);
        messagereq.setMessageid(messageId);

        messagereq.setSendtime(sendTime);
        messagereq.setMsgtype(0);
        message.setMessagereq(messagereq);
        socket.send(message.serializeBinary());

        //添加记录
        showSendMsg(text, 0, dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
    }

    //发送图片消息
    function sendImgMsg(imgUrl, width, height) {
        var message = new proto.im.Message();
        message.setCommand(5);
        message.setVersion(1);
        //拼装消息包
        var messagereq = new proto.im.MessageReq();
        //拼装内容信息包
        var content = new proto.im.Content();
        content.setText(imgUrl);
        content.setWidth(width);
        content.setHigth(height);
        //消息ID
        var nowTime = new Date();
        var messageId = nowTime.getTime() + toUserId;
        var sendTime = nowTime.getTime();
        //发送用户
        var fromUser = new proto.im.User();
        fromUser.setAvatar(selfAvatar);
        fromUser.setNickname(selfUserName);
        fromUser.setUserid(selfUserId);
        fromUser.setOrgid(orgId);
        //接受用户
        var toUser = new proto.im.User();
        toUser.setUserid(toUserId);
        toUser.setNickname(toUserName);
        toUser.setAvatar(toAvatar);
        //瓶装消息包
        messagereq.setContent(content);
        messagereq.setFromuser(fromUser);
        messagereq.setTouser(toUser);
        messagereq.setMessageid(messageId);

        messagereq.setSendtime(sendTime);
        messagereq.setMsgtype(1);
        message.setMessagereq(messagereq);
        socket.send(message.serializeBinary());

        //添加记录
        showSendMsg(imgUrl, 1, dateFormat(nowTime.getTime(), 'Y-m-d H:i:s'));
    }

    //上传图片
    function uploadImg() {
        index = layer.msg('图片上传中...', {
            icon: 16,
            style: 'background: rgba(216,100,125,0.9); color:#000000; border:none;',
            shade: 0.01, time: false
        });

        var data = new FormData();
        var file = $("#uploadImgs")[0].files[0];
        if (file != null) {
            data.append("im_img", file);
            $.ajax({
                type: "post",
                url: httpurl + "/chatApi/imgUpload",
                data: data,
                contentType: false,
                crossDomain: true,
                //默认文件类型application/x-www-form-urlencoded  设置之后multipart/form-data
                processData: false,
                // 默认情况下会对发送的数据转化为对象 不需要转化的信息
                success: function (data) {
                    var url = data.url;
                    sendImgMsg(url, 100, 100)
                    layer.close(index);
                }
            });
        }
    }
</script>
</html>
