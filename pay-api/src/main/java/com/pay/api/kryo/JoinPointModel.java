package com.pay.api.kryo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import com.pay.common.core.annotation.Log;
import lombok.Data;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class JoinPointModel implements Serializable {

    private String methodName;

    private List<Object> argValues = new ArrayList<>();

    private List<String> argNames = new ArrayList<>();

    private String methodValue;

    private String name;

    public JoinPointModel(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        this.methodValue = signature.getMethod().getAnnotation(Log.class).value();
        this.name = signature.getName();

        this.methodName = joinPoint.getTarget().getClass().getName()+"."+signature.getName()+"()";
        if (ArrayUtil.isNotEmpty(joinPoint.getArgs())) {
            this.argValues.toArray(joinPoint.getArgs());
        }

        //参数名称
        if (ArrayUtil.isNotEmpty(((MethodSignature)joinPoint.getSignature()).getParameterNames())) {
            this.argNames.toArray(((MethodSignature)joinPoint.getSignature()).getParameterNames());
        }
    }
}
