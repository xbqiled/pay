package com.pay.api.kryo;




import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.security.entity.OnlineUser;
import com.pay.common.core.utils.PageUtils;
import org.apache.dubbo.common.serialize.support.SerializationOptimizer;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SerializationOptimizerImpl implements SerializationOptimizer {
    public Collection<Class> getSerializableClasses() {
        List<Class> classes = new LinkedList<Class>();
        classes.add(PageUtils.class);
        classes.add(Page.class);
        classes.add(JoinPointModel.class);
        classes.add(OnlineUser.class);
        return classes;
    }
}