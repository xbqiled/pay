package com.pay.api.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class BankPayConfig extends PayConfig {

    private String bankAccount;
    private String bankPayee;
    private String ascriptionBank;
    private String openAccountBank;

}
