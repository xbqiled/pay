package com.pay.api.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class AliPayConfig extends PayConfig {

    private String aliPayAccount;
    private String aliPayNickName;
    private String aliPayCodeUrl;

}
