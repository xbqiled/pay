package com.pay.api.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddPointResult implements Serializable {

    private Integer code;
    private String msg;
    private Result data;

    @Data
    public class Result implements Serializable {
        private String channelId;
        private String userId;
        private String amount;
        private Integer status;
    }
}
