package com.pay.api.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class WxPayConfig extends PayConfig {

    private String wxAccount;
    private String wxNickName;
    private String wxCodeUrl;

}
