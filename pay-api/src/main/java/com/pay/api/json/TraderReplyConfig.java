package com.pay.api.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class TraderReplyConfig implements Serializable {

    private Integer type;
    private String payName;
    private String wxAccount;
    private String wxNickName;
    private String wxCodeUrl;

    private String bankAccount;
    private String bankPayee;
    private String ascriptionBank;
    private String openAccountBank;

    private String aliPayAccount;
    private String aliPayNickName;
    private String aliPayCodeUrl;
}
