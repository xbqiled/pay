package com.pay.api.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class PayConfig implements Serializable {

    private Integer type;

}
