package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
@Data
@TableName("sys_visits")
public class SysVisitsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;
	/**
	 *
	 */
	private Date createTime;
	/**
	 *
	 */
	private String date;
	/**
	 *
	 */
	private Long ipCounts;
	/**
	 *
	 */
	private Long pvCounts;
	/**
	 *
	 */
	private String weekDay;

}
