package com.pay.api.modules.tools.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-14 12:19:09
 */
@Data
@TableName("oss_sts_token")
public class OssStsTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Long configId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 
	 */
	private String securityToken;
	/**
	 * 
	 */
	private String accessKeySecret;
	/**
	 * 
	 */
	private String accessKeyId;
	/**
	 * 到期时间
	 */
	private Date expirationTime;


	private String domain;

	private String bucketName;
}
