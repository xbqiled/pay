package com.pay.api.modules.im.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;


/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImOrgSpecialDTO implements Serializable {

    // 主键
    private Integer id;

    // 组织机构编码
    private Long orgId;

    // 特殊字符
    private String specialStr;

    // 0，发言
    private Integer type;

    // 状态 0 正常  1 失效
    private Integer status;

    // 创建时间
    private Timestamp createTime;

    // 创建人
    private String createBy;

    // 修改时间
    private Timestamp modifyTime;

    // 修改人
    private String modifyBy;

    // 备注
    private String note;
}