package com.pay.api.modules.trader.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ReplyConfigDto implements Serializable {

    private Long id;
    private Integer type;
    private Long orgId;

    private String config;
    private Date createTime;
    private String createBy;

    private Date modifyTime;
    private String modifyBy;
    private String note;

    private String wxAccount;
    private String wxNickName;
    private String wxCodeUrl;

    private String aliPayAccount;
    private String aliPayNickName;
    private String aliPayCodeUrl;

    private String bankAccount;
    private String bankPayee;
    private String ascriptionBank;
    private String openAccountBank;
}
