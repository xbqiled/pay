package com.pay.api.modules.trader.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.trader.criteria.OrgAccountQueryCriteria;
import com.pay.api.modules.trader.entity.TraderOrgAccountEntity;
import com.pay.api.modules.trader.entity.TraderRechargeRecordEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
public interface TraderOrgAccountService extends IService<TraderOrgAccountEntity> {


    Map<String,Object> addPoint(Map<String,Object> map);

    Map<String,Object> recharge(TraderRechargeRecordEntity traderRechargeRecord);

    PageUtils queryOrgAccountPage(OrgAccountQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(OrgAccountQueryCriteria criteria);

    TraderOrgAccountEntity saveEntity(TraderOrgAccountEntity traderOrgAccount);

    boolean updateEntity(TraderOrgAccountEntity traderOrgAccount);

    Integer deleteById(Long id);

    Integer deleteByOrgId(Long orgId);

}

