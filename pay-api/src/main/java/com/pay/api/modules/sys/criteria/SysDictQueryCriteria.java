package com.pay.api.modules.sys.criteria;

import com.pay.common.core.annotation.Query;
import lombok.Data;

import java.io.Serializable;

/**
* @author Lion
* @date 2019-04-10
*/
@Data
public class SysDictQueryCriteria implements Serializable {

    @Query(blurry = "name,remark")
    private String blurry;

}