package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysOrgChannelQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgChannelEntity;
import com.pay.api.modules.sys.vo.SysOrgChannelVo;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-04-12 18:38:50
 */
public interface SysOrgChannelService extends IService<SysOrgChannelEntity> {

    PageUtils queryOrgChannelPage(SysOrgChannelQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(SysOrgChannelQueryCriteria criteria);

    List<SysOrgChannelVo> getConfigByOrgId(Long orgId);

    Map<String, Object> getAllConfigOrgId(Long orgId);

    Boolean doConfig(String [] channeId, Long orgId);

    Integer delByOrgId(Long orgId);

    Boolean delById(Long id);
}

