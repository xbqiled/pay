package com.pay.api.modules.quartz.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.api.modules.quartz.entity.QuartzLogEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface QuartzJobService extends IService<QuartzJobEntity> {

    List<Map<String, Object>> download(JobQueryCriteria criteria);


    PageUtils queryQuartzJobPage(JobQueryCriteria criteria, Pageable pageable);


    List<QuartzJobEntity> queryAllJob(JobQueryCriteria criteria);


    /**
     * create
     * @param resources
     * @return
     */
    QuartzJobEntity saveEntity(QuartzJobEntity resources);

    /**
     * update
     * @param resources
     * @return
     */
    void updateEntity(QuartzJobEntity resources);

    /**
     * del
     * @param quartzJob
     */
    void deleteEntity(QuartzJobEntity quartzJob);

    /**
     * findById
     * @param id
     * @return
     */
    QuartzJobEntity findById(Long id);

    /**
     * 更改定时任务状态
     * @param quartzJob
     */
    void updateIsPause(QuartzJobEntity quartzJob);

    /**
     * 立即执行定时任务
     * @param quartzJob
     */
    void execution(QuartzJobEntity quartzJob);
}

