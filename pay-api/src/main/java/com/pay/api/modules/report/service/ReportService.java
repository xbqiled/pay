package com.pay.api.modules.report.service;

import com.pay.api.modules.report.criteria.ReportQueryCriteria;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ReportService {

    PageUtils queryReportPage(ReportQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(ReportQueryCriteria criteria);

    PageUtils queryChannelReportPage(ReportQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> channelDownload(ReportQueryCriteria criteria);


}

