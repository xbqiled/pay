package com.pay.api.modules.im.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-31 18:17:27
 */
@Data
@TableName("im_config")
public class ImConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 组织机构编码
	 */
	private Long orgId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 好友请求过期时间：单位(小时)
	 */
	private Integer friendReqExp;
	/**
	 * 1生效 2失效
	 */
	private Integer status;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 修改时间
	 */
	private Date modifyTime;
	/**
	 * 修改人
	 */
	private String modifyBy;

}
