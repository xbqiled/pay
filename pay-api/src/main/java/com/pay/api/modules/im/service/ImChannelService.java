package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImChannelQueryCriteria;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface ImChannelService extends IService<ImChannelEntity> {


    PageUtils queryChannelPage(ImChannelQueryCriteria criteria, Pageable pageable);


    List<Map<String, Object>> download(ImChannelQueryCriteria criteria);


    boolean updateEntity(ImChannelEntity imChannelEntity, Long userId);


    boolean deleteEntity(Long id);


    boolean refreshAuth(Long id, Long userId);


    ImChannelEntity findByChannelId(String channelId);


    boolean saveEntity(ImChannelEntity imChannelEntity, Long userId);
}

