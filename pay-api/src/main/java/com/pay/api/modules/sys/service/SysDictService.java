package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysDictQueryCriteria;
import com.pay.api.modules.sys.entity.SysDictEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysDictService extends IService<SysDictEntity> {

    List<SysDictEntity> queryAllDictPage(SysDictQueryCriteria criteria);

    PageUtils queryDictPage(SysDictQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(SysDictQueryCriteria criteria);

    boolean saveEntity(SysDictEntity sysDictEntity);

    boolean updateEntity(SysDictEntity sysDictEntity);

    Integer deleteEntity(Long id);
}

