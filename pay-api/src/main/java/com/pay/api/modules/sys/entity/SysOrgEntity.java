package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.pay.api.modules.sys.dto.SysOrgDTO;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
@Data
@TableName("sys_org")
public class SysOrgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 上级渠道
	 */
	private Long pid;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Boolean enabled;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@TableField(exist=false)
	private List<SysOrgEntity> children;

	public String getLabel() {
		return name;
	}

}
