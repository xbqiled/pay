package com.pay.api.modules.sys.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-04-12 18:38:50
 */
@Data
public class SysOrgChannelVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 云商编码
	 */
	private Long orgId;

	/**
	 * 云商名称
	 */
	private String orgName;

	/**
	 * 渠道名称
	 */
	private String channelName;
	/**
	 * 组织图标
	 */
	private String iconUrl;
	/**
	 * 0,自己渠道， 社会渠道
	 */
	private Integer type;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 鉴权码
	 */
	private String authCode;
	/**
	 * 加盐
	 */
	private String salt;


	private String note;
}
