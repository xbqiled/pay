package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImMessageQueryCriteria;
import com.pay.api.modules.im.entity.ImMessageEntity;
import com.pay.api.modules.im.vo.ImMessageVo;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface ImMessageService extends IService<ImMessageEntity> {

    PageUtils queryMessagePage(ImMessageQueryCriteria criteria, Pageable pageable);


    TraderReplyConfigEntity getPayConfig(Long orgId, Integer type);


    List<Map<String, Object>> download(ImMessageQueryCriteria criteria);


    List<ImMessageVo> getMessageHisPage(Integer userId, Integer timeOrder);


    ImMessageEntity getMessageById(String messageId, String formUserId, String toUserId);


    PageUtils getUserMessageHisPage(Integer sendUserId, Integer receiveUserId, Integer pageSize, Integer pageIndex, Integer timeOrder);
}

