package com.pay.api.modules.trader.criteria;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReplyConfigQueryCriteria implements Serializable {

    private String type;

    private String startTime;

    private String endTime;

    private Long orgId;

    private String note;
}
