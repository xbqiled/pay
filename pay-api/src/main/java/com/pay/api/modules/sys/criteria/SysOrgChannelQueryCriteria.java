package com.pay.api.modules.sys.criteria;

import lombok.Data;

import java.io.Serializable;


@Data
public class SysOrgChannelQueryCriteria implements Serializable {

    private Long orgId;

    private Long channelId;

    private String orgName;

}
