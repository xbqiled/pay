package com.pay.api.modules.tools.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置信息表
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
@Data
@TableName("oss_config")
public class OssConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * key
	 */
	private String paramKey;
	/**
	 * value
	 */
	private String paramValue;
	/**
	 * 状态   0：隐藏    1：显示
	 */
	private Integer status;
	/**
	 * 是否激活 0 ：否   1，是
	 */
	private Integer isActivate;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 
	 */
	private Date createTime;

}
