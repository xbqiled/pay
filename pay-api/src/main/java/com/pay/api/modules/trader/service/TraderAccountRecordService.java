package com.pay.api.modules.trader.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.trader.criteria.AccountRecordQueryCriteria;
import com.pay.api.modules.trader.entity.TraderAccountRecordEntity;
import com.pay.api.modules.trader.entity.TraderCsInfoEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
public interface TraderAccountRecordService extends IService<TraderAccountRecordEntity> {

    PageUtils queryAccountRecordPage(AccountRecordQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(AccountRecordQueryCriteria criteria);

    TraderAccountRecordEntity saveEntity(TraderAccountRecordEntity traderAccountRecord);

    boolean updateEntity(TraderAccountRecordEntity traderAccountRecord);

    void deleteEntity(Long id);

}

