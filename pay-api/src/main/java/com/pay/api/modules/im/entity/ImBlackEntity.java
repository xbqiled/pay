package com.pay.api.modules.im.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
@Data
@TableName("im_black")
public class ImBlackEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 拉黑开始时间
	 */
	private Date blackStartTime;
	/**
	 * 拉黑结束时间
	 */
	private Date blackEndTime;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 被拉黑的用户
	 */
	private Integer blackUserId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 修改时间
	 */
	private Date modifyTime;
	/**
	 * 修改人
	 */
	private String modifyBy;
	/**
	 * 备注
	 */
	private String note;

}
