package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-21 17:28:57
 */
@Data
@TableName("sys_seq")
public class SysSeqEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * seq名字
	 */
	private String seqName;
	/**
	 * 最小值
	 */
	private Integer minValue;
	/**
	 * 最大值
	 */
	private Integer maxValue;
	/**
	 * 当前值
	 */
	private String currentValue;
	/**
	 * 归属组织机构
	 */
	private Long orgId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 修改时间
	 */
	private Date modifyTime;
	/**
	 * 修改人
	 */
	private String modifyBy;
	/**
	 * 备注
	 */
	private String note;

}
