package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysDictDetailQueryCriteria;
import com.pay.api.modules.sys.entity.SysDictDetailEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface SysDictDetailService extends IService<SysDictDetailEntity> {

    PageUtils queryAll(String name, Integer pageSize, Integer pageIndex);

    PageUtils queryDictDetailPage(SysDictDetailQueryCriteria criteria, Pageable pageable);

    SysDictDetailEntity saveEntity(SysDictDetailEntity resources);

    boolean updateEntity(SysDictDetailEntity resources);

    boolean delEntity(Long id);
}

