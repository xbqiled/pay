package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImUserInfoQueryCriteria;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.vo.ImUserInfoVo;
import com.pay.common.core.utils.Result;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface ImUserInfoService extends IService<ImUserInfoEntity> {

    ImUserInfoEntity findUserById(Integer id);

    ImUserInfoVo getUserInfoById(Integer queryUserId, Integer myUserId);

    List<Map<String, Object>> getCsList(Long orgId, Integer isOnLine);

    List<Map<String, Object>> getChannelCsList(Long orgId, String channelId, Integer isOnLine);

    List<ImUserInfoVo> findUserByKey(String key, String userId);

    ImUserInfoEntity loginByUserId(Long orgId, String userId);

    ImUserInfoEntity loginByUserName(Long orgId, String userName);

    Result createUser(Long orgId, String channelId, String authCode, String userId, String nickName, String passWord);

    ImUserInfoEntity initUser(Long orgId, String userId, Integer platformType, String signIn);

    ImUserInfoEntity initCacUser(String userId, Integer platformType, String channelId, String nickName);

    ImUserInfoEntity registerUser(Long orgId, String nickName);

    ImUserInfoEntity registerUser(Long orgId, String nickName, String passWord, Integer supportedPms,String avatar);

    ImUserInfoEntity modifyUserInfo(ImUserInfoEntity imUserInfoEntity);

    Integer modifyPw(Integer userId, String oldPw, String newPw);

    Integer modifyPwByAccount(String account, String nickName, Integer status, Long orgId, String possWord, Integer supportedPms,String avatar);

    Integer delImUser(String account, String orgId);

    List<ImUserInfoEntity> queryList(ImUserInfoQueryCriteria criteria);

    ImUserInfoEntity create(ImUserInfoEntity resources);

    void update(ImUserInfoEntity resources);

    void delete(Integer id);

    ImUserInfoEntity findByUserNamePassword(String userName, String password);

    ImUserInfoEntity verifyCreateUser(String authCode, String userId, String userName, String passwd, Long orgId);

    void modifyOnline(Integer userId, Integer isOnline);

    void modifyRechargeNum(Integer userId);
}

