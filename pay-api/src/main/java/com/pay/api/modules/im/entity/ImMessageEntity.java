package com.pay.api.modules.im.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
@Data
@TableName("im_message")
public class ImMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;

	private Long orgId;
	/**
	 * 消息ID
	 */
	private String messageId;
	/**
	 * 发送人
	 */
	private Integer sendUserId;
	/**
	 * 接收人
	 */
	private Integer receiveUserId;
	/**
	 * 群组ID
	 */
	private Integer groupId;
	/**
	 * 是否已读 0, 已读 1, 未读取
	 */
	private Integer isRead;
	/**
	 * 状态 0,待发送 1, 已接受待查看  2，已查看
	 */
	private Integer status;
	/**
	 * 消息类型0, 文本  1,图片 2,语音 3,视频 4,音乐 5,图文
	 */
	private Integer messageType;
	/**
	 * 0, 未知 1,公聊 2,私聊
	 */
	private Integer chatType;
	/**
	 * 设备标识UUID
	 */
	private String deviceId;
	/**
	 * 设备类型 0,未知 1,PC  2,android 3,ios
	 */
	private Integer deviceType;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 修改时间
	 */
	private Date modifyTime;
	/**
	 * 修改人
	 */
	private String modifyBy;

}
