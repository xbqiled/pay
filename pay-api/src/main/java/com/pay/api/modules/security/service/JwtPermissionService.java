package com.pay.api.modules.security.service;


import com.pay.api.modules.security.dto.SecurityDTO;
import com.pay.api.modules.sys.dto.SysUserDTO;

import java.io.Serializable;

public interface JwtPermissionService  extends Serializable {
      SecurityDTO mapToGrantedAuthorities(SysUserDTO user);
}
