package com.pay.api.modules.sys.criteria;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
* @author Lion
* @date 2019-6-4 14:49:34
*/
@Data
@NoArgsConstructor
public class SysJobQueryCriteria  implements Serializable  {

    private Long id;

    private String name;

    private Integer enabled;

    private String startTime;

    private String endTime;

    private Long deptId;

    private Set<Long> deptIds;

}