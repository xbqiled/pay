package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImBlackQueryCriteria;
import com.pay.api.modules.im.dto.ImBlackDTO;
import com.pay.api.modules.im.entity.ImBlackEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImBlackService extends IService<ImBlackEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * queryAll 分页
     * @param criteria
     * @return
     */
    PageUtils queryAll(ImBlackQueryCriteria criteria);

    /**
     * queryAll 不分页
     * @param criteria
     * @return
     */
    public Object queryList(ImBlackQueryCriteria criteria);

    /**
     * findById
     * @param id
     * @return
     */
    ImBlackDTO findById(Integer id);

    /**
     * create
     * @param resources
     * @return
     */
    ImBlackDTO create(ImBlackEntity resources);

    /**
     * update
     * @param resources
     */
    void update(ImBlackEntity resources);

    /**
     * delete
     * @param id
     */
    void delete(Integer id);

}

