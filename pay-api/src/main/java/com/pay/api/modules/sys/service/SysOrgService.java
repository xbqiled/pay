package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysOrgsQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgEntity;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysOrgService extends IService<SysOrgEntity> {

    SysOrgEntity findById(Long id);


    String findNameById(Long pid);;


    SysOrgEntity saveEntity(SysOrgEntity resources);


    void updateEntity(SysOrgEntity resources);


    Map<String,Object> deleteEntity(Long id);


    Object buildTree(List<SysOrgEntity> sysOrgEntityList);


    List<SysOrgEntity> findByPid(long pid);


    Set<SysOrgEntity> findByRoleIds(Long id);


    List<SysOrgEntity> queryAll(SysOrgsQueryCriteria criteria);


    List<Map<String, Object>> download(SysOrgsQueryCriteria criteria);

}

