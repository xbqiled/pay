package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysUsersRolesEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysUsersRolesService extends IService<SysUsersRolesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

