package com.pay.api.modules.sys.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Lion
 * @date 2018-12-17
 */
@Data
public class SysMenuDTO implements Serializable {

    private Long id;

    private String name;

    private Long sort;

    private String path;

    private String component;

    private Long pid;

    private Boolean iFrame;

    private String icon;

    private Boolean cache;

    private Boolean hidden;

    //类型
    private Integer type;

    // 权限
    private String permission;

    private String componentName;

    private List<SysMenuDTO> children;

    private Timestamp createTime;
}
