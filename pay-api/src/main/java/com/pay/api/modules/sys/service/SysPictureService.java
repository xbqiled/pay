package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysPictureEntity;
import com.pay.api.modules.tools.criteria.PictureQueryCriteria;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysPictureService extends IService<SysPictureEntity> {

    PageUtils queryPage(PictureQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(PictureQueryCriteria criteria);

    SysPictureEntity findById(Long id);

    void delete(Long id);

    void deleteAll(List<Long> ids);
}

