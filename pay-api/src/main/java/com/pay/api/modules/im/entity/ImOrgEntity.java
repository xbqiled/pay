package com.pay.api.modules.im.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
@Data
@TableName("im_org")
public class ImOrgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 组织机构id 
	 */
	@TableId
	private Integer id;
	/**
	 * 组织机构编码
	 */
	private Long orgId;
	/**
	 * 组织图标
	 */
	private String iconUrl;
	/**
	 * 0,自己渠道， 社会渠道
	 */
	private Integer type;


	private String contact;


	private String phoneNumber;

	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private Long createBy;

	/**
	 * 备注
	 */
	private String note;

}
