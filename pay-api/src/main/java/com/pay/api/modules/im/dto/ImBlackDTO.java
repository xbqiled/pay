package com.pay.api.modules.im.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;


/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImBlackDTO implements Serializable {

    // 主键
    private Integer id;

    // 拉黑开始时间
    private Timestamp blackStartTime;

    // 拉黑结束时间
    private Timestamp blackEndTime;

    // 用户id
    private Integer userId;

    // 被拉黑的用户
    private Integer blackUserId;

    // 创建时间
    private Timestamp createTime;

    // 创建人
    private String createBy;

    // 修改时间
    private Timestamp modifyTime;

    // 修改人
    private String modifyBy;

    // 备注
    private String note;
}