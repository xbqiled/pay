package com.pay.api.modules.sys.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class SysOrgChannelDTO implements Serializable {

    private String [] channelId;

    private Long orgId;

}
