package com.pay.api.modules.log.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.kryo.JoinPointModel;
import com.pay.api.modules.log.criteria.LogQueryCriteria;
import com.pay.api.modules.log.entity.SysLogEntity;
import com.pay.common.core.utils.PageUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface SysLogService extends IService<SysLogEntity> {


    List<Map<String, Object>> download(LogQueryCriteria criteria);


    PageUtils queryLogPage(LogQueryCriteria criteria, Pageable pageable);


    List<SysLogEntity> queryAll(LogQueryCriteria criteria);

    @Async
    void save(String username, String browser, String ip, Long orgId, JoinPointModel joinPoint, SysLogEntity log);


    Object findByErrDetail(Long id);


    void batchDelLog(String endTime);
}

