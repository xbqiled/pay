package com.pay.api.modules.quartz.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
@Data
@TableName("quartz_log")
public class QuartzLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private String beanName;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private String cronExpression;
	/**
	 * 
	 */
	private String exceptionDetail;
	/**
	 * 
	 */
	private Boolean isSuccess;
	/**
	 * 
	 */
	private String jobName;
	/**
	 * 
	 */
	private String methodName;
	/**
	 * 
	 */
	private String params;
	/**
	 * 
	 */
	private Long time;

}
