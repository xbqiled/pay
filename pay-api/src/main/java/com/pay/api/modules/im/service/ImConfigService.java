package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.entity.ImConfigEntity;
import com.pay.api.modules.im.vo.ImConfigVo;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-31 18:17:27
 */
public interface ImConfigService extends IService<ImConfigEntity> {

    ImConfigVo getConfigByOrgId(Long orgId);
}

