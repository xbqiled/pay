package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysSeqEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-21 17:28:57
 */
public interface SysSeqService extends IService<SysSeqEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

