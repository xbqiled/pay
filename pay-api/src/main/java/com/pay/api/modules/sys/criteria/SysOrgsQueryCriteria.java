package com.pay.api.modules.sys.criteria;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
* @author Lion
* @date 2019-03-25
*/
@Data
public class SysOrgsQueryCriteria implements Serializable {

    private Set<Long> ids;

    private String name;

    private Integer enabled;

    private Long pid;

    private String startTime;

    private String endTime;
}