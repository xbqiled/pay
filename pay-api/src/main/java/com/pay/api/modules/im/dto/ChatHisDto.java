package com.pay.api.modules.im.dto;

import com.pay.common.core.utils.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class ChatHisDto implements Serializable {

    @ApiModelProperty("orgId信息")
    private Long orgId;

    @ApiModelProperty("token信息")
    private String token;

    @ApiModelProperty("分页查询信息")
    private Page page;
}
