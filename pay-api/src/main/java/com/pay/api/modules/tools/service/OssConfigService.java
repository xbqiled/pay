package com.pay.api.modules.tools.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.tools.criteria.OssConfigQueryCriteria;
import com.pay.api.modules.tools.entity.OssConfigEntity;
import com.pay.api.modules.tools.entity.OssConfigParams;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 系统配置信息表
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface OssConfigService extends IService<OssConfigEntity> {

    PageUtils queryPage(OssConfigQueryCriteria criteria, Pageable pageable);

    JSONObject  saveEntity(OssConfigParams bean);

    JSONObject updateEntity(OssConfigParams resources);

    JSONObject deleteEntity(Long id);

    <T> T getConfigObject(String key, Class<T> clazz);

    List<OssConfigEntity> queryByOssConfigEntity(OssConfigEntity bean);
}

