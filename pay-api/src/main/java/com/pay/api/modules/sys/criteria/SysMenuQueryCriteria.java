package com.pay.api.modules.sys.criteria;

import com.pay.common.core.annotation.Query;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysMenuQueryCriteria implements Serializable {


    @Query(blurry = "name,component")
    private String blurry;

    private String startTime;

    private String endTime;
}
