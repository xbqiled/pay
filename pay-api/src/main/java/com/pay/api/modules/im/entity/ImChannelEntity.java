package com.pay.api.modules.im.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
@Data
@TableName("im_channel")
public class ImChannelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 组织机构id 
	 */
	@TableId
	private Long id;
	/**
	 * 组织机构编码
	 */
	private String channelId;
	/**
	 * 渠道名称
	 */
	private String channelName;
	/**
	 * 组织图标
	 */
	private String iconUrl;
	/**
	 * 0,自己渠道， 社会渠道
	 */
	private Integer type;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 鉴权码
	 */
	private String authCode;
	/**
	 * 加盐
	 */
	private String salt;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 更新人
	 */
	private Long updateBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private Long createBy;
	/**
	 * 备注
	 */
	private String note;
}
