package com.pay.api.modules.security.service;


import com.pay.api.modules.sys.dto.SysUserDTO;

import java.io.Serializable;

public interface SysUserDetailsService extends Serializable {

     SysUserDTO loadUserByUsername(String username);

}
