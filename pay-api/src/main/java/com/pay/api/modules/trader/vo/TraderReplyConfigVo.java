package com.pay.api.modules.trader.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class TraderReplyConfigVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 类型 1银行卡2支付宝3微信
     */
    private Integer type;
    /**
     * 归属组织
     */
    private Long orgId;


    private String orgName;
    /**
     * 内容配置
     */
    private String config;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private Long createBy;

    private String createName;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 修改人
     */
    private Long modifyBy;

    private String modifyName;

    private String note;
}
