package com.pay.api.modules.im.criteria;

import lombok.Data;

import java.io.Serializable;

/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImOrgQueryCriteria implements Serializable  {

    private String orgName;

    private Long orgId;

    private String startTime;

    private String endTime;
}