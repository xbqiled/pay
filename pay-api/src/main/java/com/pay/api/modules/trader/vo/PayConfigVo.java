package com.pay.api.modules.trader.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PayConfigVo implements Serializable {

    private Long orgId;

    private String config;

    private Integer type;

}
