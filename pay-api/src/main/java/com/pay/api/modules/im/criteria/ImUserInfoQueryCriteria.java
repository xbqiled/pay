package com.pay.api.modules.im.criteria;


import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImUserInfoQueryCriteria implements Serializable {
    //账号
    private String userName;
    //密码
    private String password;
    //昵称
    private String nickName;
    //组织Code
    private Long orgId;
    //用户状态
    private Integer status;
    //在线状态
    private Integer isOnline;
    //用户类型
    private Integer userType;
    //群组ID
    private Integer groupId;
    //用户ID
    private Integer userId;
    //禁言记录ID
    private Integer banedId;
    //拉黑记录ID
    private Integer blackId;
    //开始时间
    private Timestamp startDate;
    //结束时间
    private Timestamp endDate;
    //备注
    private String note;
    //用户ID组
    private Integer[] ids;
}
