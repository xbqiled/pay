package com.pay.api.modules.tools.service;


import com.alibaba.fastjson.JSONObject;
import com.pay.api.modules.tools.entity.SysEmailConfigEntity;
import com.pay.api.modules.tools.vo.EmailVo;

import java.io.Serializable;

/**
 * @author Lion
 * @date 2018-12-26
 */
public interface SysEmailConfigService extends Serializable {

    /**
     * 更新邮件配置
     * @param emailConfig
     * @return
     */
    void update(SysEmailConfigEntity emailConfig);

    /**
     * 查询配置
     * @return
     */
    SysEmailConfigEntity find();

    /**
     * 发送邮件
     * @param emailVo
     * @throws Exception
     */
    JSONObject send(EmailVo emailVo) throws Exception;
}
