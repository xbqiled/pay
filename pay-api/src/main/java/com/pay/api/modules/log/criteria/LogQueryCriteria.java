package com.pay.api.modules.log.criteria;

import com.pay.common.core.annotation.Query;
import lombok.Data;

import java.io.Serializable;

/**
 * 日志查询类
 * @author Lion
 * @date 2019-6-4 09:23:07
 */
@Data
public class LogQueryCriteria implements Serializable {

    @Query(blurry = "username,description,address,requestIp,method,params")
    private String blurry;

    private String logType;

    private Long orgId;

    private String startTime;

    private String endTime;
}
