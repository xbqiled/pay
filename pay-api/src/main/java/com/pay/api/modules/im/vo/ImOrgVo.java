package com.pay.api.modules.im.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class ImOrgVo implements Serializable {


    /**
     * 组织机构id
     */
    private Integer id;
    /**
     * 组织机构编码
     */
    private Long orgId;

    /**
     * 名称
     */
    private String orgName;
    /**
     * 组织图标
     */
    private String iconUrl;
    /**
     * 0,  正常  1，失效
     */
    private Integer type;

    private String contact;

    private String phoneNumber;

    private Boolean enabled;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private Long createBy;


    /**
     * 备注
     */
    private String note;

}
