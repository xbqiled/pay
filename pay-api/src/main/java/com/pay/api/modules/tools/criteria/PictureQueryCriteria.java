package com.pay.api.modules.tools.criteria;

import lombok.Data;

import java.io.Serializable;

@Data
public class PictureQueryCriteria implements Serializable {

    private Long orgId;

    private String filename;

    private String username;

    private String startTime;

    private String endTime;
}
