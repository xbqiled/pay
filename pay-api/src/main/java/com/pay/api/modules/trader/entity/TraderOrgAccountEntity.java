package com.pay.api.modules.trader.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
@Data
@TableName("trader_org_account")
public class TraderOrgAccountEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private Long orgId;

	/**
	 * 账户余额
	 */
	private BigDecimal blance;
	/**
	 * 总充值
	 */
	private BigDecimal totalRecharge;
	/**
	 * 总充值笔数
	 */
	private Integer totalRechargeNum;
	/**
	 * 总上分金额
	 */
	private BigDecimal totalAddpoints;
	/**
	 * 总上分笔数
	 */
	private Integer totalAddpointsNum;
}
