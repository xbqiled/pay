package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
@Data
@TableName("sys_picture")
public class SysPictureEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 上传日期
	 */
	private Date createTime;
	/**
	 * 删除的URL
	 */
	private String deleteUrl;
	/**
	 * 图片名称
	 */
	private String filename;
	/**
	 * 图片高度
	 */
	private String height;
	/**
	 * 图片大小
	 */
	private String size;
	/**
	 * 图片地址
	 */
	private String url;
	/**
	 * 用户名称
	 */
	private String username;
	/**
	 * 图片宽度
	 */
	private String width;

	private Long orgId;

	// 用于检测文件是否重复
	private String md5Code;

}
