package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysMenuQueryCriteria;
import com.pay.api.modules.sys.dto.*;
import com.pay.api.modules.sys.entity.SysMenuEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysMenuService extends IService<SysMenuEntity> {

    List<SysMenuDTO> queryAll(SysMenuQueryCriteria criteria);

    Object getMenuTree(List<SysMenuEntity> menus);

    List<SysMenuEntity> findByPid(long pid);

    Map buildTree(List<SysMenuDTO> menu);

    List<SysMenuDTO> findByRoles(List<SysRoleSmallDTO> roles);

    Object buildMenus(List<SysMenuDTO> byRoles);

    SysMenuEntity findById(Long id);

    SysMenuEntity saveEntity(SysMenuEntity resources);

    void updateEntity(SysMenuEntity resources);

    void deleteEntity(Long id);

    List<Map<String, Object>> download(SysMenuQueryCriteria criteria);
}

