package com.pay.api.modules.im.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class ImOfflineNoticeVo implements Serializable {

    private Integer id;
    /**
     * 群组id
     */
    private Integer groupId;
    /**
     * 发送用户
     */
    private Integer sendUser;
    /**
     * 0群组通知， 1个人通知
     */
    private Integer type;
    /**
     * 接受用户
     */
    private Integer receiveUser;
    /**
     * 包数据
     */
    private String noticeContent;
    /**
     * 0,待发送 1, 已接受待查看  2，已查看
     */
    private Integer status;
    /**
     * 0,用户上线 1,加入群组，2,用户下线， 3离开群组, 4,分享， 5,跟投，6, 红包
     */
    private Integer noticeMode;
    /**
     * 创建时间
     */
    private Date sendTime;

    /**
     * 是否在線
     */
    private Integer isOnline;


    private String nickName;


    private String userAvatar;


    private String groupName;


    private String groupAvatar;


    private Integer groupType;


}
