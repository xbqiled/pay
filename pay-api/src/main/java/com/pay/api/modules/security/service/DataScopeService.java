package com.pay.api.modules.security.service;




import java.io.Serializable;
import java.util.List;
import java.util.Set;
import com.pay.api.modules.sys.entity.SysOrgEntity;

public interface DataScopeService extends Serializable {

    Set<Long> getOrgIds(String username);

    List<Long> getOrgChildren(List<SysOrgEntity> deptList);

}
