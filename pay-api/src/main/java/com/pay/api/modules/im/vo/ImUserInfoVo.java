package com.pay.api.modules.im.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ImUserInfoVo implements Serializable {


    private Integer id;
    /**
     * 用户ID
     */
    private String accountId;
    /**
     * 归属组织机构
     */
    private Long orgId;
    /**
     * 是否在线 0,未知 1,下线 2,离线
     */
    private Integer isOnline;
    /**
     * 用户类型
     */
    private Integer userType;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 性别  0, 男  1,女  2,未知
     */
    private Integer sex;
    /**
     * 状态  0,正常  1,失效  2, 冻结
     */
    private Integer status;
    /**
     * 个性签名
     */
    private String sign;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 地址
     */
    private String address;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 生日
     */
    private String birthDay;
    /**
     * 身份证号码
     */
    private String cardNumber;
    /**
     * 毕业院校
     */
    private String school;
    /**
     * 学历  0，初中 1，高中  2，大专 3，本科  4，研究生  5，博士  6，博士后
     */
    private Integer education;
    /**
     * 头像
     */
    private String userAvatar;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 备注
     */
    private String note;

    //添加时间
    private Date addTime;

    //好友备注
    private String remark;

}
