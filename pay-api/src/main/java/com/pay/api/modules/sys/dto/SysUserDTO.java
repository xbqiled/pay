package com.pay.api.modules.sys.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pay.api.modules.sys.entity.SysUsersRolesEntity;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Lion
 * @date 2018-11-23
 */
@Data
public class SysUserDTO implements Serializable {

    private Long id;

    private String username;

    private String avatar;

    private String email;

    private String phone;

    private Long orgId;

    private Boolean enabled;

    @JsonIgnore
    private String password;

    private Timestamp createTime;

    private Date lastPasswordResetTime;

    private Set<SysRoleSmallDTO> roles;

    private List<SysUsersRolesEntity> roleList;

    private SysJobSmallDTO job;

    private SysDeptSmallDTO dept;

    private Long deptId;
}
