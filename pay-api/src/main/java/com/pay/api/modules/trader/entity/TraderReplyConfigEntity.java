package com.pay.api.modules.trader.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:09
 */
@Data
@TableName("trader_reply_config")
public class TraderReplyConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *  主键
	 */
	@TableId
	private Long id;
	/**
	 * 类型 1银行卡2支付宝3微信
	 */
	private Integer type;
	/**
	 * 归属组织
	 */
	private Long orgId;
	/**
	 * 内容配置
	 */
	private String config;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private Long createBy;
	/**
	 * 修改时间
	 */
	private Date modifyTime;
	/**
	 * 修改人
	 */
	private Long modifyBy;


	private String note;
}
