package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.quartz.entity.SysJobEntity;
import com.pay.api.modules.sys.criteria.SysJobQueryCriteria;
import com.pay.api.modules.sys.vo.SysJobVo;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface SysJobService extends IService<SysJobEntity> {

    PageUtils queryJobPage(SysJobQueryCriteria criteria, Pageable pageable);

    SysJobEntity saveEntity(SysJobEntity sysJobEntity);

    void updateEntity(SysJobEntity sysJobEntity);

    Map<String,Object> delEntity(Long id);

    List<SysJobVo> queryAll(SysJobQueryCriteria criteria);

    List<Map<String, Object>> download(SysJobQueryCriteria criteria);
}

