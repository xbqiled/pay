package com.pay.api.modules.trader.criteria;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrgAccountQueryCriteria implements Serializable {

    private String filename;

    private String username;

    private String startTime;

    private String endTime;

    private Long orgId;

    private String orgName;

}
