package com.pay.api.modules.quartz.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
@Data
@TableName("quartz_job")
public class QuartzJobEntity implements Serializable {
	public static final String JOB_KEY = "JOB_KEY";
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * Spring Bean名称
	 */
	private String beanName;
	/**
	 * cron 表达式
	 */
	private String cronExpression;
	/**
	 * 状态：1暂停、0启用
	 */
	private Integer isPause;

//	private Boolean pause;
	/**
	 * 任务名称
	 */
	private String jobName;
	/**
	 * 方法名称
	 */
	private String methodName;
	/**
	 * 参数
	 */
	private String params;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建或更新日期
	 */
	private Date updateTime;


	public @interface Update {}

}
