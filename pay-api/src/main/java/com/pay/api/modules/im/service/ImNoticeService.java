package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImNoticeQueryCriteria;
import com.pay.api.modules.im.dto.ImNoticeDTO;
import com.pay.api.modules.im.entity.ImNoticeEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImNoticeService extends IService<ImNoticeEntity> {

    /**
     * queryAll 分页
     * @param criteria
     * @return
     */
    PageUtils queryAll(ImNoticeQueryCriteria criteria);

    /**
     * queryAll 不分页
     * @param criteria
     * @return
     */
    List<ImNoticeEntity> queryList(ImNoticeQueryCriteria criteria);

    /**
     * findById
     * @param id
     * @return
     */
    ImNoticeDTO findById(Integer id);

    /**
     * create
     * @param resources
     * @return
     */
    ImNoticeDTO create(ImNoticeEntity resources);

    /**
     * update
     * @param resources
     */
    void update(ImNoticeEntity resources);

    /**
     * delete
     * @param id
     */
    void delete(Integer id);
}

