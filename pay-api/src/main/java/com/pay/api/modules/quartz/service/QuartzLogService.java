package com.pay.api.modules.quartz.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzLogEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface QuartzLogService extends IService<QuartzLogEntity> {

    PageUtils queryQuartzLogPage(JobQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> downloadLog(JobQueryCriteria criteria);

    List<QuartzLogEntity> queryAllLog(JobQueryCriteria criteria);

    void addLog(QuartzLogEntity entity);

}

