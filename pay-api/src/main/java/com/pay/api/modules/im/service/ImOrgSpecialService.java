package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImOrgSpecialQueryCriteria;
import com.pay.api.modules.im.dto.ImOrgSpecialDTO;
import com.pay.api.modules.im.entity.ImOrgSpecialEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImOrgSpecialService extends IService<ImOrgSpecialEntity> {

    /**
     * queryAll 分页
     * @param criteria
     * @return
     */
    PageUtils queryAll(ImOrgSpecialQueryCriteria criteria);

    /**
     * queryAll 不分页
     * @param criteria
     * @return
     */
    List<ImOrgSpecialEntity> queryList(ImOrgSpecialQueryCriteria criteria);

    /**
     * findById
     * @param id
     * @return
     */
    ImOrgSpecialDTO findById(Integer id);

    /**
     * create
     * @param resources
     * @return
     */
    ImOrgSpecialEntity create(ImOrgSpecialEntity resources);

    /**
     * update
     * @param resources
     */
    void update(ImOrgSpecialEntity resources);

    /**
     * delete
     * @param id
     */
    void delete(Integer id);
}

