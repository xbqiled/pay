package com.pay.api.modules.trader.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysDictEntity;
import com.pay.api.modules.trader.criteria.CsQueryCriteria;
import com.pay.api.modules.trader.entity.TraderCsInfoEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 13:21:04
 */
public interface TraderCsInfoService extends IService<TraderCsInfoEntity> {

    PageUtils queryCsPage(CsQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(CsQueryCriteria criteria);

    TraderCsInfoEntity saveEntity(TraderCsInfoEntity traderCsInfo, Long userId);

    boolean updateEntity(TraderCsInfoEntity traderCsInfo);

    void deleteEntity(Long id);

}

