package com.pay.api.modules.tools.criteria;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
* @author Lion
* @date 2019-6-4 14:49:34
*/
@Data
@NoArgsConstructor
public class OssConfigQueryCriteria implements Serializable {

    private String remark;

    private Boolean status;


}