package com.pay.api.modules.sys.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysJobVo implements Serializable {

    private Long id;

    /**
     * 名称
     */
    private String name;
    /**
     * 状态
     */
    private Boolean enabled;
    /**
     * 排序
     */
    private Long sort;

    /**
     * 组织机构ID
     */
    private Long orgId;

    /**
     * 上级id
     */
    private Long pId;
    /**
     *
     */
    private String orgName;

    /**
     * 创建日期
     */
    private Date createTime;
}
