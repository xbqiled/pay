package com.pay.api.modules.trader.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
@Data
@TableName("trader_recharge_record")
public class TraderRechargeRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 组织机构编码
	 */
	private Long orgId;
	/**
	 * 会计项目 1给云商充值  2给云商扣款
	 */
	private Integer accountItem;
	/**
	 * 状态信息 1成功
	 */
	private Integer status;
	/**
	 * 金额
	 */
	private BigDecimal amount;
	/**
	 * 变化前账户余额
	 */
	private BigDecimal beforeBalance;
	/**
	 * 变化后账户余额
	 */
	private BigDecimal afterBalance;
	/**
	 * 操作时间
	 */
	private Date operateTime;
	/**
	 * 操作用户
	 */
	private String operateUser;

}
