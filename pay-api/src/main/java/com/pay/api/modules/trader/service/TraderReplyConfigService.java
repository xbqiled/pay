package com.pay.api.modules.trader.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.json.PayConfig;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.api.modules.trader.criteria.ReplyConfigQueryCriteria;
import com.pay.api.modules.trader.dto.ReplyConfigDto;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:09
 */
public interface TraderReplyConfigService extends IService<TraderReplyConfigEntity> {

    PageUtils queryReplyConfigPage(ReplyConfigQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(ReplyConfigQueryCriteria criteria);

    List<TraderReplyConfigEntity> selectConfigByOrgId(Long orgId);

    TraderReplyConfigEntity saveEntity(ReplyConfigDto replyConfigDto, JwtUser user);

    boolean updateEntity(ReplyConfigDto replyConfigDto, JwtUser user);

    Integer deleteEntity(Long id);

    List<PayConfig> getReplyConfigList(Integer userId);
}

