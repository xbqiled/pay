package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysRolesMenusEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysRolesMenusService extends IService<SysRolesMenusEntity> {

    List<Long> getRoleMenuIds(Long roleId);
}

