package com.pay.api.modules.security.service;

import com.pay.api.modules.security.entity.OnlineUser;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;


public interface OnlineUserService extends Serializable {


    void saveOnlineUser(String token, Long orgId, String job, String ip, String browser, String userName, String onlineKey, Long expiration);


    PageUtils getAll(String filter, Long orgId, String onlineKey, Pageable pageable) ;


    List<OnlineUser> getAll(String filter, Long orgId, String onlineKey);


    void kickOut(String val, String onlineKey) throws Exception;


    void logout(String token, String onlineKey);


    List<Map<String, Object>> download(String filter, Long orgId, String onlineKey) throws IOException;

}
