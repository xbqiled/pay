package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImOrgQueryCriteria;
import com.pay.api.modules.im.entity.ImOrgEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface ImOrgService extends IService<ImOrgEntity> {
    /**
     * queryAll 分页
     * @param criteria,pageable
     * @return
     */
    PageUtils queryOrgPage(ImOrgQueryCriteria criteria, Pageable pageable);

    /**
     * 导出银商数据
     * @param criteria
     * @return
     */
    List<Map<String, Object>> download(ImOrgQueryCriteria criteria);


    /**
     * 修改银商详情
     * @param imOrgEntity
     * @return
     */
    boolean saveEntity(ImOrgEntity imOrgEntity);


    /**
     * 修改银商详情
     * @param imOrgEntity
     * @return
     */
    boolean updateEntity(ImOrgEntity imOrgEntity);


    /**
     *
     * @param orgId
     * @return
     */
    Integer deleteByOrgId(Long orgId);
}

