package com.pay.api.modules.tools.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.tools.entity.OssStsTokenEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-14 12:19:09
 */
public interface OssStsTokenService extends IService<OssStsTokenEntity> {


    OssStsTokenEntity getLastToken();

    void createOssStsTonken(OssStsTokenEntity ossStsTokenEntity);

}

