package com.pay.api.modules.trader.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.trader.criteria.CsQueryCriteria;
import com.pay.api.modules.trader.criteria.RechargeRecordQueryCriteria;
import com.pay.api.modules.trader.entity.TraderCsInfoEntity;
import com.pay.api.modules.trader.entity.TraderRechargeRecordEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
public interface TraderRechargeRecordService extends IService<TraderRechargeRecordEntity> {

    PageUtils queryRechargeRecordPage(RechargeRecordQueryCriteria criteria, Pageable pageable);

    List<Map<String, Object>> download(RechargeRecordQueryCriteria criteria);

    TraderRechargeRecordEntity saveEntity(TraderRechargeRecordEntity traderRechargeRecord);

    void updateEntity(TraderRechargeRecordEntity traderRechargeRecord);

    void deleteEntity(Long id);

}

