package com.pay.api.modules.monitor.service;


import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.core.utils.PageUtils;

/**
 * 可自行扩展
 * @author Lion
 * @date 2018-12-10
 */
public interface RedisService {

    /**
     * 查询验证码的值
     * @param key
     * @return
     */
    String getCodeVal(String key);

    /**
     * 保存验证码
     * @param key
     * @param val
     */
    void saveCode(String key, Object val);

    /**
     * delete
     * @param key
     */
    void delete(String key);

    /**
     * 清空所有缓存
     */
    void flushdb();

    /**
     * findById
     * @param key
     * @return
     */
    PageUtils findByKey(String key, int pageNum, int pageSize);



    ImUserInfoEntity findUserInfoByToken(String token);

}
