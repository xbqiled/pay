package com.pay.api.modules.report.entity;

import lombok.Data;

import java.io.Serializable;


@Data
public class ReportEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long orgId;
	private String orgName;
	private String date;
	private String totalRecharge;
	private Integer totalRechargeNum;
	private String totalAddpoints;
	private Integer totalAddpointsNum;
}
