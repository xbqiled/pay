package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-04-12 18:38:50
 */
@Data
@TableName("sys_org_channel")
public class SysOrgChannelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 云商编码
	 */
	private Long orgId;
}
