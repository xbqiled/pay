package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysVerificationCodeEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysVerificationCodeService extends IService<SysVerificationCodeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

