package com.pay.api.modules.trader.criteria;

import lombok.Data;

import java.io.Serializable;

@Data
public class CsQueryCriteria implements Serializable {

    private String imAccount;

    private String csName;

    private String startTime;

    private String endTime;

    private Long orgId;
}
