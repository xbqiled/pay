package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
@Data
@TableName("sys_dict_detail")
public class SysDictDetailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 字典标签
	 */
	private String label;
	/**
	 * 字典值
	 */
	private String value;
	/**
	 * 排序
	 */
	private String sort;
	/**
	 * 字典id
	 */
	private Long dictId;

}
