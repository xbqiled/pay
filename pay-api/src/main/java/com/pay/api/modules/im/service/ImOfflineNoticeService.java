package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImOfflineNoticeQueryCriteria;
import com.pay.api.modules.im.dto.ImOfflineNoticeDTO;
import com.pay.api.modules.im.entity.ImOfflineNoticeEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.List;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImOfflineNoticeService extends IService<ImOfflineNoticeEntity> {

    PageUtils queryOfflineNoticePage(Integer userId, Integer pageSize, Integer pageIndex, Integer timeOrder);
    /**
     * queryAll 分页
     * @param criteria
     * @return
     */
    PageUtils queryAll(ImOfflineNoticeQueryCriteria criteria);


    /**
     * <B>通过排序来查询</B>
     * @return
     */
    List<ImOfflineNoticeEntity> queryByUserId(String userId, Integer pageIndex, Integer pageSize, Integer timeOrder);

    /**
     * queryAll 不分页
     * @param criteria
     * @return
     */
    List<ImOfflineNoticeEntity> queryList(ImOfflineNoticeQueryCriteria criteria);

    /**
     * findById
     * @param id
     * @return
     */
    ImOfflineNoticeDTO findById(Integer id);

    /**
     * create
     * @param resources
     * @return
     */
    ImOfflineNoticeDTO create(ImOfflineNoticeEntity resources);

    /**
     * update
     * @param resources
     */
    void update(ImOfflineNoticeEntity resources);

    /**
     * delete
     * @param id
     */
    void delete(Integer id);


}

