package com.pay.api.modules.sys.vo;

import lombok.Data;

import java.util.Date;

/**
 * 修改密码的 Vo 类
 * @author Lion
 * @date 2019年7月11日13:59:49
 */
@Data
public class SysDictDetailVo {

    private Long id;
    /**
     * 字典名称
     */
    private String name;
    /**
     * 描述
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 字典标签
     */
    private String label;
    /**
     * 字典值
     */
    private String value;
    /**
     * 排序
     */
    private String sort;
    /**
     * 字典id
     */
    private Long dictId;
}
