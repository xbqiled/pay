package com.pay.api.modules.tools.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
@Data
@TableName("sys_email_config")
public class SysEmailConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 发件人邮箱
	 */
	private String fromUser;
	/**
	 * 邮件服务器SMTP地址
	 */
	private String host;
	/**
	 * 密码
	 */
	private String pass;
	/**
	 * 端口
	 */
	private String port;
	/**
	 * 发件者用户名
	 */
	private String user;

}
