package com.pay.api.modules.log.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
@Data
@TableName("sys_log")
public class SysLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String exceptionDetail;
	/**
	 * 
	 */
	private String logType;
	/**
	 * 
	 */
	private String method;
	/**
	 * 
	 */
	private String params;
	/**
	 * 
	 */
	private String requestIp;
	/**
	 * 
	 */
	private Long time;
	/**
	 * 
	 */
	private String username;

	private String address;

	private String browser;

	private Long orgId;

}
