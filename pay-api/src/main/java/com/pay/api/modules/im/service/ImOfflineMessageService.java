package com.pay.api.modules.im.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.im.criteria.ImOfflineMessageQueryCriteria;
import com.pay.api.modules.im.dto.ImOfflineMessageDTO;
import com.pay.api.modules.im.entity.ImOfflineMessageEntity;
import com.pay.common.core.utils.PageUtils;

import java.util.List;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:46
 */
public interface ImOfflineMessageService extends IService<ImOfflineMessageEntity> {

    PageUtils queryOfflineMessagePage(Integer userId, Integer pageSize, Integer pageIndex, Integer timeOrder);

    PageUtils getOfflineMessagePage(Integer userId, Integer type, Integer fromId, Integer pageSize, Integer pageIndex, Integer timeOrder);

    Integer batchDelOfflineMessage(List<Integer> idArray, Integer type, Integer userId, Integer sourceId);

    /**
     * queryAll 分页
     * @param criteria
     * @return
     */
    PageUtils queryAll(ImOfflineMessageQueryCriteria criteria);

    /**
     * queryAll 不分页
     * @param criteria
     * @return
     */
    List<ImOfflineMessageEntity> queryList(ImOfflineMessageQueryCriteria criteria);



    Object queryByUserId(String userId, Integer pageIndex, Integer pageSize, Integer timeOrder);

    /**
     * findById
     * @param messageId
     * @return
     */
    ImOfflineMessageDTO findById(Long messageId);

    /**
     * create
     * @param resources
     * @return
     */
    ImOfflineMessageDTO create(ImOfflineMessageEntity resources);

    /**
     * update
     * @param resources
     */
    void update(ImOfflineMessageEntity resources);

    /**
     * delete
     * @param messageId
     */
    void delete(Long messageId);
}

