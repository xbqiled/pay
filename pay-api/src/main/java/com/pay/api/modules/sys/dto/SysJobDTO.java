package com.pay.api.modules.sys.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author Lion
* @date 2019-03-29
*/
@Data
public class SysJobDTO implements Serializable {

    /**
     * ID
     */
    private Long id;

    /**
     * 排序
     */
    private Long sort;

    /**
     * 名称
     */
    private String name;

    /**
     * 状态
     */
    private Boolean enabled;

    private SysDeptSmallDTO dept;
    /**
     *
     */
    private String deptSuperiorName;

    /**
     * 创建日期
     */
    private Timestamp createTime;

    public SysJobDTO(String name, Boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }
}