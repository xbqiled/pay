package com.pay.api.modules.sys.criteria;

import lombok.Data;

import java.io.Serializable;

/**
* @author Lion
* @date 2019-04-10
*/
@Data
public class SysDictDetailQueryCriteria  implements Serializable {

    private String label;

    private String dictName;
}