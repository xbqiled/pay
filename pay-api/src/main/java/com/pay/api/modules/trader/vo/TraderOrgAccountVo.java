package com.pay.api.modules.trader.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
@Data
public class TraderOrgAccountVo implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Long id;
	/**
	 * 
	 */
	private Long orgId;


	private Long pId;


	private String orgName;
	/**
	 * 账户余额
	 */
	private BigDecimal blance;
	/**
	 * 总充值
	 */
	private BigDecimal totalRecharge;
	/**
	 * 总充值笔数
	 */
	private Integer totalRechargeNum;
	/**
	 * 总上分金额
	 */
	private BigDecimal totalAddpoints;
	/**
	 * 总上分笔数
	 */
	private Integer totalAddpointsNum;


	private List<TraderOrgAccountVo> children;
}
