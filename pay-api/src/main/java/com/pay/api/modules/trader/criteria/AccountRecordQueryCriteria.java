package com.pay.api.modules.trader.criteria;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountRecordQueryCriteria implements Serializable {

    private String filename;

    private String username;

    private String startTime;

    private String endTime;

    private Integer status;

    private Long orgId;

    private String userId;

}
