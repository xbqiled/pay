package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
@Data
@TableName("sys_verification_code")
public class SysVerificationCodeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 验证码
	 */
	private String code;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 状态：1有效、0过期
	 */
	private Boolean status;
	/**
	 * 验证码类型：email或者短信
	 */
	private String type;
	/**
	 * 接收邮箱或者手机号码
	 */
	private String value;
	/**
	 * 业务名称：如重置邮箱、重置密码等
	 */
	private String scenes;

}
