package com.pay.api.modules.security.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lion
 */
@Data
public class OnlineUser implements Serializable {

    private Long orgId;

    private String userName;

    private String job;

    private String browser;

    private String ip;

    private String address;

    private String key;

    private Date loginTime;
}
