package com.pay.api.modules.security.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lion
 * @date 2018-11-23
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDetails implements Serializable {

    private  Long id;

    private  String username;

    private  String password;

    private  String avatar;

    private  String email;

    private  String phone;

    private  Long orgId;

    private  String orgName;

    private  String job;

    private Date createTime;
}
