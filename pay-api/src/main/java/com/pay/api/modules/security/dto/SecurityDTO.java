package com.pay.api.modules.security.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import com.pay.api.modules.sys.dto.SysRoleDTO;

@Data
@NoArgsConstructor
public class SecurityDTO implements Serializable {

    private static final long serialVersionUID = -3567389873213334710L;

    private String roleStr;

    private Set<SysRoleDTO> roleSet;
}
