package com.pay.api.modules.quartz.dto;


import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Lion
 * @date 2019-6-4 10:33:02
 */
@Data
public class JobQueryCriteria implements Serializable {

    private String jobName;

    private Integer isPause;

    private Boolean success;

    private String startTime;

    private String endTime;

}
