package com.pay.api.modules.im.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class ImMessageVo implements Serializable {

    private Long id;

    private String messageId;
    /**
     * 发送人
     */
    private String sendUserId;


    private String sendUserName;
    /**
     * 接收人
     */
    private String receiveUserId;


    private String receiveUserName;
    /**
     * 群组ID
     */
    private Integer groupId;


    private String groupName;
    /**
     * 是否已读 0, 已读 1, 未读取
     */
    private Integer isRead;
    /**
     * 状态 0,待发送 1, 已接受待查看  2，已查看
     */
    private Integer status;
    /**
     * 消息类型0, 文本  1,图片 2,语音 3,视频 4,音乐 5,图文
     */
    private Integer messageType;
    /**
     * 0, 未知 1,公聊 2,私聊
     */
    private Integer chatType;
    /**
     * 设备标识UUID
     */
    private String deviceId;
    /**
     * 设备类型 0,未知 1,PC  2,android 3,ios
     */
    private Integer deviceType;
    /**
     * 消息内容
     */
    private String content;


    private Date createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date sendTime;


    private String sendNickName;


    private String sendUserAvatar;


    private String sendUserSign;


    private String receiveNickName;


    private String receiveUserAvatar;


    private String receiveUserSign;

}
