package com.pay.api.modules.im.criteria;


import lombok.Data;

import java.io.Serializable;

/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImMessageQueryCriteria implements Serializable {

    private String sendUser;

    private String receiveUser;

    private Integer chatType;

    private Integer isRead;

    private Integer status;

    private Integer messageType;

    private Long orgId;

    private String startTime;

    private String endTime;


}