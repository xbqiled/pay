package com.pay.api.modules.im.criteria;

import lombok.Data;

import java.io.Serializable;

/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImChannelQueryCriteria implements Serializable  {

    private String channelName;

    private Long channelId;

    private String startTime;

    private String endTime;

    private Long orgId;
}