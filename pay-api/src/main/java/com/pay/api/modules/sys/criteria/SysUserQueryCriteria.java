package com.pay.api.modules.sys.criteria;

import com.pay.common.core.annotation.Query;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Lion
 * @date 2018-11-23
 */
@Data
public class SysUserQueryCriteria  implements Serializable {

    // 多字段模糊
    @Query(blurry = "email,username")
    private String blurry;

    private String startTime;

    private String endTime;

    private Long id;

    private Long orgId;

    private Set<Long> deptIds;

    private String username;

    private String email;

    private Boolean enabled;

    private Long deptId;

    private String deptName;
}
