package com.pay.api.modules.im.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-31 18:17:27
 */
@Data
public class ImConfigVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 组织机构编码
	 */
	private Long orgId;

	/**
	 * 好友请求过期时间：单位(小时)
	 */
	private Integer friendReqExp;

}
