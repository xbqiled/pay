package com.pay.api.modules.sys.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Lion
 * @date 2018-11-23
 */
@Data
public class SysRoleSmallDTO implements Serializable {

    private Long id;

    private String name;

    private Integer level;

    private String dataScope;
}
