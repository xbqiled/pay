package com.pay.api.modules.trader.criteria;

import lombok.Data;

import java.io.Serializable;

@Data
public class RechargeRecordQueryCriteria implements Serializable {

    private Long orgId;

    private Integer status;

    private String filename;

    private String username;

    private String startTime;

    private String endTime;
}
