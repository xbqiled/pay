package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
@Data
@TableName("sys_user")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 头像地址
	 */
	private String avatar;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 状态：1启用、0禁用
	 */
	private boolean enabled;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 最后修改密码的日期
	 */
	private Date lastPasswordResetTime;
	/**
	 * 
	 */
	private Long orgId;
	/**
	 * 
	 */
	private String phone;
	/**
	 * 
	 */
	private Long jobId;
}
