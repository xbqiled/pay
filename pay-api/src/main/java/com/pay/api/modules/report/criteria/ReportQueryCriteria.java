package com.pay.api.modules.report.criteria;


import lombok.Data;

import java.io.Serializable;

@Data
public class ReportQueryCriteria implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long orgId;

	private boolean week;

	private String channelId;

	private String startDate;

	private String endDate;


}
