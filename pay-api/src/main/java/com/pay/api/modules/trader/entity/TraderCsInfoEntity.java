package com.pay.api.modules.trader.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 13:21:04
 */
@Data
@TableName("trader_cs_info")
public class TraderCsInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 客服名称
	 */
	private String csName;
	/**
	 * 绑定IM账号
	 */
	private String imAccount;
	/**
	 * IM密码
	 */
	private String imPassword;
	/**
	 * 归属组织机构
	 */
	private Long orgId;
	/**
	 * 类型:  1,普通客服
	 */
	private Integer csType;
	/**
	 * 客服类型: 1,推广客服 2,游戏客服
	 */
	private Integer type;
	/**
	 * 账号或URL
	 */
	private String url;
	/**
	 * 状态 1, 正常， 0，失效
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 创建人
	 */
	private Long createBy;

	/**
	 * 支付方式
	 */
	private Integer supportedPms;

	/**
	 * 头像
	 */
	private String avatarUrl;

}
