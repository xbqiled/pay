package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysUserQueryCriteria;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.entity.SysUserEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysUserService extends IService<SysUserEntity> {

    PageUtils queryUserPage(SysUserQueryCriteria criteria, Pageable pageable);

    SysUserDTO findById(long id);

    Map<String,Object> saveEntity(SysUserDTO resources);

    Map<String,Object> updateEntity(SysUserDTO userDTO);

    void deleteEntity(Long id);

    SysUserDTO findByName(String userName);

    SysUserDTO findByJobId(Long jobId);

    SysUserDTO findByOrgId(Long orgId);

    List<SysUserEntity> queryOrgIdList(Long orgId);

    void updatePass(Long id, String encryptPassword);

    void updateAvatar(String username, String url);

    void updateEmail(String username, String email);

    List<Map<String, Object>> download(SysUserQueryCriteria criteria) throws IOException;
}

