package com.pay.api.modules.monitor.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.entity.SysVisitsEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.scheduling.annotation.Async;

import java.util.Map;

/**
 *
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysVisitsService extends IService<SysVisitsEntity> {

    /**
     * 提供给定时任务，每天0点执行
     */
    void save();

    /**
     * 新增记录
     */
    @Async
    void count();

    /**
     * 获取数据
     * @return
     */
    Object get(Long orgId);

    /**
     * getChartData
     * @return
     */
    Object getChartData(Long orgId);
}

