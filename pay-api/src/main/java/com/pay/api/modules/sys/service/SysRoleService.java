package com.pay.api.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.pay.api.modules.sys.criteria.SysRoleQueryCriteria;
import com.pay.api.modules.sys.dto.*;
import com.pay.api.modules.sys.entity.SysMenuEntity;
import com.pay.api.modules.sys.entity.SysRoleEntity;
import com.pay.common.core.utils.PageUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    PageUtils queryRolePage(SysRoleQueryCriteria criteria, Pageable pageable);

    /**
     * 下载
     * @param criteria
     * @return
     */
    List<Map<String, Object>> download(SysRoleQueryCriteria criteria);

    /**
     * 保存信息
     * @param sysRoleDTO
     * @return
     */
    Map<String,Object> saveEntity(SysRoleDTO sysRoleDTO);


    /**
     * 更新信息
     * @param sysRoleEntity
     */
    Map<String,Object> updateEntity(SysRoleEntity sysRoleEntity);


    /**
     * 删除信息
     * @param id
     */
    void deleteEntity(Long id);


    /**
     * @param id
     * @return
     */
    SysRoleDTO findByMenuId(long id);


    /**
     * key的名称如有修改，请同步修改 UserServiceImpl 中的 update 方法
     * findByUsers_Id
     * @param id
     * @return
     */
    List<SysRoleSmallDTO> findByUsers_Id(Long id);


    Set<SysRoleDTO> findDtoByUsers_Id(Long id);


    Integer findByRoles(Set<SysRoleSmallDTO> roles);

    /**
     * updatePermission
     * @param roleDTO
     */
    void updatePermission(SysRoleDTO roleDTO);

    /**
     * updateMenu
     * @param roleDTO
     */
    void updateMenu(SysRoleDTO roleDTO);



    void untiedMenu(SysMenuEntity menu);

    /**
     * queryAll
     * @return
     */
    Map queryAll(SysRoleQueryCriteria criteria);

    /**
     * queryAll
     * @return
     */
    Object queryAll();
    
}

