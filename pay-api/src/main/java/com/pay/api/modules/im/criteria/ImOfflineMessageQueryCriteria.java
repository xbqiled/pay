package com.pay.api.modules.im.criteria;

import lombok.Data;

import java.io.Serializable;


/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImOfflineMessageQueryCriteria implements Serializable {

    // 精确
    private Long messageId;

    // 精确
    private String sendUser;

    // 精确
    private String receiveUser;

    // 精确
    private Integer groupId;

    // 精确
    private Integer isRead;

    // 精确
    private Integer status;

    // 精确
    private Integer messageType;


}