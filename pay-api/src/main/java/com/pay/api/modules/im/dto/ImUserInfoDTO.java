package com.pay.api.modules.im.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;


/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImUserInfoDTO implements Serializable {

    // 主键
    private Integer id;

    // 用户ID
    private String accountId;

    private Long orgId;


    // 是否在线 0,未知 1,下线 2,离线
    private Integer isOnline;

    // 用户类型  
    private Integer userType;

    // 用户名称
    private String userName;

    //昵称
    private String nickName;

    // 密码
    private String password;

    // 用户昵称
    private String nickname;

    // 性别  0, 男  1,女  2,未知
    private Integer sex;

    // 状态  0,正常  1,失效  2, 冻结
    private Integer status;

    // 个性签名
    private String sign;

    // 手机号码
    private String phone;

    // 地址
    private String address;

    // 邮箱
    private String email;

    // 生日
    private String birthDay;

    // 身份证号码
    private String cardNumber;

    // 毕业院校
    private String school;

    // 学历  0，初中 1，高中  2，大专 3，本科  4，研究生  5，博士  6，博士后
    private Integer education;

    // 头像
    private String userAvatar;

    // 创建时间
    private Timestamp createTime;

    // 创建人
    private String createBy;

    // 修改时间
    private Timestamp modifyTime;

    // 修改人
    private String modifyBy;

    // 备注
    private String note;

    //加入群组时间
    private Timestamp joinTime;

    //群组昵称
    private String  groupNickName;

    //群组备注
    private String groupRemark;

    //管理类型 1，主管理 2，副管理 3 群员
    private Integer groupLevel;

    //管理状态 0 正常  1 失效
    private Integer manageStatus;

    //是否禁言中 1否2是
    private Integer isBaned;

    //是否拉黑中 1否2是
    private Integer isBlack;

    private Timestamp prohibitStartTime;

    private Timestamp prohibitEndTime;

    private Timestamp blackStartTime;

    private Timestamp blackEndTime;

    private Integer prohibitId;

    private Integer blackId;

    private String banedNote;

    private String blackNote;

    private Integer isExit;



}