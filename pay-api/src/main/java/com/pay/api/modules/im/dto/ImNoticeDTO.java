package com.pay.api.modules.im.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;


/**
* @author lion
* @date 2019-10-01
*/
@Data
public class ImNoticeDTO implements Serializable {

    // 通知Id
    private Integer id;

    // 群组id
    private Integer groupId;

    // 发送用户
    private Integer sendUser;

    // 0群组通知， 1个人通知
    private Integer type;

    // 接受用户
    private Integer receiveUser;

    // 包数据
    private String noticeContent;

    //  0,待发送 1, 已接受待查看  2，已查看
    private Integer status;

    // 0,用户上线或加入群组，1,用户下线或离开群组, 2,分享， 3,跟投，4, 红包
    private Integer noticeMode;

    // 创建时间
    private Timestamp createTime;

    // 创建人
    private String createBy;

    // 修改时间
    private Timestamp modifyTime;

    // 修改人
    private String modifyBy;
}