package com.pay.api.modules.trader.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:09
 */
@Data
@TableName("trader_reply_config")
public class TraderReplyConfigParams implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *  主键
	 */
	@TableId
	private Long id;
	private String wxAccount;
	private String wxNickName;
	private String wxCodeUrl;
	private String aliPayAccount;
	private String aliPayNickName;
	private String aliPayCodeUrl;
	private String bankAccount;
	private String bankPayee;
	private String ascriptionBank;
	private String openAccountBank;
	private Integer type;
	private String config;
	private String note;

}
