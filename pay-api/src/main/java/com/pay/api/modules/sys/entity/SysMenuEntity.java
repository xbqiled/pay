package com.pay.api.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
@Data
@TableName("sys_menu")
public class SysMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 是否外链
	 */
	private Boolean iFrame;
	/**
	 * 菜单名称
	 */
	private String name;
	/**
	 * 组件
	 */
	private String component;
	/**
	 * 上级菜单ID
	 */
	private Long pid;
	/**
	 * 排序
	 */
	private Long sort;
	/**
	 * 图标
	 */
	private String icon;
	/**
	 * 链接地址
	 */
	private String path;
	/**
	 * 是否缓存
	 */
	private Boolean cache;
	/**
	 * 是否隐藏
	 */
	private Boolean hidden;

	//类型
	private Integer type;

	// 权限
	private String permission;

	//
	private String componentName;

	/**
	 * 创建日期
	 */
	private Date createTime;

}
