package com.pay.api.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.*;

public class PingYinUtils {


    private static String [] strArr = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" , "M", "N",  "O",  "P" , "Q", "R", "S", "T", "U", "V", "W",  "X", "Y" , "Z", "#"};

    /** public static List<Map<String, Object>> filledData(List<ImFriendVo> list) {
       List<Map<String, ImFriendVo>> mSortList = new ArrayList<>();
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (ImFriendVo imFriendVo : list) {
            Map<String, ImFriendVo> map = new HashMap<>();
            //判斷是否有昵稱，如果有顯示昵稱
            String nickName = StrUtil.isEmpty(imFriendVo.getRemark()) ? imFriendVo.getNickName() : imFriendVo.getRemark();
            String pinyin = getPingYin(nickName);
            String sortString = pinyin.substring(0, 1).toUpperCase();

            if (sortString.matches("[A-Z]")) {
                map.put(sortString.toUpperCase(), imFriendVo);
            } else {
                map.put("#", imFriendVo);
            }
            mSortList.add(map);
        }

        for (String str : strArr) {
            Map<String, Object> tagMap = new HashMap<>();
            List<ImFriendVo> resultFriendList = new ArrayList<>();
            for (Map<String, ImFriendVo> map : mSortList) {
                Iterator<String> iter = map.keySet().iterator();

                while(iter.hasNext()){
                    String key = iter.next();
                    if (StrUtil.equals(str, key )) {
                        resultFriendList.add(map.get(key));
                    }
                }
            }
            if (CollUtil.isNotEmpty(resultFriendList)) {
                tagMap.put("index", str);
                tagMap.put("list", resultFriendList);
                resultList.add(tagMap);
            }
        }
       return  resultList;
    }**/


    /**
     * 获取拼音
     *
     * @param inputString
     * @return
     */
    public static String getPingYin(String inputString) {
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        format.setVCharType(HanyuPinyinVCharType.WITH_V);

        char[] input = inputString.trim().toCharArray();
        String output = "";

        try {
            for (char curChar : input) {
                if (Character.toString(curChar).matches("[\\u4E00-\\u9FA5]+")) {
                    String[] temp = PinyinHelper.toHanyuPinyinStringArray(curChar, format);
                    output += temp[0];
                } else
                    output += Character.toString(curChar);
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return output;
    }

    /**
     * 获取第一个字的拼音首字母
     * @param chinese
     * @return
     */
    public static String getFirstSpell(String chinese) {
        StringBuffer pinYinBF = new StringBuffer();
        char[] arr = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (char curChar : arr) {
            if (curChar > 128) {
                try {
                    String[] temp = PinyinHelper.toHanyuPinyinStringArray(curChar, defaultFormat);
                    if (temp != null) {
                        pinYinBF.append(temp[0].charAt(0));
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                pinYinBF.append(curChar);
            }
        }
        return pinYinBF.toString().replaceAll("\\W", "").trim();
    }
}
