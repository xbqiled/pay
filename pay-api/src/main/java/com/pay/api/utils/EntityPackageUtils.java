package com.pay.api.utils;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.pay.api.modules.im.entity.*;

import java.util.*;

import static com.pay.common.core.constant.PayConstant.*;


public class EntityPackageUtils {

    public static ImUserInfoEntity packageUserInfo(String userId, String nickName, String userName, String passwd, Long orgId) {
        ImUserInfoEntity saveModel = new ImUserInfoEntity();
        saveModel.setAddress("");
        saveModel.setBirthDay("");
        saveModel.setCardNumber("");

        saveModel.setIsOnline(0);
        saveModel.setStatus(0);
        saveModel.setUserType(1);
        saveModel.setSex(0);
        //默认给个头像
        saveModel.setUserAvatar(DEFAULT_USER_AVATAR);
        if (StrUtil.isNotBlank(userId)) {
            saveModel.setAccountId(userId);
        } else {
            saveModel.setAccountId(IM_SUFFIX + IdUtil.fastSimpleUUID());
        }

        saveModel.setNickName(nickName);
        saveModel.setUserName(userName);
        saveModel.setPassword(passwd);
        saveModel.setSign("");

        saveModel.setOrgId(orgId);
        saveModel.setCreateTime(new Date());
        saveModel.setCreateBy("USER");
        return saveModel;
    }


    public static ImUserInfoEntity packageCacUserInfo(String userId, String nickName, String userName, String passwd, Long orgId, String note) {
        ImUserInfoEntity saveModel = new ImUserInfoEntity();
        saveModel.setAddress("");
        saveModel.setBirthDay("");
        saveModel.setCardNumber("");

        saveModel.setIsOnline(0);
        saveModel.setStatus(0);
        saveModel.setUserType(1);
        saveModel.setSex(0);
        //默认给个头像
        saveModel.setUserAvatar(DEFAULT_USER_AVATAR);
        if (StrUtil.isNotBlank(userId)) {
            saveModel.setAccountId(userId);
        } else {
            saveModel.setAccountId(IM_SUFFIX + IdUtil.fastSimpleUUID());
        }

        saveModel.setNickName(nickName);
        saveModel.setUserName(userName);
        saveModel.setPassword(passwd);
        saveModel.setSign("");
        saveModel.setNote(note);
        saveModel.setOrgId(orgId);
        saveModel.setCreateTime(new Date());
        saveModel.setCreateBy("USER");
        return saveModel;
    }


    public static ImUserInfoEntity packageCsUserInfo(String userId, String nickName, String userName, String passwd, Long orgId, String avatar) {
        try {
            ImUserInfoEntity saveModel = new ImUserInfoEntity();
            saveModel.setAddress("");
            saveModel.setBirthDay("");
            saveModel.setCardNumber("");

            saveModel.setIsOnline(0);
            saveModel.setStatus(0);
            saveModel.setUserType(2);
            saveModel.setSex(0);
            //默认给个头像
            saveModel.setUserAvatar(StrUtil.isNotBlank(avatar) ? avatar : DEFAULT_USER_AVATAR);
            if (StrUtil.isNotBlank(userId)) {
                saveModel.setAccountId(userId);
            } else {
                saveModel.setAccountId(IM_SUFFIX + IdUtil.fastSimpleUUID());
            }

            saveModel.setNickName(nickName);
            saveModel.setUserName(userName);
            saveModel.setPassword(passwd);
            saveModel.setSign("");

            saveModel.setOrgId(orgId);
            saveModel.setCreateTime(new Date());
            saveModel.setCreateBy("USER");
            return saveModel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static ImOfflineMessageEntity packageOfflineUserMessage(Integer sendUserId, Integer receiveUserId,
                                                                   Integer msgType, String content, String messsageId, Date sendTime) {
        ImOfflineMessageEntity imOfflineMessageEntity = new ImOfflineMessageEntity();
        imOfflineMessageEntity.setSendUser(sendUserId.toString());
        imOfflineMessageEntity.setReceiveUser(receiveUserId.toString());

        imOfflineMessageEntity.setMessageType(msgType);
        imOfflineMessageEntity.setContent(content);
        imOfflineMessageEntity.setCreateBy("USER");

        imOfflineMessageEntity.setStatus(0);
        imOfflineMessageEntity.setIsRead(1);
        imOfflineMessageEntity.setMessageId(messsageId);

        imOfflineMessageEntity.setChatType(2);
        imOfflineMessageEntity.setCreateTime(sendTime);
        return imOfflineMessageEntity;
    }


    public static ImMessageEntity packageMessage(Integer sendUserId, Integer receiveUserId, Integer msgType, Integer chatType,
                                                 String content, Integer groupId, String messsageId, Date sendTime) {

        ImMessageEntity imMessageEntity = new ImMessageEntity();
        imMessageEntity.setSendUserId(sendUserId);
        imMessageEntity.setReceiveUserId(receiveUserId);

        imMessageEntity.setMessageType(msgType);
        imMessageEntity.setChatType(chatType);
        imMessageEntity.setContent(content);

        imMessageEntity.setCreateBy("USER");
        if (ObjectUtil.isNotNull(groupId)) {
            imMessageEntity.setGroupId(groupId);
        }

        imMessageEntity.setStatus(0);
        imMessageEntity.setIsRead(1);
        imMessageEntity.setMessageId(messsageId);

        imMessageEntity.setChatType(2);
        imMessageEntity.setCreateTime(sendTime);
        return imMessageEntity;
    }


    public static List<ImOfflineMessageEntity> packageOfflineGroupMessage(Integer sendUserId, Set<Integer> receiveUserList, Integer msgType,
                                                                          Integer chatType, String content, Integer groupId, String messsageId,
                                                                          Date sendTime) {
        List<ImOfflineMessageEntity> resultList = new ArrayList<>();
        if (CollUtil.isNotEmpty(receiveUserList)) {
            for (Integer userId : receiveUserList) {
                ImOfflineMessageEntity imOfflineMessageEntity = new ImOfflineMessageEntity();
                imOfflineMessageEntity.setSendUser(sendUserId.toString());
                imOfflineMessageEntity.setReceiveUser(userId.toString());

                imOfflineMessageEntity.setMessageType(msgType);
                imOfflineMessageEntity.setChatType(chatType);
                imOfflineMessageEntity.setContent(content);
                imOfflineMessageEntity.setCreateBy("USER");

                if (ObjectUtil.isNotNull(groupId)) {
                    imOfflineMessageEntity.setGroupId(groupId);
                }

                imOfflineMessageEntity.setStatus(0);
                imOfflineMessageEntity.setIsRead(1);
                imOfflineMessageEntity.setMessageId(messsageId);

                imOfflineMessageEntity.setCreateTime(sendTime);
                resultList.add(imOfflineMessageEntity);
            }
        }
        return resultList;
    }
}
