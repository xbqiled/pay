package com.pay.api.utils;


import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.auth.sts.AssumeRoleRequest;
import com.aliyuncs.auth.sts.AssumeRoleResponse;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.pay.common.core.oss.CloudStorageConfig;


public class AliOssUtils {

    //public static final String STS_API_VERSION = "2015-04-01";

    public static  AssumeRoleResponse getTonken(CloudStorageConfig cloudStorageConfig) {
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", cloudStorageConfig.getAliyunAccessKeyId(), cloudStorageConfig.getAliyunAccessKeySecret());
        DefaultAcsClient client = new DefaultAcsClient(profile);

        // 创建一个 AssumeRoleRequest 并设置请求参数
        final AssumeRoleRequest request = new AssumeRoleRequest();
        request.setSysMethod(MethodType.POST);

        request.setRoleArn("acs:ram::1589202110434482:role/aliyunosstokengeneratorrole");
        request.setRoleSessionName("external-username");
//        request.setPolicy("{\n" +
//                "    \"Statement\": [\n" +
//                "        {\n" +
//                "            \"Action\": \"sts:AssumeRole\",\n" +
//                "            \"Effect\": \"Allow\",\n" +
//                "            \"Principal\": {\n" +
//                "                \"RAM\": [\n" +
//                "                    \"acs:ram::1589202110434482:root\"\n" +
//                "                ]\n" +
//                "            }\n" +
//                "        }\n" +
//                "    ],\n" +
//                "    \"Version\": \"1\"\n" +
//                "}");
        // 发起请求，并得到response
        AssumeRoleResponse response = null;
        try {
            response = client.getAcsResponse(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  response;
    }

}
