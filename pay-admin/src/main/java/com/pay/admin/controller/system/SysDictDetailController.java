package com.pay.admin.controller.system;


import com.pay.api.modules.sys.criteria.SysDictDetailQueryCriteria;
import com.pay.api.modules.sys.entity.SysDictDetailEntity;
import com.pay.api.modules.sys.service.SysDictDetailService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
* @author Lion
* @date 2019-04-10
*/
@RestController
@RequestMapping("/api/dictDetail")
public class SysDictDetailController {

    @Reference(version = "${api.service.version}")
    private SysDictDetailService dictDetailService;

    private static final String ENTITY_NAME = "dictDetail";

    @Log("查询字典详情")
    @GetMapping
    public ResponseEntity getDictDetails(SysDictDetailQueryCriteria criteria, @PageableDefault(sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable){
        return new ResponseEntity<>(dictDetailService.queryDictDetailPage(criteria, pageable), HttpStatus.OK);
    }

    @Log("查询多个字典详情")
    @GetMapping(value = "/map")
    public ResponseEntity getDictDetailMaps(SysDictDetailQueryCriteria criteria, @PageableDefault(value = 10, sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable){
        String[] names = criteria.getDictName().split(",");
        Map map = new HashMap(names.length);
        for (String name : names) {
            Map<String, Object> params = new HashMap<>();
            params.put("page",pageable.getPageNumber());
            params.put("limit",pageable.getPageSize());
            params.put("dictName",name);
            map.put(name,dictDetailService.queryAll(name,pageable.getPageSize(),pageable.getPageNumber()).getList());
        }
        return new ResponseEntity(map,HttpStatus.OK);
    }


    @Log("新增字典详情")
    @PostMapping
    @PreAuthorize("@el.check('dict:add')")
    public ResponseEntity create(@Validated @RequestBody SysDictDetailEntity resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new ResponseEntity(dictDetailService.saveEntity(resources),HttpStatus.CREATED);
    }


    @Log("修改字典详情")
    @PutMapping
    @PreAuthorize("@el.check('dict:edit')")
    public ResponseEntity update( @RequestBody SysDictDetailEntity resources){
        dictDetailService.updateEntity(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @Log("删除字典详情")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('dict:del')")
    public ResponseEntity delete(@PathVariable Long id){
        dictDetailService.delEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}