package com.pay.admin.controller.system;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.sys.criteria.SysOrgChannelQueryCriteria;
import com.pay.api.modules.sys.dto.SysOrgChannelDTO;
import com.pay.api.modules.sys.service.SysOrgChannelService;
import com.pay.api.modules.sys.vo.SysOrgChannelVo;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-04-12 18:38:50
 */
@RestController
@RequestMapping("/api/orgchannel")
public class SysOrgChannelController {

    @Reference(version = "${api.service.version}")
    private SysOrgChannelService sysOrgChannelService;


    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('orgchl:list')")
    public void download(HttpServletResponse response, SysOrgChannelQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> resultList = sysOrgChannelService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }


    @Log("查询云商渠道权限")
    @GetMapping
    @PreAuthorize("@el.check('orgchl:list')")
    public ResponseEntity getOrgChannel(SysOrgChannelQueryCriteria criteria, Pageable pageable) {
        // 数据权限
        return new ResponseEntity(sysOrgChannelService.queryOrgChannelPage(criteria, pageable), HttpStatus.OK);
    }


    @Log("配置云商渠道权限")
    @PostMapping
    @PreAuthorize("@el.check('orgchl:config')")
    public ResponseEntity doConfig(@RequestBody SysOrgChannelDTO resources) {
        if (resources.getChannelId() == null || resources.getOrgId() == null) {
            throw new BadRequestException("参数错误");
        }
        return new ResponseEntity(sysOrgChannelService.doConfig(resources.getChannelId(), resources.getOrgId()), HttpStatus.CREATED);
    }


    @GetMapping(value = "/getConfig")
    public ResponseEntity getConfig() throws IOException {
        List<SysOrgChannelVo> resultList = sysOrgChannelService.getConfigByOrgId(SecurityUtils.getUserDetails().getOrgId());
        return new ResponseEntity(resultList, HttpStatus.OK);
    }


    @GetMapping(value = "/getAllConfig/{orgId}")
    public ResponseEntity getAllConfig(@PathVariable Long orgId) throws IOException {
        return new ResponseEntity(sysOrgChannelService.getAllConfigOrgId(orgId), HttpStatus.OK);
    }


    @Log("删除鉴权渠道")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('orgchl:del')")
    public ResponseEntity delete(@PathVariable Long id) {
        return new ResponseEntity(sysOrgChannelService.delById(id), HttpStatus.OK);
    }

}
