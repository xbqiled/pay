package com.pay.admin.controller.quartz;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.api.modules.quartz.service.QuartzJobService;
import com.pay.api.modules.quartz.service.QuartzLogService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @date 2019-01-07
 */
@Slf4j
@RestController
@RequestMapping("/api/jobs")
public class QuartzJobController {

    @Reference(version = "${api.service.version}")
    private QuartzJobService quartzJobService;

    @Reference(version = "${api.service.version}")
    private QuartzLogService quartzLogService;

    private static final String ENTITY_NAME = "quartzJob";


    @Log("查询定时任务")
    @GetMapping
    @PreAuthorize("@el.check('timing:list')")
    public ResponseEntity getJobs(JobQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(quartzJobService.queryQuartzJobPage(criteria, pageable), HttpStatus.OK);
    }


    @Log("查询任务日志数据")
    @GetMapping(value = "/logs")
    @PreAuthorize("@el.check('timing:list')")
    public ResponseEntity getJobLogs(JobQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity<>(quartzLogService.queryQuartzLogPage(criteria, pageable), HttpStatus.OK);
    }


    @Log("导出任务数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('timing:list')")
    public void download(HttpServletResponse response, JobQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> resultList = quartzJobService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }


    @Log("导出日志数据")
    @GetMapping(value = "/download/log")
    @PreAuthorize("@el.check('timing:list')")
    public void downloadLog(HttpServletResponse response, JobQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> resultList = quartzLogService.downloadLog(criteria);
        FileUtil.downloadExcel(resultList, response);
    }


    @Log("新增定时任务")
    @PostMapping
    @PreAuthorize("@el.check('timing:add')")
    public ResponseEntity create(@Validated @RequestBody QuartzJobEntity resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new ResponseEntity<>(quartzJobService.saveEntity(resources), HttpStatus.CREATED);
    }


    @Log("修改定时任务")
    @PutMapping
    @PreAuthorize("@el.check('timing:edit')")
    public ResponseEntity update(@Validated(QuartzJobEntity.Update.class) @RequestBody QuartzJobEntity resources){
        quartzJobService.updateEntity(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @Log("更改定时任务状态")
    @PutMapping(value = "/{id}")
    @PreAuthorize("@el.check('timing:edit')")
    public ResponseEntity updateIsPause(@PathVariable Long id){
        quartzJobService.updateIsPause(quartzJobService.findById(id));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @Log("执行定时任务")
    @PutMapping(value = "/exec/{id}")
    @PreAuthorize("@el.check('timing:edit')")
    public ResponseEntity execution(@PathVariable Long id){
        quartzJobService.execution(quartzJobService.findById(id));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @Log("删除定时任务")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('timing:del')")
    public ResponseEntity delete(@PathVariable Long id){
        quartzJobService.deleteEntity(quartzJobService.findById(id));
        return new ResponseEntity(HttpStatus.OK);
    }
}
