package com.pay.admin.controller.trader;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.trader.criteria.RechargeRecordQueryCriteria;
import com.pay.api.modules.trader.service.TraderRechargeRecordService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
@RestController
@RequestMapping("/api/rechargeRecord")
public class TraderRechargeRecordController {

    @Reference(version = "${api.service.version}")
    private TraderRechargeRecordService rechargeRecordService;

    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('rechargeRecord:list')")
    public void download(HttpServletResponse response, RechargeRecordQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> download = rechargeRecordService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }

    /**
     * 列表
     */
    @GetMapping
    @PreAuthorize("@el.check('rechargeRecord:list')")
    public ResponseEntity queryRechargeRecord(RechargeRecordQueryCriteria criteria, Pageable pageable) {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(rechargeRecordService.queryRechargeRecordPage(criteria, pageable), HttpStatus.OK);
    }

}
