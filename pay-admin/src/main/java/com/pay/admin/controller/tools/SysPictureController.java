package com.pay.admin.controller.tools;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.sys.entity.SysPictureEntity;
import com.pay.api.modules.sys.service.SysPictureService;
import com.pay.api.modules.tools.criteria.PictureQueryCriteria;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.oss.CloudStorageConfig;
import com.pay.common.core.utils.FileUtil;
import com.pay.common.core.utils.OSSFactory;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @date 2018/09/20 14:13:32
 */
@RestController
@RequestMapping("/api/pictures")
public class SysPictureController {

    @Reference(version = "${api.service.version}")
    private SysPictureService pictureService;

    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;

    @Log("查询图片")
    @GetMapping
    @PreAuthorize("@el.check('pictures:list')")
    public ResponseEntity getPictures(PictureQueryCriteria criteria, Pageable pageable) {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(pictureService.queryPage(criteria, pageable), HttpStatus.OK);
    }

    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('pictures:list')")
    public void download(HttpServletResponse response, PictureQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> reusltList = pictureService.download(criteria);
        FileUtil.downloadExcel(reusltList, response);
    }

    /**
     * 上传图片
     *
     * @param file
     * @return
     * @throws Exception
     */
    @Log("上传图片")
    @PreAuthorize("@el.check('pictures:add')")
    @PostMapping(value = "/upload")
    public ResponseEntity upload(@RequestParam MultipartFile file) throws IOException {

        Map map = new HashMap(3);
        try {
            BufferedImage image = ImageIO.read(file.getInputStream());
            if (image == null) {
                throw new Exception("此文件不是一个图片");
            }
            SysPictureEntity picture = new SysPictureEntity();
            picture.setWidth(image.getWidth() + "");
            picture.setHeight(image.getHeight() + "");
            picture.setUsername(SecurityUtils.getUsername());
            picture.setFilename(file.getOriginalFilename());
            picture.setSize(FileUtil.getSize(file.getSize()));
            picture.setCreateTime(new Date());
            picture.setOrgId(SecurityUtils.getJwtUser().getOrgId());
            CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
            String url = OSSFactory.build(config).uploadSuffix(file.getBytes(), FileUtil.getExtensionName(file.getOriginalFilename()));
            if (url != null) {
                picture.setUrl(url);
                pictureService.insert(picture);
                map.put("msg", "上传成功");
                map.put("url", url);
            }
        } catch (Exception e) {
            map.put("errno", 1);
            map.put("msg", e.getMessage());
        }
        return new ResponseEntity(map, HttpStatus.OK);
    }

    /**
     * 删除图片
     *
     * @param id
     * @return
     */
    @Log("删除图片")
    @PreAuthorize("@el.check('pictures:del')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        pictureService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 删除多张图片
     *
     * @param ids
     * @return
     */
    @Log("多选删除图片")
    @PreAuthorize("@el.check('pictures:del')")
    @DeleteMapping
    public ResponseEntity deleteAll(@RequestBody List<Long> ids) {
        pictureService.deleteAll(ids);
        return new ResponseEntity(HttpStatus.OK);
    }
}
