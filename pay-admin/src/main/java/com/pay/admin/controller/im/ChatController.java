package com.pay.admin.controller.im;


import cn.hutool.core.util.ObjectUtil;
import com.pay.api.modules.im.dto.ChatHisDto;
import com.pay.api.modules.im.dto.UserChatHisDto;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.ImMessageService;
import com.pay.api.modules.im.service.ImUserInfoService;
import com.pay.api.modules.im.vo.ImMessageVo;
import com.pay.api.modules.monitor.service.RedisService;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.common.core.annotation.AnonymousAccess;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.exception.AppException;
import com.pay.common.core.oss.CloudStorageConfig;
import com.pay.common.core.utils.OSSFactory;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Api(tags = "聊天接口")
@RestController
@RequestMapping("/api/chatApi")
public class ChatController {

    //用户信息远程服务
    @Reference(version = "${api.service.version}")
    private ImUserInfoService imUserInfoService;

    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;

    @Reference(version = "${api.service.version}")
    private ImMessageService imMessageService;

    @Reference(version = "${api.service.version}")
    private RedisService redisService;

    @Autowired
    RedisTemplate redisTemplate;

    @AnonymousAccess
    @ApiOperation(value = "获取聊天用户列表历史信息", notes = "获取聊天用户列表历史信息")
    @RequestMapping(value = "/getChatHis", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getChatHis(@RequestBody ChatHisDto chatHisDto) {
        //判断请求参数
        if (chatHisDto.getOrgId() == null || ObjectUtil.isEmpty(chatHisDto.getToken()) || ObjectUtil.isEmpty(chatHisDto.getOrgId())) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = this.findUserInfoByToken(chatHisDto.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        List<ImMessageVo> list = imMessageService.getMessageHisPage(userInfo.getId(), chatHisDto.getPage().getTimeOrder());
        return R.ok("获取用户消息列表信息成功!").put("data", list);
    }


    @AnonymousAccess
    @ApiOperation(value = "获取聊天用户历史信息", notes = "获取聊天用户历史信息")
    @RequestMapping(value = "/getUserChatHis", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    private R getUserChatHis (@RequestBody UserChatHisDto userChatHisDto) {
        //判断请求参数
        if (userChatHisDto.getUserId() == null || ObjectUtil.isEmpty(userChatHisDto.getToken()) || userChatHisDto.getPage() == null) {
            return R.parmError("请求参数错误!");
        }

        ImUserInfoEntity userInfo = this.findUserInfoByToken(userChatHisDto.getToken());
        if (userInfo == null) {
            return R.tokenError("您已经在其它地方登录!");
        }

        PageUtils page = imMessageService.getUserMessageHisPage(Integer.valueOf(userChatHisDto.getUserId()), userInfo.getId(), userChatHisDto.getPage().getPageSize(),
                userChatHisDto.getPage().getPageIndex(), userChatHisDto.getPage().getTimeOrder());
        return R.ok("获取用户消息列表信息成功!").put("data", page);
    }


    @AnonymousAccess
    @ApiOperation(value = "图片上传接口", notes = "图片上传接口")
    @PostMapping(value = "/imgUpload")
    public R imgUpload(@RequestParam MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new AppException("上传文件不能为空");
        }
        //获取oss配置信息
        CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
        String url = "";
        try {
            //上传文件
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //返回文件的URL
            url = OSSFactory.build(config).uploadSuffix(file.getBytes(), suffix);
            return R.ok().put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @AnonymousAccess
    @ApiOperation(value = "文件上传接口", notes = "文件上传接口")
    @RequestMapping(value = "/fileUpload", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public R fileUpload(@RequestParam("im_file") MultipartFile file, HttpServletRequest request) {
        if (file.isEmpty()) {
            throw new AppException("上传文件不能为空");
        }
        //获取oss配置信息
        CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
        String url = "";
        try {
            //上传文件
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //返回文件的URL
            url = OSSFactory.build(config).uploadSuffix(file.getBytes(), suffix);
            return R.ok().put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public ImUserInfoEntity findUserInfoByToken(String token) {
        ImUserInfoEntity userInfo  = (ImUserInfoEntity)redisTemplate.opsForValue().get("token_" + token);
        return userInfo;
    }
}
