package com.pay.admin.controller.monitor;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.monitor.service.SysVisitsService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Lion
 * @date 2018-12-13
 */
@RestController
@RequestMapping("api")
public class VisitsController {

    @Reference(version = "${api.service.version}")
    private SysVisitsService visitsService;

    @PostMapping(value = "/visits")
    public ResponseEntity create(){
        visitsService.count();
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/visits")
    public ResponseEntity get(){
        return new ResponseEntity(visitsService.get(SecurityUtils.getJwtUser().getOrgId()),HttpStatus.OK);
    }

    @GetMapping(value = "/visits/chartData")
    public ResponseEntity getChartData(){
        return new ResponseEntity(visitsService.getChartData(SecurityUtils.getJwtUser().getOrgId()),HttpStatus.OK);
    }
}
