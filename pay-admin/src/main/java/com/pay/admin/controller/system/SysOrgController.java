package com.pay.admin.controller.system;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.security.service.DataScopeService;
import com.pay.api.modules.sys.criteria.SysOrgsQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgEntity;
import com.pay.api.modules.sys.service.SysOrgService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* @author Lion
* @date 2019-03-25
*/
@RestController
@RequestMapping("/api/org")
public class SysOrgController {

    @Reference(version = "${api.service.version}")
    private SysOrgService sysOrgService;

    @Reference(version = "${api.service.version}")
    private DataScopeService  dataScopeService;

    private static final String ENTITY_NAME = "dept";


    @Log("导出部门数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('org:list')")
    public void download(HttpServletResponse response, SysOrgsQueryCriteria criteria) throws IOException {
        List<Map<String , Object>> resultList = sysOrgService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }


    @Log("查询机构")
    @GetMapping
    //@PreAuthorize("@el.check('user:list','org:list')")
    public ResponseEntity getOrgs(SysOrgsQueryCriteria criteria){
        // 数据权限
        //criteria.setIds(dataScopeService.getOrgIds(SecurityUtils.getUsername()));
        List<SysOrgEntity> sysOrgEntityList = sysOrgService.queryAll(criteria);
        return new ResponseEntity(sysOrgService.buildTree(sysOrgEntityList), HttpStatus.OK);
    }

    @Log("新增部门")
    @PostMapping
    @PreAuthorize("@el.check('org:add')")
    public ResponseEntity create(@Validated @RequestBody SysOrgEntity resources) {
        if (resources.getId() != null) {
            throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
        }
        return new ResponseEntity(sysOrgService.saveEntity(resources), HttpStatus.CREATED);
    }


    @Log("修改部门")
    @PutMapping
    @PreAuthorize("@el.check('org:edit')")
    public ResponseEntity update(@RequestBody SysOrgEntity resources){
        sysOrgService.updateEntity(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @Log("删除部门")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('org:del')")
    public ResponseEntity delete(@PathVariable Long id){
        return new ResponseEntity(sysOrgService.deleteEntity(id), HttpStatus.OK);
    }
}