package com.pay.admin.controller.security;

import cn.hutool.core.util.IdUtil;
import com.pay.admin.security.AuthenticationInfo;
import com.pay.admin.security.AuthorizationUser;
import com.pay.admin.security.ImgResult;
import com.pay.admin.security.JwtUserDetailsService;
import com.pay.admin.utils.JwtTokenUtil;
import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.monitor.service.RedisService;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.api.modules.security.service.OnlineUserService;
import com.pay.common.core.annotation.AnonymousAccess;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.EncryptUtils;
import com.pay.common.core.utils.StringUtils;
import com.wf.captcha.ArithmeticCaptcha;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Lion
 * @date 2018-11-23
 * 授权、根据token获取用户详细信息
 */
@Slf4j
@RestController
@RequestMapping("auth")
public class AuthenticationController {

    @Value("${jwt.codeKey}")
    private String codeKey;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.online}")
    private String onlineKey;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Qualifier("jwtUserDetailsService")
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Reference(version = "${api.service.version}")
    private RedisService redisService;

    @Reference(version = "${api.service.version}")
    private OnlineUserService onlineUserService;

    /**
     * 登录授权
     * @param authorizationUser
     * @return
     */
    @ApiOperation("登录授权")
    @AnonymousAccess
    @PostMapping(value = "/login")
    public ResponseEntity login(@Validated @RequestBody AuthorizationUser authorizationUser, HttpServletRequest request){
        // 查询验证码
        String code = redisService.getCodeVal(authorizationUser.getUuid());
        // 清除验证码
        redisService.delete(authorizationUser.getUuid());
        if (StringUtils.isBlank(code)) {
            throw new BadRequestException("验证码已过期");
        }

        if (StringUtils.isBlank(authorizationUser.getCode()) || !authorizationUser.getCode().equalsIgnoreCase(code)) {
            throw new BadRequestException("验证码错误");
        }

        final JwtUser jwtUser = jwtUserDetailsService.loadUserByUsername(authorizationUser.getUsername());
        if (jwtUser == null) {
            throw new BadRequestException("账户不存在或密码错误");
        }

        if(!jwtUser.getPassword().equals(EncryptUtils.encryptPassword(authorizationUser.getPassword()))){
            throw new BadRequestException("密码错误");
        }

        if(!jwtUser.isEnabled()){
            throw new AccountExpiredException("账号已停用，请联系管理员");
        }

        // 生成令牌
        String job = jwtUser.getOrgName() + "/" + jwtUser.getJob();
        Long orgId = jwtUser.getOrgId();
        String ip = StringUtils.getIp(request);
        String browser = StringUtils.getBrowser(request);
        final String token = jwtTokenUtil.generateToken(jwtUser);

        // 保存在线信息
        onlineUserService.saveOnlineUser(token, orgId, job, ip, browser, jwtUser.getUsername(), onlineKey, expiration);
        // 返回token
        return ResponseEntity.ok(new AuthenticationInfo(token,jwtUser));
    }

    /**
     * 获取用户信息
     * @return
     */
    @ApiOperation("获取用户信息")
    @GetMapping(value = "/info")
    public ResponseEntity getUserInfo(){
       JwtUser jwtUser = (JwtUser)jwtUserDetailsService.loadUserByUsername(SecurityUtils.getUsername());
       return ResponseEntity.ok(jwtUser);
    }

    /**
     * 获取验证码
     */
    @ApiOperation("获取验证码")
    @AnonymousAccess
    @GetMapping(value = "/code")
    public ImgResult getCode(HttpServletResponse response) throws IOException {
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(150, 36);
        // 几位数运算，默认是两位
        captcha.setLen(3);
        // 获取运算的结果：5
        String result = captcha.text();
        String uuid = codeKey + IdUtil.simpleUUID();
        redisService.saveCode(uuid,result);
        return new ImgResult(captcha.toBase64(),uuid);
    }


    @ApiOperation("退出登录")
    @AnonymousAccess
    @DeleteMapping(value = "/logout")
    public ResponseEntity logout(HttpServletRequest request){
        onlineUserService.logout(jwtTokenUtil.getToken(request), onlineKey);
        return new ResponseEntity(HttpStatus.OK);
    }
}
