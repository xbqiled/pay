package com.pay.admin.controller.trader;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.api.modules.trader.criteria.ReplyConfigQueryCriteria;
import com.pay.api.modules.trader.dto.ReplyConfigDto;
import com.pay.api.modules.trader.service.TraderReplyConfigService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:09
 */
@RestController
@RequestMapping("/api/replyConfig")
public class TraderReplyConfigController {

    @Reference(version = "${api.service.version}")
    private TraderReplyConfigService replyConfigService;


    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('replyConfig:list')")
    public void download(HttpServletResponse response, ReplyConfigQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> download = replyConfigService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }


    /**
     * 列表
     */
    @GetMapping
    @PreAuthorize("@el.check('replyConfig:list')")
    public ResponseEntity queryReplyConfig(ReplyConfigQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(replyConfigService.queryReplyConfigPage(criteria, pageable), HttpStatus.OK);
    }


    /**
     * 保存
     */
    @Log("新增支付配置")
    @PostMapping
    @PreAuthorize("@el.check('replyConfig:add')")
    public ResponseEntity save(@RequestBody ReplyConfigDto replyConfigDto) {
        JwtUser user = SecurityUtils.getJwtUser();
        return new ResponseEntity(replyConfigService.saveEntity(replyConfigDto, user), HttpStatus.OK);
    }

    /**
     * 修改
     */
    @Log("修改支付配置")
    @PutMapping
    @PreAuthorize("@el.check('replyConfig:edit')")
    public ResponseEntity update(@RequestBody ReplyConfigDto replyConfigDto){
        JwtUser user = SecurityUtils.getJwtUser();
        return new ResponseEntity(replyConfigService.updateEntity(replyConfigDto, user), HttpStatus.OK);
    }

    /**
     * 删除
     */
    @Log("删除支付配置")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('replyConfig:del')")
    public ResponseEntity delete(@PathVariable Long id){
        replyConfigService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
