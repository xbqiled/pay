package com.pay.admin.controller.tools;

import com.pay.api.modules.tools.entity.SysEmailConfigEntity;
import com.pay.api.modules.tools.service.SysEmailConfigService;
import com.pay.api.modules.tools.vo.EmailVo;
import com.pay.common.core.annotation.Log;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 发送邮件
 * @date 2018/09/28 6:55:53
 */
@Slf4j
@RestController
@RequestMapping("/api/email")
public class SysEmailController {

    @Reference(version = "${api.service.version}")
    private SysEmailConfigService sysEmailConfigService;

    @GetMapping
    public ResponseEntity get(){
        return new ResponseEntity(sysEmailConfigService.find(),HttpStatus.OK);
    }

    @Log("配置邮件")
    @PostMapping(value = "/config")
    public ResponseEntity emailConfig(@Validated @RequestBody SysEmailConfigEntity emailConfig){
        sysEmailConfigService.update(emailConfig);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Log("发送邮件")
    @PostMapping(value = "/send")
    public ResponseEntity send(@Validated @RequestBody EmailVo emailVo) throws Exception {
        log.warn("REST request to send Email : {}" +emailVo);
        return new ResponseEntity(sysEmailConfigService.send(emailVo),HttpStatus.OK);
    }
}
