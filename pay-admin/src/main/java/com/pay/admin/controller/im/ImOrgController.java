package com.pay.admin.controller.im;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.im.criteria.ImOrgQueryCriteria;
import com.pay.api.modules.im.entity.ImOrgEntity;
import com.pay.api.modules.im.service.ImOrgService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Howard
 * @date 2020-01-31
 */
@RestController
@RequestMapping("api/imOrg")
public class ImOrgController {

    @Reference(version = "${api.service.version}")
    private ImOrgService imOrgService;

    @Log("查询银商信情数据")
    @PreAuthorize("@el.check('orgInfo:list')")
    @GetMapping
    public ResponseEntity getImOrgs(ImOrgQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(imOrgService.queryOrgPage(criteria,pageable),HttpStatus.OK);
    }


    @Log("导出银商信情数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('orgInfo:list')")
    public void download(HttpServletResponse response, ImOrgQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String , Object>> resultList = imOrgService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }

    @Log("修改银商数据信息")
    @PutMapping
    @PreAuthorize("@el.check('orgInfo:edit')")
    public ResponseEntity update( @RequestBody ImOrgEntity imOrgEntity){
        imOrgService.updateEntity(imOrgEntity);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}