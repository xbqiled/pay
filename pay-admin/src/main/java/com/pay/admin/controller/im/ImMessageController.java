package com.pay.admin.controller.im;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.im.criteria.ImMessageQueryCriteria;
import com.pay.api.modules.im.service.ImMessageService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lion
 * @date 2020-01-31
 */
@RestController
@RequestMapping("api/imMessage")
public class ImMessageController {

    @Reference(version = "${api.service.version}")
    private ImMessageService imMessageService;

    @Log("分页查询")
    @GetMapping
    @PreAuthorize("@el.check('chat:list')")
    public ResponseEntity queryPage(ImMessageQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(imMessageService.queryMessagePage(criteria,pageable),HttpStatus.OK);
    }

    @Log("导出")
    @GetMapping(value = "/download")
    public ResponseEntity download(ImMessageQueryCriteria criteria){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(imMessageService.download(criteria),HttpStatus.OK);
    }

}