package com.pay.admin.controller.im;


import com.pay.api.modules.im.criteria.ImNoticeQueryCriteria;
import com.pay.api.modules.im.entity.ImNoticeEntity;
import com.pay.api.modules.im.service.ImNoticeService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
* @author lion
* @date 2019-10-01
*/
@RestController
@RequestMapping("api")
public class ImNoticeController {

    @Reference(version = "${api.service.version}")
    private ImNoticeService imNoticeService;

    @Log("查询ImNotice")
    @GetMapping(value = "/imNotice")
    public ResponseEntity getImNotices(ImNoticeQueryCriteria criteria){
        return new ResponseEntity(imNoticeService.queryAll(criteria),HttpStatus.OK);
    }


    @Log("新增ImNotice")
    @PostMapping(value = "/imNotice")
    public ResponseEntity create(@Validated @RequestBody ImNoticeEntity resources){
        return new ResponseEntity(imNoticeService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改ImNotice")
    @PutMapping(value = "/imNotice")
    public ResponseEntity update(@Validated @RequestBody ImNoticeEntity resources){
        imNoticeService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除ImNotice")
    @DeleteMapping(value = "/imNotice/{id}")
    public ResponseEntity delete(@PathVariable Integer id){
        imNoticeService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}