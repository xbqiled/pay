package com.pay.admin.controller.security;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.security.service.OnlineUserService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/auth/online")
public class OnlineController {

    @Value("${jwt.online}")
    private String onlineKey;

    @Reference(version = "${api.service.version}")
    private OnlineUserService onlineUserService;


    @GetMapping
    @PreAuthorize("@el.check('online:list')")
    public ResponseEntity getAll(String filter, Pageable pageable){
        Long orgId = SecurityUtils.getUserOrg();
        return new ResponseEntity<>(onlineUserService.getAll(filter, orgId, onlineKey, pageable),HttpStatus.OK);
    }


    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('online:list')")
    public void download(HttpServletResponse response, String filter) throws IOException {
        Long orgId = SecurityUtils.getUserOrg();
        List<Map<String, Object>> resultList = onlineUserService.download(filter, orgId, onlineKey);
        FileUtil.downloadExcel(resultList, response);
    }


    @DeleteMapping(value = "/{key}")
    @PreAuthorize("@el.check('online:kick')")
    public ResponseEntity delete(@PathVariable String key) throws Exception {
        onlineUserService.kickOut(key, onlineKey);
        return new ResponseEntity(HttpStatus.OK);
    }
}
