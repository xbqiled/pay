package com.pay.admin.controller.system;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.security.service.DataScopeService;
import com.pay.api.modules.sys.criteria.SysUserQueryCriteria;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.entity.SysPictureEntity;
import com.pay.api.modules.sys.entity.SysUserEntity;
import com.pay.api.modules.sys.service.SysOrgService;
import com.pay.api.modules.sys.service.SysPictureService;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.api.modules.sys.vo.SysUserPassVo;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.oss.CloudStorageConfig;
import com.pay.common.core.utils.EncryptUtils;
import com.pay.common.core.utils.FileUtil;
import com.pay.common.core.utils.OSSFactory;
import com.pay.common.core.utils.PageUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Lion
 * @date 2018-11-23
 */
@RestController
@RequestMapping("/api/users")
public class SysUserController {

    @Reference(version = "${api.service.version}")
    private SysUserService userService;

    @Reference(version = "${api.service.version}")
    private SysPictureService pictureService;

    @Reference(version = "${api.service.version}")
    private DataScopeService dataScopeService;

    @Reference(version = "${api.service.version}")
    private SysOrgService sysOrgService;

    @Reference(version = "${api.service.version}")
    private SysRoleService roleService;

    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;


    @Log("导出用户数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('user:list')")
    public void download(HttpServletResponse response, SysUserQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> reusultList = userService.download(criteria);
        FileUtil.downloadExcel(reusultList, response);
    }


    @Log("查询用户")
    @GetMapping
    @PreAuthorize("@el.check('user:list')")
    public ResponseEntity getUsers(SysUserQueryCriteria criteria, Pageable pageable){
        Set<Long> deptSet = new HashSet<>();
        Set<Long> result = new HashSet<>();
        PageUtils pageUtils = null;
        if (!ObjectUtils.isEmpty(criteria.getOrgId())) {
            deptSet.add(criteria.getOrgId());
            deptSet.addAll(dataScopeService.getOrgChildren(sysOrgService.findByPid(criteria.getOrgId())));
        }
        // 数据权限
        Set<Long> deptIds = dataScopeService.getOrgIds(SecurityUtils.getUsername());
        // 查询条件不为空并且数据权限不为空则取交集
        if (!CollectionUtils.isEmpty(deptIds) && !CollectionUtils.isEmpty(deptSet)){
            // 取交集
            result.addAll(deptSet);
            result.retainAll(deptIds);

            // 若无交集，则代表无数据权限
            criteria.setDeptIds(result);
            if(result.size() == 0){
                return new ResponseEntity(new PageUtils(null,0,0,0),HttpStatus.OK);
            } else {
                pageUtils = userService.queryUserPage(criteria, pageable);
            }
        // 否则取并集
        } else {
            result.addAll(deptSet);
            result.addAll(deptIds);
            criteria.setDeptIds(result);
            pageUtils = userService.queryUserPage(criteria, pageable);
        }
        return new ResponseEntity(pageUtils, HttpStatus.OK);
    }

    @Log("新增用户")
    @PostMapping
    @PreAuthorize("@el.check('user:add')")
    public ResponseEntity create(@Validated @RequestBody SysUserDTO userDTO) {
        return new ResponseEntity(userService.saveEntity(userDTO), HttpStatus.OK);
    }

    @Log("修改用户")
    @PutMapping
    @PreAuthorize("@el.check('user:edit')")
    public ResponseEntity update(@RequestBody SysUserDTO userDTO) {
        return new ResponseEntity(userService.updateEntity(userDTO), HttpStatus.OK);
    }

    @Log("删除用户")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('user:del')")
    public ResponseEntity delete(@PathVariable Long id){
        Integer currentLevel =  Collections.min(roleService.findByUsers_Id(SecurityUtils.getUserId()).stream().map(SysRoleSmallDTO::getLevel).collect(Collectors.toList()));
        Integer optLevel =  Collections.min(roleService.findByUsers_Id(id).stream().map(SysRoleSmallDTO::getLevel).collect(Collectors.toList()));

        if (currentLevel > optLevel) {
            throw new BadRequestException("角色权限不足");
        }
        userService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 修改密码
     * @param user
     * @return
     */
    @PostMapping(value = "/updatePass")
    public ResponseEntity updatePass(@RequestBody SysUserPassVo user){
        UserDetails userDetails = SecurityUtils.getUserDetails();

        if(!userDetails.getPassword().equals(EncryptUtils.encryptPassword(user.getOldPass()))){
            throw new BadRequestException("修改失败，旧密码错误");
        }
        if(userDetails.getPassword().equals(EncryptUtils.encryptPassword(user.getNewPass()))){
            throw new BadRequestException("新密码不能与旧密码相同");
        }
        userService.updatePass(SecurityUtils.getUserDetails().getId(),EncryptUtils.encryptPassword(user.getNewPass()));
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 修改头像
     * @param file
     * @return
     */
    @PostMapping(value = "/updateAvatar")
    public ResponseEntity updateAvatar(@RequestParam MultipartFile file){
        Map<String,Object> map = new HashMap(3);
        map.put("flag",true);
        try{
            BufferedImage image = ImageIO.read(file.getInputStream());
            if (image == null) {
                throw new Exception("此文件不是一个图片");
            }
            SysPictureEntity picture = new SysPictureEntity();
            picture.setWidth(image.getWidth()+"");
            picture.setHeight(image.getHeight()+"");
            picture.setUsername(SecurityUtils.getUsername());
            picture.setFilename(file.getOriginalFilename());
            picture.setSize(FileUtil.getSize(file.getSize()));
            picture.setCreateTime(new Date());
            picture.setOrgId(SecurityUtils.getJwtUser().getOrgId());
            CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
            String url = OSSFactory.build(config).uploadSuffix(file.getBytes(), FileUtil.getExtensionName(file.getOriginalFilename()));
            if(url != null){
                picture.setUrl(url);
                pictureService.insert(picture);
                userService.updateAvatar(SecurityUtils.getUsername(),picture.getUrl());
                map.put("msg", "图片上传成功");
                map.put("url", url);
            }
        }catch (Exception e){
            map.put("flag",false);
            map.put("msg", e.getMessage());
        }
        return new ResponseEntity(map, HttpStatus.OK);

    }

    /**
     * 修改邮箱
     * @param user
     * @param user
     * @return
     */
    @Log("修改邮箱")
    @PostMapping(value = "/updateEmail/{code}")
    public ResponseEntity updateEmail(@PathVariable String code,@RequestBody SysUserEntity user){
        UserDetails userDetails = SecurityUtils.getUserDetails();
//        VerificationCode verificationCode = new VerificationCode(code, LargehatConstant.RESET_MAIL,"email",user.getEmail());
//        verificationCodeService.validated(verificationCode);
        userService.updateEmail(userDetails.getUsername(),user.getEmail());
        return new ResponseEntity(HttpStatus.OK);
    }



    /**
     * 如果当前用户的角色级别低于创建用户的角色级别，则抛出权限不足的错误
     * @param resources
     */
    private void checkLevel(SysUserDTO resources) {
        Integer currentLevel =  Collections.min(roleService.findByUsers_Id(SecurityUtils.getUserId()).stream().map(SysRoleSmallDTO::getLevel).collect(Collectors.toList()));
        Integer optLevel = roleService.findByRoles(resources.getRoles());
        if (currentLevel > optLevel) {
            throw new BadRequestException("角色权限不足");
        }
    }
}
