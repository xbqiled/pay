package com.pay.admin.controller.im;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.im.criteria.ImChannelQueryCriteria;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.api.modules.im.service.ImChannelService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Lion
 * @date 2020-01-31
 */
@RestController
@RequestMapping("api/imChannel")
public class ImChannelController {

    @Reference(version = "${api.service.version}")
    private ImChannelService imChannelService;

    @Log("查询渠道数据信息")
    @PreAuthorize("@el.check('channel:list')")
    @GetMapping
    public ResponseEntity getChannel(ImChannelQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getJwtUser().getOrgId());
        return new ResponseEntity(imChannelService.queryChannelPage(criteria,pageable),HttpStatus.OK);
    }


    @Log("导出渠道数据信息")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('channel:list')")
    public void download(HttpServletResponse response, ImChannelQueryCriteria criteria) throws IOException {
        List<Map<String , Object>> resultList = imChannelService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }

    @Log("添加渠道数据信息")
    @PostMapping
    @PreAuthorize("@el.check('channel:add')")
    public ResponseEntity save(@Validated @RequestBody ImChannelEntity imChannelEntity) {
        Boolean result = imChannelService.saveEntity(imChannelEntity, SecurityUtils.getUserId());
        return new ResponseEntity(result, HttpStatus.CREATED);
    }


    @Log("修改渠道数据信息")
    @PutMapping
    @PreAuthorize("@el.check('channel:edit')")
    public ResponseEntity update( @RequestBody ImChannelEntity imChannelEntity){
        Boolean result = imChannelService.updateEntity(imChannelEntity, SecurityUtils.getUserId());
        return new ResponseEntity(result, HttpStatus.NO_CONTENT);
    }


    @Log("删除渠道数据信息")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('channel:del')")
    public ResponseEntity delete(@PathVariable Long id){
        imChannelService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }


    @Log("刷新生成新的鉴权码")
    @PostMapping(value = "/refresh/{id}")
    @PreAuthorize("@el.check('channel:refresh')")
    public ResponseEntity refresh(@PathVariable Long id){
        imChannelService.refreshAuth(id, SecurityUtils.getUserId());
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}