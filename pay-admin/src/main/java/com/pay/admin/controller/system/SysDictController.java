package com.pay.admin.controller.system;


import com.pay.api.modules.sys.criteria.SysDictQueryCriteria;
import com.pay.api.modules.sys.entity.SysDictEntity;
import com.pay.api.modules.sys.service.SysDictService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* @author Lion
* @date 2019-04-10
*/
@RestController
@RequestMapping("/api/dict")
public class SysDictController {

    @Reference(version = "${api.service.version}")
    private SysDictService dictService;

    private static final String ENTITY_NAME = "dict";

    @Log("导出字典数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('dict:list')")
    public void download(HttpServletResponse response, SysDictQueryCriteria criteria) throws IOException {
        List<Map<String , Object>> resultList = dictService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }


    @Log("查询字典")
    @GetMapping
    @PreAuthorize("@el.check('dict:list')")
    public ResponseEntity getDicts(SysDictQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity(dictService.queryDictPage(criteria, pageable), HttpStatus.OK);
    }

    @Log("新增字典")
    @PostMapping
    @PreAuthorize("@el.check('dict:add')")
    public ResponseEntity create(@Validated @RequestBody SysDictEntity resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new ResponseEntity(dictService.saveEntity(resources), HttpStatus.CREATED);
    }

    @Log("修改字典")
    @PutMapping
    @PreAuthorize("@el.check('dict:edit')")
    public ResponseEntity update( @RequestBody SysDictEntity resources){
        dictService.updateEntity(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除字典")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('dict:del')")
    public ResponseEntity delete(@PathVariable Long id){
        dictService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}