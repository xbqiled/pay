package com.pay.admin.controller.system;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.sys.criteria.SysMenuQueryCriteria;
import com.pay.api.modules.sys.dto.SysMenuDTO;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.entity.SysMenuEntity;
import com.pay.api.modules.sys.service.SysMenuService;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Lion
 * @date 2018-12-03
 */
@RestController
@RequestMapping("/api/menus")
public class SysMenuController {

    @Reference(version = "${api.service.version}")
    private SysMenuService menuService;

    @Reference(version = "${api.service.version}")
    private SysUserService userService;

    @Reference(version = "${api.service.version}")
    private SysRoleService roleService;

    private static final String ENTITY_NAME = "menu";


    @Log("导出菜单数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('menu:list')")
    public void download(HttpServletResponse response, SysMenuQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> resultList = menuService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }

    /**
     * 构建前端路由所需要的菜单
     * @return
     */
    @GetMapping(value = "/build")
    public ResponseEntity buildMenus(){
        SysUserDTO user = userService.findByName(SecurityUtils.getUsername());
        List<SysMenuDTO> menuDTOList = menuService.findByRoles(roleService.findByUsers_Id(user.getId()));
        List<SysMenuDTO> menuDTOTree = (List<SysMenuDTO>)menuService.buildTree(menuDTOList).get("content");
        return new ResponseEntity(menuService.buildMenus(menuDTOTree),HttpStatus.OK);
    }

    /**
     * 返回全部的菜单
     * @return
     */
    @GetMapping(value = "/tree")
    @PreAuthorize("@el.check('menu:list','roles:list')")
    public ResponseEntity getMenuTree(){
        return new ResponseEntity(menuService.getMenuTree(menuService.findByPid(0L)),HttpStatus.OK);
    }


    @GetMapping(value = "/getMenuIds/{id}")
    public ResponseEntity getMenuIds(@PathVariable Long id){

        return null;
    }




    @Log("查询菜单")
    @GetMapping
    @PreAuthorize("@el.check('menu:list')")
    public ResponseEntity getMenus(SysMenuQueryCriteria criteria){
        List<SysMenuDTO> menuList = menuService.queryAll(criteria);
        return new ResponseEntity(menuService.buildTree(menuList),HttpStatus.OK);
    }

    @Log("新增菜单")
    @PostMapping
    @PreAuthorize("@el.check('menu:add')")
    public ResponseEntity create(@Validated @RequestBody SysMenuEntity resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new ResponseEntity(menuService.saveEntity(resources),HttpStatus.CREATED);
    }

    @Log("修改菜单")
    @PutMapping
    @PreAuthorize("@el.check('menu:edit')")
    public ResponseEntity update( @RequestBody SysMenuEntity resources){
        menuService.updateEntity(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除菜单")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('menu:del')")
    public ResponseEntity delete(@PathVariable Long id){
        List<SysMenuEntity> menuList = menuService.findByPid(id);

        // 特殊情况，对级联删除进行处理
        for (SysMenuEntity menu : menuList) {
            roleService.untiedMenu(menu);
            menuService.deleteEntity(menu.getId());
        }
        roleService.untiedMenu(menuService.findById(id));
        menuService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
