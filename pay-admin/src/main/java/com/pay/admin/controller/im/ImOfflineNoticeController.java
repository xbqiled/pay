package com.pay.admin.controller.im;


import com.pay.api.modules.im.criteria.ImOfflineNoticeQueryCriteria;
import com.pay.api.modules.im.entity.ImOfflineNoticeEntity;
import com.pay.api.modules.im.service.ImOfflineNoticeService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
* @author lion
* @date 2019-10-01
*/
@RestController
@RequestMapping("api")
public class ImOfflineNoticeController {

    @Reference(version = "${api.service.version}")
    private ImOfflineNoticeService imOfflineNoticeService;

    @Log("查询ImOfflineNotice")
    @GetMapping(value = "/imOfflineNotice")
    @PreAuthorize("hasAnyRole('ADMIN','IMOFFLINENOTICE_ALL','IMOFFLINENOTICE_SELECT')")
    public ResponseEntity getImOfflineNotices(ImOfflineNoticeQueryCriteria criteria){
        return new ResponseEntity(imOfflineNoticeService.queryAll(criteria),HttpStatus.OK);
    }

    @Log("新增ImOfflineNotice")
    @PostMapping(value = "/imOfflineNotice")
    @PreAuthorize("hasAnyRole('ADMIN','IMOFFLINENOTICE_ALL','IMOFFLINENOTICE_CREATE')")
    public ResponseEntity create(@Validated @RequestBody ImOfflineNoticeEntity resources){
        return new ResponseEntity(imOfflineNoticeService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改ImOfflineNotice")
    @PutMapping(value = "/imOfflineNotice")
    @PreAuthorize("hasAnyRole('ADMIN','IMOFFLINENOTICE_ALL','IMOFFLINENOTICE_EDIT')")
    public ResponseEntity update(@Validated @RequestBody ImOfflineNoticeEntity resources){
        imOfflineNoticeService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除ImOfflineNotice")
    @DeleteMapping(value = "/imOfflineNotice/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','IMOFFLINENOTICE_ALL','IMOFFLINENOTICE_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id){
        imOfflineNoticeService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}