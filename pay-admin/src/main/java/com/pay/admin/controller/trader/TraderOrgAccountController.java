package com.pay.admin.controller.trader;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.trader.criteria.OrgAccountQueryCriteria;
import com.pay.api.modules.trader.entity.TraderRechargeRecordEntity;
import com.pay.api.modules.trader.service.TraderOrgAccountService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
@RestController
@RequestMapping("/api/orgAccount")
public class TraderOrgAccountController {

    @Reference(version = "${api.service.version}")
    private TraderOrgAccountService traderOrgAccountService;


    @Log("导出银商账户数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('orgAccount:list')")
    public void download(HttpServletResponse response, OrgAccountQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> download = traderOrgAccountService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }

    /**
     * 列表
     */
    @GetMapping
    @PreAuthorize("@el.check('orgAccount:list')")
    public ResponseEntity queryPage(OrgAccountQueryCriteria criteria, Pageable pageable){
        if (criteria.getOrgId() == null) {
            criteria.setOrgId(SecurityUtils.getUserOrg());
        }
        return new ResponseEntity(traderOrgAccountService.queryOrgAccountPage(criteria, pageable), HttpStatus.OK);
    }


    @Log("玩家上分")
    @PostMapping(value = "/addPoint")
    @PreAuthorize("@el.check('orgAccount:addPoint')")
    public ResponseEntity addPoint(@RequestBody Map<String,Object> map){
        map.put("orgId", SecurityUtils.getJwtUser().getOrgId());
        map.put("operateUserId", SecurityUtils.getJwtUser().getId());
        map.put("operateUser", SecurityUtils.getJwtUser().getUsername());
        return new ResponseEntity(traderOrgAccountService.addPoint(map), HttpStatus.CREATED);
    }


    @Log("充值")
    @PostMapping(value = "/recharge")
    @PreAuthorize("@el.check('orgAccount:recharge')")
    public ResponseEntity recharge(@RequestBody TraderRechargeRecordEntity traderRechargeRecord){
        traderRechargeRecord.setOperateUser(SecurityUtils.getJwtUser().getUsername());
        traderRechargeRecord.setOperateTime(new Date());
        return new ResponseEntity(traderOrgAccountService.recharge(traderRechargeRecord), HttpStatus.CREATED);
    }

}
