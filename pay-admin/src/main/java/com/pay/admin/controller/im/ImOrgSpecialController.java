package com.pay.admin.controller.im;


import com.pay.api.modules.im.criteria.ImOrgSpecialQueryCriteria;
import com.pay.api.modules.im.entity.ImOrgSpecialEntity;
import com.pay.api.modules.im.service.ImOrgSpecialService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
* @author lion
* @date 2019-10-01
*/
@RestController
@RequestMapping("api")
public class ImOrgSpecialController {

    @Reference(version = "${api.service.version}")
    private ImOrgSpecialService imOrgSpecialService;

    @Log("查询ImOrgSpecial")

    @GetMapping(value = "/imOrgSpecial")
    @PreAuthorize("hasAnyRole('ADMIN','IMORGSPECIAL_ALL','IMORGSPECIAL_SELECT')")
    public ResponseEntity getImOrgSpecials(ImOrgSpecialQueryCriteria criteria){
        return new ResponseEntity(imOrgSpecialService.queryAll(criteria),HttpStatus.OK);
    }

    @Log("新增ImOrgSpecial")
    @PostMapping(value = "/imOrgSpecial")
    @PreAuthorize("hasAnyRole('ADMIN','IMORGSPECIAL_ALL','IMORGSPECIAL_CREATE')")
    public ResponseEntity create(@Validated @RequestBody ImOrgSpecialEntity resources){
        return new ResponseEntity(imOrgSpecialService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改ImOrgSpecial")
    @PutMapping(value = "/imOrgSpecial")
    @PreAuthorize("hasAnyRole('ADMIN','IMORGSPECIAL_ALL','IMORGSPECIAL_EDIT')")
    public ResponseEntity update(@Validated @RequestBody ImOrgSpecialEntity resources){
        imOrgSpecialService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除ImOrgSpecial")
    @DeleteMapping(value = "/imOrgSpecial/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','IMORGSPECIAL_ALL','IMORGSPECIAL_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id){
        imOrgSpecialService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}