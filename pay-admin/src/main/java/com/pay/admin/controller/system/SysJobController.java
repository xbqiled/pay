package com.pay.admin.controller.system;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.quartz.entity.SysJobEntity;
import com.pay.api.modules.security.service.DataScopeService;
import com.pay.api.modules.sys.criteria.SysJobQueryCriteria;
import com.pay.api.modules.sys.service.SysJobService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author Lion
* @date 2019-03-29
*/
@RestController
@RequestMapping("/api/job")
public class SysJobController {

    private static final String ENTITY_NAME = "job";

    @Reference(version = "${api.service.version}")
    private SysJobService sysJobService;

    @Reference(version = "${api.service.version}")
    private DataScopeService dataScopeService;

    @Log("导出岗位数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('job:list')")
    public void download(HttpServletResponse response, SysJobQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> download  = sysJobService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }

    @Log("查询岗位")
    @GetMapping
    @PreAuthorize("@el.check('job:list','user:list')")
    public ResponseEntity getJobs(SysJobQueryCriteria criteria, Pageable pageable){
        // 数据权限
        criteria.setDeptIds(dataScopeService.getOrgIds(SecurityUtils.getUsername()));
        return new ResponseEntity(sysJobService.queryJobPage(criteria, pageable), HttpStatus.OK);
    }

    @Log("新增岗位")
    @PostMapping
    @PreAuthorize("@el.check('job:add')")
    public ResponseEntity create(@Validated @RequestBody SysJobEntity resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        resources.setCreateTime(new Date());
        return new ResponseEntity(sysJobService.insert(resources),HttpStatus.CREATED);
    }

    @Log("修改岗位")
    @PutMapping
    @PreAuthorize("@el.check('job:edit')")
    public ResponseEntity update(@RequestBody SysJobEntity resources){
        sysJobService.updateEntity(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除岗位")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('job:del')")
    public ResponseEntity delete(@PathVariable Long id){
        return new ResponseEntity(sysJobService.delEntity(id),HttpStatus.OK);
    }
}