package com.pay.admin.controller.report;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.report.criteria.ReportQueryCriteria;
import com.pay.api.modules.report.service.ReportService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author howard
 * @date 2020-1-28
 */
@RestController
@RequestMapping("/api/report")
public class StatisController {

    @Reference(version = "${api.service.version}")
    private ReportService reportService;


    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('statisReport:list')")
    public void download(HttpServletResponse response, ReportQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> download = reportService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }

    /**
     * 列表
     */
    @GetMapping
    @PreAuthorize("@el.check('statisReport:list')")
    public ResponseEntity queryReportPage(ReportQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(reportService.queryReportPage(criteria, pageable), HttpStatus.OK);
    }






}
