package com.pay.admin.controller.tools;


import com.pay.api.modules.tools.criteria.OssConfigQueryCriteria;
import com.pay.api.modules.tools.entity.OssConfigParams;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
* @author lion
* @date 2019-09-12
*/
@RestController
@RequestMapping("/api/oss")
public class OssConfigController {

    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;

    @Log("查询oss配置")
    @GetMapping
    @PreAuthorize("@el.check('oss:list')")
    public ResponseEntity getOss(OssConfigQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity(ossConfigService.queryPage(criteria, pageable),HttpStatus.OK);
    }

    @Log("新增oss配置")
    @PostMapping
    @PreAuthorize("@el.check('oss:add')")
    public ResponseEntity create(@RequestBody OssConfigParams bean){
        return new ResponseEntity(ossConfigService.saveEntity(bean),HttpStatus.CREATED);
    }

    @Log("修改oss配置")
    @PutMapping
    @PreAuthorize("@el.check('oss:edit')")
    public ResponseEntity update( @RequestBody OssConfigParams bean){
        ossConfigService.updateEntity(bean);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除oss配置")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('oss:del')")
    public ResponseEntity delete(@PathVariable Long id){
        ossConfigService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}