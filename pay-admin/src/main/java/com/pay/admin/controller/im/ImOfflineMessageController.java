package com.pay.admin.controller.im;


import com.pay.api.modules.im.criteria.ImOfflineMessageQueryCriteria;
import com.pay.api.modules.im.entity.ImOfflineMessageEntity;
import com.pay.api.modules.im.service.ImOfflineMessageService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
* @author lion
* @date 2019-10-01
*/
@RestController
@RequestMapping("api")
public class ImOfflineMessageController {

    @Reference(version = "${api.service.version}")
    private ImOfflineMessageService imOfflineMessageService;

    @Log("查询ImOfflineMessage")
    @GetMapping(value = "/imOfflineMessage")
    public ResponseEntity getImOfflineMessages(ImOfflineMessageQueryCriteria criteria){
        return new ResponseEntity(imOfflineMessageService.queryAll(criteria),HttpStatus.OK);
    }

    @Log("新增ImOfflineMessage")
    @PostMapping(value = "/imOfflineMessage")
    public ResponseEntity create(@Validated @RequestBody ImOfflineMessageEntity resources){
        return new ResponseEntity(imOfflineMessageService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改ImOfflineMessage")
    @PutMapping(value = "/imOfflineMessage")
    public ResponseEntity update(@Validated @RequestBody ImOfflineMessageEntity resources){
        imOfflineMessageService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除ImOfflineMessage")
    @DeleteMapping(value = "/imOfflineMessage/{messageId}")
    public ResponseEntity delete(@PathVariable Long messageId){
        imOfflineMessageService.delete(messageId);
        return new ResponseEntity(HttpStatus.OK);
    }
}