package com.pay.admin.controller.log;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.log.criteria.LogQueryCriteria;
import com.pay.api.modules.log.service.SysLogService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Lion
 * @date 2018-11-24
 */
@RestController
@RequestMapping("/api/logs")
public class LogController {

    @Reference(version = "${api.service.version}")
    private SysLogService sysLogService;


    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('log:list')")
    public void download(HttpServletResponse response, LogQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> download = sysLogService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }

    @GetMapping
    @PreAuthorize("@el.check('log:list')")
    public ResponseEntity getLogs(LogQueryCriteria criteria, Pageable pageable){
        criteria.setLogType("INFO");
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(sysLogService.queryLogPage(criteria, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/user")
    public ResponseEntity getUserLogs(LogQueryCriteria criteria, Pageable pageable){
        criteria.setLogType("INFO");
        criteria.setBlurry(SecurityUtils.getUsername());
        return new ResponseEntity(sysLogService.queryLogPage(criteria, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/error")
    @PreAuthorize("@el.check('errorlog:list')")
    public ResponseEntity getErrorLogs(LogQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        criteria.setLogType("ERROR");
        return new ResponseEntity(sysLogService.queryLogPage(criteria, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/error/{id}")
    @PreAuthorize("@el.check('errorlog:list')")
    public ResponseEntity getErrorLogs(@PathVariable Long id){
        return new ResponseEntity(sysLogService.findByErrDetail(id), HttpStatus.OK);
    }
}
