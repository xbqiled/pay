package com.pay.admin.controller.im;


import com.pay.api.modules.im.criteria.ImBlackQueryCriteria;
import com.pay.api.modules.im.entity.ImBlackEntity;
import com.pay.api.modules.im.service.ImBlackService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
* @author lion
* @date 2019-10-01
*/
@RestController
@RequestMapping("api")
public class ImBlackController {

    @Reference(version = "${api.service.version}")
    private ImBlackService imBlackService;

    @Log("查询ImBlack")
    @GetMapping(value = "/imBlack")
    public ResponseEntity getImBlacks(ImBlackQueryCriteria criteria){
        return new ResponseEntity(imBlackService.queryAll(criteria),HttpStatus.OK);
    }

    @Log("新增ImBlack")
    @PostMapping(value = "/imBlack")
    public ResponseEntity create(@Validated @RequestBody ImBlackEntity resources){
        return new ResponseEntity(imBlackService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改ImBlack")
    @PutMapping(value = "/imBlack")
    public ResponseEntity update(@Validated @RequestBody ImBlackEntity resources){
        imBlackService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除ImBlack")
    @DeleteMapping(value = "/imBlack/{id}")
    public ResponseEntity delete(@PathVariable Integer id){
        imBlackService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}