package com.pay.admin.controller.im;


import com.pay.api.modules.im.criteria.ImUserInfoQueryCriteria;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.ImUserInfoService;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.common.core.annotation.Log;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
* @author
* @date 2019-09-18
*/
@RestController
@RequestMapping("api")
public class ImUserInfoController {

    @Reference(version = "${api.service.version}")
    private ImUserInfoService imUserInfoService;

    @Reference(version = "${api.service.version}")
    private SysUserService sysUserService;

    @Reference(version = "${api.service.version}")
    private SysRoleService roleService;


    @Log("新增ImUserInfo")
    @PostMapping(value = "/imUserInfo")
    public ResponseEntity create(@Validated @RequestBody ImUserInfoEntity resources){
        return new ResponseEntity(imUserInfoService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改ImUserInfo")
    @PutMapping(value = "/imUserInfo")
    public ResponseEntity update(@Validated @RequestBody ImUserInfoEntity resources){
        imUserInfoService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除ImUserInfo")
    @DeleteMapping(value = "/imUserInfo/{id}")
    public ResponseEntity delete(@PathVariable Integer id){
        imUserInfoService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Log("禁言ImUser")
    @PutMapping(value = "/imUserBaned")
    public ResponseEntity baned(@Validated @RequestBody ImUserInfoQueryCriteria criteria){
//        criteria.setOperator(SecurityUtils.getUsername());
//        imUserInfoService.baned(criteria);
//        return new ResponseEntity(HttpStatus.OK);
        return null;
    }

    @Log("拉黑ImUser")
    @PutMapping(value = "/imUserBlack")
    public ResponseEntity black(@Validated @RequestBody ImUserInfoQueryCriteria criteria){
//        criteria.setOperator(SecurityUtils.getUsername());
//        imUserInfoService.black(criteria);
//        return new ResponseEntity(HttpStatus.OK);
        return null;
    }
}