package com.pay.admin.controller.im;

import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.sys.entity.SysPictureEntity;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.api.modules.trader.criteria.CsQueryCriteria;
import com.pay.api.modules.trader.entity.TraderCsInfoEntity;
import com.pay.api.modules.trader.service.TraderCsInfoService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.oss.CloudStorageConfig;
import com.pay.common.core.utils.FileUtil;
import com.pay.common.core.utils.OSSFactory;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Howard
 * @date 2020-01-31
 */
@RestController
@RequestMapping("api/cs")
public class ImCsController {

    @Reference(version = "${api.service.version}")
    private TraderCsInfoService traderCsInfoService;

    @Reference(version = "${api.service.version}")
    private OssConfigService ossConfigService;

    @Log("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('cs:list')")
    public void download(HttpServletResponse response, CsQueryCriteria criteria) throws IOException {
        criteria.setOrgId(SecurityUtils.getUserOrg());
        List<Map<String, Object>> download = traderCsInfoService.download(criteria);
        FileUtil.downloadExcel(download, response);
    }

    /**
     * 列表
     */
    @GetMapping
    @PreAuthorize("@el.check('cs:list')")
    public ResponseEntity queryCs(CsQueryCriteria criteria, Pageable pageable){
        criteria.setOrgId(SecurityUtils.getUserOrg());
        return new ResponseEntity(traderCsInfoService.queryCsPage(criteria, pageable), HttpStatus.OK);
    }

    /**
     * 保存
     */
    @Log("新增客服")
    @PostMapping
    @PreAuthorize("@el.check('cs:add')")
    public ResponseEntity save(@RequestBody TraderCsInfoEntity traderCsInfo) {
        traderCsInfo.setOrgId(SecurityUtils.getJwtUser().getOrgId());
        return new ResponseEntity(traderCsInfoService.saveEntity(traderCsInfo,
                SecurityUtils.getUserInfo().getId()), HttpStatus.CREATED);
    }

    /**
     * 修改
     */
    @PutMapping
    @PreAuthorize("@el.check('cs:edit')")
    public ResponseEntity update(@RequestBody TraderCsInfoEntity traderCsInfo){
        traderCsInfoService.updateEntity(traderCsInfo);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * 删除
     */
    @Log("删除客服")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('cs:del')")
    public ResponseEntity delete(@PathVariable Long id){
        traderCsInfoService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Log("上传客服头像")
    @PreAuthorize("@el.check('cs:edit')")
    @PostMapping(value = "/avatar")
    public ResponseEntity avatar(@RequestParam MultipartFile file){
        Map map = new HashMap(3);
        try {
            BufferedImage image = ImageIO.read(file.getInputStream());
            if (image == null) {
                throw new Exception("此文件不是一个图片");
            }
            CloudStorageConfig config = ossConfigService.getConfigObject(PayConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
            String url = OSSFactory.build(config).uploadSuffix(file.getBytes(), "." + FileUtil.getExtensionName(file.getOriginalFilename()));
            if (url != null) {
                map.put("msg", "上传成功");
                map.put("url", url);
            }
        } catch (Exception e) {
            map.put("errno", 1);
            map.put("msg", e.getMessage());
        }
        return new ResponseEntity(map, HttpStatus.OK);
    }
}
