package com.pay.admin.controller.system;

import cn.hutool.core.lang.Dict;
import com.pay.admin.utils.SecurityUtils;
import com.pay.api.modules.sys.criteria.SysRoleQueryCriteria;
import com.pay.api.modules.sys.dto.SysRoleDTO;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.entity.SysRoleEntity;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.api.modules.sys.service.SysRolesMenusService;
import com.pay.common.core.annotation.Log;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.FileUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Lion
 * @date 2018-12-03
 */
@RestController
@RequestMapping("/api/roles")
public class SysRoleController {

    @Reference(version = "${api.service.version}")
    private SysRoleService roleService;

    @Reference(version = "${api.service.version}")
    private SysRolesMenusService sysRolesMenusService;


    private static final String ENTITY_NAME = "role";

    /**
     * 获取单个role
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}")
    @PreAuthorize("@el.check('roles:list')")
    public ResponseEntity getRoles(@PathVariable Long id){
        return new ResponseEntity(roleService.selectById(id), HttpStatus.OK);
    }


    @Log("导出角色数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('roles:list')")
    public void download(HttpServletResponse response, SysRoleQueryCriteria criteria) throws IOException {
        List<Map<String, Object>> resultList = roleService.download(criteria);
        FileUtil.downloadExcel(resultList, response);
    }

    @Log("获取角色对应的菜单")
    @GetMapping(value = "/getMenuIds/{id}")
    public ResponseEntity getMenuIds(@PathVariable Long id){
        List<Long> menuIds = sysRolesMenusService.getRoleMenuIds(id);
        return new ResponseEntity(menuIds, HttpStatus.OK);
    }

    /**
     * 返回全部的角色，新增用户时下拉选择
     * @return
     */
    @GetMapping(value = "/all")
    @PreAuthorize("@el.check('roles:list','user:add','user:edit')")
    public ResponseEntity getAll(){
        return new ResponseEntity(roleService.queryAll(), HttpStatus.OK);
    }


    @Log("查询角色")
    @GetMapping
    @PreAuthorize("@el.check('roles:list')")
    public ResponseEntity getRoles(SysRoleQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity(roleService.queryRolePage(criteria, pageable),HttpStatus.OK);
    }


    @GetMapping(value = "/level")
    public ResponseEntity getLevel(){
        List<Integer> levels = roleService.findByUsers_Id(SecurityUtils.getUserId()).stream().map(SysRoleSmallDTO::getLevel).collect(Collectors.toList());
        return new ResponseEntity(Dict.create().set("level", Collections.min(levels)),HttpStatus.OK);
    }


    @Log("新增角色")
    @PostMapping
    @PreAuthorize("@el.check('roles:add')")
    public ResponseEntity create(@Validated @RequestBody SysRoleDTO resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new ResponseEntity(roleService.saveEntity(resources), HttpStatus.CREATED);
    }


    @Log("修改角色")
    @PutMapping
    @PreAuthorize("@el.check('roles:edit')")
    public ResponseEntity update(@RequestBody SysRoleEntity roleEntity){
        return new ResponseEntity(roleService.updateEntity(roleEntity),HttpStatus.OK);
    }


    @Log("修改角色菜单")
    @PutMapping(value = "/menu")
    @PreAuthorize("@el.check('roles:edit')")
    public ResponseEntity updateMenu(@RequestBody SysRoleDTO resources){
        roleService.updateMenu(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @Log("删除角色")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('roles:del')")
    public ResponseEntity delete(@PathVariable Long id){
        roleService.deleteEntity(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
