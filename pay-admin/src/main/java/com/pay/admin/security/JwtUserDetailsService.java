package com.pay.admin.security;


import cn.hutool.core.util.StrUtil;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.api.modules.security.service.JwtPermissionService;
import com.pay.api.modules.security.service.SysUserDetailsService;
import com.pay.api.modules.sys.dto.*;
import com.pay.common.core.exception.BadRequestException;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Service("jwtUserDetailsService")
public class JwtUserDetailsService implements UserDetailsService {

    @Reference(version = "${api.service.version}")
    private SysUserDetailsService sysUserDetailsService;

    @Reference(version = "${api.service.version}")
    private JwtPermissionService JwtPermissionService;

    @Override
    public JwtUser loadUserByUsername(String username){
        SysUserDTO user = sysUserDetailsService.loadUserByUsername(username);
        if (user == null) {
            return null;
            //throw new BadRequestException("账号不存在");
        } else {
            Set<SysRoleDTO> set = JwtPermissionService.mapToGrantedAuthorities(user).getRoleSet();
            JwtUser userDetails = this.createJwtUser(user, set);
            userDetails.setOrgId(user.getOrgId());
            return userDetails;
        }
    }

    public JwtUser createJwtUser(SysUserDTO user, Set<SysRoleDTO> roleSet) {
        Set<String> permissions = roleSet.stream().filter(role -> StrUtil.isNotBlank(role.getPermission())).map(SysRoleDTO::getPermission).collect(Collectors.toSet());

        permissions.addAll(
                roleSet.stream().flatMap(role -> role.getMenus().stream())
                        .filter(menu -> StrUtil.isNotBlank(menu.getPermission()))
                        .map(SysMenuDTO::getPermission).collect(Collectors.toSet())
        );

        Collection<GrantedAuthority> roleResult =  permissions.stream().map(permission -> new SimpleGrantedAuthority(permission))
                .collect(Collectors.toList());

        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getAvatar(),
                user.getEmail(),
                user.getPhone(),
                user.getDeptId(),
                Optional.ofNullable(user.getDept()).map(SysDeptSmallDTO::getName).orElse(null),
                Optional.ofNullable(user.getJob()).map(SysJobSmallDTO::getName).orElse(null),
                roleResult,
                user.getEnabled(),
                user.getCreateTime(),
                user.getLastPasswordResetTime()
        );
    }
}
