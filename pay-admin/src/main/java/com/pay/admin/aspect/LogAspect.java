package com.pay.admin.aspect;


import com.pay.admin.utils.SecurityUtils;
import com.pay.api.kryo.JoinPointModel;
import com.pay.api.modules.log.entity.SysLogEntity;
import com.pay.api.modules.log.service.SysLogService;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.common.core.utils.RequestHolder;
import com.pay.common.core.utils.StringUtils;
import com.pay.common.core.utils.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lion
 * @date 2018-11-24
 */
@Component
@Aspect
@Slf4j
public class LogAspect {

    @Reference(version = "${api.service.version}")
    private SysLogService sysLogService;

    private long currentTime = 0L;

    /**
     * 配置切入点
     */
    @Pointcut("@annotation(com.pay.common.core.annotation.Log)")
    public void logPointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    /**
     * 配置环绕通知,使用在方法logPointcut()上注册的切入点
     */
    @Around("logPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        currentTime = System.currentTimeMillis();
        result = joinPoint.proceed();
        SysLogEntity log = new SysLogEntity();
        StringBuilder params = new StringBuilder("{");
        //参数值
        Object[] argValues = joinPoint.getArgs();
        if(argValues != null){
            for (int i = 0; i < argValues.length; i++) {
                params.append(argValues[i]);
            }
        }
        JwtUser user = getUserInfo();
        String userName = "未登录";
        Long orgId = 0L;
        if(user != null){
            orgId = user.getOrgId();
            userName = user.getUsername();
        }
        log.setParams(params.toString()+"}");
        log.setLogType("INFO");
        log.setTime(System.currentTimeMillis() - currentTime);
        HttpServletRequest request = RequestHolder.getHttpServletRequest();
        sysLogService.save(userName, StringUtils.getBrowser(request), StringUtils.getIp(RequestHolder.getHttpServletRequest()), orgId, new JoinPointModel(joinPoint), log);
        return result;
    }

    /**
     * 配置异常通知
     */
    @AfterThrowing(pointcut = "logPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        SysLogEntity log = new SysLogEntity();
        log.setLogType("ERROR");
        log.setTime(System.currentTimeMillis() - currentTime);
        log.setExceptionDetail(ThrowableUtil.getStackTrace(e).toLowerCase());
        HttpServletRequest request = RequestHolder.getHttpServletRequest();
        JwtUser user = getUserInfo();
        String userName = "未登录";
        Long orgId = 0L;
        if (user != null) {
            orgId = user.getOrgId();
            userName = user.getUsername();
        }
        sysLogService.save(userName, StringUtils.getBrowser(request), StringUtils.getIp(RequestHolder.getHttpServletRequest()), orgId, new JoinPointModel((ProceedingJoinPoint) joinPoint), log);
    }


    public JwtUser getUserInfo() {
        try {
            return SecurityUtils.getUserInfo();
        }catch (Exception e){
            return null;
        }
    }

}
