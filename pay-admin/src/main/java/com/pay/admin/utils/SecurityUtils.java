package com.pay.admin.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONObject;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.common.core.exception.BadRequestException;
import org.springframework.http.HttpStatus;

/**
 * 获取当前登录的用户
 * @author Lion
 * @date 2019-01-17
 */
public class SecurityUtils {

    public static JwtUser getUserDetails() {
        JwtUser userDetails = null;
        try {
            userDetails = (JwtUser) org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "登录状态过期");
        }
        return userDetails;
    }

    /**
     * 获取系统用户名称
     * @return 系统用户名称
     */
    public static String getUsername(){
        Object obj = getUserDetails();
        JSONObject json = new JSONObject(obj);
        return json.get("username", String.class);
    }


    public static JwtUser getUserInfo() {
        Object obj = getUserDetails();
        if (obj != null) {
           return (JwtUser)obj;
        }
        return null;
    }


    public static com.pay.api.modules.security.entity.UserDetails getUser() {
        Object obj = getUserDetails();
        if (obj != null) {
            JwtUser user = (JwtUser)obj;
            com.pay.api.modules.security.entity.UserDetails userDetails = new com.pay.api.modules.security.entity.UserDetails();
            BeanUtil.copyProperties(user, userDetails);
            return userDetails;
        }
        return null;
    }

    public static JwtUser getJwtUser() {
        JwtUser user = getUserDetails();
        return user;
    }

    public static Long getUserOrg() {
        JwtUser user = getUserDetails();
        //判断是否为根节点
        if (user.getOrgId() == 1) {
            return null;
        } else {
            return user.getOrgId();
        }
    }

    /**
     * 获取系统用户id
     * @return 系统用户id
     */
    public static Long getUserId(){
        Object obj = getUserDetails();
        JSONObject json = new JSONObject(obj);
        return json.get("id", Long.class);
    }
}
