package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.dto.SysOrgDTO;
import com.pay.api.modules.sys.entity.SysRolesOrgsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysRolesOrgsDao extends BaseMapper<SysRolesOrgsEntity> {
    /**
     * 批量插入指定角色关联
     * @param depts
     * @param id
     */
    void insertBatch(Set<SysOrgDTO> depts, Long id);

    /**
     * 批量删除指定角色关联
     * @param roleId
     * @return
     */
    int deleteByRoleId(@Param("roleId") Long roleId);

}
