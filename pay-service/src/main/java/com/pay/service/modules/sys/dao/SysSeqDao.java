package com.pay.service.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.entity.SysSeqEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-21 17:28:57
 */
public interface SysSeqDao extends BaseMapper<SysSeqEntity> {

    SysSeqEntity getBySeqName(@Param("seqName") String seqName, @Param("orgId") Long orgId);

    void updateToIncrease(@Param("seqName") String seqName, @Param("orgId") Long orgId);
	
}
