package com.pay.service.modules.security.service;

import com.pay.api.modules.security.service.DataScopeService;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.entity.SysOrgEntity;
import com.pay.api.modules.sys.service.SysOrgService;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.api.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
@CacheConfig(cacheNames = "role")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class DataScopeServiceImpl implements DataScopeService {

    private final String[] scopeType = {"全部","本级","自定义"};

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysOrgService sysOrgService;

    @Override
    public Set<Long> getOrgIds(String username) {
        SysUserDTO user = userService.findByName(username);
        // 用于存储ORGid
        Set<Long> orgIds = new HashSet<>();
        // 查询用户角色
        List<SysRoleSmallDTO> roleSet = roleService.findByUsers_Id(user.getId());
        for (SysRoleSmallDTO role : roleSet) {
            if (scopeType[0].equals(role.getDataScope())) {
                return new HashSet<>() ;
            }

            // 存储本级的数据权限
            if (scopeType[1].equals(role.getDataScope())) {
                orgIds.add(user.getDept().getId());
            }

            // 存储自定义的数据权限
            if (scopeType[2].equals(role.getDataScope())) {
                Set<SysOrgEntity> sysOrgEntities = sysOrgService.findByRoleIds(role.getId());
                for (SysOrgEntity sysOrgEntity : sysOrgEntities) {
                    orgIds.add(sysOrgEntity.getId());
                    List<SysOrgEntity> orgChildren = sysOrgService.findByPid(sysOrgEntity.getId());
                    if (orgChildren != null && orgChildren.size() != 0) {
                        orgIds.addAll(getOrgChildren(orgChildren));
                    }
                }
            }
        }
        return orgIds;
    }


    @Override
    public List<Long> getOrgChildren(List<SysOrgEntity> orgList) {
        List<Long> list = new ArrayList<>();
        orgList.forEach(org -> {
                    if (org != null && org.getEnabled()) {
                        List<SysOrgEntity> orgs = sysOrgService.findByPid(org.getId());
                        if (orgList != null && orgList.size() != 0) {
                            list.addAll(getOrgChildren(orgs));
                        }
                        list.add(org.getId());
                    }
                }
        );
        return list;
    }
}
