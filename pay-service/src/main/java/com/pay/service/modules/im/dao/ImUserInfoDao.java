package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.im.criteria.ImUserInfoQueryCriteria;
import com.pay.api.modules.im.dto.ImUserInfoDTO;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.vo.ImUserInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface ImUserInfoDao extends BaseMapper<ImUserInfoEntity> {

    ImUserInfoEntity loginByUserId(@Param("orgId") Long orgId, @Param("userId") String userId);

    ImUserInfoVo getUserInfoById(@Param("userId") Integer userId, @Param("myUserId") Integer myUserId);

    List<ImUserInfoVo> findUserByKey(@Param("userKey") String userKey, @Param("userId") String userId);

    List<Map<String, Object>> getCsList(@Param("orgId") Long orgId, @Param("isOnLine") Integer isOnLine);

    List<Map<String, Object>> getChannelCsList(@Param("channelId") String channelId, @Param("isOnLine") Integer isOnLine);

    ImUserInfoEntity loginByUserName(@Param("orgId") Long orgId, @Param("userName") String userName);

    ImUserInfoEntity getByUserName(@Param("orgId") Long orgId, @Param("userName") String userName);

    void moidfyUserPw(@Param("userId") Integer userId, @Param("passWord") String passWord);

    void moidfyUserPwByAccount(@Param("orgId") Long orgId, @Param("nickName") String nickName,
                               @Param("status") Integer status,
                               @Param("userName") String userName,
                               @Param("passWord") String passWord,
                               @Param("supportedPms") Integer supportedPms,
                               @Param("avatar") String avatar);

    Integer delUserByAccount(@Param("orgId") String orgId, @Param("userName") String userName);

    void modifyOnline(@Param("userId") Integer userId, @Param("isOnline") Integer isOnline);

    void modifyRechargeNum(@Param("userId") Integer userId);

    ImUserInfoEntity verifyCreateUser(@Param("userId") String userId, @Param("nickName") String nickName, @Param("passwd") String passwd, @Param(
            "orgId") Long orgId);

    void modifyUserInfo(@Param("userId") Integer userId,
                        @Param("nickName") String nickName,
                        @Param("sex") String sex,
                        @Param("sign") String sign,
                        @Param("phone") String phone,
                        @Param("address") String address,
                        @Param("email") String email,
                        @Param("birthDay") String birthDay,
                        @Param("cardNumber") String cardNumber,
                        @Param("school") String school,
                        @Param("education") String education,
                        @Param("userAvatar") String userAvatar);

    List<ImUserInfoDTO> queryAll(Page<ImUserInfoDTO> page, ImUserInfoQueryCriteria criteria);

    List<ImUserInfoDTO> queryGroupUsers(Page<ImUserInfoDTO> page, ImUserInfoQueryCriteria criteria);

    List<ImUserInfoDTO> queryGroupSimpleUser(Page<ImUserInfoDTO> page, ImUserInfoQueryCriteria criteria);
}
