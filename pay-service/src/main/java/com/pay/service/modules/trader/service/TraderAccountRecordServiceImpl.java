package com.pay.service.modules.trader.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.trader.criteria.AccountRecordQueryCriteria;
import com.pay.api.modules.trader.entity.TraderAccountRecordEntity;
import com.pay.api.modules.trader.service.TraderAccountRecordService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.trader.dao.TraderAccountRecordDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Service("traderAccountRecordService")
@CacheConfig(cacheNames = "accountRecord")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class TraderAccountRecordServiceImpl extends ServiceImpl<TraderAccountRecordDao, TraderAccountRecordEntity> implements TraderAccountRecordService {


    @Override
    public PageUtils queryAccountRecordPage(AccountRecordQueryCriteria criteria, Pageable pageable) {
        Page<TraderAccountRecordEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<TraderAccountRecordEntity> resultList = baseMapper.queryAccountRecordPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public List<Map<String, Object>> download(AccountRecordQueryCriteria criteria) {
        List<TraderAccountRecordEntity> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();

        for (TraderAccountRecordEntity accountRecord : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("归属机构", accountRecord.getOrgName());

            map.put("账目类型", accountRecord.getAccountItem() == 1 ? "给玩家上分" : "其他");
            map.put("账户变化前", accountRecord.getBeforeBalance());

            map.put("上分金额", accountRecord.getAmount());
            map.put("账户变化后", accountRecord.getAfterBalance());
            map.put("状态", accountRecord.getStatus() == 1 ? "成功" : "失败");

            map.put("操作时间", accountRecord.getOperateTime());
            map.put("操作人", accountRecord.getOperateUser());
            list.add(map);
        }
        return  list;
    }

    @Override
    public TraderAccountRecordEntity saveEntity(TraderAccountRecordEntity traderAccountRecord) {
        this.baseMapper.insert(traderAccountRecord);
        return traderAccountRecord;
    }

    @Override
    public boolean updateEntity(TraderAccountRecordEntity traderAccountRecord) {
        return this.updateById(traderAccountRecord);
    }

    @Override
    public void deleteEntity(Long id) {
        this.deleteById(id);
    }
}
