package com.pay.service.modules.trader.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.pay.api.json.AddPointResult;
import com.pay.api.modules.trader.criteria.OrgAccountQueryCriteria;
import com.pay.api.modules.trader.entity.TraderAccountRecordEntity;
import com.pay.api.modules.trader.entity.TraderOrgAccountEntity;
import com.pay.api.modules.trader.entity.TraderRechargeRecordEntity;
import com.pay.api.modules.trader.service.TraderAccountRecordService;
import com.pay.api.modules.trader.service.TraderOrgAccountService;
import com.pay.api.modules.trader.service.TraderRechargeRecordService;
import com.pay.api.modules.trader.vo.TraderOrgAccountVo;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.utils.MD5Util;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.trader.dao.TraderOrgAccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;


@Service("traderOrgAccountService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class TraderOrgAccountServiceImpl extends ServiceImpl<TraderOrgAccountDao, TraderOrgAccountEntity> implements TraderOrgAccountService {

    @Autowired
    private TraderAccountRecordService traderAccountRecordService;

    @Autowired
    private TraderRechargeRecordService traderRechargeRecordService;

    @Override
    @Transactional
    public Map<String, Object> addPoint(Map<String, Object> map) {
        Map<String, Object> resMap = new HashMap<>();
        resMap.put("flag", false);
        try{
            if("".equalsIgnoreCase(map.get("userId").toString())){
                throw new Exception("用户ID不可为空!");
            }
            if("".equalsIgnoreCase(map.get("amount").toString())){
                throw new Exception("上分金额不可为空!");
            }
            if("".equalsIgnoreCase(map.get("ascription").toString())){
                throw new Exception("上分类型不可为空!");
            }
            BigDecimal amount = new BigDecimal(map.get("amount").toString());
            Long orgId = Long.parseLong(map.get("orgId").toString());
            TraderOrgAccountEntity account = this.baseMapper.queryByOrgId(orgId);
            if(account.getBlance().compareTo(amount) == -1){
                throw new Exception("余额不足");
            }

            JSONObject json = new JSONObject();
            json.put("amount", map.get("amount").toString());
            json.put("random", MD5Util.getRandomString(10));
            json.put("userId", map.get("userId").toString());
            json.put("orgId", map.get("orgId").toString());
            json.put("channelId", map.get("channelId").toString());
            json.put("operateUserId", map.get("operateUserId").toString());

            StringBuilder sb = new StringBuilder();
            sb.append("amount=" + json.getString("amount"));
            sb.append("random=" + json.getString("random"));
            sb.append("userId=" + json.getString("userId"));
            sb.append("orgId=" + json.getString("orgId"));
            sb.append("operateUserId=" + json.getString("operateUserId"));
            String md5 = MD5Util.encryption(sb.toString()) + "f396546c11a24dc78dc21415723a9992";
            json.put("sign", MD5Util.encryption(md5));

            String resultStr = HttpRequest.post(PayConstant.GAME_ADD_POINT_URL).body(json.toString()).timeout(20000).execute().body();
            AddPointResult resultJson = null;
            if (StrUtil.isNotBlank(resultStr)) {
                resultJson = new Gson().fromJson(resultStr, AddPointResult.class);
            }

            if (StrUtil.isEmpty(resultStr) || resultJson.getCode() != 0) {
                resMap.put("msg", "服务器繁忙,上分失败");
                return resMap;
                //throw new Exception("服务器繁忙,上分失败");
            }

            //创建上分记录
            TraderAccountRecordEntity tae = new TraderAccountRecordEntity();
            tae.setStatus(2);
            tae.setOrgId(orgId);
            tae.setOperateTime(new Date());
            tae.setOperateUser(map.get("operateUser").toString());
            tae.setAccountItem(Integer.parseInt(map.get("ascription").toString()));
            tae.setAmount(amount);
            tae.setBeforeBalance(account.getBlance());
            tae.setChannelId(null);
            tae.setUserId(json.getLong("userId"));
            tae.setAscription(Integer.parseInt(map.get("ascription").toString()));

            if (resultJson != null && resultJson.getCode() == 0 && resultJson.getData().getStatus() == 1) {
                account.setBlance(account.getBlance().subtract(amount));
                account.setTotalAddpointsNum(account.getTotalAddpointsNum() + 1);
                account.setTotalAddpoints(account.getTotalAddpoints().add(amount));
                this.baseMapper.updateById(account);
                resMap.put("flag", true);
                tae.setStatus(1);
                tae.setChannelId(resultJson.getData().getChannelId());
                //tae.setAfterBalance(account.getBlance().subtract(amount));
                tae.setAfterBalance(account.getBlance());
                //插入上分记录
                traderAccountRecordService.saveEntity(tae);
            }
            resMap.put("msg", resultJson.getMsg());
        } catch (Exception e) {
            resMap.put("msg", "上分失败");
        }
        return resMap;
    }

    @Override
    public Map<String, Object> recharge(TraderRechargeRecordEntity traderRechargeRecord) {
        Map<String, Object> map = new HashMap<>();
        try{
            if(traderRechargeRecord.getOrgId() == null){
                throw new Exception("请选择归属渠道!");
            }

            if(traderRechargeRecord.getAmount() == null){
                throw new Exception("请输入金额!");
            }

            if(traderRechargeRecord.getAccountItem() == null){
                throw new Exception("请选择会计项目!");
            }

            Long orgId = traderRechargeRecord.getOrgId();
            TraderOrgAccountEntity account = this.baseMapper.queryByOrgId(orgId);

            if(account == null){
                account = new TraderOrgAccountEntity();
                account.setOrgId(orgId);
                this.baseMapper.insert(account);
            } else {
                if (traderRechargeRecord.getAccountItem() == 2 && account.getBlance().compareTo(traderRechargeRecord.getAmount()) == -1) {
                    throw new Exception("扣款金额不够!");
                }
            }

            traderRechargeRecord.setBeforeBalance(account.getBlance());
            if(traderRechargeRecord.getAccountItem() == 1){
                //充值
                account.setBlance(account.getBlance().add(traderRechargeRecord.getAmount()));
                account.setTotalRecharge(account.getTotalRecharge().add(traderRechargeRecord.getAmount()));
                account.setTotalRechargeNum(account.getTotalRechargeNum() + 1);
                traderRechargeRecord.setAfterBalance(account.getBlance().add(traderRechargeRecord.getAmount()));
            } else {
                //扣款
                account.setBlance(account.getBlance().subtract(traderRechargeRecord.getAmount()));
                traderRechargeRecord.setAfterBalance(account.getBlance().subtract(traderRechargeRecord.getAmount()));
            }

            traderRechargeRecord.setStatus(1);
            this.baseMapper.updateById(account);
            traderRechargeRecordService.saveEntity(traderRechargeRecord);
            map.put("flag",true);
            map.put("msg","操作成功");
        }catch (Exception e){
            map.put("flag",false);
            map.put("msg",e.getMessage());
        }
        return map;
    }


    @Override
    public PageUtils queryOrgAccountPage(OrgAccountQueryCriteria criteria, Pageable pageable) {
        Page<TraderOrgAccountVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<TraderOrgAccountVo> resultList = baseMapper.queryOrgAccountPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<Map<String, Object>> download(OrgAccountQueryCriteria criteria) {
        List<TraderOrgAccountVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();

        for (TraderOrgAccountVo orgAccountEntity : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("归属渠道", orgAccountEntity.getOrgName());

            map.put("账户金额", orgAccountEntity.getBlance());
            map.put("总充值金额", orgAccountEntity.getTotalRecharge());
            map.put("总充值笔数", orgAccountEntity.getTotalRechargeNum());

            map.put("总上分金额", orgAccountEntity.getTotalAddpoints());
            map.put("总上分笔数", orgAccountEntity.getTotalAddpointsNum());
            list.add(map);

        }
        return  list;
    }

    @Override
    public TraderOrgAccountEntity saveEntity(TraderOrgAccountEntity traderOrgAccount) {
        this.baseMapper.insert(traderOrgAccount);
        return traderOrgAccount;
    }

    @Override
    public boolean updateEntity(TraderOrgAccountEntity traderOrgAccount) {
        return this.updateById(traderOrgAccount);
    }

    @Override
    public Integer deleteById(Long id) {
        return this.baseMapper.deleteById(id);
    }

    @Override
    public Integer deleteByOrgId(Long orgId) {
        return null;
    }
}
