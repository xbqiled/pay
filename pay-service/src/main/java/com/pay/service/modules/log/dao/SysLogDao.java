package com.pay.service.modules.log.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.log.criteria.LogQueryCriteria;
import com.pay.api.modules.log.entity.SysLogEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface SysLogDao extends BaseMapper<SysLogEntity> {

    long findIp(@Param("start") String start, @Param("end") String end);

    String findExceptionById(@Param("id") Long id);

    List<SysLogEntity> queryAll(LogQueryCriteria criteria);

    List<SysLogEntity> queryLogPage(Page page, LogQueryCriteria criteria);
}
