package com.pay.service.modules.trader.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.trader.criteria.AccountRecordQueryCriteria;
import com.pay.api.modules.trader.entity.TraderAccountRecordEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
public interface TraderAccountRecordDao extends BaseMapper<TraderAccountRecordEntity> {

    List<TraderAccountRecordEntity> queryAccountRecordPage(Page page, AccountRecordQueryCriteria criteria);

    List<TraderAccountRecordEntity> queryAll(AccountRecordQueryCriteria criteria);

    BigDecimal queryTodayTotalAmountByOrgId(@Param("orgId") Long orgId);

    Integer queryTodayTotalCountByOrgId(@Param("orgId") Long orgId);

    BigDecimal queryWeekTotalAmountByOrgId(@Param("orgId") Long orgId);

    Integer queryWeekTotalCountByOrgId(@Param("orgId") Long orgId);
}
