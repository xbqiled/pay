package com.pay.service.modules.security.service;



import com.pay.api.modules.security.service.SysUserDetailsService;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.common.core.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Lion
 * @date 2018-11-22
 */
@Service
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class SysUserDetailsServiceImpl implements SysUserDetailsService {

    @Autowired
    private SysUserService userService;

    @Override
    public SysUserDTO loadUserByUsername(String username){
        SysUserDTO user = userService.findByName(username);
        if (user == null) {
            return null;
            //throw new BadRequestException("账号不存在");
        } else {
            return user;
        }
    }
}
