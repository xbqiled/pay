package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.criteria.SysOrgsQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysOrgDao extends BaseMapper<SysOrgEntity> {

    Set<SysOrgEntity> findDeptByRoleId(@Param("roleId") Long id);

    List<SysOrgEntity> findByPid(@Param("pid") long pid);

    List<SysOrgEntity> queryAll(SysOrgsQueryCriteria criteria);

    String findNameById(Long pid);

    @Insert("insert into sys_org (name,pid,enabled,create_time) values(#{name},#{pid},#{enabled},#{createTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Integer insert(SysOrgEntity resources);
}
