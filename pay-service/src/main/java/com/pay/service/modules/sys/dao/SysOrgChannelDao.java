package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.sys.criteria.SysOrgChannelQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgChannelEntity;
import com.pay.api.modules.sys.vo.SysOrgChannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-04-12 18:38:50
 */
public interface SysOrgChannelDao extends BaseMapper<SysOrgChannelEntity> {

    List<SysOrgChannelVo> queryOrgChannelPage(Page page, SysOrgChannelQueryCriteria criteria);

    List<SysOrgChannelVo> queryAll(SysOrgChannelQueryCriteria criteria);

    Integer delByOrgId(@Param("orgId") Long orgId);

    List<SysOrgChannelVo> findByOrgId(@Param("orgId") Long orgId);
}
