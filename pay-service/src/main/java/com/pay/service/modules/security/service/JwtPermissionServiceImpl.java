package com.pay.service.modules.security.service;



import com.pay.api.modules.security.dto.SecurityDTO;
import com.pay.api.modules.security.service.JwtPermissionService;
import com.pay.api.modules.sys.dto.SysMenuDTO;
import com.pay.api.modules.sys.dto.SysRoleDTO;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.service.modules.sys.dao.SysOrgDao;
import com.pay.service.modules.sys.dao.SysMenuDao;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@AllArgsConstructor
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
@CacheConfig(cacheNames = "sysRole")
public class JwtPermissionServiceImpl implements JwtPermissionService {

    private final SysRoleService roleDao;

    private final SysMenuDao sysMenuDao;

    private final SysOrgDao sysOrgDao;

    /**
     * key的名称如有修改，请同步修改 UserServiceImpl 中的 update 方法
     * @param user
     * @return
     */
    @Override
    @Cacheable(key = "'loadPermissionByUser:' + #p0.username")
    public SecurityDTO mapToGrantedAuthorities(SysUserDTO user) {
        Set<SysRoleDTO> roleSet = roleDao.findDtoByUsers_Id(user.getId());

        for(SysRoleDTO sysRole : roleSet){
            Set<SysMenuDTO> sysMenus = sysMenuDao.findMenusByRoleId(sysRole.getId());
//            for (SysMenuDTO sysMenuDTO : sysMenus) {
//                if (sysMenuDTO instanceof SysMenuDTO) {
//                    System.out.println("===================sysMenuDTO===================>"  + sysMenuDTO);
//                } else {
//                    System.out.println("===================没匹配上===================>");
//                }
//            }

            sysRole.setMenus(sysMenus);
            //Set<SysDeptDTO> sysDept = sysDeptDao.findDeptByRoleId(sysRole.getId());
            //sysRole.setDepts(sysDept);
        }

        SecurityDTO securityDTO =  new SecurityDTO();
        securityDTO.setRoleSet(roleSet);
        return securityDTO;
    }
}
