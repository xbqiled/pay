package com.pay.service.modules.tools.service;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.api.modules.tools.criteria.OssConfigQueryCriteria;
import com.pay.api.modules.tools.entity.OssConfigEntity;
import com.pay.api.modules.tools.entity.OssConfigParams;
import com.pay.api.modules.tools.service.OssConfigService;
import com.pay.common.core.exception.AppException;
import com.pay.common.core.utils.FileUtil;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.tools.dao.OssConfigDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("ossConfigService")
@CacheConfig(cacheNames = "ossConfig")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class OssConfigServiceImpl extends ServiceImpl<OssConfigDao, OssConfigEntity> implements OssConfigService {

    @Override
    public PageUtils queryPage(OssConfigQueryCriteria bean, Pageable pageable) {
        Page<OssConfigEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<OssConfigEntity> resultList = this.baseMapper.queryOssConfigPage(page, bean);
        return new PageUtils(page.setRecords(resultList));
    }


    public OssConfigEntity paramsToEntity(OssConfigParams bean){
        OssConfigEntity entity = new OssConfigEntity();
        if(bean.getType() == 1){
            entity.setParamKey("QILIU_CLOUD");
            bean.setAliyunPrefix(null);
            bean.setAliyunAccessKeyId(null);
            bean.setAliyunAccessKeySecret(null);
            bean.setAliyunBucketName(null);
            bean.setAliyunDomain(null);
            bean.setAliyunEndPoint(null);
        }else if(bean.getType() == 2){
            entity.setParamKey("ALI_CLOUD");
            bean.setQiniuAccessKey(null);
            bean.setQiniuBucketName(null);
            bean.setQiniuDomain(null);
            bean.setQiniuPrefix(null);
            bean.setQiniuSecretKey(null);
        }else{
            return null;
        }
        entity.setStatus(bean.getStatus());
        entity.setIsActivate(bean.getIsActivate());
        entity.setRemark(bean.getRemark());

        bean.setStatus(null);
        bean.setIsActivate(null);
        bean.setRemark(null);
        entity.setParamValue(JSONObject.toJSONString(bean));
        return entity;
    }

    @Override
    @CacheEvict(allEntries = true)
    public JSONObject saveEntity(OssConfigParams bean) {
        JSONObject result = new JSONObject();
        result.put("flag",true);
        result.put("msg","添加成功");
        try{
            bean.setId(null);
            OssConfigEntity entity = this.paramsToEntity(bean);
            if(entity == null){
                throw new Exception("无效的配置");
            }
            entity.setCreateTime(new Date());
            this.baseMapper.insert(entity);
        }catch (Exception e){
            result.put("flag",false);
            result.put("msg",e.getMessage());
        }
        return result;
    }

    @Override
    @CacheEvict(allEntries = true)
    public JSONObject updateEntity(OssConfigParams bean) {
        JSONObject result = new JSONObject();
        result.put("flag",true);
        result.put("msg","修改成功");
        try{
            Long id = bean.getId();
            bean.setId(null);
            OssConfigEntity entity = this.paramsToEntity(bean);
            if(entity == null){
                throw new Exception("无效的配置");
            }
            entity.setId(id);
            this.baseMapper.updateById(entity);
        }catch (Exception e){
            result.put("flag",false);
            result.put("msg",e.getMessage());
        }
        return result;
    }

    @Override
    @CacheEvict(allEntries = true)
    public JSONObject deleteEntity(Long id) {
        JSONObject result = new JSONObject();
        result.put("flag",true);
        result.put("msg","删除成功");
        try{
            this.baseMapper.deleteById(id);
        }catch (Exception e){
            result.put("flag",false);
            result.put("msg",e.getMessage());
        }
        return result;
    }


    @Override
    @Cacheable(key = "'loadossconfig:' + #key")
    public <T> T getConfigObject(String key, Class<T> clazz) {
        OssConfigEntity ossConfig = baseMapper.getOssConfig();

        if (ossConfig != null && StrUtil.isNotEmpty(ossConfig.getParamValue())) {
            String value = ossConfig.getParamValue();
            if(StrUtil.isNotBlank(value)){
                return new Gson().fromJson(value, clazz);
            }
            try {
                return clazz.newInstance();
            } catch (Exception e) {
                throw new AppException("获取参数失败");
            }
        }
        return null;
    }


    @Override
    @Cacheable(key = "'loadossconfig:' + #p0.id")
    public List<OssConfigEntity> queryByOssConfigEntity(OssConfigEntity bean) {
        return this.baseMapper.queryByOssConfigEntity(bean);
    }
}
