package com.pay.service.modules.report.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.report.criteria.ReportQueryCriteria;
import com.pay.api.modules.report.entity.ReportEntity;
import com.pay.api.modules.report.service.ReportService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.report.dao.ReportDao;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Service("reportService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ReportServiceImpl extends ServiceImpl<ReportDao, ReportEntity> implements ReportService {


    @Override
    public PageUtils queryChannelReportPage(ReportQueryCriteria criteria, Pageable pageable) {
        Page<ReportEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<ReportEntity> resultList = baseMapper.queryChannelReportPage(criteria, page);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<Map<String, Object>> channelDownload(ReportQueryCriteria criteria) {
        List<Map<String, Object>> resultList = baseMapper.queryChannelAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (Map<String, Object> repoty : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("银商编码", repoty.get("orgId") == null  ? "" : repoty.get("orgId"));
            map.put("渠道编码", repoty.get("channeId") == null  ? "" : repoty.get("channeId"));
            map.put("上分金额", repoty.get("amount") == null  ? "" : repoty.get("amount"));
            map.put("上分次数", repoty.get("operateCount") == null  ? "" : repoty.get("operateCount"));
            map.put("会计项目", "玩家上分");
            map.put("统计时间", repoty.get("operateTime") == null  ? "" : repoty.get("operateTime"));
            list.add(map);
        }
        return  list;
    }

    @Override
    public PageUtils queryReportPage(ReportQueryCriteria criteria, Pageable pageable) {
        Page<ReportEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<ReportEntity> resultList = baseMapper.queryReportPage(criteria,page);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<Map<String, Object>> download(ReportQueryCriteria criteria) {
        List<ReportEntity> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (ReportEntity repoty : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("银商名称", repoty.getOrgName());
            map.put("日期", repoty.getDate());
            map.put("上分金额", repoty.getTotalAddpoints());
            map.put("上分次数", repoty.getTotalAddpointsNum());
            map.put("充值金额", repoty.getTotalRecharge());
            map.put("充值次数", repoty.getTotalRechargeNum());
            list.add(map);
        }
        return  list;
    }
}
