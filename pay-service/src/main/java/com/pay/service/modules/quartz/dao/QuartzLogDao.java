package com.pay.service.modules.quartz.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzLogEntity;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface QuartzLogDao extends BaseMapper<QuartzLogEntity> {

    List<QuartzLogEntity> queryQuartzLogPage(Page page, JobQueryCriteria criteria);

    List<QuartzLogEntity> queryAllLog(JobQueryCriteria criteria);

    void addLog(QuartzLogEntity entity);

}
