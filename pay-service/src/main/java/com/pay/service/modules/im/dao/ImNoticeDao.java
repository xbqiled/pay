package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.im.entity.ImNoticeEntity;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImNoticeDao extends BaseMapper<ImNoticeEntity> {
	
}
