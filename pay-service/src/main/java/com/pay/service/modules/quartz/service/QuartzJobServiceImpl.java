package com.pay.service.modules.quartz.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.api.modules.quartz.service.QuartzJobService;
import com.pay.common.core.exception.BadRequestException;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.ValidationUtil;
import com.pay.service.modules.quartz.dao.QuartzJobDao;
import com.pay.service.modules.quartz.utils.QuartzManage;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("quartzJobService")
@CacheConfig(cacheNames = "quartzJob")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class QuartzJobServiceImpl extends ServiceImpl<QuartzJobDao, QuartzJobEntity> implements QuartzJobService {

    @Autowired
    private QuartzManage quartzManage;

    @Override
    @Cacheable(key = "'downloadQuartzJob:'+#p0.jobName+#p0.isPause")
    public List<Map<String, Object>> download(JobQueryCriteria criteria) {
        List<QuartzJobEntity> resultList = baseMapper.queryAllJob(criteria);

        List<Map<String, Object>> list = new ArrayList<>();
        for (QuartzJobEntity quartzJob : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();

            map.put("任务名称", quartzJob.getJobName());
            map.put("Bean名称", quartzJob.getBeanName());
            map.put("执行方法", quartzJob.getMethodName());

            map.put("参数", quartzJob.getParams());
            map.put("表达式", quartzJob.getCronExpression());
            map.put("状态", quartzJob.getIsPause()==1 ? "暂停中" : "运行中");

            map.put("描述", quartzJob.getRemark());
            map.put("创建日期", quartzJob.getUpdateTime());
            list.add(map);
        }
        return  list;
    }


    @Override
    public PageUtils queryQuartzJobPage(JobQueryCriteria criteria, Pageable pageable) {
        Page<QuartzJobEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<QuartzJobEntity> resultList = this.baseMapper.queryQuartzJobPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public List<QuartzJobEntity> queryAllJob(JobQueryCriteria criteria) {
        return baseMapper.queryAllJob(criteria);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public QuartzJobEntity saveEntity(QuartzJobEntity resources) {
        if (!CronExpression.isValidExpression(resources.getCronExpression())){
            throw new BadRequestException("cron表达式格式错误");
        }
        this.baseMapper.insertQuartzJob(resources);
        quartzManage.addJob(resources);
        return resources;
    }


    @Override
    @CachePut(key = "'loadById:'+#p0.id")
    @Transactional(rollbackFor = Exception.class)
    public void updateEntity(QuartzJobEntity resources) {
        if(resources.getId().equals(1L)){
            throw new BadRequestException("该任务不可操作");
        }
        if (!CronExpression.isValidExpression(resources.getCronExpression())){
            throw new BadRequestException("cron表达式格式错误");
        }
        resources.setUpdateTime(new Date());
        quartzManage.updateJobCron(resources);
        this.baseMapper.updateQuartzJobById(resources);
    }


    @Override
    @CacheEvict(key = "'loadById:'+#p0.id")
    @Transactional(rollbackFor = Exception.class)
    public void deleteEntity(QuartzJobEntity quartzJob) {
        if(quartzJob.getId().equals(1L)){
            throw new BadRequestException("该任务不可操作");
        }
        quartzManage.deleteJob(quartzJob);
        this.deleteById(quartzJob.getId());
    }


    @Override
    @Cacheable(key = "'loadById:'+#p0")
    public QuartzJobEntity findById(Long id) {
        QuartzJobEntity quartzJob =  this.baseMapper.queryById(id);
        ValidationUtil.isNull(quartzJob.getId(),"QuartzJob","id",id);
        return quartzJob;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateIsPause(QuartzJobEntity quartzJob) {
        if(quartzJob.getId().equals(1L)){
            throw new BadRequestException("该任务不可操作");
        }
        if (quartzJob.getIsPause()==1) {
            quartzJob.setIsPause(0);
            quartzManage.pauseJob(quartzJob);
        } else {
            quartzJob.setIsPause(1);
            quartzManage.resumeJob(quartzJob);
        }
        this.baseMapper.updateById(quartzJob);
    }


    @Override
    public void execution(QuartzJobEntity quartzJob) {
        if(quartzJob.getId().equals(1L)){
            throw new BadRequestException("该任务不可操作");
        }
        quartzManage.runAJobNow(quartzJob);
    }
}
