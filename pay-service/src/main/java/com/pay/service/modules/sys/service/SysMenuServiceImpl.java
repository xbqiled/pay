package com.pay.service.modules.sys.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.criteria.SysMenuQueryCriteria;
import com.pay.api.modules.sys.dto.SysMenuDTO;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.entity.SysMenuEntity;
import com.pay.api.modules.sys.service.SysMenuService;
import com.pay.api.modules.sys.vo.SysMenuMetaVo;
import com.pay.api.modules.sys.vo.SysMenuVo;
import com.pay.service.modules.sys.dao.SysMenuDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("sysMenuService")
@CacheConfig(cacheNames = "sysMenus")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenuEntity> implements SysMenuService {

    @Override
    public List<SysMenuDTO> queryAll(SysMenuQueryCriteria criteria) {
        return this.baseMapper.queryAll(criteria);
    }

    @Override
    public Object getMenuTree(List<SysMenuEntity> menus) {
        List<Map<String,Object>> list = new LinkedList<>();
        menus.forEach(menu -> {
                    if (menu!=null){
                        List<SysMenuEntity> menuList = this.baseMapper.findByPid(menu.getId());
                        Map<String,Object> map = new HashMap<>();
                        map.put("id",menu.getId());
                        map.put("label",menu.getName());
                        if(menuList!=null && menuList.size()!=0){
                            map.put("children",getMenuTree(menuList));
                        }
                        list.add(map);
                    }
                }
        );
        return list;
    }

    @Override
    @Cacheable(key="'loadListById:'+#p0")
    public List<SysMenuEntity> findByPid(long pid) {
        return this.baseMapper.findByPid(pid);
    }

    @Override
    public Map buildTree(List<SysMenuDTO> menuDTOS) {
        List<SysMenuDTO> trees = new ArrayList<>();

        for (SysMenuDTO menuDTO : menuDTOS) {
            if ("0".equals(menuDTO.getPid().toString())) {
                trees.add(menuDTO);
            }

            for (SysMenuDTO it : menuDTOS) {
                if (it.getPid().equals(menuDTO.getId())) {
                    if (menuDTO.getChildren() == null) {
                        menuDTO.setChildren(new ArrayList<SysMenuDTO>());
                    }
                    menuDTO.getChildren().add(it);
                }
            }
        }
        Map map = new HashMap();
        map.put("content", trees.size() == 0 ? trees : trees);
        map.put("totalElements", menuDTOS != null ? trees.size() : 0);
        return map;
    }

    @Override
    public List<SysMenuDTO> findByRoles(List<SysRoleSmallDTO> roles) {
        Set<SysMenuDTO> menus = new LinkedHashSet<>();
        for (SysRoleSmallDTO role : roles) {
            List<SysMenuDTO> menus1 = this.baseMapper.findByRoles_IdOrderBySortAsc(role.getId()).stream().collect(Collectors.toList());
            menus.addAll(menus1);
        }
        return menus.stream().collect(Collectors.toList());
    }

    @Override
    public List<SysMenuVo> buildMenus(List<SysMenuDTO> menuDTOS) {
        List<SysMenuVo> list = new LinkedList<>();
        menuDTOS.forEach(menuDTO -> {
                    if (menuDTO!=null){
                        List<SysMenuDTO> menuDTOList = menuDTO.getChildren();
                        SysMenuVo menuVo = new SysMenuVo();
                        menuVo.setName(menuDTO.getName());
                        menuVo.setPath(menuDTO.getPath());

                        // 如果不是外链
                        if(!menuDTO.getIFrame()){
                            if(menuDTO.getPid().equals(0L)){
                                //一级目录需要加斜杠，不然访问 会跳转404页面
                                menuVo.setPath("/" + menuDTO.getPath());
                                menuVo.setComponent(StrUtil.isEmpty(menuDTO.getComponent())?"Layout":menuDTO.getComponent());
                            }else if(!StrUtil.isEmpty(menuDTO.getComponent())){
                                menuVo.setComponent(menuDTO.getComponent());
                            }
                        }
                        menuVo.setMeta(new SysMenuMetaVo(menuDTO.getName(),menuDTO.getIcon()));
                        if(menuDTOList!=null && menuDTOList.size()!=0){
                            menuVo.setAlwaysShow(true);
                            menuVo.setRedirect("noredirect");
                            menuVo.setChildren(buildMenus(menuDTOList));
                            // 处理是一级菜单并且没有子菜单的情况
                        } else if(menuDTO.getPid().equals(0L)){
                            SysMenuVo menuVo1 = new SysMenuVo();
                            menuVo1.setMeta(menuVo.getMeta());
                            // 非外链
                            if(!menuDTO.getIFrame()){
                                menuVo1.setPath("index");
                                menuVo1.setName(menuVo.getName());
                                menuVo1.setComponent(menuVo.getComponent());
                            } else {
                                menuVo1.setPath(menuDTO.getPath());
                            }
                            menuVo.setName(null);
                            menuVo.setMeta(null);
                            menuVo.setComponent("Layout");
                            List<SysMenuVo> list1 = new ArrayList<SysMenuVo>();
                            list1.add(menuVo1);
                            menuVo.setChildren(list1);
                        }
                        list.add(menuVo);
                    }
                }
        );
        return list;
    }

    @Override
    @Cacheable(key="'loadById:'+#p0")
    public SysMenuEntity findById(Long id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public SysMenuEntity saveEntity(SysMenuEntity resources) {
        resources.setCreateTime(new Date());
        this.baseMapper.insert(resources);
        return resources;
    }

    @Override
    @CachePut(key="'loadById:'+#p0.id")
    public void updateEntity(SysMenuEntity resources) {
        this.updateById(resources);
    }

    @Override
    @CacheEvict(key="'loadById:'+#p0" , allEntries = true)
    public void deleteEntity(Long id) {
        this.deleteById(id);
    }

    @Override
    public List<Map<String, Object>> download(SysMenuQueryCriteria criteria) {
        List<SysMenuDTO> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (SysMenuDTO sysMenu : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("菜单名称", sysMenu.getName());
            map.put("菜单类型", sysMenu.getType() == 0 ? "目录" : sysMenu.getType() == 1 ? "菜单" : "按钮");
            map.put("权限标识", sysMenu.getPermission());
            map.put("外链菜单", sysMenu.getIFrame() ? "是" : "否");
            map.put("菜单可见", sysMenu.getHidden() ? "否" : "是");
            map.put("是否缓存", sysMenu.getCache() ? "是" : "否");
            map.put("创建日期", sysMenu.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
