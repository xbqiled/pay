package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.entity.SysUsersRolesEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysUsersRolesDao extends BaseMapper<SysUsersRolesEntity> {

    int insertBatch(Set<SysRoleSmallDTO> roles, @Param("userId") Long userId);

    int deleteByUserId(@Param("userId") Long userId);

    int deleteByRoleId(@Param("roleId") Long roleId);

    List<SysUsersRolesEntity> queryByUserId(@Param("userId") Long userId);
}
