package com.pay.service.modules.sys.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.criteria.SysUserQueryCriteria;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.entity.SysUserEntity;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.common.core.exception.EntityExistException;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.StringUtils;
import com.pay.common.core.utils.ValidationUtil;
import com.pay.service.modules.sys.dao.SysUserDao;
import com.pay.service.modules.sys.dao.SysUsersRolesDao;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("sysUserService")
@AllArgsConstructor
@CacheConfig(cacheNames = "sysUser")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {

    private final SysUsersRolesDao usersRolesDao;

    private final SysUsersRolesDao sysUsersRolesDao;

    @Override
    public PageUtils queryUserPage(SysUserQueryCriteria criteria, Pageable pageable) {
        Page<SysUserDTO> page = new Page<>(pageable.getPageNumber() , pageable.getPageSize());
        List<SysUserDTO> resultList = this.baseMapper.queryUserPage(page, criteria);
        for (int i = 0; i <resultList.size() ; i++) {
            resultList.get(i).setRoleList(sysUsersRolesDao.queryByUserId(resultList.get(i).getId()));
        }
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    @Cacheable(key="'loadById:'+#p0")
    public SysUserDTO findById(long id) {
        SysUserDTO sysUserDTO = new SysUserDTO();
        SysUserEntity entity = this.baseMapper.selectById(id);
        BeanUtil.copyProperties(entity,sysUserDTO);
        return sysUserDTO;
    }

    @Override
    @CacheEvict(allEntries = true)
    public Map<String,Object> saveEntity(SysUserDTO dto) {
        Map<String,Object> map = new HashMap<>();
        map.put("flag",true);
        try{
            if(this.baseMapper.findByName(dto.getUsername())!=null){
                throw new EntityExistException(SysUserDTO.class,"username",dto.getUsername());
            }
            SysUserEntity user = new SysUserEntity();
            // 默认密码 123456，此密码是加密后的字符
            if(StringUtils.isBlank(dto.getPassword())){
                user.setPassword("e10adc3949ba59abbe56e057f20f883e");
            }else{
                user.setPassword(dto.getPassword());
            }
            if(StringUtils.isBlank(dto.getAvatar())){
                user.setAvatar("https://i.loli.net/2019/04/04/5ca5b971e1548.jpeg");
            }else{
                user.setAvatar(dto.getAvatar());
            }
            user.setEmail(dto.getEmail());
            user.setUsername(dto.getUsername());
            user.setUsername(dto.getUsername());
            user.setPhone(dto.getPhone());
            Date date = new Date();
            user.setLastPasswordResetTime(date);
            user.setCreateTime(date);
            user.setOrgId(dto.getDept().getId());
            user.setJobId(dto.getJob().getId());
            user.setEnabled(dto.getEnabled());
            try{
                this.baseMapper.create(user);
            }catch (Exception e){
                throw new Exception("用户名或邮箱地址已存在");
            }
            Set<SysRoleSmallDTO> roles  = dto.getRoles();
            //批量插入角色关联记录
            usersRolesDao.insertBatch(roles,user.getId());
        }catch (Exception e){
            map.put("msg",e.getMessage());
            map.put("flag",false);
        }
        return map;
    }

    @Override
    @CacheEvict(allEntries = true)
    public Map<String,Object> updateEntity(SysUserDTO dto) {
        Map<String,Object> map = new HashMap<>();
        map.put("flag",true);
        try{
            SysUserEntity user = new SysUserEntity();
            user.setId(dto.getId());
            if(StringUtils.isBlank(dto.getAvatar())){
                user.setAvatar("https://i.loli.net/2019/04/04/5ca5b971e1548.jpeg");
            }else{
                user.setAvatar(dto.getAvatar());
            }
            user.setEmail(dto.getEmail());
            user.setUsername(dto.getUsername());
            user.setUsername(dto.getUsername());
            user.setPhone(dto.getPhone());
            user.setOrgId(dto.getDept().getId());
            user.setJobId(dto.getJob().getId());
            user.setEnabled(dto.getEnabled());
            try{
                baseMapper.updateById(user);
            }catch (Exception e){
                throw new Exception("用户名或邮箱地址已存在");
            }
            //修改角色关联
            usersRolesDao.deleteByUserId(user.getId());
            usersRolesDao.insertBatch(dto.getRoles(),user.getId());
        }catch (Exception e){
            map.put("msg",e.getMessage());
            map.put("flag",false);
        }
        return map;
    }

    @Override
    @CacheEvict(allEntries = true)
    public void deleteEntity(Long id) {
        //删除角色关联
        usersRolesDao.deleteByUserId(id);
        this.deleteById(id);
    }


    @Override
    @Cacheable(key = "'loadUserByUsername:'+#p0")
    public SysUserDTO findByName(String userName) {
        if(ValidationUtil.isEmail(userName)){
            return this.baseMapper.findByName(userName);
        } else {
            return this.baseMapper.findByName(userName);
        }
    }


    @Override
    @Cacheable(key = "'loadUserByJobId:'+#p0")
    public SysUserDTO findByJobId(Long jobId) {
        return this.baseMapper.findByJobId(jobId);
    }


    @Override
    @Cacheable(key = "'loadUserByOrgId:'+#p0")
    public SysUserDTO findByOrgId(Long orgId) {
        return this.baseMapper.findByOrgId(orgId);
    }


    @Override
    @CacheEvict(allEntries = true)
    public void updatePass(Long id, String encryptPassword) {
        SysUserEntity user = new SysUserEntity();
        user.setPassword(encryptPassword);
        user.setId(id);
        this.baseMapper.updatePassByEntity(user);
    }


    @Override
    @CacheEvict(allEntries = true)
    public void updateAvatar(String username, String url) {
        SysUserEntity user = new SysUserEntity();
        user.setAvatar(url);
        user.setUsername(username);
        this.baseMapper.updateByName(user);
    }


    @Override
    @CacheEvict(allEntries = true)
    public void updateEmail(String username, String email) {
        SysUserEntity user = new SysUserEntity();
        user.setEmail(email);
        user.setUsername(username);
        this.baseMapper.updateByName(user);
    }

    @Override
    public List<SysUserEntity> queryOrgIdList(Long orgId) {
        return baseMapper.queryOrgIdList(orgId);
    }

    @Override
    public  List<Map<String, Object>> download(SysUserQueryCriteria criteria) {
        List<Map<String, Object>> list = new ArrayList<>();
        List<SysUserDTO> resultList = baseMapper.queryAll(criteria);

        for (SysUserDTO userDTO : resultList) {
            List roles = userDTO.getRoles().stream().map(SysRoleSmallDTO::getName).collect(Collectors.toList());
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("用户名", userDTO.getUsername());
            map.put("头像", userDTO.getAvatar());
            map.put("邮箱", userDTO.getEmail());
            map.put("状态", userDTO.getEnabled() ? "启用" : "禁用");
            map.put("手机号码", userDTO.getPhone());
            map.put("角色", roles);
            map.put("部门", userDTO.getDept().getName());
            map.put("岗位", userDTO.getJob().getName());
            map.put("最后修改密码的时间", userDTO.getLastPasswordResetTime());
            map.put("创建日期", userDTO.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
