package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.pay.api.modules.im.entity.ImOfflineNoticeEntity;
import com.pay.api.modules.im.vo.ImOfflineNoticeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImOfflineNoticeDao extends BaseMapper<ImOfflineNoticeEntity> {

    List<ImOfflineNoticeVo> queryOfflineNoticePage(Pagination page, @Param("userId") Integer userId, @Param("timeOrder") Integer timeOrder);

}
