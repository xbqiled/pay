package com.pay.service.modules.trader.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.trader.criteria.OrgAccountQueryCriteria;
import com.pay.api.modules.trader.entity.TraderOrgAccountEntity;
import com.pay.api.modules.trader.vo.TraderOrgAccountVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
public interface TraderOrgAccountDao extends BaseMapper<TraderOrgAccountEntity> {

    List<TraderOrgAccountVo> queryOrgAccountPage(Page page, OrgAccountQueryCriteria criteria);

    List<TraderOrgAccountVo> queryAll(OrgAccountQueryCriteria criteria);

    TraderOrgAccountEntity queryByOrgId(@Param("orgId") Long orgId);

    @Delete("delete from trader_account_record where org_id=#{orgId}")
    TraderOrgAccountEntity deleteByOrgId(@Param("orgId") Long orgId);
}
