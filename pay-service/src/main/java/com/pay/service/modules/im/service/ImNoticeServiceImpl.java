package com.pay.service.modules.im.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImNoticeQueryCriteria;
import com.pay.api.modules.im.dto.ImNoticeDTO;
import com.pay.api.modules.im.entity.ImNoticeEntity;
import com.pay.api.modules.im.service.ImNoticeService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImNoticeDao;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("imNoticeService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImNoticeServiceImpl extends ServiceImpl<ImNoticeDao, ImNoticeEntity> implements ImNoticeService {

    @Override
    public PageUtils queryAll(ImNoticeQueryCriteria criteria) {
        return null;
    }

    @Override
    public List<ImNoticeEntity> queryList(ImNoticeQueryCriteria criteria) {
        return null;
    }

    @Override
    public ImNoticeDTO findById(Integer id) {
        return null;
    }

    @Override
    public ImNoticeDTO create(ImNoticeEntity resources) {
        return null;
    }

    @Override
    public void update(ImNoticeEntity resources) {

    }

    @Override
    public void delete(Integer id) {

    }

}
