package com.pay.service.modules.tools.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.tools.entity.OssStsTokenEntity;

/**
 * 
 * 
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-14 12:19:09
 */
public interface OssStsTokenDao extends BaseMapper<OssStsTokenEntity> {

    OssStsTokenEntity getLastToken();
	
}
