package com.pay.service.modules.im.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImOfflineMessageQueryCriteria;
import com.pay.api.modules.im.dto.ImOfflineMessageDTO;
import com.pay.api.modules.im.entity.ImOfflineMessageEntity;
import com.pay.api.modules.im.service.ImOfflineMessageService;
import com.pay.api.modules.im.vo.ImOfflineMessageVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImOfflineMessageDao;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("imOfflineMessageService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImOfflineMessageServiceImpl extends ServiceImpl<ImOfflineMessageDao, ImOfflineMessageEntity> implements ImOfflineMessageService {



    @Override
    public PageUtils queryOfflineMessagePage(Integer userId, Integer pageSize, Integer pageIndex, Integer timeOrder ) {
        Page<ImOfflineMessageVo> page = new Page<>(pageIndex, pageSize);
        List<ImOfflineMessageVo> resultList = this.baseMapper.queryOfflineMessagePage(page, userId, timeOrder);
        return new PageUtils(page.setRecords(resultList));

    }

    @Override
    public PageUtils getOfflineMessagePage(Integer userId, Integer type, Integer fromId, Integer pageSize, Integer pageIndex, Integer timeOrder) {
        Page<ImOfflineMessageVo> page = new Page<>(pageIndex, pageSize);
        List<ImOfflineMessageVo> resultList = this.baseMapper.getOfflineMessagePage(page, userId, type, fromId, timeOrder);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public Integer batchDelOfflineMessage(List<Integer> idArray, Integer type, Integer userId, Integer sourceId) {
        if (type == 1) {
            return baseMapper.batchDelOfflineMessage(userId, idArray);
        } else if (type == 2) {
            return baseMapper.delOfflineMessageByUserId(userId, sourceId);
        } else if (type == 3) {
            return baseMapper.delOfflineMessageByGroupId(userId, sourceId);
        } else {
            return null;
        }
    }

    @Override
    public PageUtils queryAll(ImOfflineMessageQueryCriteria criteria) {
        return null;
    }

    @Override
    public List<ImOfflineMessageEntity> queryList(ImOfflineMessageQueryCriteria criteria) {
        return null;
    }

    @Override
    public Object queryByUserId(String userId, Integer pageIndex, Integer pageSize, Integer timeOrder) {
        return null;
    }

    @Override
    public ImOfflineMessageDTO findById(Long messageId) {
        return null;
    }

    @Override
    public ImOfflineMessageDTO create(ImOfflineMessageEntity resources) {
        return null;
    }

    @Override
    public void update(ImOfflineMessageEntity resources) {

    }

    @Override
    public void delete(Long messageId) {

    }
}
