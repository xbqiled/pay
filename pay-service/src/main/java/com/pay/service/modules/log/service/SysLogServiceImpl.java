package com.pay.service.modules.log.service;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.kryo.JoinPointModel;
import com.pay.api.modules.log.criteria.LogQueryCriteria;
import com.pay.api.modules.log.entity.SysLogEntity;
import com.pay.api.modules.log.service.SysLogService;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.StringUtils;
import com.pay.service.modules.log.dao.SysLogDao;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("sysLogService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryLogPage(LogQueryCriteria criteria, Pageable pageable) {
        Page<SysLogEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<SysLogEntity> resultList = this.baseMapper.queryLogPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public  List<SysLogEntity> queryAll(LogQueryCriteria criteria) {
        return baseMapper.queryAll(criteria);
    }


    @Override
    public void save(String username, String browser, String ip, Long orgId, JoinPointModel joinPoint, SysLogEntity log) {
        // 方法路径
        String methodName = joinPoint.getMethodName();
        StringBuilder params = new StringBuilder("{");

        // 描述
        if (log != null) {
            log.setDescription(joinPoint.getMethodValue());
        }
        assert log != null;
        log.setRequestIp(ip);
        log.setCreateTime(new Date());
        log.setAddress(StringUtils.getCityInfo(log.getRequestIp()));
        log.setMethod(methodName);
        log.setUsername(username);
        log.setBrowser(browser);
        log.setOrgId(orgId);
        this.insert(log);
    }

    @Override
    public Object findByErrDetail(Long id) {
        return Dict.create().set("exception",this.baseMapper.findExceptionById(id));
    }


    @Override
    public void batchDelLog(String endTime) {

    }


    @Override
    public List<Map<String, Object>> download(LogQueryCriteria criteria) {
        List<Map<String, Object>> list = new ArrayList<>();
        List<SysLogEntity> resultList = baseMapper.queryAll(criteria);
        for (SysLogEntity log : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("用户名", log.getUsername());
            map.put("IP", log.getRequestIp());
            map.put("IP来源", log.getAddress());
            map.put("描述", log.getDescription());
            map.put("浏览器", log.getBrowser());
            map.put("请求耗时/毫秒", log.getTime());
            map.put("异常详情", ObjectUtil.isNotNull(log.getExceptionDetail()) ? log.getExceptionDetail() : "");
            map.put("创建日期", log.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
