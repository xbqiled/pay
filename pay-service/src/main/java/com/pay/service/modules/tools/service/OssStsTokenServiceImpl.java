package com.pay.service.modules.tools.service;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.tools.entity.OssStsTokenEntity;
import com.pay.api.modules.tools.service.OssStsTokenService;
import com.pay.service.modules.tools.dao.OssStsTokenDao;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 *  建议由服务提供方设置超时,在 Provider 上尽量多配置 Consumer 端属性
 *  timeout 方法调用超时
 *  retries 失败重试次数，缺省是 2
 *  loadbalance 负载均衡算法 [3]，缺省是随机 random。还可以有轮询 roundrobin、最不活跃优先 [4] leastactive 等
 *  actives 消费者端，最大并发调用限制，即当 Consumer 对一个服务的并发调用到上限后，新调用会阻塞直到超时
 */
@Service("ossStsTokenService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class OssStsTokenServiceImpl extends ServiceImpl<OssStsTokenDao, OssStsTokenEntity> implements OssStsTokenService {

    @Override
    public OssStsTokenEntity getLastToken() {
        return baseMapper.getLastToken();
    }

    @Override
    public void createOssStsTonken(OssStsTokenEntity ossStsTokenEntity) {
        ossStsTokenEntity.setCreateTime(new Date());
        //设置时间偏移55分钟
        ossStsTokenEntity.setExpirationTime(DateUtil.offsetMinute(new Date(),  55));
        this.insert(ossStsTokenEntity);
    }
}
