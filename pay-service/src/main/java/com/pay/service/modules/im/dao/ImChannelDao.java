package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.im.criteria.ImChannelQueryCriteria;
import com.pay.api.modules.im.entity.ImChannelEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface ImChannelDao extends BaseMapper<ImChannelEntity> {

    List<ImChannelEntity> queryChannelPage(Page page, ImChannelQueryCriteria criteria);

    List<ImChannelEntity> queryAllChannel();

    List<ImChannelEntity> queryAll(ImChannelQueryCriteria criteria);

    ImChannelEntity findByChannelId(@Param("channelId") String channelId);

    Integer refreshAuth(@Param("id") Long id, @Param("authCode") String authCode,
                        @Param("salt") String salt,  @Param("userId") Long userId);

}
