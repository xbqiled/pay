package com.pay.service.modules.im.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImOrgQueryCriteria;
import com.pay.api.modules.im.entity.ImOrgEntity;
import com.pay.api.modules.im.service.ImOrgService;
import com.pay.api.modules.im.vo.ImOrgVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImOrgDao;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Service("imOrgService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImOrgServiceImpl extends ServiceImpl<ImOrgDao, ImOrgEntity> implements ImOrgService {


    @Override
    public PageUtils queryOrgPage(ImOrgQueryCriteria criteria, Pageable pageable) {
        Page<ImOrgVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<ImOrgVo> resultList = baseMapper.queryOrgPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public boolean updateEntity(ImOrgEntity imOrgEntity) {
        this.updateById(imOrgEntity);
        return Boolean.TRUE;
    }


    @Override
    public boolean saveEntity(ImOrgEntity imOrgEntity) {
        this.insert(imOrgEntity);
        return Boolean.TRUE;
    }

    @Override
    public List<Map<String, Object>> download(ImOrgQueryCriteria criteria) {
        List<ImOrgVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (ImOrgVo imOrgVo : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("银商名称", imOrgVo.getOrgName());

            map.put("图标URL", imOrgVo.getIconUrl() == null ? "" : imOrgVo.getIconUrl());
            map.put("类型",   imOrgVo.getType() != null && imOrgVo.getType() == 0 ? "自有渠道": "社会渠道");
            map.put("联系人", imOrgVo.getContact() == null ? "" : imOrgVo.getContact());

            map.put("联系电话", imOrgVo.getPhoneNumber() == null ? "" : imOrgVo.getPhoneNumber());
            map.put("状态",  imOrgVo.getEnabled() != null && imOrgVo.getEnabled() ? "正常" : "失效");
            map.put("创建日期", imOrgVo.getCreateTime());

            map.put("创建人", imOrgVo.getCreateBy());
            map.put("备注", imOrgVo.getNote() == null ? "" : imOrgVo.getNote());
            list.add(map);
        }
        return  list;
    }


    @Override
    public Integer deleteByOrgId(Long orgId) {
        return this.baseMapper.deleteByOrgId(orgId);
    }
}
