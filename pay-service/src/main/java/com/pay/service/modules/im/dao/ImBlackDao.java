package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.im.dto.ImBlackDTO;
import com.pay.api.modules.im.entity.ImBlackEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:47
 */
public interface ImBlackDao extends BaseMapper<ImBlackEntity> {

    ImBlackDTO findById(@Param("id") Integer id);

    ImBlackDTO create(ImBlackEntity resources);
}
