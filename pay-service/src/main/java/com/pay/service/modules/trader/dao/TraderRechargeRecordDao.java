package com.pay.service.modules.trader.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.trader.criteria.RechargeRecordQueryCriteria;
import com.pay.api.modules.trader.entity.TraderRechargeRecordEntity;
import com.pay.api.modules.trader.vo.TraderRechargeRecordVo;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:08
 */
public interface TraderRechargeRecordDao extends BaseMapper<TraderRechargeRecordEntity> {

    List<TraderRechargeRecordVo> queryRechargeRecordPage(Page page, RechargeRecordQueryCriteria criteria);

    List<TraderRechargeRecordVo> queryAll(RechargeRecordQueryCriteria criteria);

}
