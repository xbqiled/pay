package com.pay.service.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.report.criteria.ReportQueryCriteria;
import com.pay.api.modules.report.entity.ReportEntity;

import java.util.List;
import java.util.Map;

public interface ReportDao extends BaseMapper<ReportEntity> {

    List<ReportEntity> queryReportPage(ReportQueryCriteria criteria, Page page);

    List<ReportEntity> queryAll(ReportQueryCriteria criteria);

    List<ReportEntity> queryReportCounts(ReportQueryCriteria criteria);

    List<ReportEntity> queryChannelReportPage(ReportQueryCriteria criteria, Page page);

    List<Map<String, Object>> queryChannelAll(ReportQueryCriteria criteria);
}
