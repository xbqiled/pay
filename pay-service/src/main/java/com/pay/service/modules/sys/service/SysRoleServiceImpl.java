package com.pay.service.modules.sys.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.criteria.SysRoleQueryCriteria;
import com.pay.api.modules.sys.dto.SysRoleDTO;
import com.pay.api.modules.sys.dto.SysRoleSmallDTO;
import com.pay.api.modules.sys.entity.SysMenuEntity;
import com.pay.api.modules.sys.entity.SysRoleEntity;
import com.pay.api.modules.sys.service.SysRoleService;
import com.pay.common.core.exception.SysException;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.config.RedisConfig;
import com.pay.service.modules.sys.dao.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@AllArgsConstructor
@Service("sysRoleService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRoleEntity> implements SysRoleService {

    @Autowired
    private RedisTemplate redisTemplate;

    private final SysMenuDao sysMenuDao;

    private final SysRolesOrgsDao rolesOrgsDao;

    private final SysUsersRolesDao sysUsersRolesDao;

    private final SysRolesMenusDao rolesMenusDao;


    @Override
    public PageUtils queryRolePage(SysRoleQueryCriteria criteria, Pageable pageable) {
        Page<SysRoleEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<SysRoleEntity> resultList = baseMapper.queryRolePage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    @CacheEvict(cacheNames = "sysRole", allEntries = true)
    @Transactional
    public Map<String, Object> saveEntity(SysRoleDTO sysRoleDTO) {
        Map<String, Object> map = new HashMap<>();
        map.put("flag", true);
        try {
            SysRoleEntity sysRole = new SysRoleEntity();
            BeanUtil.copyProperties(sysRoleDTO, sysRole);
            List<Map> list = this.baseMapper.findByName(sysRoleDTO.getName());
            if (list != null && list.size() > 0) {
                throw new Exception("该角色名称已存在");
            }
            sysRole.setCreateTime(new Date());
            this.baseMapper.insert(sysRole);
            Long id = sysRole.getId();
            //批量插入菜单关联
            if (sysRoleDTO.getMenus() != null && sysRoleDTO.getMenus().size() > 0) {
                rolesMenusDao.insertBatch(sysRoleDTO.getMenus(), id);
            }
            sysRoleDTO = this.baseMapper.findById(id);
            sysRoleDTO.setMenus(sysMenuDao.findMenusByRoleId(id));
        } catch (Exception e) {
            map.put("flag", false);
            map.put("msg", e.getMessage());
        }
        return map;
    }

    @Override
    @CachePut(cacheNames = "sysRole", key = "'loadById:'+#p0.id")
    public Map<String, Object> updateEntity(SysRoleEntity sysRoleEntity) {
        Map<String, Object> map = new HashMap<>();
        map.put("flag", true);
        try {
            SysRoleDTO bean = this.baseMapper.findById(sysRoleEntity.getId());
            if (!bean.getName().equalsIgnoreCase(sysRoleEntity.getName())) {
                List<Map> list = this.baseMapper.findByName(sysRoleEntity.getName());
                if (list != null && list.size() > 0) {
                    throw new Exception("该角色名称已存在");
                }
            }
            this.updateById(sysRoleEntity);
        } catch (Exception e) {
            map.put("flag", false);
            map.put("msg", e.getMessage());
        }
        return map;

    }

    @Override
    public void deleteEntity(Long id) {
        //批量删除菜单关联
        rolesMenusDao.deleteByRoleId(id);
        //批量删除部门关联
        rolesOrgsDao.deleteByRoleId(id);
        //批量删除用户关联
        sysUsersRolesDao.deleteByRoleId(id);
        //删除自己数据
        this.baseMapper.deleteById(id);
    }


    //    @Override
//    public PageUtils queryRolePage(SysCommonQueryCriteria criteria) {
//        Page<SysRoleDTO> page = new Page<>(criteria.getPage() , criteria.getSize());
//        List<SysRoleDTO> resultList = this.baseMapper.queryRolePage(page, criteria);
//        resultList.forEach(role->{
//                if (role!=null){
//                    //role.setPermissions(permissionDao.findPermissionByRoleId(role.getId()));
//                    role.setMenus(menuDao.findMenusByRoleId(role.getId()));
//                    role.setDepts(deptDao.findDeptByRoleId(role.getId()));
//                }
//            }
//        );
//        return new PageUtils(page.setRecords(resultList));
//
//    }

    @Override
    @Cacheable(cacheNames = "sysRole", key = "'loadByMenuId:'+#p0")
    public SysRoleDTO findByMenuId(long id) {
        return this.baseMapper.findByMenuId(id);
    }

//    @Override
//    public SysRoleDTO create(SysRoleDTO roleDTO) {
//        SysRoleEntity role = new SysRoleEntity();
//        role.setLevel(roleDTO.getLevel());
//        role.setCreateTime(new Date());
//        role.setDataScope(roleDTO.getDataScope());
//        role.setName(roleDTO.getName());
//        role.setRemark(roleDTO.getRemark());
//        this.baseMapper.insert(role);
//        Long id = role.getId();
//        //批量插入权限关联
////        if(roleDTO.getPermissions()!=null){
////            rolesPermissionsDao.insertBatch(roleDTO.getPermissions(),id);
////        }
//        //批量插入菜单关联
//        if(roleDTO.getMenus()!=null){
//            rolesMenusDao.insertBatch(roleDTO.getMenus(),id);
//        }
//        //批量插入部门关联
//        if(roleDTO.getDepts()!=null) {
//            rolesDeptsDao.insertBatch(roleDTO.getDepts(), id);
//        }
//        roleDTO =  this.baseMapper.findById(id);
//        roleDTO.setDepts(deptDao.findDeptByRoleId(id));
//        return roleDTO;
//    }

//    @Override
//    public void update(SysRoleDTO roleDTO) {
//        if(roleDTO.getId()==null){
//            throw new SysException("id不能为空");
//        }
//        SysRoleEntity role = new SysRoleEntity();
//        Long id = roleDTO.getId();
//        role.setId(id);
//        role.setLevel(roleDTO.getLevel());
//        role.setDataScope(roleDTO.getDataScope());
//        role.setName(roleDTO.getName());
//        role.setRemark(roleDTO.getRemark());
//        this.baseMapper.updateById(role);
    //批量插入权限关联
//        if(roleDTO.getPermissions()!=null) {
//            rolesPermissionsDao.deleteByRoleId(id);
//            rolesPermissionsDao.insertBatch(roleDTO.getPermissions(), id);
//        }
//        //批量插入菜单关联
//        if(roleDTO.getMenus()!=null) {
//            rolesMenusDao.deleteByRoleId(id);
//            rolesMenusDao.insertBatch(roleDTO.getMenus(), id);
//        }
//        //批量插入部门关联
//        if(roleDTO.getDepts()!=null) {
//            rolesDeptsDao.deleteByRoleId(id);
//            rolesDeptsDao.inser  tBatch(roleDTO.getDepts(), id);
//        }
//    }
//    @Override
//    public void delete(Long id) {
//        if(id==null){
//            throw new SysException("id不能为空");
//        }
//        //批量删除权限关联
//        //rolesPermissionsDao.deleteByRoleId(id);
//        //批量删除菜单关联
//        rolesMenusDao.deleteByRoleId(id);
//        //批量删除部门关联
//        rolesDeptsDao.deleteByRoleId(id);
//        this.baseMapper.deleteById(id);
//    }

    @Override
    @Cacheable(cacheNames = "sysRole", key = "'loadListByUserId'+#p0")
    public List<SysRoleSmallDTO> findByUsers_Id(Long id) {
        return this.baseMapper.findSmallByUsersId(id).stream().collect(Collectors.toList());
    }

    @Override
    @Cacheable(cacheNames = "sysRole", key = "'loadSetByUserId'+#p0")
    public Set<SysRoleDTO> findDtoByUsers_Id(Long id) {
        Set<SysRoleDTO> roleDTOS = this.baseMapper.findByUsersId(id);
//        Iterator<SysRoleDTO> it = roleDTOS.iterator();
//        while (it.hasNext()) {
//            SysRoleDTO role = it.next();
//            role.setPermissions(permissionDao.findPermissionByRoleId(role.getId()));
//            role.setDepts(deptDao.findDeptByRoleId(role.getId()));
//            role.setMenus(menuDao.findMenusByRoleId(role.getId()));
//        }
        return roleDTOS;
    }

    @Override
    public Integer findByRoles(Set<SysRoleSmallDTO> roles) {
        return this.baseMapper.findByRoles(roles);
    }

    @Override
    public void updatePermission(SysRoleDTO roleDTO) {
        if (roleDTO.getId() == null) {
            throw new SysException("id不能为空");
        }
//        rolesPermissionsDao.deleteByRoleId(roleDTO.getId());
//        rolesPermissionsDao.insertBatch(roleDTO.getPermissions(),roleDTO.getId());
    }


    @Override
    @CacheEvict(cacheNames = "sysUser", key = "loadUserByUsername", allEntries = true)
    public void updateMenu(SysRoleDTO roleDTO) {
        if (roleDTO.getId() == null) {
            throw new SysException("id不能为空");
        }
        rolesMenusDao.deleteByRoleId(roleDTO.getId());
        if (roleDTO.getMenus() != null && roleDTO.getMenus().size() > 0) {
            rolesMenusDao.insertBatch(roleDTO.getMenus(), roleDTO.getId());
        }

//        Set<Object> keys = redisTemplate.keys("*sysUser::loadUserByUsername*");
//        redisTemplate.delete(keys);
    }

    @Override
    public void untiedMenu(SysMenuEntity menu) {
        rolesMenusDao.deleteByMenuId(menu.getId());
    }

    @Override
    public Map queryAll(SysRoleQueryCriteria criteria) {
//        Page<SysRoleEntity> page = this.selectPage(
//                new Page<SysRoleEntity>(criteria.getPage(),criteria.getSize()),
//                new EntityWrapper<SysRoleEntity>()
//                        .eq(false,"name",criteria.getName())
//                        .orderBy("level",true)
//        );
        List<SysRoleEntity> resultList = baseMapper.queryAll(criteria);

        Map<String, Object> result = new HashMap<>();
//        result.put("content",page.getRecords());
//        result.put("totalElements",page.getTotal());
        return result;
    }

    @Override
    public Object queryAll() {
        return this.baseMapper.selectList(null);
    }

    @Override
    public List<Map<String, Object>> download(SysRoleQueryCriteria criteria) {
        List<Map<String, Object>> list = new ArrayList<>();
        List<SysRoleEntity> resultList = baseMapper.queryAll(criteria);
        for (SysRoleEntity role : resultList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("角色名称", role.getName());
            map.put("默认权限", role.getPermission());
            map.put("角色级别", role.getLevel());
            map.put("描述", role.getRemark());
            map.put("创建日期", role.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
