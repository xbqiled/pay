package com.pay.service.modules.trader.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.ImUserInfoService;
import com.pay.api.modules.trader.criteria.CsQueryCriteria;
import com.pay.api.modules.trader.entity.TraderCsInfoEntity;
import com.pay.api.modules.trader.service.TraderCsInfoService;
import com.pay.api.modules.trader.vo.TraderCsInfoVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.trader.dao.TraderCsInfoDao;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.pay.common.core.constant.PayConstant.DEFAULT_USER_AVATAR;
import static com.pay.common.core.constant.PayConstant.IM_USER_URL;


@AllArgsConstructor
@Service("traderCsInfoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class TraderCsInfoServiceImpl extends ServiceImpl<TraderCsInfoDao, TraderCsInfoEntity> implements TraderCsInfoService {

    private final ImUserInfoService imUserInfoService;

    @Override
    public PageUtils queryCsPage(CsQueryCriteria criteria, Pageable pageable) {
        Page<TraderCsInfoVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<TraderCsInfoVo> resultList = baseMapper.queryCsPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<Map<String, Object>> download(CsQueryCriteria criteria) {
        List<TraderCsInfoVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();

        for (TraderCsInfoVo csInfo : resultList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("客服名称", csInfo.getCsName());

            map.put("归属组织", csInfo.getOrgName());
            map.put("IM账号", csInfo.getImAccount());
            map.put("IM密码", csInfo.getImPassword());

            map.put("银商联系人", "普通客服");
            map.put("客服类型", csInfo.getType() == 1 ? "推广客服" : "普通客服");
            map.put("URL地址", csInfo.getUrl());

            map.put("状态", csInfo.getStatus() == 1 ? "正常" : "失效");
            map.put("创建日期", csInfo.getCreateTime());
            map.put("创建人", csInfo.getCreateName());
            list.add(map);
        }
        return list;
    }

    @Override
    public TraderCsInfoEntity saveEntity(TraderCsInfoEntity traderCsInfo, Long userId) {
        traderCsInfo.setCreateBy(userId);
        traderCsInfo.setCreateTime(new Date());
        if (StrUtil.isBlank(traderCsInfo.getAvatarUrl())) {
            traderCsInfo.setAvatarUrl(DEFAULT_USER_AVATAR);
        }
        //生成对于的IM账号和ID
        ImUserInfoEntity userInfo = imUserInfoService.registerUser(traderCsInfo.getOrgId(), traderCsInfo.getCsName(), traderCsInfo.getImPassword(),
                traderCsInfo.getSupportedPms(), traderCsInfo.getAvatarUrl());
        traderCsInfo.setImAccount(userInfo.getUserName());
        traderCsInfo.setUrl(IM_USER_URL + "/" + userInfo.getUserName());
        traderCsInfo.setImPassword(traderCsInfo.getImPassword());
        this.insert(traderCsInfo);
        return traderCsInfo;
    }

    @Override
    public boolean updateEntity(TraderCsInfoEntity traderCsInfo) {
        //修改密码,状态
        imUserInfoService.modifyPwByAccount(
                traderCsInfo.getImAccount(), traderCsInfo.getCsName(), traderCsInfo.getStatus(), traderCsInfo.getOrgId(),
                traderCsInfo.getImPassword(), traderCsInfo.getSupportedPms(), traderCsInfo.getAvatarUrl());
        //修改其他信息
        return this.updateById(traderCsInfo);
    }

    @Override
    public void deleteEntity(Long id) {
        TraderCsInfoEntity traderCsInfo = selectById(id);
        imUserInfoService.delImUser(traderCsInfo.getImAccount(), traderCsInfo.getOrgId().toString());
        this.baseMapper.deleteById(id);
    }
}
