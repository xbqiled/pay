package com.pay.service.modules.trader.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.trader.criteria.ReplyConfigQueryCriteria;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import com.pay.api.modules.trader.vo.PayConfigVo;
import com.pay.api.modules.trader.vo.TraderReplyConfigVo;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-03 13:56:09
 */
public interface TraderReplyConfigDao extends BaseMapper<TraderReplyConfigEntity> {

    List<TraderReplyConfigVo> queryTraderReplyConfigPage(Page page, ReplyConfigQueryCriteria criteria);

    List<TraderReplyConfigVo> queryAll(ReplyConfigQueryCriteria criteria);

    List<TraderReplyConfigEntity> selectConfigByOrgId(Long orgId);

    List<PayConfigVo> getReplyConfigList(Integer userId);
}
