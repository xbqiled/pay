package com.pay.service.modules.im.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImOfflineNoticeQueryCriteria;
import com.pay.api.modules.im.dto.ImOfflineNoticeDTO;
import com.pay.api.modules.im.entity.ImOfflineNoticeEntity;
import com.pay.api.modules.im.service.ImOfflineNoticeService;
import com.pay.api.modules.im.vo.ImOfflineNoticeVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImOfflineNoticeDao;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("imOfflineNoticeService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImOfflineNoticeServiceImpl extends ServiceImpl<ImOfflineNoticeDao, ImOfflineNoticeEntity> implements ImOfflineNoticeService {

    @Override
    public PageUtils queryOfflineNoticePage(Integer userId, Integer pageSize, Integer pageIndex, Integer timeOrder) {
        Page<ImOfflineNoticeVo> page = new Page<>(pageIndex,pageSize );
        List<ImOfflineNoticeVo> resultList = this.baseMapper.queryOfflineNoticePage(page, userId, timeOrder);
        return new PageUtils(page.setRecords(resultList));
    }
    @Override
    public PageUtils queryAll(ImOfflineNoticeQueryCriteria criteria) {
        return null;
    }

    @Override
    public List<ImOfflineNoticeEntity> queryByUserId(String userId, Integer pageIndex, Integer pageSize, Integer timeOrder) {
        return null;
    }

    @Override
    public List<ImOfflineNoticeEntity> queryList(ImOfflineNoticeQueryCriteria criteria) {
        return null;
    }

    @Override
    public ImOfflineNoticeDTO findById(Integer id) {
        return null;
    }

    @Override
    public ImOfflineNoticeDTO create(ImOfflineNoticeEntity resources) {
        return null;
    }

    @Override
    public void update(ImOfflineNoticeEntity resources) {

    }

    @Override
    public void delete(Integer id) {

    }

}
