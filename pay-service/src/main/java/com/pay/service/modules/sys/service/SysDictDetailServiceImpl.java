package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.criteria.SysDictDetailQueryCriteria;
import com.pay.api.modules.sys.entity.SysDictDetailEntity;
import com.pay.api.modules.sys.service.SysDictDetailService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.sys.dao.SysDictDetailDao;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("sysDictDetailService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysDictDetailServiceImpl extends ServiceImpl<SysDictDetailDao, SysDictDetailEntity> implements SysDictDetailService {

    @Override
    public PageUtils queryAll(String name, Integer pageSize, Integer pageIndex) {
        Page<SysDictDetailEntity> page = new Page<>(pageIndex, pageSize);
        List<SysDictDetailEntity> resultList = this.baseMapper.queryDictDetailPage(page, name);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public PageUtils queryDictDetailPage(SysDictDetailQueryCriteria criteria, Pageable pageable) {
        Page<SysDictDetailEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<SysDictDetailEntity> resultList = this.baseMapper.queryDictDetailPage(page, criteria.getDictName());
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public SysDictDetailEntity saveEntity(SysDictDetailEntity resources) {
        this.baseMapper.insert(resources);
        return resources;
    }

    @Override
    public boolean updateEntity(SysDictDetailEntity resources) {
        return this.updateById(resources);
    }

    @Override
    public boolean delEntity(Long id) {
        return this.deleteById(id);
    }
}
