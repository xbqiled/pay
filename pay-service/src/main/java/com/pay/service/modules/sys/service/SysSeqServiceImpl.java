package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.entity.SysSeqEntity;
import com.pay.api.modules.sys.service.SysSeqService;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.Query;
import com.pay.service.modules.sys.dao.SysSeqDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


@Service("sysSeqService")
@CacheConfig(cacheNames = "sysSeq")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysSeqServiceImpl extends ServiceImpl<SysSeqDao, SysSeqEntity> implements SysSeqService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysSeqEntity> page = this.selectPage(
                new Query<SysSeqEntity>(params).getPage(),
                new EntityWrapper<SysSeqEntity>()
        );

        return new PageUtils(page);
    }

}
