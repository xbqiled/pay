package com.pay.service.modules.im.service;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImChannelQueryCriteria;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.api.modules.im.service.ImChannelService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImChannelDao;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("imChannelService")
@AllArgsConstructor
@CacheConfig(cacheNames = "imChannel")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImChannelServiceImpl extends ServiceImpl<ImChannelDao, ImChannelEntity> implements ImChannelService {

    @Override
    public List<Map<String, Object>> download(ImChannelQueryCriteria criteria) {
        List<ImChannelEntity> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();

        for (ImChannelEntity imChannelEntity : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("渠道名称", imChannelEntity.getChannelName());

            map.put("图标URL", imChannelEntity.getIconUrl() == null ? "" : imChannelEntity.getIconUrl());
            map.put("类型",   imChannelEntity.getType() != null && imChannelEntity.getType() == 0 ? "自有渠道": "社会渠道");
            map.put("状态",   imChannelEntity.getStatus() != null && imChannelEntity.getStatus() == 1 ? "正常": "失效");

            map.put("创建日期", imChannelEntity.getCreateTime());
            map.put("创建人", imChannelEntity.getCreateBy());
            map.put("备注", imChannelEntity.getNote() == null ? "" : imChannelEntity.getNote());

            map.put("修改人", imChannelEntity.getUpdateBy());
            map.put("修改日期", imChannelEntity.getUpdateTime());

            map.put("偏移", imChannelEntity.getSalt());
            map.put("秘钥", imChannelEntity.getAuthCode());
            list.add(map);
        }
        return  list;
    }


    @Override
    public PageUtils queryChannelPage(ImChannelQueryCriteria criteria, Pageable pageable) {
        Page<ImChannelEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<ImChannelEntity> resultList = baseMapper.queryChannelPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    @CacheEvict(allEntries = true)
    public boolean refreshAuth(Long id, Long userId) {
        String authCode = RandomUtil.randomString(16);
        String salt = authCode;
        baseMapper.refreshAuth(id, authCode, salt, userId);
        return Boolean.TRUE;
    }


    @Override
    @Cacheable(key="'loadById:'+#p0")
    public ImChannelEntity findByChannelId(String channelId) {
        return baseMapper.findByChannelId(channelId);
    }


    @Override
    @CacheEvict(allEntries = true)
    public boolean deleteEntity(Long id) {
        this.deleteById(id);
        return Boolean.TRUE;
    }


    @Override
    @CacheEvict(allEntries = true)
    public boolean updateEntity(ImChannelEntity imChannelEntity, Long userId) {
        imChannelEntity.setUpdateTime(new Date());
        imChannelEntity.setUpdateBy(userId);
        this.updateById(imChannelEntity);
        return Boolean.TRUE;
    }


    @Override
    @CacheEvict(allEntries = true)
    public boolean saveEntity(ImChannelEntity imChannelEntity, Long userId) {
        ImChannelEntity resultModel = baseMapper.findByChannelId(imChannelEntity.getChannelId());
        if (resultModel != null) {
           return Boolean.FALSE;
        }

        imChannelEntity.setCreateBy(userId);
        imChannelEntity.setCreateTime(new Date());

        imChannelEntity.setUpdateBy(userId);
        imChannelEntity.setUpdateTime(new Date());
        String authCode = RandomUtil.randomString(16);

        String salt = authCode;
        imChannelEntity.setAuthCode(authCode);
        imChannelEntity.setSalt(salt);

        this.insert(imChannelEntity);
        return Boolean.TRUE;
    }
}
