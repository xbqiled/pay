package com.pay.service.modules.sys.service;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.monitor.service.SysVisitsService;
import com.pay.api.modules.report.criteria.ReportQueryCriteria;
import com.pay.api.modules.report.entity.ReportEntity;
import com.pay.api.modules.sys.entity.SysVisitsEntity;
import com.pay.common.core.utils.StringUtils;
import com.pay.service.modules.log.dao.SysLogDao;
import com.pay.service.modules.report.dao.ReportDao;
import com.pay.service.modules.sys.dao.SysVisitsDao;
import com.pay.service.modules.trader.dao.TraderAccountRecordDao;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("sysVisitsService")
@AllArgsConstructor
@CacheConfig(cacheNames = "sysVisits")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysVisitsServiceImpl extends ServiceImpl<SysVisitsDao, SysVisitsEntity> implements SysVisitsService {

    private final SysLogDao logDao;

    private final TraderAccountRecordDao traderAccountRecordDao;

    private final ReportDao reportDao;


    @Override
    public void save() {
        LocalDate localDate = LocalDate.now();
        SysVisitsEntity visits = this.baseMapper.findByDate(localDate.toString());
        if(visits == null){
            visits = new SysVisitsEntity();
            visits.setWeekDay(StringUtils.getWeekDay());
            visits.setPvCounts(1L);
            visits.setIpCounts(1L);
            visits.setDate(localDate.toString());
            this.baseMapper.insert(visits);
        }
    }

    @Override
    public void count() {
        LocalDate localDate = LocalDate.now();
        SysVisitsEntity visits = this.baseMapper.findByDate(localDate.toString());
        visits.setPvCounts(visits.getPvCounts()+1);
        long ipCounts = logDao.findIp(localDate.toString(), localDate.plusDays(1).toString());
        visits.setIpCounts(ipCounts);
        this.baseMapper.updateById(visits);
    }

    @Override
    public Object get(Long orgId) {
        Map map = new HashMap();
        map.put("todayTotalAmount",traderAccountRecordDao.queryTodayTotalAmountByOrgId(orgId));
        map.put("todayTotalCount",traderAccountRecordDao.queryTodayTotalCountByOrgId(orgId));
        map.put("weekTotalAmount",traderAccountRecordDao.queryWeekTotalAmountByOrgId(orgId));
        map.put("weekTotalCount",traderAccountRecordDao.queryWeekTotalCountByOrgId(orgId));
        return map;
    }

    @Override
    public Object getChartData(Long orgId) {
        Map map = new HashMap();
        ReportQueryCriteria bean = new ReportQueryCriteria();
        bean.setWeek(true);
        bean.setOrgId(orgId);
        List<ReportEntity> list = reportDao.queryReportCounts(bean);
        List<String> days = new ArrayList<>();
        List<String> totalAmount = new ArrayList<>();
        List<String> totalCount = new ArrayList<>();
        for (int i = 0; i <list.size() ; i++) {
            days.add(list.get(i).getDate());
            totalAmount.add(list.get(i).getTotalAddpoints());
            totalCount.add(list.get(i).getTotalAddpointsNum().toString());
        }
        map.put("days", days);
        map.put("totalAmounts", totalAmount);
        map.put("totalCounts", totalCount);
        return map;
    }

}
