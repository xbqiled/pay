package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.pay.api.modules.im.entity.ImOfflineMessageEntity;
import com.pay.api.modules.im.vo.ImOfflineMessageVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:46
 */
public interface ImOfflineMessageDao extends BaseMapper<ImOfflineMessageEntity> {

    List<ImOfflineMessageVo> queryOfflineMessagePage(Pagination page, @Param("userId") Integer userId, @Param("timeOrder") Integer timeOrder);

    List<ImOfflineMessageVo> getOfflineMessagePage(Pagination page, @Param("userId") Integer userId, @Param("type") Integer type, @Param("fromId") Integer fromId, @Param("timeOrder") Integer timeOrder);

    Integer batchDelOfflineMessage(@Param("userId") Integer userId, @Param("list") List<Integer> list);

    Integer delOfflineMessageByUserId(@Param("userId") Integer userId, @Param("sourceId") Integer sourceId);

    Integer delOfflineMessageByGroupId(@Param("userId") Integer userId, @Param("sourceId") Integer sourceId);

    //delete data older than several days
    @Delete("delete FROM im_message WHERE DATE_SUB(CURDATE(), INTERVAL #{days} DAY) > DATE(create_time)")
    void deleteByDaysBefore(@Param("days") Integer days);
}
