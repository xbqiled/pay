package com.pay.service.modules.trader.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.pay.api.json.AliPayConfig;
import com.pay.api.json.BankPayConfig;
import com.pay.api.json.PayConfig;
import com.pay.api.json.WxPayConfig;
import com.pay.api.modules.security.entity.JwtUser;
import com.pay.api.modules.trader.criteria.ReplyConfigQueryCriteria;
import com.pay.api.modules.trader.dto.ReplyConfigDto;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import com.pay.api.modules.trader.service.TraderReplyConfigService;
import com.pay.api.modules.trader.vo.PayConfigVo;
import com.pay.api.modules.trader.vo.TraderReplyConfigVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.trader.dao.TraderReplyConfigDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("traderReplyConfigService")
@CacheConfig(cacheNames = "replyConfig")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class TraderReplyConfigServiceImpl extends ServiceImpl<TraderReplyConfigDao, TraderReplyConfigEntity> implements TraderReplyConfigService {


    @Override
    public PageUtils queryReplyConfigPage(ReplyConfigQueryCriteria criteria, Pageable pageable) {
        Page<TraderReplyConfigVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<TraderReplyConfigVo> resultList = baseMapper.queryTraderReplyConfigPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public List<PayConfig> getReplyConfigList(Integer userId) {
        List<PayConfigVo> list = baseMapper.getReplyConfigList(userId);
        List<PayConfig> resultList = new ArrayList<>();
        if (CollUtil.isNotEmpty(list)) {
            for (PayConfigVo vo : list) {
                Integer type = vo.getType();
                switch (type) {
                    case 1: {
                        PayConfig config = new Gson().fromJson(vo.getConfig(), WxPayConfig.class);
                        resultList.add(config);
                    }
                    break;
                    case 2: {
                        PayConfig config = new Gson().fromJson(vo.getConfig(), AliPayConfig.class);
                        resultList.add(config);
                    }
                    break;

                    case 3:
                    case 4:
                    case 5:{
                        PayConfig config = new Gson().fromJson(vo.getConfig(), BankPayConfig.class);
                        resultList.add(config);
                    }
                    break;
                }
            }
        }
        return resultList;
    }


    @Override
    public List<Map<String, Object>> download(ReplyConfigQueryCriteria criteria) {
        List<TraderReplyConfigVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        Map<Integer, String> types = new HashMap<>();
        types.put(1, "微信扫码");
        types.put(2, "支付宝扫码");
        types.put(3, "银行卡转账");
        types.put(4, "微信转账");
        types.put(5, "支付宝转账");
        for (TraderReplyConfigVo replyConfig : resultList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("ID", replyConfig.getId());
            map.put("类型", types.get(replyConfig.getType()));
            map.put("云商编码", replyConfig.getOrgName());
            map.put("配置", replyConfig.getConfig());
            map.put("创建时间", replyConfig.getCreateTime());
            map.put("创建人", replyConfig.getCreateName());
            map.put("修改时间", replyConfig.getModifyTime());
            map.put("修改人", replyConfig.getModifyName());
            list.add(map);
        }
        return list;
    }


    @Override
    @CacheEvict(cacheNames = "replyConfig:details", allEntries = true)
    public TraderReplyConfigEntity saveEntity(ReplyConfigDto replyConfigDto, JwtUser user) {
        TraderReplyConfigEntity replyConfig = new TraderReplyConfigEntity();
        BeanUtil.copyProperties(replyConfigDto, replyConfig);

        String configStr = "";
        switch (replyConfigDto.getType()) {
            case 1: {
                WxPayConfig config = new WxPayConfig();
                BeanUtil.copyProperties(replyConfigDto, config);
                configStr = new Gson().toJson(config);
            }
            break;
            case 2: {
                AliPayConfig config = new AliPayConfig();
                BeanUtil.copyProperties(replyConfigDto, config);
                configStr = new Gson().toJson(config);
            }
            break;
            case 3:
            case 4:
            case 5: {
                BankPayConfig config = new BankPayConfig();
                BeanUtil.copyProperties(replyConfigDto, config);
                configStr = new Gson().toJson(config);
            }
            break;
        }
        replyConfig.setCreateTime(new Date());
        replyConfig.setCreateBy(user.getId());
        replyConfig.setConfig(configStr);
        replyConfig.setOrgId(user.getOrgId());
        this.insert(replyConfig);
        return replyConfig;
    }


    @Override
    @CacheEvict(cacheNames = "replyConfig:details", allEntries = true)
    public boolean updateEntity(ReplyConfigDto replyConfigDto, JwtUser user) {
        TraderReplyConfigEntity replyConfig = new TraderReplyConfigEntity();
        BeanUtil.copyProperties(replyConfigDto, replyConfig);

        String configStr = "";
        switch (replyConfigDto.getType()) {
            case 1: {
                WxPayConfig config = new WxPayConfig();
                BeanUtil.copyProperties(replyConfigDto, config);
                configStr = new Gson().toJson(config);
            }
            break;
            case 2: {
                AliPayConfig config = new AliPayConfig();
                BeanUtil.copyProperties(replyConfigDto, config);
                configStr = new Gson().toJson(config);
            }
            break;
            case 3:
            case 4:
            case 5: {
                BankPayConfig config = new BankPayConfig();
                BeanUtil.copyProperties(replyConfigDto, config);
                configStr = new Gson().toJson(config);
            }
            break;
        }
        replyConfig.setModifyTime(new Date());
        replyConfig.setModifyBy(user.getId());
        replyConfig.setConfig(configStr);
        return this.updateById(replyConfig);
    }


    @Override
    @CacheEvict(cacheNames = "replyConfig:details", allEntries = true)
    public Integer deleteEntity(Long id) {
        return this.baseMapper.deleteById(id);
    }


    @Override
    @CacheEvict(cacheNames = "replyConfig:details", key = "#orgId")
    public List<TraderReplyConfigEntity> selectConfigByOrgId(Long orgId) {
        return baseMapper.selectConfigByOrgId(orgId);
    }
}
