package com.pay.service.modules.im.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImMessageQueryCriteria;
import com.pay.api.modules.im.entity.ImMessageEntity;
import com.pay.api.modules.im.service.ImMessageService;
import com.pay.api.modules.im.vo.ImMessageVo;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImMessageDao;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("imMessageService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImMessageServiceImpl extends ServiceImpl<ImMessageDao, ImMessageEntity> implements ImMessageService {

    @Override
    public PageUtils queryMessagePage(ImMessageQueryCriteria criteria, Pageable pageable) {
        Page<ImMessageVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<ImMessageVo> resultList = baseMapper.queryMessagePage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public ImMessageEntity getMessageById(String messageId, String formUserId, String toUserId) {
        return null;
    }


    @Override
    public TraderReplyConfigEntity getPayConfig(Long orgId, Integer type) {
        return baseMapper.getPayConfig(orgId,type);
    }


    @Override
    public List<Map<String, Object>> download(ImMessageQueryCriteria criteria) {
        List<ImMessageVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        Map<Integer, String> statuses = new HashMap<>();
        statuses.put(0, "待发送");
        statuses.put(1, "已接受待查看");
        statuses.put(2, "已查看");
        statuses.put(3, "撤销");
        Map<Integer, String> deviceTypes = new HashMap<>();
        deviceTypes.put(0, "未知");
        deviceTypes.put(1, "PC");
        deviceTypes.put(2, "android");
        deviceTypes.put(3, "ios");
        Map<Integer, String> chatTypes = new HashMap<>();
        chatTypes.put(0, "未知");
        chatTypes.put(1, "公聊");
        chatTypes.put(2, "私聊");
        Map<Integer, String> messageTypes = new HashMap<>();
        messageTypes.put(0, "文本");
        messageTypes.put(1, "图片");
        messageTypes.put(2, "语音");
        messageTypes.put(3, "视频");
        messageTypes.put(4, "音乐");
        messageTypes.put(5, "图文");
        for (ImMessageVo entity : resultList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("消息ID", entity.getMessageId());
            map.put("发送人", entity.getSendUserName());
            map.put("接收人", entity.getReceiveUserName());
            map.put("群组名称", entity.getGroupName());
            map.put("是否已读", entity.getIsRead() == 0 ? "已读" : "未读");
            map.put("状态", statuses.get(entity.getStatus()));
            map.put("消息类型", messageTypes.get(entity.getMessageType()));
            map.put("聊天类型", chatTypes.get(entity.getChatType()));
            map.put("设备标识", entity.getDeviceId());
            map.put("设备类型", deviceTypes.get(entity.getDeviceType()));
            map.put("消息内容", entity.getContent());
            map.put("创建日期", entity.getCreateTime());
            map.put("创建人", entity.getCreateBy());
            list.add(map);
        }
        return list;
    }


    @Override
    public List<ImMessageVo> getMessageHisPage(Integer userId, Integer timeOrder) {
        List<ImMessageVo> resultList = this.baseMapper.getMessageHisPage(userId, timeOrder);
        List<ImMessageVo> list = new ArrayList<>();

        Map<Integer, Date> dataMap = new HashMap<>();
        Map<Integer, ImMessageVo> messageMap = new HashMap<>();
        //循环提取数据
        if (CollUtil.isNotEmpty(resultList)) {
            for (ImMessageVo vo : resultList) {
                System.out.println(vo.getSendTime());

                //判断是否有记录userId,
                Integer sendUserId = Integer.parseInt(vo.getReceiveUserId()) == userId ? Integer.parseInt(vo.getSendUserId()) : Integer.parseInt(vo.getReceiveUserId());
                //获取是否有对应的记录
                if (dataMap.get(sendUserId) == null) {
                    dataMap.put(sendUserId, vo.getSendTime());
                    messageMap.put(sendUserId, vo);
                } else {
                    //判断时间是否对应
                    Date date = dataMap.get(sendUserId);
                    //
                    if (date != null && date.before(vo.getSendTime())) {
                        dataMap.put(sendUserId, vo.getSendTime());
                        messageMap.put(sendUserId, vo);
                    }
                }
            }

            if (CollUtil.isNotEmpty(messageMap)) {
                for (Map.Entry<Integer, ImMessageVo> entry : messageMap.entrySet()) {
                    ImMessageVo vo = entry.getValue();
                    list.add(vo);
                }
            }
        }
        return list;
    }

    @Override
    public PageUtils getUserMessageHisPage(Integer sendUserId, Integer receiveUserId, Integer pageSize, Integer pageIndex, Integer timeOrder) {
        Page<ImMessageVo> page = new Page<>(pageIndex, pageSize);
        List<ImMessageVo> resultList = this.baseMapper.getUserMessageHisPage(page, receiveUserId, sendUserId, timeOrder);
        return new PageUtils(page.setRecords(resultList));
    }
}
