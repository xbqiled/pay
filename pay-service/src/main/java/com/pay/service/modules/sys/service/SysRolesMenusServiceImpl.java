package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.entity.SysRolesMenusEntity;
import com.pay.api.modules.sys.service.SysRolesMenusService;
import com.pay.service.modules.sys.dao.SysRolesMenusDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("sysRolesMenusService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysRolesMenusServiceImpl extends ServiceImpl<SysRolesMenusDao, SysRolesMenusEntity> implements SysRolesMenusService {

    @Override
    public List<Long> getRoleMenuIds(Long roleId) {
        return baseMapper.getRoleMenuIds(roleId);
    }

}
