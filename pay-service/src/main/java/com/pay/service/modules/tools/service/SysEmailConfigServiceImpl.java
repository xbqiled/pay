package com.pay.service.modules.tools.service;

import com.alibaba.fastjson.JSONObject;
import com.pay.api.modules.tools.entity.SysEmailConfigEntity;
import com.pay.api.modules.tools.service.SysEmailConfigService;
import com.pay.api.modules.tools.vo.EmailVo;
import com.pay.common.core.utils.MailUtils;
import com.pay.service.modules.tools.dao.SysEmailConfigDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service("sysEmailService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysEmailConfigServiceImpl implements SysEmailConfigService {

    @Resource
    private SysEmailConfigDao sysEmailConfigDao;

    @Override
    public void update(SysEmailConfigEntity emailConfig) {
        SysEmailConfigEntity config = sysEmailConfigDao.queryFirstOnly();
        if(config == null){
            sysEmailConfigDao.insert(emailConfig);
        }else{
            sysEmailConfigDao.update(emailConfig);
        }
    }

    @Override
    public SysEmailConfigEntity find() {
        SysEmailConfigEntity bean = sysEmailConfigDao.queryFirstOnly();
        if(bean == null){
            bean = new SysEmailConfigEntity();
        }
        return bean;
    }

    @Override
    public JSONObject send(EmailVo emailVo){
        JSONObject json = new JSONObject();
        json.put("msg","发送成功");
        json.put("flag",true);
        try{
            SysEmailConfigEntity config = sysEmailConfigDao.queryFirstOnly();
            if(config == null){
                throw new Exception("请先配置邮件参数");
            }
            if(emailVo.getTos() == null || emailVo.getTos().size() < 1){
                throw new Exception("至少填写一个收件人邮箱地址");
            }
            MailUtils mail = new MailUtils();
            mail.setAddress(emailVo.getTos());
            mail.setConfig(config.getFromUser(),config.getHost(),config.getPort(),config.getUser(),config.getPass());
            boolean flag = mail.send(emailVo.getSubject(),emailVo.getContent());
            if(!flag){
                throw new Exception("发送失败,请检查邮件参数配置");
            }
        }catch (Exception e){
            json.put("flag",false);
            json.put("msg",e.getMessage());
        }
        return json;
    }
}
