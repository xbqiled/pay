package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.sys.entity.SysDictEntity;
import com.pay.api.modules.sys.vo.SysDictDetailVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysDictDao extends BaseMapper<SysDictEntity> {

    List<SysDictEntity> queryDictPage(Page page, @Param("name") String name);

    List<SysDictEntity> queryAllDict(@Param("name") String name);

    List<SysDictDetailVo> queryDictDetail(@Param("name") String name);
}
