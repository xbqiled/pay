package com.pay.service.modules.sys.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.api.modules.sys.criteria.SysOrgChannelQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgChannelEntity;
import com.pay.api.modules.sys.service.SysOrgChannelService;
import com.pay.api.modules.sys.vo.SysOrgChannelVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImChannelDao;
import com.pay.service.modules.sys.dao.SysOrgChannelDao;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;


@CacheConfig(cacheNames = "orgChannel")
@Service("sysOrgChannelService")
@AllArgsConstructor
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysOrgChannelServiceImpl extends ServiceImpl<SysOrgChannelDao, SysOrgChannelEntity> implements SysOrgChannelService {

    private final ImChannelDao imChannelDao;

    @Override
    public PageUtils queryOrgChannelPage(SysOrgChannelQueryCriteria criteria, Pageable pageable) {
            Page<SysOrgChannelVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
            List<SysOrgChannelVo> resultList = this.baseMapper.queryOrgChannelPage(page, criteria);
            return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<Map<String, Object>> download(SysOrgChannelQueryCriteria criteria) {
        List<SysOrgChannelVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();

        for (SysOrgChannelVo vo : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("银商编码", vo.getOrgId());

            map.put("银商名称", vo.getOrgName());
            map.put("渠道编码", vo.getChannelId());
            map.put("渠道名称", vo.getChannelName());

            map.put("图标URL", vo.getIconUrl() == null ? "" : vo.getIconUrl());
            map.put("类型", vo.getType() != null && vo.getType() == 0 ? "自有渠道" : "社会渠道");
            map.put("状态", vo.getStatus() != null && vo.getStatus() == 1 ? "正常" : "失效");

            map.put("备注", vo.getNote() == null ? "" : vo.getNote());
            map.put("偏移", vo.getSalt());
            map.put("秘钥", vo.getAuthCode());
            list.add(map);
        }
        return  list;
    }


    @Override
    @Cacheable(key = "'loadorgchannel:' + #p0")
    public List<SysOrgChannelVo> getConfigByOrgId(Long orgId) {
        return baseMapper.findByOrgId(orgId);
    }

    @Override
    public Map<String, Object> getAllConfigOrgId(Long orgId) {
        Map<String, Object> resultMap = new HashMap<>();
        List<String> selectList = new ArrayList<>();
        List<Map<String, Object>> allList = new ArrayList<>();

        List<SysOrgChannelVo> selectChannelList = baseMapper.findByOrgId(orgId);
        List<ImChannelEntity> allChannelList = imChannelDao.queryAllChannel();

        if (CollUtil.isNotEmpty(selectChannelList)) {
            for (SysOrgChannelVo vo : selectChannelList) {
                selectList.add(vo.getChannelId());
            }
        }

        if (CollUtil.isNotEmpty(allChannelList)) {
            for (ImChannelEntity channelEntity : allChannelList) {
                Map<String, Object> allMap = new HashMap<>();
                allMap.put("key", channelEntity.getChannelId());
                allMap.put("label", channelEntity.getChannelName());
                allList.add(allMap);
            }
        }

//        if (CollUtil.isNotEmpty(allChannelList)) {
//            for (ImChannelEntity channelEntity : allChannelList) {
//                allList.add(channelEntity.getChannelId());
//            }
//        }
//
//        //清除
//        allList.removeAll(selectList);
//
//        if (CollUtil.isNotEmpty(allList)) {
//            for (String str : allList) {
//                for (ImChannelEntity channelEntity : allChannelList) {
//                    if (str.equals(channelEntity.getChannelId())) {
//                        allMap.put("key",  channelEntity.getChannelId());
//                        allMap.put("label",  channelEntity.getChannelName());
//                    }
//                }
//            }
//        }

        resultMap.put("select", selectList);
        resultMap.put("all", allList);
        return resultMap;
    }

    @Override
    @CacheEvict(allEntries = true)
    public Boolean doConfig(String [] channeId, Long orgId) {
        List <SysOrgChannelEntity> list = new ArrayList<>();
        if (channeId != null && channeId.length > 0) {
            for (String str : channeId) {
                SysOrgChannelEntity orgChannel = new SysOrgChannelEntity();
                orgChannel.setChannelId(str);
                orgChannel.setOrgId(orgId);
                list.add(orgChannel);
            }
        }

        baseMapper.delByOrgId(orgId);
        this.insertBatch(list);
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(allEntries = true)
    public Integer delByOrgId(Long orgId) {
        return baseMapper.delByOrgId(orgId);
    }


    @Override
    @CacheEvict(allEntries = true)
    public Boolean delById(Long id) {
        return this.deleteById(id);
    }

}
