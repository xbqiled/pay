package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.entity.SysPictureEntity;
import com.pay.api.modules.sys.service.SysPictureService;
import com.pay.api.modules.tools.criteria.PictureQueryCriteria;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.sys.dao.SysPictureDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("sysPictureService")
@CacheConfig(cacheNames = "sysPictures")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysPictureServiceImpl extends ServiceImpl<SysPictureDao, SysPictureEntity> implements SysPictureService {


    @Override
    public PageUtils queryPage(PictureQueryCriteria criteria, Pageable pageable) {
        Page<SysPictureEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<SysPictureEntity> resultList = this.baseMapper.queryPicturePage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    @Cacheable(key="'loadById:'+#p0")
    public SysPictureEntity findById(Long id) {
        return this.findById(id);
    }


    @Override
    @CacheEvict(key="'loadById:'+#p0" , allEntries = true)
    public void delete(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public void deleteAll(List<Long> ids) {
        this.baseMapper.deleteBatchIds(ids);
    }


    @Override
    public List<Map<String, Object>> download(PictureQueryCriteria criteria) {
        List<SysPictureEntity> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (SysPictureEntity picture : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("文件名", picture.getFilename());
            map.put("图片地址", picture.getUrl());
            map.put("文件大小", picture.getSize());
            map.put("操作人", picture.getUsername());
            map.put("高度", picture.getHeight());
            map.put("宽度", picture.getWidth());
            map.put("删除地址", picture.getDeleteUrl());
            map.put("创建日期", picture.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
