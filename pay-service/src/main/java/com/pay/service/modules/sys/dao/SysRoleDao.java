package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.sys.criteria.SysRoleQueryCriteria;
import com.pay.api.modules.sys.dto.*;
import com.pay.api.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
    /**
     * 获取用户对应角色集合
     * @param userId
     * @return
     */
    Set<SysRoleDTO> findByUsersId(@Param("userId") Long userId);

    /**
     * 获取用户对应角色集合
     * @param userId
     * @return
     */
    Set<SysRoleSmallDTO> findSmallByUsersId(@Param("userId") Long userId);


    /**
     * 获取菜单对应角色集合
     * @param menuId
     * @return
     */
    List<SysRoleSmallDTO> findByMenusId(@Param("menuId") Long menuId);

    @Select("select * from sys_role where `name`=#{name}")
    List<Map> findByName(@Param("name") String name);

    /**
     * 获取角色详情信息
     * @param menuId
     * @return
     */
    SysRoleDTO findByMenuId(@Param("menuId") long menuId);

    /**
     * 查詢指定角色中最大等級
     * @param roles
     * @return
     */
    Integer findByRoles(Set<SysRoleSmallDTO> roles);

    /**
     * 分頁查詢
     * @param page
     * @param criteria
     * @return
     */
    List<SysRoleEntity> queryRolePage(Page page, SysRoleQueryCriteria criteria);


    SysRoleDTO findById(@Param("id") Long id);


    List<SysRoleEntity> queryAll(SysRoleQueryCriteria criteria);
}
