package com.pay.service.modules.im.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImBlackQueryCriteria;
import com.pay.api.modules.im.dto.ImBlackDTO;
import com.pay.api.modules.im.entity.ImBlackEntity;
import com.pay.api.modules.im.service.ImBlackService;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.Query;
import com.pay.service.modules.im.dao.ImBlackDao;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("imBlackService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImBlackServiceImpl extends ServiceImpl<ImBlackDao, ImBlackEntity> implements ImBlackService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ImBlackEntity> page = this.selectPage(
                new Query<ImBlackEntity>(params).getPage(),
                new EntityWrapper<ImBlackEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryAll(ImBlackQueryCriteria criteria) {
//        Page<ImBlackEntity> page = this.selectPage(
//                new Page<ImBlackEntity>(criteria.getPage(),criteria.getSize()),
//                new EntityWrapper<ImBlackEntity>()
//        );

        return new PageUtils(null);
    }

    @Override
    public Object queryList(ImBlackQueryCriteria criteria) {
        return null;
    }

    @Override
    public ImBlackDTO findById(Integer id) {
        ImBlackEntity imBlackEntity = this.baseMapper.selectById(id);
        ImBlackDTO imBlackDTO = new ImBlackDTO();
        BeanUtil.copyProperties(imBlackEntity,imBlackDTO);
        return imBlackDTO;
//        return this.baseMapper.findById(id);
    }

    @Override
    public ImBlackDTO create(ImBlackEntity resources) {
        return this.baseMapper.create(resources);
    }

    @Override
    public void update(ImBlackEntity resources) {
        this.baseMapper.updateById(resources);
    }

    @Override
    public void delete(Integer id) {
        this.baseMapper.deleteById(id);
    }

}
