package com.pay.service.modules.quartz.task;

import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.api.modules.quartz.entity.QuartzLogEntity;
import com.pay.api.modules.quartz.service.QuartzLogService;
import com.pay.service.modules.im.dao.ImMessageDao;
import com.pay.service.modules.im.dao.ImOfflineMessageDao;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Howard
 * @date 2020-01-30
 */
@Slf4j
@Component
public class DeleteMessageTask implements Job {

    @Resource
    private QuartzLogService quartzLogService;

    @Resource
    private ImMessageDao imMessageDao;

    @Resource
    private ImOfflineMessageDao imOfflineMessageDao;

    @Override
    public void execute(JobExecutionContext context) {
        QuartzLogEntity entity = new QuartzLogEntity();
        long startTime = System.currentTimeMillis();
        try{
            QuartzJobEntity job = (QuartzJobEntity)context.getMergedJobDataMap().get("JOB_KEY");
            Integer days = 30;
            if(context.getMergedJobDataMap().containsKey("data")){
                days = context.getMergedJobDataMap().getInt("data");
            }
            log.warn("定时任务--正在删除"+days+"天以前的消息,表--imOfflineMessage、imMessage");
            imOfflineMessageDao.deleteByDaysBefore(days);
            imMessageDao.deleteByDaysBefore(days);

            entity.setCronExpression(job.getCronExpression());
            entity.setBeanName(job.getBeanName());
            entity.setJobName(job.getJobName());
            entity.setMethodName(job.getMethodName());
            entity.setParams(job.getParams());
            entity.setIsSuccess(true);

        }catch (Exception e){
            entity.setIsSuccess(false);
            entity.setExceptionDetail(e.getMessage());
        }
        long endTime = System.currentTimeMillis();
        entity.setTime((endTime-startTime)/1000);
        //quartzLogService.addLog(entity);
    }
}
