package com.pay.service.modules.tools.dao;


import com.pay.api.modules.tools.entity.SysEmailConfigEntity;

/**
 * 
 * 
 * @author Lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-14 12:19:09
 */
public interface SysEmailConfigDao {

    SysEmailConfigEntity queryFirstOnly();

    int update(SysEmailConfigEntity bean);

    int insert(SysEmailConfigEntity bean);

	
}
