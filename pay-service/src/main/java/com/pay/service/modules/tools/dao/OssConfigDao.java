package com.pay.service.modules.tools.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.api.modules.tools.criteria.OssConfigQueryCriteria;
import com.pay.api.modules.tools.entity.OssConfigEntity;

import java.util.List;

/**
 * 系统配置信息表
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface OssConfigDao extends BaseMapper<OssConfigEntity> {

    OssConfigEntity getOssConfig();

    List<OssConfigEntity> queryOssConfigPage(Page page, OssConfigQueryCriteria bean);

    List<OssConfigEntity> queryByOssConfigEntity(OssConfigEntity bean);

    List<OssConfigEntity> queryAll(OssConfigEntity bean);
	
}
