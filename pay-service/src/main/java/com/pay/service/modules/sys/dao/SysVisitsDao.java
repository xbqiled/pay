package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.entity.SysVisitsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysVisitsDao extends BaseMapper<SysVisitsEntity> {

    List<SysVisitsEntity> findAllVisits(@Param("start") String toString, @Param("end") String toString1);

    SysVisitsEntity findByDate(@Param("date") String date);
}
