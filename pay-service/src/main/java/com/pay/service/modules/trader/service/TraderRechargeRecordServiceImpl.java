package com.pay.service.modules.trader.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.trader.criteria.RechargeRecordQueryCriteria;
import com.pay.api.modules.trader.entity.TraderRechargeRecordEntity;
import com.pay.api.modules.trader.service.TraderRechargeRecordService;
import com.pay.api.modules.trader.vo.TraderRechargeRecordVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.trader.dao.TraderRechargeRecordDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Service("traderRechargeRecordService")
@CacheConfig(cacheNames = "replyRecord")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class TraderRechargeRecordServiceImpl extends ServiceImpl<TraderRechargeRecordDao, TraderRechargeRecordEntity> implements TraderRechargeRecordService {


    @Override
    public PageUtils queryRechargeRecordPage(RechargeRecordQueryCriteria criteria, Pageable pageable) {
        Page<TraderRechargeRecordVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<TraderRechargeRecordVo> resultList = baseMapper.queryRechargeRecordPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<Map<String, Object>> download(RechargeRecordQueryCriteria criteria) {
        List<TraderRechargeRecordVo> resultList = baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();

        for (TraderRechargeRecordVo rechargeRecord : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("银商编码", rechargeRecord.getOrgId());
            map.put("银商名称", rechargeRecord.getOrgName());

            map.put("会计账目", rechargeRecord.getAccountItem() == 1 ? "充值" : "扣款");
            map.put("状态", rechargeRecord.getStatus() == 1 ? "成功" : "失败");
            map.put("充值前金额", rechargeRecord.getBeforeBalance());

            map.put("充值后金额", rechargeRecord.getAfterBalance());
            map.put("充值金额", rechargeRecord.getAmount());
            map.put("操作时间", rechargeRecord.getOperateTime());

            map.put("操作人", rechargeRecord.getOperateUser());
            list.add(map);
        }
        return  list;
    }

    @Override
    public TraderRechargeRecordEntity saveEntity(TraderRechargeRecordEntity traderRechargeRecord) {
        this.insert(traderRechargeRecord);
        return traderRechargeRecord;
    }

    @Override
    public void updateEntity(TraderRechargeRecordEntity traderRechargeRecord) {
        this.updateById(traderRechargeRecord);
    }

    @Override
    public void deleteEntity(Long id) {
        this.deleteById(id);
    }
}
