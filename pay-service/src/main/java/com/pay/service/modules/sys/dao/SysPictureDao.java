package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.sys.entity.SysPictureEntity;
import com.pay.api.modules.tools.criteria.PictureQueryCriteria;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysPictureDao extends BaseMapper<SysPictureEntity> {

    List<SysPictureEntity> queryPicturePage(Page page, PictureQueryCriteria criteria);

    List<SysPictureEntity> queryAll(PictureQueryCriteria criteria);

}
