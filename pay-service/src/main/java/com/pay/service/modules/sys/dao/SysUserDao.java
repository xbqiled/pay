package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.sys.criteria.SysUserQueryCriteria;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface SysUserDao extends BaseMapper<SysUserEntity> {
    /**
     * 修改用户信息
     */
    void updateByName(SysUserEntity user);

    void updatePassByEntity(SysUserEntity user);

    SysUserDTO findByName(@Param("username") String userName);

    SysUserDTO findByJobId(@Param("jobId") Long jobId);

    SysUserDTO findByOrgId(@Param("orgId") Long orgId);

    List<SysUserEntity> queryOrgIdList(@Param("orgId") Long orgId);

    List<SysUserDTO> queryUserPage(Page page, SysUserQueryCriteria criteria);

    List<SysUserDTO> queryAll(SysUserQueryCriteria criteria);

    int create(SysUserEntity user);
}
