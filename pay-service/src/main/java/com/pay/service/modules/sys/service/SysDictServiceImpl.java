package com.pay.service.modules.sys.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.criteria.SysDictQueryCriteria;
import com.pay.api.modules.sys.entity.SysDictEntity;
import com.pay.api.modules.sys.service.SysDictService;
import com.pay.api.modules.sys.vo.SysDictDetailVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.sys.dao.SysDictDao;
import com.pay.service.modules.sys.dao.SysDictDetailDao;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("sysDictService")
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements SysDictService {

    private final SysDictDetailDao sysDictDetailDao;

    @Override
    public PageUtils queryDictPage(SysDictQueryCriteria criteria, Pageable pageable) {
        Page<SysDictEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<SysDictEntity> resultList = baseMapper.queryDictPage(page, criteria.getBlurry());
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<SysDictEntity> queryAllDictPage(SysDictQueryCriteria criteria) {
        return baseMapper.queryAllDict(criteria.getBlurry());
    }

    @Override
    public List<Map<String, Object>> download(SysDictQueryCriteria criteria) {
        List<Map<String, Object>> list = new ArrayList<>();
        List<SysDictDetailVo> resultList = baseMapper.queryDictDetail(criteria.getBlurry());

        if (CollUtil.isNotEmpty(resultList)) {
            for (SysDictDetailVo dict : resultList) {
                Map<String,Object> map = new LinkedHashMap<>();
                map.put("字典名称", dict.getName());
                map.put("字典描述", dict.getRemark());
                map.put("字典标签", dict.getLabel());
                map.put("字典值", dict.getValue());
                map.put("创建日期", dict.getCreateTime());
                list.add(map);
            }
        }
        return list;
    }


    @Override
    public boolean saveEntity(SysDictEntity sysDictEntity) {
        sysDictEntity.setCreateTime(new Date());
        return this.insert(sysDictEntity);
    }

    @Override
    public boolean updateEntity(SysDictEntity sysDictEntity) {
        return this.updateById(sysDictEntity);
    }

    @Override
    public Integer deleteEntity(Long id) {
        this.deleteById(id);
        return sysDictDetailDao.delByDictId(id);
    }
}
