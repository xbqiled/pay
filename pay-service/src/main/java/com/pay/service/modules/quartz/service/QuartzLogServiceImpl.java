package com.pay.service.modules.quartz.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzLogEntity;
import com.pay.api.modules.quartz.service.QuartzLogService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.quartz.dao.QuartzLogDao;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Service("quartzLogService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class QuartzLogServiceImpl extends ServiceImpl<QuartzLogDao, QuartzLogEntity> implements QuartzLogService {

    @Override
    public PageUtils queryQuartzLogPage(JobQueryCriteria criteria, Pageable pageable) {
        Page<QuartzLogEntity> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<QuartzLogEntity> resultList = baseMapper.queryQuartzLogPage(page, criteria);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public List<QuartzLogEntity> queryAllLog(JobQueryCriteria criteria) {
        return baseMapper.queryAllLog(criteria);
    }


    @Override
    public void addLog(QuartzLogEntity entity) {
        this.baseMapper.addLog(entity);
    }


    @Override
    public List<Map<String, Object>> downloadLog(JobQueryCriteria criteria) {
        List<QuartzLogEntity> resultList = baseMapper.queryAllLog(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (QuartzLogEntity quartzLog : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("任务名称", quartzLog.getJobName());
            map.put("Bean名称", quartzLog.getBeanName());
            map.put("执行方法", quartzLog.getMethodName());
            map.put("参数", quartzLog.getParams());
            map.put("表达式", quartzLog.getCronExpression());
            map.put("异常详情", quartzLog.getExceptionDetail());
            map.put("耗时/毫秒", quartzLog.getTime());
            map.put("状态", quartzLog.getIsSuccess() ? "成功" : "失败");
            map.put("创建日期", quartzLog.getCreateTime());
            list.add(map);
        }
        return  list;
    }


}
