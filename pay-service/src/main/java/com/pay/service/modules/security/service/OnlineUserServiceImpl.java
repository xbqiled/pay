package com.pay.service.modules.security.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.security.entity.OnlineUser;
import com.pay.api.modules.security.service.OnlineUserService;
import com.pay.common.core.utils.EncryptUtils;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.ReidsPageUtils;
import com.pay.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;


@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class OnlineUserServiceImpl  implements OnlineUserService {


    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public void saveOnlineUser(String token, Long orgId, String job, String ip, String browser, String userName, String onlineKey, Long expiration) {
        OnlineUser onlineUser = null;
        try {
            onlineUser = new OnlineUser();
            onlineUser.setUserName(userName);
            onlineUser.setJob(job);
            onlineUser.setBrowser(browser);
            onlineUser.setOrgId(orgId);
            onlineUser.setIp(ip);
            onlineUser.setAddress( StringUtils.getCityInfo(ip));
            onlineUser.setKey(EncryptUtils.desEncrypt(token));
            onlineUser.setLoginTime(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        redisTemplate.opsForValue().set(onlineKey + token, onlineUser);
        redisTemplate.expire(onlineKey + token,expiration, TimeUnit.MILLISECONDS);
    }


    @Override
    public PageUtils getAll(String filter, Long orgId, String onlineKey, Pageable pageable) {
        List<OnlineUser> onlineUsers = getAll(filter, orgId, onlineKey);
        Page<OnlineUser> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        page.setTotal(onlineUsers.size());
        List<OnlineUser> pageList = ReidsPageUtils.toPage(pageable.getPageNumber(),pageable.getPageSize(),onlineUsers);

        List<OnlineUser> resultList = new ArrayList<>();
        resultList.addAll(pageList);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public List<OnlineUser> getAll(String filter, Long orgId, String onlineKey) {
        List<String> keys = new ArrayList<>(redisTemplate.keys(onlineKey + "*"));
        Collections.reverse(keys);
        List<OnlineUser> onlineUsers = new ArrayList<>();
        for (String key : keys) {
            OnlineUser onlineUser = (OnlineUser) redisTemplate.opsForValue().get(key);
            if(StrUtil.isNotBlank(filter)){
                if (onlineUser.getOrgId() == null || onlineUser.getOrgId() == 1) {
                    if(onlineUser.toString().contains(filter)){
                        onlineUsers.add(onlineUser);
                    }
                } else {
                    if(onlineUser.toString().contains(filter) || onlineUser.getOrgId() == orgId){
                        onlineUsers.add(onlineUser);
                    }
                }
            } else {
                onlineUsers.add(onlineUser);
            }
        }
        Collections.sort(onlineUsers, (o1, o2) -> {
            return o2.getLoginTime().compareTo(o1.getLoginTime());
        });
        return onlineUsers;
    }

    @Override
    public void kickOut(String val, String onlineKey) throws Exception {
        String key = onlineKey + EncryptUtils.desDecrypt(val);
        redisTemplate.delete(key);
    }

    @Override
    public void logout(String token, String onlineKey) {
        String key = onlineKey + token;
        redisTemplate.delete(key);
    }

    @Override
    public List<Map<String, Object>> download(String filter, Long orgId, String onlineKey) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        List<OnlineUser> result =  this.getAll(filter, orgId, onlineKey);
        for (OnlineUser user : result) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("用户名", user.getUserName());
            map.put("岗位", user.getJob());
            map.put("登录IP", user.getIp());
            map.put("登录地点", user.getAddress());
            map.put("浏览器", user.getBrowser());
            map.put("登录日期", user.getLoginTime());
            list.add(map);
        }
        return list;
    }
}
