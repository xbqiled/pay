package com.pay.service.modules.sys.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.entity.ImOrgEntity;
import com.pay.api.modules.im.service.ImOrgService;
import com.pay.api.modules.sys.criteria.SysOrgsQueryCriteria;
import com.pay.api.modules.sys.entity.SysOrgEntity;
import com.pay.api.modules.sys.entity.SysUserEntity;
import com.pay.api.modules.sys.service.SysOrgService;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.api.modules.trader.entity.TraderOrgAccountEntity;
import com.pay.api.modules.trader.service.TraderOrgAccountService;
import com.pay.service.modules.sys.dao.SysOrgDao;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;


@Service("sysOrgService")
@CacheConfig(cacheNames = "sysOrgs")
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysOrgServiceImpl extends ServiceImpl<SysOrgDao, SysOrgEntity> implements SysOrgService {

    @Autowired
    private TraderOrgAccountService traderOrgAccountService;

    @Autowired
    private ImOrgService imOrgService;

    @Autowired
    private SysUserService sysUserService;

    @Override
    @Cacheable(key="'loadById:'+#p0")
    public SysOrgEntity findById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    @Cacheable(key="'loadNameByPid:'+#p0")
    public String findNameById(Long pid) {
        return this.baseMapper.findNameById(pid);
    }

    @Override
    @CacheEvict(allEntries = true)
    public SysOrgEntity saveEntity(SysOrgEntity resources) {
        resources.setCreateTime(new Date());
        this.baseMapper.insert(resources);
        //添加银商账户
        TraderOrgAccountEntity traderOrgAccountEntity = new TraderOrgAccountEntity();
        traderOrgAccountEntity.setOrgId(resources.getId());
        traderOrgAccountService.saveEntity(traderOrgAccountEntity);
        //添加银商信息
        ImOrgEntity imOrgEntity = new ImOrgEntity();
        imOrgEntity.setOrgId(resources.getId());
        imOrgEntity.setCreateTime(new Date());
        imOrgService.saveEntity(imOrgEntity);
        return resources;
    }

    @Override
    @CachePut(key="'loadById:'+#p0.id")
    public void updateEntity(SysOrgEntity resources) {
        this.baseMapper.updateById(resources);
    }

    @Override
    public Map<String, Object> deleteEntity(Long id) {
        Map<String, Object> map = new HashMap<>();
        List<SysUserEntity> list = sysUserService.queryOrgIdList(id);
        if (CollUtil.isNotEmpty(list)) {
            map.put("flag", false);
            map.put("msg", "请先删除关联该银商的用户");
        } else {
            imOrgService.deleteByOrgId(id);
            traderOrgAccountService.deleteByOrgId(id);
            this.baseMapper.deleteById(id);
            map.put("flag", true);
        }
        return map;
    }

    @Override
    public Object buildTree(List<SysOrgEntity> sysOrgEntityList) {
        Set<SysOrgEntity> trees = new LinkedHashSet<>();
        Set<SysOrgEntity> depts= new LinkedHashSet<>();

        List<String> deptNames = sysOrgEntityList.stream().map(SysOrgEntity::getName).collect(Collectors.toList());
        Boolean isChild;
        for (SysOrgEntity deptDTO : sysOrgEntityList) {
            isChild = false;
            if (null == deptDTO.getPid() || 0L == deptDTO.getPid()) {
                trees.add(deptDTO);
            }
            for (SysOrgEntity it : sysOrgEntityList) {
                if (it.getPid().equals(deptDTO.getId())) {
                    isChild = true;
                    if (deptDTO.getChildren() == null) {
                        deptDTO.setChildren(new ArrayList<SysOrgEntity>());
                    }
                    deptDTO.getChildren().add(it);
                }
            }
            if(isChild)
                depts.add(deptDTO);
            else if(!deptNames.contains(this.baseMapper.findNameById(deptDTO.getPid())))
                depts.add(deptDTO);
        }

        if (CollectionUtils.isEmpty(trees)) {
            trees = depts;
        }

        Integer totalElements = sysOrgEntityList!=null?sysOrgEntityList.size():0;
        Map map = new HashMap();
        map.put("totalElements",totalElements);
        map.put("content",CollectionUtils.isEmpty(trees)?sysOrgEntityList:trees);
        return map;
    }

    @Override
    @Cacheable(key="'loadListByPid:'+#p0")
    public List<SysOrgEntity> findByPid(long pid) {
        return this.baseMapper.findByPid(pid);
    }

    @Override
    @Cacheable(key="'findByRoleIds:'+#p0")
    public Set<SysOrgEntity> findByRoleIds(Long id) {
        return this.baseMapper.findDeptByRoleId(id);
    }

    @Override
    public List<SysOrgEntity> queryAll(SysOrgsQueryCriteria criteria) {
        return this.baseMapper.queryAll(criteria);
    }

    @Override
    public List<Map<String, Object>> download(SysOrgsQueryCriteria criteria) {
        List<SysOrgEntity> resultList = this.baseMapper.queryAll(criteria);
        List<Map<String, Object>> list = new ArrayList<>();
        for (SysOrgEntity sysOrg : resultList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("云商名称", sysOrg.getName());
            map.put("云商状态", sysOrg.getEnabled() ? "正常" : "禁用");
            map.put("创建日期", sysOrg.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
