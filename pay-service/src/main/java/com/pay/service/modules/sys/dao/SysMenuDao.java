package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.criteria.SysMenuQueryCriteria;
import com.pay.api.modules.sys.dto.SysMenuDTO;
import com.pay.api.modules.sys.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {

    Set<SysMenuDTO> findMenusByRoleId(@Param("roleId") Long roleId);

    LinkedHashSet<SysMenuDTO> findByRoles_IdOrderBySortAsc(@Param("roleId") Long roleId);

    List<SysMenuEntity> findByPid(long pid);

    List<SysMenuDTO> queryAll(SysMenuQueryCriteria criteria);

}
