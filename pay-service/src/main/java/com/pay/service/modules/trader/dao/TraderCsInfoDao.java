package com.pay.service.modules.trader.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.trader.criteria.CsQueryCriteria;
import com.pay.api.modules.trader.entity.TraderCsInfoEntity;
import com.pay.api.modules.trader.vo.TraderCsInfoVo;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 13:21:04
 */
public interface TraderCsInfoDao extends BaseMapper<TraderCsInfoEntity> {

    List<TraderCsInfoVo> queryCsPage(Page page, CsQueryCriteria criteria);

    List<TraderCsInfoVo> queryAll(CsQueryCriteria criteria);
}
