package com.pay.service.modules.quartz.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.quartz.entity.SysJobEntity;
import com.pay.api.modules.sys.vo.SysJobVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface SysJobDao extends BaseMapper<SysJobEntity> {

    List<SysJobVo> queryAll(@Param("name") String name, @Param("enabled") Integer enabled, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("orgId") Long orgId, @Param("orgIds") Set<Long> orgIds);

    List<SysJobVo> queryJobPage(Page page, @Param("name") String name, @Param("enabled") Integer enabled,  @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("orgId") Long orgId, @Param("orgIds") Set<Long> orgIds);
}
