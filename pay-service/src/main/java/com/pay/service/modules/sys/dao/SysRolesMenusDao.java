package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.sys.dto.SysMenuDTO;
import com.pay.api.modules.sys.entity.SysRolesMenusEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:02:58
 */
public interface SysRolesMenusDao extends BaseMapper<SysRolesMenusEntity> {
    /**
     * 批量插入指定角色关联
     * @param menus
     * @param id
     */
    void insertBatch(Set<SysMenuDTO> menus, Long id);
    /**
     * 批量删除指定角色关联
     * @param roleId
     * @return
     */
    int deleteByRoleId(@Param("roleId") Long roleId);


    int deleteByMenuId(@Param("menuId") Long menuId);
    /**
     * 根据权限id的信息获取对应的数据信息
     * @param roleId
     * @return
     */
    List<Long> getRoleMenuIds(@Param("roleId") Long roleId);
}
