package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.entity.SysUsersRolesEntity;
import com.pay.api.modules.sys.service.SysUsersRolesService;
import com.pay.common.core.utils.PageUtils;
import com.pay.common.core.utils.Query;
import com.pay.service.modules.sys.dao.SysUsersRolesDao;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


@Service("sysUsersRolesService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysUsersRolesServiceImpl extends ServiceImpl<SysUsersRolesDao, SysUsersRolesEntity> implements SysUsersRolesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysUsersRolesEntity> page = this.selectPage(
                new Query<SysUsersRolesEntity>(params).getPage(),
                new EntityWrapper<SysUsersRolesEntity>()
        );
        return new PageUtils(page);
    }

}
