package com.pay.service.modules.im.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImOrgSpecialQueryCriteria;
import com.pay.api.modules.im.dto.ImOrgSpecialDTO;
import com.pay.api.modules.im.entity.ImOrgSpecialEntity;
import com.pay.api.modules.im.service.ImOrgSpecialService;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.im.dao.ImOrgSpecialDao;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("imOrgSpecialService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImOrgSpecialServiceImpl extends ServiceImpl<ImOrgSpecialDao, ImOrgSpecialEntity> implements ImOrgSpecialService {

    @Override
    public PageUtils queryAll(ImOrgSpecialQueryCriteria criteria) {
        return null;
    }

    @Override
    public List<ImOrgSpecialEntity> queryList(ImOrgSpecialQueryCriteria criteria) {
        return null;
    }

    @Override
    public ImOrgSpecialDTO findById(Integer id) {
        return null;
    }

    @Override
    public ImOrgSpecialEntity create(ImOrgSpecialEntity resources) {
        return null;
    }

    @Override
    public void update(ImOrgSpecialEntity resources) {

    }

    @Override
    public void delete(Integer id) {

    }

}
