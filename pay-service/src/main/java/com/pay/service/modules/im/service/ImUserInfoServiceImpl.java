package com.pay.service.modules.im.service;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.criteria.ImUserInfoQueryCriteria;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.ImUserInfoService;
import com.pay.api.modules.im.vo.ImUserInfoVo;
import com.pay.api.modules.sys.entity.SysSeqEntity;
import com.pay.api.utils.EntityPackageUtils;
import com.pay.common.core.constant.PayConstant;
import com.pay.common.core.utils.Result;
import com.pay.common.core.utils.Sha256Util;
import com.pay.service.modules.im.dao.ImChannelDao;
import com.pay.service.modules.im.dao.ImUserInfoDao;
import com.pay.service.modules.sys.dao.SysSeqDao;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.pay.common.core.constant.PayConstant.*;


@AllArgsConstructor
@Service("imUserInfoService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImUserInfoServiceImpl extends ServiceImpl<ImUserInfoDao, ImUserInfoEntity> implements ImUserInfoService {

    private ImChannelDao imChannelDao;

    private SysSeqDao SysSeqDao;

    @Override
    public ImUserInfoEntity initUser(Long orgId, String userId, Integer platformType, String signIn) {
        //首先判断用户是否存在
        ImUserInfoEntity userInfo = baseMapper.loginByUserId(orgId, orgId + userId);
        if (userInfo == null) {
            //如果不存在,那么创建新的用户
            SysSeqEntity sysSeqEntity = SysSeqDao.getBySeqName(PayConstant.SEQ_USERNAME, orgId);
            ImUserInfoEntity imUserInfoEntity = EntityPackageUtils.packageUserInfo(userId, IdUtil.simpleUUID(),
                    sysSeqEntity.getCurrentValue(), SecureUtil.md5("123456"), orgId);
            //更新seq
            SysSeqDao.updateToIncrease(PayConstant.SEQ_USERNAME, orgId);
            //保存信息
            this.insert(imUserInfoEntity);
            return imUserInfoEntity;
        }
        return userInfo;
    }


    @Override
    public ImUserInfoEntity initCacUser(String userId, Integer platformType, String channelId, String nickName) {
        Long orgId = new Long(1);
        String platformStr = platformType == 1 ? "cac" : "platform";
        String note = "channelId:" + channelId + "platform:" + platformType;
        ImUserInfoEntity userInfo = baseMapper.loginByUserId(orgId, platformStr + userId);
        if (userInfo == null) {
            //如果不存在,那么创建新的用户
            SysSeqEntity sysSeqEntity = SysSeqDao.getBySeqName(PayConstant.SEQ_USERNAME, orgId);
            ImUserInfoEntity imUserInfoEntity = EntityPackageUtils.packageCacUserInfo(platformStr + userId, nickName,
                    sysSeqEntity.getCurrentValue(), SecureUtil.md5("123456"), orgId, note);
            //更新seq
            SysSeqDao.updateToIncrease(PayConstant.SEQ_USERNAME, orgId);
            //保存信息
            this.insert(imUserInfoEntity);
            return imUserInfoEntity;
        }
        return userInfo;
    }


    @Override
    public List<Map<String, Object>> getCsList(Long orgId, Integer isOnLine) {
        return baseMapper.getCsList(orgId, isOnLine);
    }


    @Override
    public List<Map<String, Object>> getChannelCsList(Long orgId, String channelId, Integer isOnLine) {
        return baseMapper.getChannelCsList(channelId, isOnLine);
    }

    /**
     * <B>修改用户密码</B>
     *
     * @param userId
     * @param oldPw
     * @param newPw
     * @return
     */
    @Override
    public Integer modifyPw(Integer userId, String oldPw, String newPw) {
        //首先判断当前密码是否正确
        String oldPassWord = SecureUtil.md5(oldPw);
        ImUserInfoEntity imUserInfoEntity = this.selectById(userId);

        if (StrUtil.equals(oldPassWord, imUserInfoEntity.getPassword())) {
            String newPassWord = SecureUtil.md5(newPw);
            baseMapper.moidfyUserPw(userId, newPassWord);
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * <B>通过关键字查询好友信息</B>
     *
     * @param key
     * @return
     */
    @Override
    public List<ImUserInfoVo> findUserByKey(String key, String userId) {
        return baseMapper.findUserByKey(key, userId);
    }

    /**
     * <B>通过用户ID查询对应的用户信息</B>
     *
     * @param id
     * @return
     */
    @Override
    @CacheEvict(value = "imUserInfo:details", key = "#id + '_id'")
    public ImUserInfoEntity findUserById(Integer id) {
        return this.selectById(id);
    }

    /**
     * <B>查询用户信息</B>
     *
     * @param userId
     * @param orgId
     * @return
     */
    @Override
    @CacheEvict(value = "imUserInfo:loginByUserId", key = "#orgId +  '_orgId' + #userId + '_userId'")
    public ImUserInfoEntity loginByUserId(Long orgId, String userId) {
        return baseMapper.loginByUserId(orgId, userId);
    }


    @Override
    public ImUserInfoVo getUserInfoById(Integer queryUserId, Integer myUserId) {
        return baseMapper.getUserInfoById(queryUserId, myUserId);
    }

    /**
     * <B>查询用户信息</B>
     *
     * @param orgId
     * @param userName
     * @return
     */
    @Override
    @CacheEvict(value = "imUserInfo:loginByUserName", key = "#orgId +  '_orgId' + #userName + '_userName'")
    public ImUserInfoEntity loginByUserName(Long orgId, String userName) {
        return baseMapper.loginByUserName(orgId, userName);
    }

    @Override
    public ImUserInfoEntity registerUser(Long orgId, String nickName) {
        SysSeqEntity sysSeqEntity = SysSeqDao.getBySeqName(PayConstant.SEQ_USERNAME, orgId);
        ImUserInfoEntity imUserInfoEntity = EntityPackageUtils.packageUserInfo(NULL_VALUE, nickName, sysSeqEntity.getCurrentValue(),
                SecureUtil.md5(DEFAULT_USER_PASSWORD), orgId);
        //更新seq
        SysSeqDao.updateToIncrease(PayConstant.SEQ_USERNAME, orgId);
        //保存信息
        this.baseMapper.insert(imUserInfoEntity);
        return imUserInfoEntity;
    }


    @Override
    public ImUserInfoEntity registerUser(Long orgId, String nickName, String passWord, Integer supportedPms, String avatar) {
        try {
            SysSeqEntity sysSeqEntity = SysSeqDao.getBySeqName(PayConstant.SEQ_USERNAME, orgId);
            ImUserInfoEntity imUserInfoEntity = EntityPackageUtils.packageCsUserInfo(NULL_VALUE, nickName, sysSeqEntity.getCurrentValue(),
                    SecureUtil.md5(passWord), orgId, avatar);
            imUserInfoEntity.setSupportedPms(supportedPms);
            //更新seq
            SysSeqDao.updateToIncrease(PayConstant.SEQ_USERNAME, orgId);
            //保存信息
            this.baseMapper.insert(imUserInfoEntity);
            return imUserInfoEntity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public Integer modifyPwByAccount(String account, String nickName, Integer status, Long orgId, String possWord, Integer supportedPms,
                                     String avatar) {
        String passWord = SecureUtil.md5(possWord);
        baseMapper.moidfyUserPwByAccount(orgId, nickName, status, account, passWord, supportedPms,avatar);
        return 0;
    }


    @Override
    public Integer delImUser(String account, String orgId) {
        return baseMapper.delUserByAccount(orgId, account);
    }

    /**
     * <B>验证授权码是否正确</B>
     *
     * @param channelId
     * @param authCode
     * @param userId
     * @param passWord
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result createUser(Long orgId, String channelId, String authCode, String userId, String nickName, String passWord) {
        //判断是否存在
        ImChannelEntity imChannelEntity = imChannelDao.findByChannelId(channelId);
        if (imChannelEntity != null && StrUtil.isNotBlank(imChannelEntity.getSalt()) && StrUtil.isNotBlank(imChannelEntity.getAuthCode())) {
            //判断authCode是否正确
            String sha256Code = Sha256Util.sha256EncryptionStr(authCode, imChannelEntity.getSalt());
            String sha256AuthCode = imChannelEntity.getAuthCode();
            if (StrUtil.equals(sha256Code, sha256AuthCode)) {
                //查询id，昵称是否重复
                if (StrUtil.isNotBlank(userId)) {
                    ImUserInfoEntity imUserInfo = baseMapper.verifyCreateUser(userId, null, null, orgId);
                    if (imUserInfo != null && ObjectUtil.isNotEmpty(imUserInfo.getId())) {
                        //id已经被注册
                        return new Result(ERROR_CODE_THREE, null);
                    }
                }

                if (StrUtil.isNotBlank(nickName)) {
                    ImUserInfoEntity imUserInfo = baseMapper.verifyCreateUser(null, nickName, null, orgId);
                    if (imUserInfo != null && ObjectUtil.isNotEmpty(imUserInfo.getId())) {
                        //昵称已经被注册
                        return new Result(ERROR_CODE_FOUR, null);
                    }
                }

                //获取账号序列
                SysSeqEntity sysSeqEntity = SysSeqDao.getBySeqName(PayConstant.SEQ_USERNAME, orgId);
                ImUserInfoEntity imUserInfoEntity = EntityPackageUtils.packageUserInfo(userId, nickName, sysSeqEntity.getCurrentValue(),
                        SecureUtil.md5(passWord), orgId);
                //更新seq
                SysSeqDao.updateToIncrease(PayConstant.SEQ_USERNAME, orgId);
                //保存信息
                this.insert(imUserInfoEntity);
                ImUserInfoEntity userInfo = baseMapper.getByUserName(orgId, sysSeqEntity.getCurrentValue());
                return new Result(userInfo);
            } else {
                return new Result(ERROR_CODE_TWO, null);
            }
        } else {
            return new Result(ERROR_CODE_ONE, null);
        }
    }

    /**
     * <B>修改用户信息</B>
     *
     * @param imUserInfoEntity
     * @return
     */
    @Override
    @CacheEvict(value = "imUserInfo:details", allEntries = true)
    public ImUserInfoEntity modifyUserInfo(ImUserInfoEntity imUserInfoEntity) {
        if (StrUtil.isNotBlank(imUserInfoEntity.getNickName())) {
            ImUserInfoEntity imUserInfo = baseMapper.verifyCreateUser(null, imUserInfoEntity.getNickName(), null, imUserInfoEntity.getOrgId());
            if (imUserInfo != null && ObjectUtil.isNotEmpty(imUserInfo.getId())) {
                return null;
            }
        }

        baseMapper.modifyUserInfo(imUserInfoEntity.getId(), imUserInfoEntity.getNickName(), ObjectUtil.isEmpty(imUserInfoEntity.getSex()) ? "" :
                        imUserInfoEntity.getSex().toString(), imUserInfoEntity.getSign(),
                imUserInfoEntity.getPhone(), imUserInfoEntity.getAddress(), imUserInfoEntity.getEmail(), imUserInfoEntity.getBirthDay(),
                imUserInfoEntity.getCardNumber(), imUserInfoEntity.getSchool(),
                ObjectUtil.isEmpty(imUserInfoEntity.getEducation()) ? "" : imUserInfoEntity.getEducation().toString(),
                imUserInfoEntity.getUserAvatar());
        return baseMapper.selectById(imUserInfoEntity.getId());
    }

    @Override
    public ImUserInfoEntity verifyCreateUser(String authCode, String userId, String userName, String passwd, Long orgId) {
        //判断是否存在对应的用户
        String passWord = SecureUtil.md5(passwd);
        ImUserInfoEntity imUserInfoEntity = baseMapper.verifyCreateUser(userId, userName, passWord, orgId);

        if (imUserInfoEntity != null) {
            return imUserInfoEntity;
        } else {
            //判断是否有对应id的用户
            ImUserInfoEntity imUserInfo = baseMapper.verifyCreateUser(userId, userName, null, null);
            if (imUserInfo != null) {
                return null;
            } else {
                //ImUserInfoEntity saveModel = EntityPackageUtils.packageUserInfo(userId, userName, passWord, orgId);
                //this.insert(saveModel);
                //return saveModel;
            }
        }
        return null;
    }

    /**
     * <B>更新用户在线状态</B>
     *
     * @param userId
     * @param isOnline
     */
    @Override
    public void modifyOnline(Integer userId, Integer isOnline) {
        baseMapper.modifyOnline(userId, isOnline);
    }

    @Override
    public void modifyRechargeNum(Integer userId) {
        baseMapper.modifyRechargeNum(userId);
    }

    @Override
    public List<ImUserInfoEntity> queryList(ImUserInfoQueryCriteria criteria) {
        return null;
    }

    @Override
    public ImUserInfoEntity create(ImUserInfoEntity resources) {
        resources.setCreateTime(new Date());
        this.baseMapper.insert(resources);
        return resources;
    }

    @Override
    public void update(ImUserInfoEntity resources) {
        this.updateById(resources);
    }

    @Override
    public void delete(Integer id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public ImUserInfoEntity findByUserNamePassword(String userName, String password) {
        return null;
    }

}
