package com.pay.service.modules.quartz.utils;

import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;
import com.pay.common.core.exception.BadRequestException;
import com.pay.service.modules.quartz.dao.QuartzJobDao;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author Lion
 * @date 2019-01-07
 */
@Slf4j
@Component
public class QuartzManage {

    private static final String JOB_NAME = "TASK_";

    @Resource(name = "scheduler")
    private Scheduler scheduler;

    @Resource
    private QuartzJobDao quartzJobDao;

    @PostConstruct
    public void addJobs(){
        //检测定时任务
        JobQueryCriteria query = new JobQueryCriteria();
        query.setIsPause(1);
        List<QuartzJobEntity> list = quartzJobDao.queryAllJob(query);
        for (int i = 0; i < list.size(); i++) {
            addJob(list.get(i));
        }
    }

    public void addJob(QuartzJobEntity quartzJob){
        try {
            // 构  建job信息
            JobDetail jobDetail = JobBuilder.newJob((Class<? extends Job>) Class.forName(quartzJob.getBeanName())).withIdentity(JOB_NAME + quartzJob.getId()).build();

            //通过触发器名和cron 表达式创建 Trigger
            Trigger cronTrigger = newTrigger()
                    .withIdentity(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId())
                    .withSchedule(CronScheduleBuilder.cronSchedule(quartzJob.getCronExpression()))
                    .usingJobData("data",quartzJob.getParams())
                    .startNow()
                    .build();

            cronTrigger.getJobDataMap().put(QuartzJobEntity.JOB_KEY, quartzJob);

            //重置启动时间
            ((CronTriggerImpl)cronTrigger).setStartTime(new Date());

            //执行定时任务
            scheduler.scheduleJob(jobDetail,cronTrigger);

            // 暂停任务
            if (quartzJob.getIsPause()==0) {
                pauseJob(quartzJob);
            }
        } catch (Exception e){
            log.error("创建定时任务失败", e);
            throw new BadRequestException("创建定时任务失败");
        }
    }

    /**
     * 更新job cron表达式
     * @param quartzJob
     * @throws SchedulerException
     */
    public void updateJobCron(QuartzJobEntity quartzJob){
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            // 如果不存在则创建一个定时任务
            if(trigger == null){
                addJob(quartzJob);
            }else{
                CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(quartzJob.getCronExpression());
                trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).usingJobData("data",quartzJob.getParams()).build();
                //重置启动时间
                ((CronTriggerImpl)trigger).setStartTime(new Date());
                trigger.getJobDataMap().put(QuartzJobEntity.JOB_KEY,quartzJob);

                scheduler.rescheduleJob(triggerKey, trigger);
                // 暂停任务
                if (quartzJob.getIsPause()==0) {
                    pauseJob(quartzJob);
                }
            }
        } catch (Exception e){
            log.error("更新定时任务失败", e);
            throw new BadRequestException("更新定时任务失败");
        }

    }

    /**
     * 删除一个job
     * @param quartzJob
     * @throws SchedulerException
     */
    public void deleteJob(QuartzJobEntity quartzJob){
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
            JobKey jobKey = JobKey.jobKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
            scheduler.pauseTrigger(triggerKey);
            scheduler.deleteJob(jobKey);
        } catch (Exception e){
            log.error("删除定时任务失败", e);
            throw new BadRequestException("删除定时任务失败");
        }
    }

    /**
     * 立即执行job
     * @param quartzJob
     * @throws SchedulerException
     */
    public void runAJobNow(QuartzJobEntity quartzJob){
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            // 如果不存在则创建一个定时任务
            if(trigger == null){
                addJob(quartzJob);
            }else{
                JobKey jobKey = JobKey.jobKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
                scheduler.triggerJob(jobKey);
            }
        } catch (Exception e){
            log.error("定时任务执行失败", e);
            throw new BadRequestException("定时任务执行失败");
        }
    }

    /**
     * 恢复一个job
     * @param quartzJob
     * @throws SchedulerException
     */
    public void resumeJob(QuartzJobEntity quartzJob){
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
            Trigger trigger = scheduler.getTrigger(triggerKey);
            if(trigger == null){
                addJob(quartzJob);
            }else{
                scheduler.resumeTrigger(triggerKey);
            }
        } catch (Exception e){
            log.error("恢复定时任务失败", e);
            throw new BadRequestException("恢复定时任务失败");
        }
    }

    /**
     * 暂停一个job
     * @param quartzJob
     * @throws SchedulerException
     */
    public void pauseJob(QuartzJobEntity quartzJob){
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(JOB_NAME + quartzJob.getJobName()+ "_" +quartzJob.getId());
            scheduler.pauseTrigger(triggerKey);
        } catch (Exception e){
            log.error("定时任务暂停失败", e);
            throw new BadRequestException("定时任务暂停失败");
        }
    }
}
