package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.pay.api.modules.im.criteria.ImMessageQueryCriteria;
import com.pay.api.modules.im.entity.ImMessageEntity;
import com.pay.api.modules.im.vo.ImMessageVo;
import com.pay.api.modules.trader.entity.TraderReplyConfigEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface ImMessageDao extends BaseMapper<ImMessageEntity> {

    TraderReplyConfigEntity getPayConfig(@Param("orgId")Long orgId, @Param("type") Integer type);

    List<ImMessageVo> queryMessagePage(Page page, ImMessageQueryCriteria criteria);

    ImMessageEntity getMessageById(@Param("messageId") String messageId, @Param("formUserId") Integer formUserId,
                                   @Param("toUserId") Integer toUserId);

    List<ImMessageVo> queryAll(ImMessageQueryCriteria criteria);

    List<ImMessageVo> getMessageHisPage(@Param("userId") Integer userId, @Param("timeOrder") Integer timeOrder);

    List<ImMessageVo> getUserMessageHisPage(Pagination page, @Param("receiveUserId") Integer receiveUserId, @Param("sendUserId") Integer sendUserId
            , @Param("timeOrder") Integer timeOrder);

    @Delete("delete FROM im_message WHERE DATE_SUB(CURDATE(), INTERVAL #{days} DAY) > DATE(create_time)")
    void deleteByDaysBefore(@Param("days") Integer days);
}
