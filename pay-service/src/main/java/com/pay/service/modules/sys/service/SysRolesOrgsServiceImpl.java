package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.sys.entity.SysRolesOrgsEntity;
import com.pay.api.modules.sys.service.SysRolesOrgsService;
import com.pay.service.modules.sys.dao.SysRolesOrgsDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service("SysRolesOrgsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysRolesOrgsServiceImpl extends ServiceImpl<SysRolesOrgsDao, SysRolesOrgsEntity> implements SysRolesOrgsService {


}
