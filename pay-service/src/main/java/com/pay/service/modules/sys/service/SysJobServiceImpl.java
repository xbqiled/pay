package com.pay.service.modules.sys.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.quartz.entity.SysJobEntity;
import com.pay.api.modules.sys.criteria.SysJobQueryCriteria;
import com.pay.api.modules.sys.dto.SysUserDTO;
import com.pay.api.modules.sys.service.SysJobService;
import com.pay.api.modules.sys.service.SysUserService;
import com.pay.api.modules.sys.vo.SysJobVo;
import com.pay.common.core.utils.PageUtils;
import com.pay.service.modules.quartz.dao.SysJobDao;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("sysJobService")
@AllArgsConstructor
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class SysJobServiceImpl extends ServiceImpl<SysJobDao, SysJobEntity> implements SysJobService {

    private SysUserService sysUserService;

    @Override
    public  List<SysJobVo> queryAll(SysJobQueryCriteria criteria) {
        return this.baseMapper.queryAll(criteria.getName(), criteria.getEnabled(), criteria.getStartTime(), criteria.getEndTime(), criteria.getDeptId(), criteria.getDeptIds());
    }

    @Override
    public PageUtils queryJobPage(SysJobQueryCriteria criteria, Pageable pageable) {
        Page<SysJobVo> page = new Page<>(pageable.getPageNumber(), pageable.getPageSize());
        List<SysJobVo> resultList = this.baseMapper.queryJobPage(page, criteria.getName(), criteria.getEnabled(), criteria.getStartTime(), criteria.getEndTime(), criteria.getDeptId(), criteria.getDeptIds());
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public SysJobEntity saveEntity(SysJobEntity sysJobEntity) {
        this.insert(sysJobEntity);
        return sysJobEntity;
    }

    @Override
    public void updateEntity(SysJobEntity sysJobEntity) {
        this.updateById(sysJobEntity);
    }

    @Override
    public Map<String,Object> delEntity(Long id) {
        Map<String,Object> map = new HashMap<>();
        SysUserDTO dto = sysUserService.findByJobId(id);
        map.put("flag",false);
        map.put("msg","请先删除关联该岗位的用户");
        if(dto == null){
            this.deleteById(id);
            map.put("flag",true);
        }
        return map;
    }

    @Override
    public List<Map<String, Object>> download(SysJobQueryCriteria criteria) {
        List<SysJobVo>  resultList = this.baseMapper.queryAll(criteria.getName(), criteria.getEnabled(), criteria.getStartTime(), criteria.getEndTime(), criteria.getDeptId(), criteria.getDeptIds());
        List<Map<String, Object>> list = new ArrayList<>();
        for (SysJobVo vo : resultList) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("岗位名称", vo.getName());
            map.put("所属组织", vo.getOrgName());
            map.put("岗位状态", vo.getEnabled()  ? "正常" : "禁用");
            map.put("创建日期", vo.getCreateTime());
            list.add(map);
        }
        return list;
    }
}
