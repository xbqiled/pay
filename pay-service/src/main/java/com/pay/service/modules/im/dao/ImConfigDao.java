package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pay.api.modules.im.entity.ImConfigEntity;
import com.pay.api.modules.im.vo.ImConfigVo;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-31 18:17:27
 */
public interface ImConfigDao extends BaseMapper<ImConfigEntity> {

    ImConfigVo getConfigByOrgId(@Param("orgId") Long orgId);

}
