package com.pay.service.modules.im.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pay.api.modules.im.entity.ImConfigEntity;
import com.pay.api.modules.im.service.ImConfigService;
import com.pay.api.modules.im.vo.ImConfigVo;
import com.pay.service.modules.im.dao.ImConfigDao;
import org.springframework.stereotype.Service;


@Service("imConfigService")
@org.apache.dubbo.config.annotation.Service(version = "${api.service.version}")
public class ImConfigServiceImpl extends ServiceImpl<ImConfigDao, ImConfigEntity> implements ImConfigService {

    @Override
    public ImConfigVo getConfigByOrgId(Long orgId) {
        return baseMapper.getConfigByOrgId(orgId);
    }


}
