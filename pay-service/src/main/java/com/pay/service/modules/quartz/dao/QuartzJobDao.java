package com.pay.service.modules.quartz.dao;


import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.quartz.dto.JobQueryCriteria;
import com.pay.api.modules.quartz.entity.QuartzJobEntity;

import java.io.Serializable;
import java.util.List;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:05
 */
public interface QuartzJobDao extends BaseMapper<QuartzJobEntity> {

    List<QuartzJobEntity> queryQuartzJobPage(Page page, JobQueryCriteria criteria);

    List<QuartzJobEntity> queryAllJob(JobQueryCriteria criteria);

    Integer insertQuartzJob(QuartzJobEntity bean);

    QuartzJobEntity queryById(@Param("id") Long id);

    Integer updateQuartzJobById(QuartzJobEntity quartzJobEntity);
}
