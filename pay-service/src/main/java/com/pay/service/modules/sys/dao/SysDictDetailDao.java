package com.pay.service.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.sys.entity.SysDictDetailEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface SysDictDetailDao extends BaseMapper<SysDictDetailEntity> {

    List<SysDictDetailEntity> queryDictDetailPage(Page page, @Param("name") String name);

    Integer delByDictId( @Param("dictId") Long dictId);
}
