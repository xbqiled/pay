package com.pay.service.modules.im.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pay.api.modules.im.criteria.ImOrgQueryCriteria;
import com.pay.api.modules.im.entity.ImOrgEntity;
import com.pay.api.modules.im.vo.ImOrgVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2019-10-09 12:03:10
 */
public interface ImOrgDao extends BaseMapper<ImOrgEntity> {

    List<ImOrgVo> queryOrgPage(Page page, ImOrgQueryCriteria criteria);

    List<ImOrgVo> queryAll(ImOrgQueryCriteria criteria);

    Integer deleteByOrgId(@Param("orgId") Long orgId);

}
