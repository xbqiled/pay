package com.pay.server.utils;

import com.google.protobuf.Message;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.SessionCloseReason;
import io.netty.channel.Channel;
import io.netty.util.Attribute;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.pay.server.utils.ChannelUtils.SESSION_KEY;


public enum SessionManager {
    INSTANCE;

    /**
     * 缓存通信上下文环境对应的登录用户（主要用于服务）
     */
    private Map<IoSession, Integer> session2UserIds = new ConcurrentHashMap<>();

    /**
     * 缓存用户id与对应的会话
     */
    private ConcurrentMap<Integer, IoSession> userId2Sessions = new ConcurrentHashMap<>();

    /**
     * 向单一在线用户发送数据包
     */
    public void sendPacketTo(IoSession session, MessageProto.Message pact) {
        if (pact == null || session == null) return;
        session.sendPacket(pact);
    }

    public void sendPacketTo(Integer userId, Message pact) {
        if (pact == null || userId <= 0) return;

        IoSession session = userId2Sessions.get(userId);
        if (session != null) {
            session.sendPacket(pact);
        }
    }

    public void sendPacketTo(Channel channel, Message pact) {
        if (pact == null || channel == null) return;
        channel.writeAndFlush(pact);
    }

    public boolean isHaveSession(IoSession session) {
        if (session2UserIds.get(session) != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public boolean isOnline(Integer userId) {
        if (userId2Sessions.get(userId) != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * 向所有在线用户发送数据包
     */
    public void notifyToAllOnlineUsers(Message pact) {
        if (pact == null) return;
        userId2Sessions.values().forEach(session -> session.sendPacket(pact));
    }

    public IoSession getSessionBy(Integer userId) {
        return this.userId2Sessions.get(userId);
    }


    public boolean registerSession(ImUserInfoEntity user, IoSession session) {
        session.setUser(user);
        session.setSignOuted(false);
        userId2Sessions.put(user.getId(), session);
        session2UserIds.put(session, user.getId());
        return true;
    }

    /**
     * 注销用户通信渠道
     */
    public void ungisterUserContext(Channel context, SessionCloseReason reason) {
        if (context == null) {
            return;
        }
        IoSession session = ChannelUtils.getSessionBy(context);
        Integer userId = session2UserIds.remove(session);
        userId2Sessions.remove(userId);
        if (session != null) {
            session.close(reason);
        }
    }

    public void ungisterUserChannel(IoSession session) {
        if (session != null) {
            Integer userId = session2UserIds.remove(session);
            if (userId != null) {
                userId2Sessions.remove(userId);
            }
            session.setSignOuted(true);
        }
    }
}
