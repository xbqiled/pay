package com.pay.server.utils;


import com.google.gson.Gson;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.im.constant.ImConst;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.*;
import com.pay.common.im.packets.command.CommandEnum;

import java.util.Date;

import static com.pay.common.core.constant.PayConstant.PAY_CONFIG;

/**
 * <B>ProtoBuf转换类</B>
 */
public class ProToBufBuild {

    /**
     * 获取ptb实例
     *
     * @return
     */
    public static final MessageProto.Message ProToBufInstance() {
        return MessageProto.Message.getDefaultInstance();
    }

    /**
     * <B>发送消息</B>
     *
     * @param messageId
     * @param from
     * @param to
     * @param content
     * @param sendTime
     * @return
     */
    public static final MessageReqProto.MessageReq buidMessageReq(String messageId, UserProto.User from, UserProto.User to,
                                                                  ContentProto.Content content, Long sendTime) {
        return MessageReqProto.MessageReq.newBuilder().
                setMessageId(messageId).setFromUser(from).setToUser(to).setContent(content).setSendTime(sendTime).build();
    }

    /**
     * <B>组装用户数据</B>
     *
     * @param imUserInfoEntity
     * @return
     */
    public static final UserProto.User buildUser(ImUserInfoEntity imUserInfoEntity) {
        return UserProto.User.newBuilder().setUserId(imUserInfoEntity.getId()).setNickName(imUserInfoEntity.getNickName())
                .setAvatar(imUserInfoEntity.getUserAvatar()).setOrgId(String.valueOf(imUserInfoEntity.getOrgId())).build();
    }

    /**
     * <B>组装登录返回数据</B>
     *
     * @param code
     * @param msg
     * @return
     */
    public static final LoginResProto.LoginRes buildLoginRes(Integer code, String msg) {
        return LoginResProto.LoginRes.newBuilder().setCode(code).setMsg(msg).build();
    }

    /**
     * <B>组装登录返回数据</B>
     *
     * @param code
     * @param msg
     * @return
     */
    public static final LoginResProto.LoginRes buildLoginRes(Integer code, String msg, String token, UserProto.User user) {
        return LoginResProto.LoginRes.newBuilder().setCode(code).setMsg(msg).setUser(user).setToken(token).build();
    }

    /**
     * <B>组装登录返回信息</B>
     *
     * @return
     */
    public static final MessageProto.Message buildLoginResMessage(ImStatus smStatus) {
        LoginResProto.LoginRes loginRes = buildLoginRes(smStatus.getCode(), smStatus.getMsg());
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_LOGIN_RESP.getValue()).setVersion(ImConst.VERSION).setLoginRes(loginRes).build();
    }

    /**
     * <B>组装登录返回信息</B>
     *
     * @return
     */
    public static final MessageProto.Message buildLoginResMessage(ImUserInfoEntity imUserInfoEntity, String token, ImStatus smStatus) {
        UserProto.User user = buildUser(imUserInfoEntity);
        LoginResProto.LoginRes loginRes = buildLoginRes(smStatus.getCode(), smStatus.getMsg(), token, user);
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_LOGIN_RESP.getValue()).setVersion(ImConst.VERSION).setLoginRes(loginRes).build();
    }

    public static String copyMessage(ContentProto.Content content) {
        MessageContent mc = new MessageContent();
        mc.setHigth(content.getHigth());
        mc.setWidth(content.getWidth());
        mc.setText(content.getText());
        mc.setDuration(content.getDuration());
        return new Gson().toJson(mc);
    }


    public static String copyMessage(Integer width, Integer higth, String text, Integer duration) {
        MessageContent mc = new MessageContent();
        mc.setHigth(higth);
        mc.setWidth(width);
        mc.setText(text);
        mc.setDuration(duration);
        return new Gson().toJson(mc);
    }

    public static final MessageProto.Message buildHeartBeatRes() {
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_HEARTBEAT_RESP.getValue()).setVersion(ImConst.VERSION).build();
    }

    public static final MessageProto.Message buildForceDisconnMessage() {
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_FORCE_DISCONN.getValue()).setVersion(ImConst.VERSION).build();
    }

    public static final MessageProto.Message buildHandshakeRes(ImStatus smStatus) {
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_HANDSHAKE_RESP.getValue()).setVersion(ImConst.VERSION).build();
    }


    public static final MessageProto.Message buildSendMessageRes(String messageId, Date date, ImStatus smStatus) {
        MessageResProto.MessageRes messageRes = buildMessageResRes(messageId, date, smStatus);
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_CHAT_RESP.getValue()).setVersion(ImConst.VERSION).setMessageRes(messageRes).build();
    }


    public static final MessageProto.Message buildPayConfigMessageReq(String messageId, String payDesc, UserProto.User formUser,
                                                                      UserProto.User toUser,Integer payType) {
        ContentProto.Content content = ContentProto.Content.newBuilder().setPayDesc(payDesc).setText(PAY_CONFIG).setDuration(payType).build();
        MessageReqProto.MessageReq messageReq = buildMessageReq(messageId, new Date(), content, formUser, toUser);
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_CHAT_REQ.getValue()).setVersion(ImConst.VERSION).setMessageReq(messageReq).build();
    }


    public static final MessageReqProto.MessageReq buildMessageReq(String messageId, Date date, ContentProto.Content content,
                                                                   UserProto.User formUser, UserProto.User toUser) {
        return MessageReqProto.MessageReq.newBuilder().setMessageId(messageId).setMsgType(7).setContent(content)
                .setFromUser(formUser).setToUser(toUser).setSendTime(date.getTime()).build();
    }

    /**
     * <B>组装发送消息</B>
     *
     * @param smStatus
     * @return
     */
    public static final MessageProto.Message buildSendMessageRes(ImStatus smStatus) {
        MessageResProto.MessageRes messageRes = buildMessageResRes(smStatus);
        return MessageProto.Message.newBuilder().setCommand(CommandEnum.Command.COMMAND_CHAT_RESP.getValue()).
                setVersion(ImConst.VERSION).setMessageRes(messageRes).build();
    }

    /**
     * <B>组装信息</B>
     */
    public static final MessageResProto.MessageRes buildMessageResRes(ImStatus smStatus) {
        return MessageResProto.MessageRes.newBuilder().setCode(smStatus.getCode()).setMsg(smStatus.getMsg()).build();
    }

    /**
     * <B>组装信息</B>
     */
    public static final MessageResProto.MessageRes buildMessageResRes(String messageId, Date date, ImStatus smStatus) {
        return MessageResProto.MessageRes.newBuilder().setCode(smStatus.getCode()).setAcceptTime(date.getTime()).
                setMessageId(messageId).setMsg(smStatus.getMsg()).build();
    }
}
