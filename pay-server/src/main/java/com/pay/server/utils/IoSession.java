package com.pay.server.utils;


import com.google.protobuf.Message;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.im.packets.SessionCloseReason;
import io.netty.channel.Channel;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 链接的会话
 *
 * @author
 */
public class IoSession {

    /**
     * distributeKey auto generator
     */
    private AtomicInteger dispatchKeyGenerator = new AtomicInteger();

    /**
     * 网络连接channel
     */
    private Channel channel;

    //用户实体信息
    private ImUserInfoEntity user;

    /**
     * ip地址
     */
    private String ipAddr;


    private boolean reconnected;

    /**
     * 业务分发索引
     */
    private int dispatchKey;

    /**
     * 拓展用，保存一些个人数据
     */
    private Map<String, Object> attrs = new HashMap<>();

    private boolean forceDisconnected;

    private boolean signOuted;

    public IoSession() {
    }

    public IoSession(Channel channel) {
        this.channel = channel;
        this.ipAddr = ChannelUtils.getIp(channel);
        this.dispatchKey = dispatchKeyGenerator.getAndIncrement();
    }

    public void setUser(ImUserInfoEntity user) {
        this.user = user;
    }

    public Channel getChannel() {
        return channel;
    }

    public int getDispatchKey() {
        return dispatchKey;
    }

    /**
     * 向客户端发送消息
     *
     * @param packet
     */
    public void sendPacket(Message packet) {
        if (packet == null) {
            return;
        }
        if (channel != null) {
            channel.writeAndFlush(packet);
        }
    }

    public void sendPacket(Message packet, GenericFutureListener listener) {
        if (packet == null) {
            return;
        }
        if (channel != null) {
            channel.writeAndFlush(packet).addListener(listener);
        }
    }

    public boolean isForceDisconnected() {
        return forceDisconnected;
    }

    public void setForceDisconnected(boolean forceDisconnected) {
        this.forceDisconnected = forceDisconnected;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public boolean isReconnected() {
        return reconnected;
    }

    public void setReconnected(boolean reconnected) {
        this.reconnected = reconnected;
    }

    public boolean isSignOuted() {
        return signOuted;
    }

    public void setSignOuted(boolean signOuted) {
        this.signOuted = signOuted;
    }

    public ImUserInfoEntity getUser() {
        return user;
    }

    public boolean isClose() {
        if (channel == null) {
            return true;
        }
        return !channel.isActive() ||
                !channel.isOpen();
    }

    /**
     * 关闭session
     *
     * @param reason {@link SessionCloseReason}
     */
    public void close(SessionCloseReason reason) {
        try {
            if (this.channel == null) {
                return;
            }
            if (channel.isOpen()) {
                channel.close();
            } else {

            }
        } catch (Exception e) {
        }
    }

}
