package com.pay.server.logic.message;


import com.pay.common.im.constant.CodeEnumConstant;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.UserProto;
import com.pay.common.im.utils.SynSeqUtil;
import com.pay.server.async.DBEventType;
import com.pay.server.async.annotation.EventHandler;
import com.pay.server.async.event.MessageEvent;
import com.pay.server.async.event.OfflineMessageEvent;
import com.pay.server.base.SpringContext;
import com.pay.server.base.database.DatabaseApi;
import com.pay.server.logic.user.UserService;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.ProToBufBuild;
import com.pay.server.utils.SessionManager;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class MessageService {

    /**
     * <B>发送消息给用户</B>
     */
    public void sendMessageToUser(Channel channel, MessageProto.Message message) {
        //判断用户是否在线
        IoSession session = ChannelUtils.getSessionBy(channel);
        Boolean isONline = SpringContext.getBean(UserService.class).isOnlineUser(message.getMessageReq().getToUser().getUserId());
        //发送
        Date date = new Date();
        SessionManager.INSTANCE.sendPacketTo(session, ProToBufBuild.buildSendMessageRes(message.getMessageReq().getMessageId(), date, ImStatus.MESSAGE_SEND_OK));

        if (isONline) {
            //获取用户channel
            SessionManager.INSTANCE.sendPacketTo(message.getMessageReq().getToUser().getUserId(), message);
        } else {
            //异步执行
            this.createOfflineMessageEvent(message, date);
        }
        //异步存储记录信息
        this.createMessageEvent(message, date);

        //判断发送的是否为入款信息
        if (message.getMessageReq().getMsgType() == CodeEnumConstant.MSG_TYPE.MSG_TYPE_PAY.getValue()) {
            String payDesc = SpringContext.getBean(DatabaseApi.class).getPayOrgStr(message.getMessageReq().getToUser().getUserId());
            //发送信息
            MessageProto.Message payMessage = ProToBufBuild.buildPayConfigMessageReq(SynSeqUtil.createTimeMessageId(), payDesc,
                    message.getMessageReq().getToUser(), message.getMessageReq().getFromUser(),message.getMessageReq().getContent().getDuration());

            SessionManager.INSTANCE.sendPacketTo(message.getMessageReq().getFromUser().getUserId(), payMessage);
            SessionManager.INSTANCE.sendPacketTo(message.getMessageReq().getToUser().getUserId(), payMessage);
            this.createMessageEvent(payMessage, new Date());
        }
    }

    //异步存储发送消息
    private void createMessageEvent(MessageProto.Message message, Date dateTime) {
        SpringContext.getEventDispatcher().fireEvent(new MessageEvent(DBEventType.MESSAGE, message.getMessageReq().getMessageId(),
                message.getMessageReq().getFromUser().getUserId(), message.getMessageReq().getToUser().getUserId(),
                message.getMessageReq().getMsgType(), message.getMessageReq().getContent().getText(), message.getMessageReq().getContent().getWidth(),
                message.getMessageReq().getContent().getHigth(), message.getMessageReq().getContent().getDuration(),
                message.getMessageReq().getSendTime(), dateTime));
    }

    //异步存储离线消息
    private void createOfflineMessageEvent(MessageProto.Message message, Date dateTime) {
        SpringContext.getEventDispatcher().fireEvent(new OfflineMessageEvent(DBEventType.OFFLINE_MESSAGE, message.getMessageReq().getMessageId(),
                message.getMessageReq().getFromUser().getUserId(), message.getMessageReq().getToUser().getUserId(), message.getMessageReq().getMsgType(),
                message.getMessageReq().getContent().getText(), message.getMessageReq().getContent().getWidth(), message.getMessageReq().getContent().getHigth(),
                message.getMessageReq().getContent().getDuration(), message.getMessageReq().getSendTime(), dateTime));
    }

    /**
     * <B>异步执行用户离线信息</B>
     *
     * @param offlineMessageEvent
     */
    @EventHandler(value = {DBEventType.OFFLINE_MESSAGE})
    public void sendOffineUserMessage(OfflineMessageEvent offlineMessageEvent) {
        SpringContext.getBean(DatabaseApi.class).saveImOfflineUserMessage(offlineMessageEvent.sendUserId, offlineMessageEvent.receiverUserId,
                offlineMessageEvent.msgType, ProToBufBuild.copyMessage(offlineMessageEvent.width, offlineMessageEvent.higth,
                        offlineMessageEvent.content, offlineMessageEvent.duration), offlineMessageEvent.messageId, offlineMessageEvent.dateTime);
    }

    /**
     * <B>异步执行发送信息</B>
     *
     * @param messageEvent
     */
    @EventHandler(value = {DBEventType.MESSAGE})
    public void sendMessage(MessageEvent messageEvent) {
        log.info("保存消息加入到异步线程中:" + messageEvent.sendUserId  + "receiveUserId:" + messageEvent.receiverUserId  + "messsageId:" + messageEvent.messageId);
        SpringContext.getBean(DatabaseApi.class).saveMessage(messageEvent.sendUserId, messageEvent.receiverUserId, messageEvent.chatType,
                messageEvent.msgType, ProToBufBuild.copyMessage(messageEvent.width, messageEvent.higth, messageEvent.content, messageEvent.duration),
                messageEvent.groupId, messageEvent.messageId, messageEvent.dateTime);
    }
}
