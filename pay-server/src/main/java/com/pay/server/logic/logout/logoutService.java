package com.pay.server.logic.logout;


import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.SessionCloseReason;
import com.pay.server.base.SpringContext;
import com.pay.server.logic.user.UserService;
import io.netty.channel.Channel;
import org.springframework.stereotype.Component;


@Component
public class logoutService {

    public void userLogout(Channel channel, MessageProto.Message message) {
        SpringContext.getBean(UserService.class).userLogout(channel, SessionCloseReason.NORMAL);
    }

}
