package com.pay.server.logic.user;


import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.core.constant.EnumConstant;
import com.pay.common.im.packets.SessionCloseReason;
import com.pay.common.im.utils.ConcurrentHashSet;
import com.pay.server.async.DBEventType;
import com.pay.server.async.event.UserLogoutEvent;
import com.pay.server.base.SpringContext;
import com.pay.server.base.database.DatabaseApi;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.SessionManager;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class UserService {

    /**
     * 在线用户列表
     */
    private static final Set<Integer> onlneUsers = new ConcurrentHashSet<>();

    public void addUser2Online(ImUserInfoEntity user) {
        this.onlneUsers.add(user.getId());
    }

    /**
     * <B>从在线列表中删除</B>
     *
     * @param userId
     */
    public void removeFromOnline(Integer userId) {
        this.onlneUsers.remove(userId);
    }

    /**
     * <B>判断用户是否在线</B>
     *
     * @param userId
     * @return
     */
    public boolean isOnlineUser(Integer userId) {
        return this.onlneUsers.contains(userId);
    }

    /**
     * <B>是否登录</B>
     *
     * @param userId
     * @return
     */
    public boolean islogin(Integer userId) {
        return this.onlneUsers.contains(userId);
    }


    public void userLogout(Channel channel, SessionCloseReason reason) {
        IoSession session = ChannelUtils.getSessionBy(channel);
        if (session != null && session.getChannel() != null) {

            log.info("全部在线用户为:" + this.onlneUsers.toString());
            if (session.getUser() != null && session.getUser().getId() != null && !session.isForceDisconnected()) {
                Integer userId = session.getUser().getId();
                log.info("用户:" + userId + "断开退出");

                this.removeFromOnline(userId);
                SessionManager.INSTANCE.ungisterUserChannel(session);
                //异步通知下
                // SpringContext.getEventDispatcher().fireEvent(new UserLogoutEvent(DBEventType.LOGOUT, session.getUser(), session));
                CompletableFuture.runAsync(() -> {
                    log.error("modify user online >>>>>>>>>>>> user:[{}] -> [{}]", userId, EnumConstant.ONLINE_TYPE.TYPE_OFFLINE.getValue());
                    SpringContext.getBean(DatabaseApi.class).modifyUserOnLine(userId,
                            EnumConstant.ONLINE_TYPE.TYPE_OFFLINE.getValue());
                }).whenCompleteAsync((v, t) -> t.printStackTrace());
            } else {
                session.close(SessionCloseReason.OVER_TIME);
            }
        }
    }


    public void userOffline(Channel channel, SessionCloseReason reason) {
        IoSession session = ChannelUtils.getSessionBy(channel);
        if (session != null && session.getChannel() != null) {
            log.info("全部在线用户为Start:" + this.onlneUsers.toString());
            if (session.getUser() != null && session.getUser().getId() != null) {
                Integer userId = session.getUser().getId();
                log.info("用户:" + userId + "断开退出");
                this.removeFromOnline(userId);
            }
            log.info("全部在线用户为End:" + this.onlneUsers.toString());
            session.close(reason);
        }
        //群组用户断开处理
    }
}
