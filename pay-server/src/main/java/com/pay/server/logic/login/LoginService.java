package com.pay.server.logic.login;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.core.constant.EnumConstant;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.MessageProto;
import com.pay.server.async.DBEventType;
import com.pay.server.async.annotation.EventHandler;
import com.pay.server.async.event.UserLoginEvent;
import com.pay.server.async.event.UserLogoutEvent;
import com.pay.server.base.SpringContext;
import com.pay.server.base.database.DatabaseApi;
import com.pay.server.logic.user.UserService;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.ProToBufBuild;
import com.pay.server.utils.SessionManager;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

import static com.pay.common.im.packets.SessionCloseReason.FORCE_DISCONN;


@Component
@Slf4j
public class LoginService {

    //验证登录
    public void validateLogin(Channel channel, MessageProto.Message message) {
        String token = "";
        IoSession session = ChannelUtils.getSessionBy(channel);

        ImUserInfoEntity userInfoEntity = SpringContext.getBean(DatabaseApi.class).getUserInfo(Long.parseLong(message.getLoginReq().getOrgId()),
                message.getLoginReq().getUserName(), message.getLoginReq().getPassword());
        if (userInfoEntity != null && StrUtil.isNotEmpty(userInfoEntity.getUserName())) {
            if (StrUtil.equals(SecureUtil.md5(message.getLoginReq().getPassword()), userInfoEntity.getPassword())) {
                //判断账号是否被冻结
                if (userInfoEntity.getStatus() != EnumConstant.USER_STATUS.USER_NORMAL.getValue()) {
                    SessionManager.INSTANCE.sendPacketTo(session, ProToBufBuild.buildLoginResMessage(ImStatus.LOGIN_ACCOUNT_FREEZE));
                    return;
                }
                //保存用户到token
                token = SpringContext.getBean(DatabaseApi.class).saveToken(userInfoEntity);
                //更新用户是否在线
                onLoginSucc(userInfoEntity, session, token);
            } else {
                SessionManager.INSTANCE.sendPacketTo(session, ProToBufBuild.buildLoginResMessage(ImStatus.LOGIN_PASSWD_REEOR));
                return;
            }
        } else {
            SessionManager.INSTANCE.sendPacketTo(session, ProToBufBuild.buildLoginResMessage(ImStatus.LOGIN_PASSWD_REEOR));
            return;
        }
    }


    private void onLoginSucc(ImUserInfoEntity userInfo, IoSession session, String token) {
        // 踢掉旧的session
        IoSession originalSession = SessionManager.INSTANCE.getSessionBy(userInfo.getId());

        if (originalSession != null) {
            log.info(">>>>>>>>>>>>>>>>>>>>>>> 踢人 user:[{}] 原channel[{}] 当前channel[{}]", originalSession.getUser().getId(),
                    originalSession.getChannel(), session.getChannel());

            originalSession.setForceDisconnected(true);
            originalSession.sendPacket(ProToBufBuild.buildForceDisconnMessage(), (ChannelFutureListener) channelFuture -> {
                /*if (channelFuture.isSuccess()){
                    Thread.sleep(1000);
                    channelFuture.channel().close();
                }*/
            });
        }

        //注册上去
        SessionManager.INSTANCE.registerSession(userInfo, session);

        //记录在线用户信息
        SpringContext.getBean(UserService.class).addUser2Online(userInfo);
        //发送信息
        SessionManager.INSTANCE.sendPacketTo(session, ProToBufBuild.buildLoginResMessage(userInfo, token, ImStatus.LOGIN_OK));
        //异步处理
        // SpringContext.getEventDispatcher().fireEvent(new UserLoginEvent(DBEventType.LOGIN, userInfo));
        CompletableFuture.runAsync(() -> {
            log.error("modify user online >>>>>>>>>>>> user:[{}] -> [{}]", userInfo.getId(), EnumConstant.ONLINE_TYPE.TYPE_ONLINE.getValue());
            SpringContext.getBean(DatabaseApi.class).modifyUserOnLine(userInfo.getId(),
                    EnumConstant.ONLINE_TYPE.TYPE_ONLINE.getValue());
        }).whenCompleteAsync((v, t) -> t.printStackTrace());
    }


    @EventHandler(value = {DBEventType.LOGIN})
    public void onUserLogin(UserLoginEvent loginEvent) {
        //更新状态信息
        SpringContext.getBean(DatabaseApi.class).modifyUserOnLine(loginEvent.getUser().getId(), EnumConstant.ONLINE_TYPE.TYPE_ONLINE.getValue());
    }

    @EventHandler(value = {DBEventType.LOGOUT})
    public void onUserLogout(UserLogoutEvent logoutEvent) {
        //更新状态信息
        SpringContext.getBean(DatabaseApi.class).modifyUserOnLine(logoutEvent.getUser().getId(), EnumConstant.ONLINE_TYPE.TYPE_OFFLINE.getValue());
    }
}
