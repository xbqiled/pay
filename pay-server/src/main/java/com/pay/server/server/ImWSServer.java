package com.pay.server.server;


import com.pay.common.im.bootstrap.ServerNode;
import com.pay.server.base.SpringContext;

import com.pay.server.config.ImConfiguration;
import com.pay.server.filter.ImWSServerFilter;
import com.pay.server.handler.manager.HandlerManager;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;


@Slf4j
public class ImWSServer implements ServerNode {


    private EventLoopGroup boss = new NioEventLoopGroup();
    private EventLoopGroup work = new NioEventLoopGroup();
    private ImConfiguration imConfiguration;

    @Override
    public void init() {
        imConfiguration = SpringContext.getImConfiguration();
    }

    /**
     * 启动 ws server
     *
     * @return
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {
        try {
            ServerBootstrap bootstrap = new ServerBootstrap()
                    .group(boss, work)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(new InetSocketAddress(imConfiguration.getWsPort()))
                    //保持长连接
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childHandler(new ImWSServerFilter());
            Channel ch = bootstrap.bind(imConfiguration.getWsPort()).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    if (channelFuture.isSuccess()) {
                        HandlerManager.getInstance().initHandlers();
                    } else {
                        log.info("处理器启动失败!");
                    }
                }
            }).sync().channel();
            if (ch.closeFuture().isSuccess()) {
                log.info("启动 ws server 成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void shutDown() throws Exception {
        if (boss != null) {
            boss.shutdownGracefully();
        }
        if (work != null) {
            work.shutdownGracefully();
        }
    }

}
