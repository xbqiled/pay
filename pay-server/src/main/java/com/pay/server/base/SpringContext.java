package com.pay.server.base;


import com.pay.server.async.EventDispatcher;
import com.pay.server.base.cache.CacheApi;
import com.pay.server.base.cache.RedisCacheInit;
import com.pay.server.base.database.DatabaseApi;
import com.pay.server.config.ImConfiguration;
import com.pay.server.dispatch.MessageDispatcher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collection;

@Component
public class SpringContext implements ApplicationContextAware {

	private static SpringContext self;
	
	/** spring容器上下文 */
	private static ApplicationContext applicationContext = null;
	
	@PostConstruct
	private void init() {
		self = this;
	}

	//定义持reids持久化服务
	@Autowired
	private static RedisCacheInit redisCacheInit;

	//定义数据库持久化服务
	@Autowired
	private static DatabaseApi databaseApi;

	@Autowired
	private static CacheApi cacheApi;

	//服务器配置信息
	private static ImConfiguration imConfiguration;

	private static MessageDispatcher messageDispatcher;

	private static EventDispatcher eventDispatcher;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContext.applicationContext = applicationContext;
	}

	public final static <T> T getBean(Class<T> clazz) {
		return applicationContext.getBean(clazz);
	}

	public final static <T> Collection<T> getBeansOfType(Class<T> clazz) {
		return applicationContext.getBeansOfType(clazz).values();
	}

	public final static <T> T getBean(String name, Class<T> requiredType) {
		return applicationContext.getBean(name, requiredType);
	}

	public static CacheApi getCacheApi() {
		return cacheApi;
	}

	public static void setCacheApi(CacheApi cacheApi) {
		SpringContext.cacheApi = cacheApi;
	}

	@Resource
	public void setMessageDispatcher(MessageDispatcher messageDispatcher) {
		SpringContext.messageDispatcher = messageDispatcher;
	}

	public final static MessageDispatcher getMessageDispatcher() {
		return messageDispatcher;
	}

	@Resource
	public void setEventDispatcher(EventDispatcher eventDispatcher) {
		SpringContext.eventDispatcher = eventDispatcher;
	}

	public final static EventDispatcher getEventDispatcher() {
		return eventDispatcher;
	}

	public final static ImConfiguration getImConfiguration() {
		return imConfiguration;
	}

	public final static DatabaseApi getDatabaseApi() {
		return databaseApi;
	}

	@Resource
	public void setImConfiguration(ImConfiguration imConfiguration) {
		SpringContext.imConfiguration = imConfiguration;
	}

}
