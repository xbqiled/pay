package com.pay.server.base.cache;


import com.pay.api.modules.im.service.ImChannelService;
import com.pay.api.modules.im.service.ImOrgService;
import com.pay.api.modules.im.service.ImUserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * <B>启动时加载mysql数据保存到redis中</B>
 */

@Component
@Slf4j
public class LoadRedisCache implements ApplicationRunner {
    //用户信息远程服务
    @Reference(version = "${api.service.version}")
    private ImUserInfoService imUserInfoService;
    //组织信息远程服务
    @Reference(version = "${api.service.version}")
    private ImChannelService imChannelService;
    //银商信息
    @Reference(version = "${api.service.version}")
    private ImOrgService imOrgService;

    @Autowired
    private CacheApi cacheApi;

    //开一个线程执行数据的加载
    @Async
    public void loadUserInfo() {
        try {
            //获取所有的组织机构信息
//            List<ImChannelEntity>  orgList = imOrgService.selectList(null);
//            if (orgList != null && orgList.size() > 0 ) {
//                log.info("开始加载组织数据到缓存,共有" + orgList.size() + "条数据");
//
//                if (orgList != null && orgList.size() >0) {
//                    for (ImChannelEntity model :orgList) {
//                        //cacheApi.createOrgCache(model);
//                    }
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.loadUserInfo();
    }
}
