package com.pay.server.base.cache;

import com.pay.common.core.redis.RedisCache;
import com.pay.common.core.redis.RedisCacheManager;
import com.pay.common.im.entity.message.AbstractMessageHelper;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Redis获取持久化+同步消息助手;
 */
@Data
@Component
public class RedisCacheInit extends AbstractMessageHelper {
    private Logger log = LoggerFactory.getLogger(RedisCacheInit.class);
    private RedisCache orgCache = null;
    private RedisCache orgAuthCache = null;
    private RedisCache toKenCache = null;
    private RedisCache userCache = null;

    private RedisCache messageCache = null;
    private RedisCache offlineMessageCache = null;
    private RedisCache orgSpecialCache = null;
    private final String SUBFIX = ":";

    static {
        //组织机构信息
        RedisCacheManager.register(ORG, Integer.MAX_VALUE, Integer.MAX_VALUE);
        //组织机构鉴权信息
        RedisCacheManager.register(ORG_AUTH, Integer.MAX_VALUE, Integer.MAX_VALUE);
        //用户鉴权TOKEN信息
        RedisCacheManager.register(TOKEN, Integer.MAX_VALUE, Integer.MAX_VALUE);
        //用户信息
        RedisCacheManager.register(USER, Integer.MAX_VALUE, Integer.MAX_VALUE);
        //消息信息
        RedisCacheManager.register(MESSAGE, Integer.MAX_VALUE, Integer.MAX_VALUE);
        //消息信息
        RedisCacheManager.register(OFFLINE_MESSAGE, Integer.MAX_VALUE, Integer.MAX_VALUE);
        //特殊符号
        RedisCacheManager.register(ORG_SPECIAL, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public RedisCacheInit() {
        this.orgCache = RedisCacheManager.getCache(ORG);
        this.orgAuthCache = RedisCacheManager.getCache(ORG_AUTH);

        this.toKenCache = RedisCacheManager.getCache(TOKEN);
        this.userCache = RedisCacheManager.getCache(USER);
        this.messageCache = RedisCacheManager.getCache(MESSAGE);

        this.offlineMessageCache = RedisCacheManager.getCache(OFFLINE_MESSAGE);
        this.orgSpecialCache = RedisCacheManager.getCache(ORG_SPECIAL);
    }
}
