package com.pay.server.base.cache;


import cn.hutool.core.util.IdUtil;
import com.pay.api.modules.im.dto.ImOrgSpecialDTO;
import com.pay.api.modules.im.dto.ImUserInfoDTO;
import com.pay.api.modules.im.entity.ImChannelEntity;
import com.pay.api.modules.im.entity.ImOrgSpecialEntity;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.core.redis.RedisCacheManager;
import org.springframework.stereotype.Service;

import static com.pay.common.im.constant.ImConst.*;




@Service
public class CacheApi{

    /**
     * <B>token添加与创建</B>
     */
    public String  createToKen(ImUserInfoDTO imUserInfoDTO) {
        String UUID = IdUtil.fastSimpleUUID();
        RedisCacheManager.getCache(TOKEN).put(KEY + SUBFIX + UUID, imUserInfoDTO);
        return UUID;
    }

    /**
     * <B>获取token对应的用户信息</B>
     */
    public ImUserInfoDTO  getToken(String token, ImUserInfoEntity imUserInfoEntity) {
        return (ImUserInfoDTO)RedisCacheManager.getCache(TOKEN).get(KEY + SUBFIX + token);
    }

    /**
     * <B>创建用户缓存</B>
     */
    public void createUserCache(ImUserInfoEntity userInfo) {
        RedisCacheManager.getCache(USER).put(KEY + SUBFIX + userInfo.getId(), userInfo);
    }

    /**
     * <B>根据用户ID获取缓存信息</B>
     */
    public ImUserInfoEntity getUserCache(String userId) {
        return (ImUserInfoEntity)RedisCacheManager.getCache(USER).get(KEY + SUBFIX + userId);
    }

    /**
     * <B>根据用户ID获取缓存信息</B>
     */
    public ImUserInfoEntity getGroupCache(String groupId) {
        return (ImUserInfoEntity)RedisCacheManager.getCache(GROUP).get(KEY + SUBFIX + groupId);
    }

//    /**
//     * <B>获取组织机构鉴权信息</B>
//     * @param orgId
//     */
//    public ImChannelAuthDTO getOrgAuthCache(Long orgId) {
//        return (ImChannelAuthDTO)RedisCacheManager.getCache(ORG_AUTH).get(KEY + SUBFIX + orgId);
//    }

//    /**
//     * <B>创建组织机构信息</B>
//     */
//    public void createOrgCache(ImChannelEntity imOrgEntity) {
//        RedisCacheManager.getCache(ORG).put(KEY + SUBFIX + imOrgEntity.getOrgId(), imOrgEntity);
//    }

    /**
     * <B>获取组织机构信息</B>
     */
    public ImChannelEntity getOrgCache(Long orgId) {
        return (ImChannelEntity)RedisCacheManager.getCache(ORG).get(KEY + SUBFIX + orgId);
    }

    /**
     * <B>获取群组脏字缓存</B>
     * @param groupId
     */
    public String getGroupSpecialCache(String groupId) {
        return (String)RedisCacheManager.getCache(GROUP_SPECIAL).get(groupId);
    }

    /**
     * <B>创建组织脏字</B>
     * @param imOrgSpecialEntity
     */
    public void createOrgSpecialCache(ImOrgSpecialEntity imOrgSpecialEntity) {
        RedisCacheManager.getCache(ORG_SPECIAL).put(imOrgSpecialEntity.getOrgId().toString(), imOrgSpecialEntity);
    }

    /**
     * <B>获取组织脏字</B>
     * @param orgId
     * @return
     */
    public ImOrgSpecialDTO getOrgSpecialCache(String orgId) {
        return (ImOrgSpecialDTO)RedisCacheManager.getCache(ORG_SPECIAL).get(orgId);
    }
}
