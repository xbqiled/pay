package com.pay.server.base.database;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.pay.api.json.PayConfig;
import com.pay.api.modules.im.entity.ImMessageEntity;
import com.pay.api.modules.im.entity.ImOfflineMessageEntity;
import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.api.modules.im.service.ImMessageService;
import com.pay.api.modules.im.service.ImOfflineMessageService;
import com.pay.api.modules.im.service.ImUserInfoService;
import com.pay.api.modules.trader.service.TraderReplyConfigService;
import com.pay.api.utils.EntityPackageUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DatabaseApi {

    @Reference(version = "${api.service.version}")
    private ImUserInfoService imUserInfoService;

    @Reference(version = "${api.service.version}")
    private ImOfflineMessageService imOfflineMessageService;

    @Reference(version = "${api.service.version}")
    private ImMessageService imMessageService;

    @Reference(version = "${api.service.version}")
    private TraderReplyConfigService traderReplyConfigService;

    @Autowired
    private RedisTemplate redisTemplate;

    public List<PayConfig> getPayOrgList(Integer userId) {
       return traderReplyConfigService.getReplyConfigList(userId);
    }

    public String getPayOrgStr(Integer userId) {
        List<PayConfig> resultList = traderReplyConfigService.getReplyConfigList(userId);
        if (CollUtil.isNotEmpty(resultList)) {
            return new Gson().toJson(resultList);
        } else {
            return null;
        }
    }

    public ImUserInfoEntity getUserInfo(Long orgId, String userName, String password) {
        ImUserInfoEntity userInfoEntity = imUserInfoService.loginByUserName(orgId, userName);
        return userInfoEntity;
    }

    /**
     * <B>保存token信息</B>
     *
     * @param userInfoEntity
     * @return
     */
    public String saveToken(ImUserInfoEntity userInfoEntity) {
        String UUID = "";
        if (userInfoEntity != null) {
            //删除原来的id
            String str = (String) redisTemplate.opsForValue().get("token_" + userInfoEntity.getOrgId() + "_" + userInfoEntity.getId());
            if (StrUtil.isNotEmpty(str)) {
                redisTemplate.delete("token_" + str);
            }

            //生成新的token信息
            UUID = IdUtil.fastSimpleUUID();
            redisTemplate.opsForValue().set("token_" + userInfoEntity.getOrgId() + "_" + userInfoEntity.getId(), UUID);
            redisTemplate.opsForValue().set("token_" + UUID, userInfoEntity);
        }
        return UUID;
    }

    /**
     * <B>更新用户上线信息</B>
     */
    public void modifyUserOnLine(Integer userId, Integer isOnline) {
        imUserInfoService.modifyOnline(userId, isOnline);
    }

    /**
     * <B>更新客服充值笔数</B>
     */
    public void modifyCsRechargeNum(Integer userId) {
        imUserInfoService.modifyRechargeNum(userId);
    }

    /**
     * <B>保存离线消息</B>
     *
     * @param sendUserId
     * @param receiveUserId
     * @param msgType
     * @param content
     * @param messsageId
     */
    public void saveImOfflineUserMessage(Integer sendUserId, Integer receiveUserId, Integer msgType, String content, String messsageId, Date date) {
        ImOfflineMessageEntity imOfflineMessageEntity = EntityPackageUtils.packageOfflineUserMessage(sendUserId, receiveUserId, msgType, content,  messsageId, date);
        imOfflineMessageService.insert(imOfflineMessageEntity);
    }


    public void saveMessage(Integer sendUserId, Integer receiveUserId, Integer chatType, Integer msgType, String content, Integer groupId, String messsageId,  Date date ) {
        log.info("保存消息sendUserId：" + sendUserId  + "receiveUserId:" + receiveUserId  + "messsageId:" + messsageId );
        ImMessageEntity imMessageEntity = EntityPackageUtils.packageMessage(sendUserId, receiveUserId, msgType, chatType, content, groupId, messsageId, date);
        imMessageService.insert(imMessageEntity);
    }

}
