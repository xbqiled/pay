package com.pay.server.async.event;

import com.pay.server.async.BaseEvent;
import com.pay.server.async.DBEventType;

public class OfflineGroupRequestEvent extends BaseEvent {

	private String sendUserId;

	private String groupId;

	private Integer type;

	private String note;

	public OfflineGroupRequestEvent(DBEventType evtType, String sendUserId, String groupId, Integer type, String note) {
		super(evtType);
		this.sendUserId = sendUserId;
		this.groupId = groupId;
		this.type = type;
		this.note = note;
	}
}