package com.pay.server.async.event;

import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.server.async.DBEventType;
import com.pay.server.utils.IoSession;

public class UserLogoutEvent extends UserEvent {

    public IoSession ioSession;

    public UserLogoutEvent(DBEventType evtType, ImUserInfoEntity userInfo, IoSession ioSession) {
        super(evtType, userInfo);
        this.ioSession = ioSession;
    }

}
