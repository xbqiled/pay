package com.pay.server.async;

public enum DBEventType {

	/** 登录事件  */
	LOGIN,

	/** 登出事件  */
	LOGOUT,

	/**发送离线消息**/
	OFFLINE_MESSAGE,

	/**发送消息**/
	MESSAGE,

	/**发送消息**/
	OFFLINE_GROUP_MESSAGE,

	/**发送请求**/
	OFFLINE_USER_REQUEST,

	/**发送请求**/
	OFFLINE_GROUP_REQUEST,

	/**用户请求**/
	USER_REQUEST,

	/**群组请求**/
	GROUP_REQUEST,

	/**发送通知**/
	OFFLINE_NOTICE;


}
