package com.pay.server.async.event;

import com.pay.server.async.BaseEvent;
import com.pay.server.async.DBEventType;
import java.util.Date;

public class OfflineMessageEvent extends BaseEvent {

	public String messageId;

	public Integer sendUserId;

	public Integer receiverUserId;

	public Integer msgType;

	public String content;

	public Integer width;

	public Integer higth;

	public Integer duration;

	public Long sendTime;

	public Date dateTime;

	public OfflineMessageEvent(DBEventType evtType, String messageId, Integer sendUserId,
							   Integer receiverUserId, Integer msgType, String content, Integer width, Integer higth,
							   Integer duration, Long sendTime, Date dateTime) {
		super(evtType);
		this.messageId = messageId;
		this.sendUserId = sendUserId;

		this.receiverUserId = receiverUserId;
		this.msgType = msgType;
		this.content = content;

		this.width = width;
		this.higth = higth;
		this.duration = duration;

		this.sendTime = sendTime;
		this.dateTime = dateTime;
	}

}
