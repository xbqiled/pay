package com.pay.server.async.event;

import com.pay.server.async.BaseEvent;
import com.pay.server.async.DBEventType;
import java.util.Date;
import java.util.Set;

public class OfflineGroupMessageEvent extends BaseEvent {

	public String messageId;

	public Integer sendUserId;

	public Set<Integer> receiverUserList;

	public Integer msgType;

	public Integer chatType;

	public Integer groupId;

	public String content;

	public Integer width;

	public Integer higth;

	public Integer duration;

	public String sendTime;

	public Date dateTime;

	public OfflineGroupMessageEvent(DBEventType evtType, String messageId, Integer sendUserId,
									Set<Integer> receiverUserList, Integer groupId, Integer msgType, Integer chatType,
									String content, Integer width, Integer higth, Integer duration, String sendTime, Date dateTime) {
		super(evtType);
		this.messageId = messageId;

		this.sendUserId = sendUserId;
		this.receiverUserList = receiverUserList;
		this.msgType = msgType;

		this.chatType = chatType;
		this.groupId = groupId;
		this.content = content;

		this.width = width;
		this.higth = higth;
		this.duration = duration;

		this.sendTime = sendTime;
		this.dateTime = dateTime;
	}

}
