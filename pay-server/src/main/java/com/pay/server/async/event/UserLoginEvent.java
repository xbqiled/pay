package com.pay.server.async.event;

import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.server.async.DBEventType;

public class UserLoginEvent extends UserEvent {

	public UserLoginEvent(DBEventType evtType, ImUserInfoEntity userInfo) {
		super(evtType, userInfo);
	}

}
