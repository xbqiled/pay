package com.pay.server.async.event;

import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.server.async.BaseEvent;
import com.pay.server.async.DBEventType;

/**
 * 用户事件抽象类
 */
public abstract class UserEvent extends BaseEvent {

	/** 用户id */
	private final ImUserInfoEntity imUserInfoEntity;

	public UserEvent(DBEventType evtType, ImUserInfoEntity imUserInfoEntity) {
		super(evtType);
		this.imUserInfoEntity = imUserInfoEntity;
	}

	public ImUserInfoEntity getUser() {
		return this.imUserInfoEntity;
	}
}
