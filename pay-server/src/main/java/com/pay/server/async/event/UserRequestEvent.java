package com.pay.server.async.event;

import com.pay.server.async.BaseEvent;
import com.pay.server.async.DBEventType;

public class UserRequestEvent extends BaseEvent {

	public Integer sendUserId;

	public Integer toUserId;

	public Integer isReq;

	public Integer type;

	public Integer resCode;

	public String note;

	public String userRemarks;

	public UserRequestEvent(DBEventType evtType, Integer sendUserId, Integer toUserId, Integer isReq, Integer resCode, Integer type, String note, String userRemarks) {
		super(evtType);
		this.sendUserId = sendUserId;

		this.toUserId = toUserId;
		this.isReq = isReq;
		this.resCode = resCode;

		this.type = type;
		this.note = note;
		this.userRemarks = userRemarks;
	}
}
