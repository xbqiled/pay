package com.pay.server.async.annotation;



import com.pay.server.async.DBEventType;
import java.lang.annotation.*;

/**
 * 事件处理者
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {
	
	/** 绑定的事件类型列表 */
	public DBEventType[] value();
	
}
