package com.pay.server;


import com.pay.common.im.bootstrap.ServerNode;
import com.pay.server.config.ImConfiguration;
import com.pay.server.server.ImWSServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lion
 * @date 2018/11/15 9:20:19
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableAsync
public class ServerApplication implements CommandLineRunner {

    //注入配置信息
    @Autowired
    private ImConfiguration configuration ;

    //记录服务器节点信息
    private static List<ServerNode> serverNodes = new ArrayList<>();

    /**
     * <B>启动IM服务器</B>
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ServerApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        final ServerApplication server = new ServerApplication();
        server.start();
        //上来就启动多个服务器节点
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                server.stop();
            }
        });
    }

    /**
     * <B>启动所有的服务器</B>
     * @throws Exception
     */
    public void start() throws Exception {
        //获取本机ip地址
        String addr = InetAddress.getLocalHost().getHostAddress();
        ImWSServer ws = new ImWSServer();
        ws.init();
        ws.start();
    }

    /**
     * <B>停止所有的服务器</B>
     */
    public void stop() {
        for (ServerNode node : serverNodes) {
            try {
                node.shutDown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
