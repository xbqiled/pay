package com.pay.server.filter;

import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.service.codec.ws.WebSocketDecoder;
import com.pay.common.im.service.codec.ws.WebSocketEncoder;
import com.pay.server.handler.IMHeartBeatTimeOutHanlder;
import com.pay.server.handler.ImHeartBeatServerHandler;
import com.pay.server.handler.ImServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;


public class ImWSServerFilter extends ChannelInitializer<SocketChannel> {


    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        // HTTP请求的解码和编码
        pipeline.addLast(new HttpServerCodec());
        // 把多个消息转换为一个单一的FullHttpRequest或是FullHttpResponse，
        // 原因是HTTP解码器会在每个HTTP消息中生成多个消息对象HttpRequest/HttpResponse,HttpContent,LastHttpContent
        pipeline.addLast(new HttpObjectAggregator(65536));
        // 主要用于处理大数据流，比如一个1G大小的文件如果你直接传输肯定会撑暴jvm内存的; 增加之后就不用考虑这个问题了
        pipeline.addLast(new ChunkedWriteHandler());
        // WebSocket数据压缩
        pipeline.addLast(new WebSocketServerCompressionHandler());
        // 协议包长度限制
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws", null, true));
        // 协议包解码
        pipeline.addLast(new WebSocketDecoder());
        // 协议包编码
        pipeline.addLast(new WebSocketEncoder());
        // 协议包解码时指定Protobuf字节数实例化为CommonProtocol类型
        pipeline.addLast(new ProtobufDecoder(MessageProto.Message.getDefaultInstance()));
        //15 秒客户端没有向服务器发送心跳则关闭连接
        /*pipeline.addLast(new IdleStateHandler(45, 45, 45, TimeUnit.SECONDS));
        pipeline.addLast(new ImHeartBeatServerHandler());*/
        pipeline.addLast(new IMHeartBeatTimeOutHanlder(45));
        // websocket定义了传递数据的6中frame类型
        pipeline.addLast(ImServerHandler.getInstance());
    }
}
