package com.pay.server.config;



import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;


@Component
@Data
@PropertySource({ "classpath:imconfig.properties" })
public class ImConfiguration implements Serializable {

    /**
     * IP地址
     */
    @Value("${im.config.bindIp}")
    protected String bindIp = "";

    /**
     * 监听TPC端口
     */
    @Value("${im.config.tpcPort}")
    protected Integer tpcPort = 0;

    /**
     * 监听WS端口
     */
    @Value("${im.config.wsPort}")
    protected Integer wsPort = 0;

    /**
     * 节点个数
     */
    @Value("${im.config.noteSize}")
    protected String noteSize = "";

    /**
     * 心跳包发送时长
     */
    @Value("${im.config.heartbeatTimeout}")
    protected long heartbeatTimeout = 0;

    /**
     * 是否开启持久化;
     */
    @Value("${im.config.isStore}")
    protected String isStore = "off";

    /**
     * 是否开启集群;
     */
    @Value("${im.config.isCluster}")
    protected String isCluster = "off";

    /**
     *  默认的接收数据的buffer size
     */
    @Value("${im.config.readBufferSize}")
    protected long readBufferSize = 1024 * 2;
}
