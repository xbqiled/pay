package com.pay.server.handler;

import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.command.CommandEnum;
import com.pay.server.base.SpringContext;
import com.pay.server.logic.logout.logoutService;
import com.pay.server.utils.IMHandler;
import com.pay.server.utils.IoSession;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class ImLogoutHandler extends IMHandler {

    public ImLogoutHandler(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) {
        super(msg, session, ctx);
    }

    @Override
    public void excute() throws Exception {
        //首先判断用户登录参数是否满足
        if (this._msg.getCommand() != CommandEnum.Command.COMMAND_LOGOUT_REQ.getValue()) {
            return;
        }

        //验证用户退出信息
        SpringContext.getBean(logoutService.class).userLogout(this._ctx.channel(), this._msg);
    }
}
