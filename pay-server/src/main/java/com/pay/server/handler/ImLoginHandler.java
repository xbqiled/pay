package com.pay.server.handler;


import cn.hutool.core.util.StrUtil;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.command.CommandEnum;
import com.pay.server.base.SpringContext;
import com.pay.server.logic.login.LoginService;
import com.pay.server.utils.IMHandler;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.ProToBufBuild;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;


/**
 * <B>处理登录请求</B>
 */
@Slf4j
public class ImLoginHandler extends IMHandler {

    public ImLoginHandler(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) {
        super(msg, session, ctx);
    }

    @Override
    public void excute() throws Exception {
        //首先判断用户登录参数是否满足
        if (this._msg.getCommand() != CommandEnum.Command.COMMAND_LOGIN_REQ.getValue() || this._msg.getLoginReq() == null ||
                StrUtil.isEmpty(_msg.getLoginReq().getOrgId()) || StrUtil.isEmpty(_msg.getLoginReq().getUserName())  || StrUtil.isEmpty(_msg.getLoginReq().getPassword())) {
            _ctx.writeAndFlush(ProToBufBuild.buildLoginResMessage(ImStatus.ERROR_PARAMETER));
            return;
        }

        //验证用户登录信息
        SpringContext.getBean(LoginService.class).validateLogin(this._ctx.channel(), this._msg);
    }
}
