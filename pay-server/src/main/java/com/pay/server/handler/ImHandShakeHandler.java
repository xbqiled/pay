package com.pay.server.handler;


import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.SessionCloseReason;
import com.pay.common.im.packets.command.CommandEnum;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IMHandler;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.ProToBufBuild;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

/**
 * <B>处理群组等请求</B>
 */
@Slf4j
public class ImHandShakeHandler extends IMHandler {

    public ImHandShakeHandler(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) {
        super(msg, session, ctx);
    }

    @Override
    public void excute() throws Exception {
        if (this._msg.getCommand() == CommandEnum.Command.COMMAND_HANDSHAKE_REQ.getValue()) {
            //判断用户是否存在
            IoSession session = ChannelUtils.getSessionBy(this._ctx.channel());
            if (session != null && session.getUser() != null) {
                _ctx.writeAndFlush(ProToBufBuild.buildHandshakeRes(ImStatus.HEARTBEAT_OK));
            } else {
                _ctx.writeAndFlush(ProToBufBuild.buildHandshakeRes(ImStatus.HANDSHAKE_ERROR));
                session.close(SessionCloseReason.OVER_TIME);
            }
            return;
        }
    }


}
