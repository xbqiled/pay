package com.pay.server.handler.manager;


import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.command.CommandEnum;
import com.pay.server.handler.*;
import com.pay.server.utils.IMHandler;
import com.pay.server.utils.IoSession;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


@Slf4j
public class HandlerManager {

    private static final Map<Integer, Constructor<? extends IMHandler>> _handlers = new HashMap<>();

    private static HandlerManager INSTANCE = null;

    public static HandlerManager getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new HandlerManager();
        }
        return INSTANCE;
    }

    public void exec(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) {
        if (msg == null) return;
        try {
            IMHandler imHandler = getHandler(msg, session, ctx);
            Method m = imHandler.getClass().getMethod("excute");
            m.invoke(imHandler);
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static void register(int cmd, Class<? extends IMHandler> handler) {
        try {
            Constructor<? extends IMHandler> constructor = handler.getConstructor(MessageProto.Message.class, IoSession.class, ChannelHandlerContext.class);
            _handlers.put(cmd, constructor);
        }  catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static IMHandler getHandler(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<? extends IMHandler> constructor = _handlers.get(msg.getCommand());
        if(constructor == null) {
            log.error("handler not exist, Message Number: {}", msg.getCommand());
            return null;
        }
        return constructor.newInstance(msg, session, ctx);
    }

    /**
     * <B>注册处理器</B>
     */
    public static void initHandlers() {
        //登录处理器
        HandlerManager.register(CommandEnum.Command.COMMAND_LOGIN_REQ.getValue(), ImLoginHandler.class);
        //消息处理器
        HandlerManager.register(CommandEnum.Command.COMMAND_CHAT_REQ.getValue(), ImMessageHandler.class);
        //握手
        HandlerManager.register(CommandEnum.Command.COMMAND_HANDSHAKE_REQ.getValue(), ImHandShakeHandler.class);
        //心跳
        HandlerManager.register(CommandEnum.Command.COMMAND_HEARTBEAT_REQ.getValue(), ImHeartBeatHandler.class);
        //登出处理器
        HandlerManager.register(CommandEnum.Command.COMMAND_LOGOUT_REQ.getValue(), ImLogoutHandler.class);
    }
}
