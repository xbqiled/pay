package com.pay.server.handler;


import com.pay.common.im.constant.ImConst;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.exception.ImException;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.SessionCloseReason;
import com.pay.server.base.SpringContext;
import com.pay.server.dispatch.CmdTask;
import com.pay.server.logic.user.UserService;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IoSession;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

import static com.pay.common.im.packets.command.CommandEnum.Command.COMMAND_HEARTBEAT_REQ;


@Slf4j
@ChannelHandler.Sharable
public class ImServerHandler extends SimpleChannelInboundHandler<MessageProto.Message> {

    private static ImServerHandler INSTANCE = null;

    //单例模式
    public static ImServerHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ImServerHandler();
        }
        return INSTANCE;
    }

    /**
     * <B>处理断线重连</B>
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        //可能出现业务判断离线后再次触发 channelInactive
        try {
            log.warn("触发 channelInactive 掉线重连![{}]", ctx.channel().id());
            //判断是否有用户信息
            IoSession session = ChannelUtils.getSessionBy(ctx.channel());
            if (session != null && session.getUser() != null && !session.isSignOuted()) {
                //加入到在线列表
                SpringContext.getBean(UserService.class).userLogout(ctx.channel(), SessionCloseReason.NORMAL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <B>刚开始的时候添加session</B>
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //先判断是否是有这个
        IoSession session = ChannelUtils.getSessionBy(ctx.channel());
        if (session == null) {
            if (!ChannelUtils.addChannelSession(ctx.channel(), new IoSession(ctx.channel()))) {
                ctx.channel().close();
                log.error("Duplicate session,IP=[{}]", ChannelUtils.getIp(ctx.channel()));
            }
        }
    }

    /**
     * <B>逻辑处理</B>
     *
     * @param ctx
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageProto.Message message) throws Exception {
        final Channel channel = ctx.channel();
        IoSession session = ChannelUtils.getSessionBy(channel);
        //if (message.getCommand() != COMMAND_HEARTBEAT_REQ.getValue())
        log.info("================收到数据包==================> from:[{}], msg:[{}]", session.getUser() == null ? null : session.getUser().getId(),
                message.toString());
        //判断是否带了命令号
        if (message.getCommand() == 0) {
            throw new ImException(ImStatus.ERROR_COMMAND.getText());
        }
        //判断协议是否正确,判断seq是否正确
        if (message.getVersion() != ImConst.VERSION) {
            throw new ImException(ImStatus.ERROR_VERSION.getText());
        }

        //新开一个线程去处理
        CmdTask cmdTask = CmdTask.valueOf(session.getDispatchKey(), message, session, ctx);
        SpringContext.getMessageDispatcher().addMessageTask(cmdTask);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error("发生异常，关闭对应的连接", cause);
        Channel channel = ctx.channel();
        if (cause instanceof IOException && channel.isActive()) {
            log.error("simpleclient" + channel.remoteAddress() + "异常");
            SpringContext.getBean(UserService.class).userLogout(channel, SessionCloseReason.NORMAL);
        }
    }
}
