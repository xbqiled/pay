package com.pay.server.handler;

import com.pay.api.modules.im.entity.ImUserInfoEntity;
import com.pay.common.core.constant.EnumConstant;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.SessionCloseReason;
import com.pay.common.im.packets.command.CommandEnum;
import com.pay.server.base.SpringContext;
import com.pay.server.base.database.DatabaseApi;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IMHandler;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.ProToBufBuild;
import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.CompletableFuture;


public class ImHeartBeatHandler extends IMHandler {

    public ImHeartBeatHandler(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) {
        super(msg, session, ctx);
    }

    @Override
    public void excute() throws Exception {
        if (this._msg.getCommand() == CommandEnum.Command.COMMAND_HEARTBEAT_REQ.getValue()) {
            /*IoSession session = ChannelUtils.getSessionBy(this._ctx.channel());
            if (session == null || session.getUser() == null ||  session.getUser().getId() == null) {
                session.close(SessionCloseReason.OVER_TIME);
                return;
            }*/

            _ctx.writeAndFlush(ProToBufBuild.buildHeartBeatRes());
            IoSession session = ChannelUtils.getSessionBy(_ctx.channel());
            if (session != null && session.getUser() != null && !session.isSignOuted()) {
                ImUserInfoEntity user = session.getUser();
                if (user.getId() != null) {
                    CompletableFuture.runAsync(()->SpringContext.getBean(DatabaseApi.class).modifyCsRechargeNum(user.getId()));
                }
            }
        }
    }
}
