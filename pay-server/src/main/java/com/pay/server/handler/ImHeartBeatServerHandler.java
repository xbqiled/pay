package com.pay.server.handler;


import com.pay.common.im.packets.SessionCloseReason;
import com.pay.server.base.SpringContext;
import com.pay.server.logic.user.UserService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * <B>处理心跳机制</B>
 */
@Slf4j
public class ImHeartBeatServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

        if (evt instanceof IdleStateEvent) {
            IdleState state = ((IdleStateEvent) evt).state();
            Channel channel = ctx.channel();
            if (state == IdleState.ALL_IDLE) {
                log.info("读写空闲， 已经45秒没有任何消息了");
            } else if (state == IdleState.READER_IDLE) {
                log.info("读空闲， 已经45秒没有收到消息了,自动断开");
                SpringContext.getBean(UserService.class).userLogout(channel, SessionCloseReason.OVER_TIME);
            } else if  (state == IdleState.WRITER_IDLE) {
                log.info("写空闲， 已经45秒没有写消息了");
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }
}

