package com.pay.server.handler;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.pay.common.im.entity.ImStatus;
import com.pay.common.im.packets.MessageProto;
import com.pay.common.im.packets.command.CommandEnum;
import com.pay.server.base.SpringContext;
import com.pay.server.logic.message.MessageService;
import com.pay.server.utils.IMHandler;
import com.pay.server.utils.IoSession;
import com.pay.server.utils.ProToBufBuild;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

/**
 * <B>处理消息等请求</B>
 */
@Slf4j
public class ImMessageHandler extends IMHandler {

    public ImMessageHandler(MessageProto.Message msg, IoSession session, ChannelHandlerContext ctx) {
        super(msg, session, ctx);
    }

    @Override
    public void excute() throws Exception {

        if (this._msg.getCommand() != CommandEnum.Command.COMMAND_CHAT_REQ.getValue() || this._msg.getMessageReq() == null
                || StrUtil.isEmpty(_msg.getMessageReq().getMessageId()) || ObjectUtil.isEmpty(_msg.getMessageReq().getFromUser())
                || ObjectUtil.isEmpty(_msg.getMessageReq().getToUser()) || ObjectUtil.isEmpty(_msg.getMessageReq().getContent())
                || ObjectUtil.isEmpty(_msg.getMessageReq().getMsgType())) {
            _ctx.writeAndFlush(ProToBufBuild.buildSendMessageRes(ImStatus.ERROR_PARAMETER));
            return;
        }

        SpringContext.getBean(MessageService.class).sendMessageToUser(this._ctx.channel(), this._msg);
    }
}