package com.pay.server.handler;

import com.pay.common.im.packets.SessionCloseReason;
import com.pay.server.base.SpringContext;
import com.pay.server.logic.user.UserService;
import com.pay.server.utils.ChannelUtils;
import com.pay.server.utils.IoSession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.handler.timeout.ReadTimeoutException;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-06-22.
 */
@Slf4j
public class IMHeartBeatTimeOutHanlder extends IdleStateHandler {
    private boolean closed;

    public IMHeartBeatTimeOutHanlder(int timeoutSeconds) {
        this(timeoutSeconds, TimeUnit.SECONDS);
    }

    public IMHeartBeatTimeOutHanlder(long timeout, TimeUnit unit) {
        super(timeout, 0, 0, unit);
    }

    @Override
    protected final void channelIdle(ChannelHandlerContext ctx, IdleStateEvent evt) throws Exception {
        assert evt.state() == IdleState.READER_IDLE;
        readTimedOut(ctx);
    }

    /**
     * Is called when a read timeout was detected.
     */
    protected void readTimedOut(ChannelHandlerContext ctx) throws Exception {
        if (closed)
            return;
        log.info(">>>>>>>>> 读空闲， 已经45秒没有收到消息了,自动断开");
        IoSession session = ChannelUtils.getSessionBy(ctx.channel());
        SpringContext.getBean(UserService.class).userLogout(ctx.channel(), SessionCloseReason.OVER_TIME);
        ctx.fireExceptionCaught(ReadTimeoutException.INSTANCE);
        ctx.close();
        closed = true;
    }
}
