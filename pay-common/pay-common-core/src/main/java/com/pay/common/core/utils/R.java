package com.pay.common.core.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 * @author lion
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public R() {
		put("code", 0);
		put("msg", "success");
	}
	
	public static R tokenError() {
		return error(500, "token信息错误");
	}

	public static R tokenError(String msg) {
		return error(500, msg);
	}

	public static R parmError() {
		return error(600, "请求参数错误");
	}

	public static R parmError(String msg) {
		return error(600, msg);
	}

	public static R error(String msg) {
		return error(1, msg);
	}
	
	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}
	
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}
	
	public static R ok() {
		return new R();
	}

	@Override
	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
