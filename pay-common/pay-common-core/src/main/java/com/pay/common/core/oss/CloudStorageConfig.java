package com.pay.common.core.oss;

import lombok.Data;

import java.io.Serializable;


@Data
public class CloudStorageConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    //类型 1：七牛  2：阿里云
    private Integer type;

    //七牛绑定的域名
    private String qiniuDomain;

    //七牛路径前缀
    private String qiniuPrefix;

    //七牛ACCESS_KEY
    private String qiniuAccessKey;

    //七牛SECRET_KEY
    private String qiniuSecretKey;

    //七牛存储空间名
    private String qiniuBucketName;

    //阿里云绑定的域名
    private String aliyunDomain;

    //阿里云路径前缀
    private String aliyunPrefix;

    //阿里云EndPoint
    private String aliyunEndPoint;

    //阿里云AccessKeyId
    private String aliyunAccessKeyId;

    //阿里云AccessKeySecret
    private String aliyunAccessKeySecret;

    //阿里云BucketName
    private String aliyunBucketName;

    private Integer status;
    private Integer isActivate;
    private String remark;

}
