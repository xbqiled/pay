package com.pay.common.core.utils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.List;
import java.util.Properties;

public class MailUtils {

    private String host = ""; // smtp服务器
    private String port = ""; // smtp端口
    private String affix = null; // 附件地址
    private String affixName = ""; // 附件名称
    private String fromUser = ""; // 发件人邮箱地址
    private String user = ""; // 用户名
    private String pwd = ""; // 密码
    private String subject = ""; // 邮件标题
    private String content = ""; // 邮件正文
    private InternetAddress[] addresses = null; // 收件人地址

    public void setAddress(List<String> tos) throws AddressException {
        this.addresses = new InternetAddress[tos.size()];
        for (int i = 0; i <tos.size() ; i++) {
            this.addresses[i] = (new InternetAddress(tos.get(i)));
        }
    }

    public void setAffix(String affix, String affixName) {
        this.affix = affix;
        this.affixName = affixName;
    }

    public void setConfig(String fromUser,String host, String port, String user, String pwd) {
        this.fromUser = fromUser;
        this.host = host;
        this.port = port;
        this.user = user;
        this.pwd = pwd;
    }

    public boolean send(String subject,String content) throws Exception {
        boolean flag = true;

        Properties props = new Properties();

        // 设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        // 需要经过授权，也就是有户名和密码的校验，这样才能通过验证（一定要有这一条）
        props.put("mail.smtp.auth", "true");

        // 用刚刚设置好的props对象构建一个session
        Session session = Session.getDefaultInstance(props);

        // 有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使
        // 用（你可以在控制台（console)上看到发送邮件的过程）
        session.setDebug(true);

        // 用session为参数定义消息对象
        MimeMessage message = new MimeMessage(session);
        try {
            // 加载发件人地址
            message.setFrom(new InternetAddress(fromUser,user,"UTF-8"));

            // 加载收件人地址
            message.addRecipients(Message.RecipientType.TO, addresses);
            // 加载标题
            message.setSubject(subject);

            // 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
            Multipart multipart = new MimeMultipart();

            // 设置邮件的文本内容
            BodyPart contentPart = new MimeBodyPart();
            contentPart.setContent(content,"text/html;charset=utf-8");
            multipart.addBodyPart(contentPart);
            if(affix != null){
                // 添加附件
                BodyPart messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(affix);
                // 添加附件的内容
                messageBodyPart.setDataHandler(new DataHandler(source));
                // 添加附件的标题
                // 这里很重要，通过下面的Base64编码的转换可以保证你的中文附件标题名在发送时不会变成乱码
                sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
                messageBodyPart.setFileName("=?GBK?B?"
                        + enc.encode(affixName.getBytes()) + "?=");
                multipart.addBodyPart(messageBodyPart);
            }

            // 将multipart对象放到message中
            message.setContent(multipart);
            // 保存邮件
            message.saveChanges();
            // 发送邮件
            Transport transport = session.getTransport("smtp");
            // 连接服务器的邮箱
            transport.connect(host, fromUser, pwd);
            // 把邮件发送出去
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }


}
