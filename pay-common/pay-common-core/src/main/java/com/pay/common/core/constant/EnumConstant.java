package com.pay.common.core.constant;


public class EnumConstant {

    public enum USER_STATUS {
        USER_NORMAL(0, "正常"),

        USER_INVALID(1, "失效"),

        USRE_FROZEN(2, "冻结");

        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        USER_STATUS(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum ONLINE_TYPE {
        TYPE_UNKNOWN(0, "未知"),

        TYPE_ONLINE(1, "在线"),

        TYPE_OFFLINE(2, "离线");

        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        ONLINE_TYPE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum RESULT_CODE {
        /**
         * 目录
         */
        CODE_OK(0, "成功"),

        CODE_REEOR(1, "错误"),

        CODE_FAIL(2, "失敗"),

        CODE_VERIFYERROR(3, "验证错误"),

        CODE_VERIFYFAIL(4, "验证失败");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        RESULT_CODE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


}



