package com.pay.common.core.constant;

public interface CommonConstant {

    /**
     * 删除
     */
    public String STATUS_DEL = "1";
    /**
     * 正常
     */
    public String STATUS_NORMAL = "0";

    /**
     * 锁定
     */
    public String STATUS_LOCK = "9";

    /**
     * 菜单
     */
    public String MENU = "0";

    /**
     * 编码
     */
    public String UTF8 = "UTF-8";

    /**
     * JSON 资源
     */
    public String CONTENT_TYPE = "application/json; charset=utf-8";

    /**
     * 路由存放
     */
    public String ROUTE_KEY = "gateway_route_key";

    /**
     * spring boot admin 事件key
     */
    public String EVENT_KEY = "event_key";

    /**
     * 验证码前缀
     */
    public String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY_";

    /**
     * 成功标记
     */
    public Integer SUCCESS=0;
    /**
     * 失败标记
     */
    public Integer FAIL=1;
}
