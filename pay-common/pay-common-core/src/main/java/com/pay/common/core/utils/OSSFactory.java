package com.pay.common.core.utils;


import com.pay.common.core.oss.AliyunCloudStorageService;
import com.pay.common.core.oss.CloudStorageConfig;
import com.pay.common.core.oss.CloudStorageService;
import com.pay.common.core.oss.QiniuCloudStorageService;

/**
 * 文件上传Factory
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2017-03-26 10:18
 */
public final class OSSFactory {

    public static CloudStorageService build(CloudStorageConfig config){
        //获取云存储配置信息
        if(config.getType() == 1){
            return new QiniuCloudStorageService(config);
        }else if(config.getType() == 2){
            return new AliyunCloudStorageService(config);
        }
        return null;
    }

}
