package com.pay.common.core.utils;

public class AuthUtils {

    /**
     * <B>判断是当前用户是否有权限</B>
     * @param orgId
     * @param userId
     * @param platformType
     * @param key
     * @param signIn
     * @return
     */
   public static Boolean isHaveAuth(Long orgId, String userId, Integer platformType, String key, String signIn) {
       StringBuffer sb = new StringBuffer();
       sb.append("orgId=" +  orgId);

       sb.append("&userId=" +  userId);
       sb.append("&platformType=" +  platformType);
       sb.append("&key=" +  key);

       String md5Sign = MD5Util.encryption(sb.toString());
       return md5Sign == signIn ? Boolean.TRUE : Boolean.FALSE;
   }

}
