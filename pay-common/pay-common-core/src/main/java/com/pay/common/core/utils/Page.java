package com.pay.common.core.utils;

import lombok.Data;

import java.io.Serializable;


@Data
public class Page implements Serializable {

      private Integer pageSize;
      private Integer pageIndex;
      private Integer timeOrder;

}
