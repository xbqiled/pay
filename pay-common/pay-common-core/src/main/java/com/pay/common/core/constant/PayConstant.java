package com.pay.common.core.constant;

/**
 * 常用静态常量
 * @author Lion
 * @date 2018-12-26
 */
public class PayConstant {

    public static final String PAY_CONFIG = "https://xinbotrader.oss-cn-hongkong.aliyuncs.com/im/hb1.png";

    public static final String RESET_PASS = "重置密码";

    public static final String RESET_MAIL = "重置邮箱";

    public static final String SEQ_USERNAME = "user.user_name";

    public static final String IM_USER_URL = "http://front.xinboyinshang.com/pay-front/chatApi/chat";

    public static final String NULL_VALUE = null;

    public static final String DEFAULT_USER_PASSWORD = "123456";

    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";

    public static final String DEFAULT_USER_AVATAR = "https://xinboim.oss-cn-hangzhou.aliyuncs.com/images/default_user_avatar.png";

    public static final String DEFAULT_GROUP_AVATAR = "https://xinboim.oss-cn-hangzhou.aliyuncs.com/images/default_group_avatar.jpg";

    public static final String DEFAULT_SIGN = "信博棋牌是最好玩的棋牌游戏";

//    public static final String GAME_ADD_POINT_URL = "http://27.50.48.40:9999/api/itf/org/addPoint";

    public static final String GAME_ADD_POINT_URL = "http://localhost:9999/api/itf/org/addPoint";

    public static final String IM_SUFFIX = "IM_";

    public static final String AND_SUFFIX = "_";
    public static final String SPLIT_SUFFIX = ",";
    public static final String SPACE_SUFFIX = " ";
    public static final String COLON_SUFFIX = ":";
    public static final String FEN_SUFFIX = ";";

    /**
     * 常用接口
     */
    public static class Url{
        public static final String SM_MS_URL = "https://sm.ms/api/upload";
    }

    public static Integer ERROR_CODE_ONE = 1;

    public static Integer ERROR_CODE_TWO = 2;

    public static Integer ERROR_CODE_THREE = 3;

    public static Integer ERROR_CODE_FOUR = 4;

    public static Integer ERROR_CODE_FIVE = 5;
}
