package com.pay.common.core.utils;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.RandomStringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;


/**
 * <B>加密生成对应的字符串</B>
 */
public class Sha256Util {

    /**  加密算法 */
    public final static String hashAlgorithmName = "SHA-256";
    /**  循环次数 */
    public final static int hashRounds = 10;

    public static String sha256EncryptionStr(String password, String salt) {
        try {
            if (StrUtil.isEmpty(salt)) {
                salt = RandomStringUtils.randomAlphanumeric(20);
            }
            return  hashSHA(password, salt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }


    public static Map<String, String> sha256EncryptionMap(String password, String salt) {
        try {
            if (StrUtil.isEmpty(salt)) {
                salt = RandomStringUtils.randomAlphanumeric(20);
            }

            String value =  hashSHA(password, salt);
            Map map =  new HashMap<String, String>();
            map.put("salt",  salt);

            map.put("value",  value);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }


    private static String hashSHA(String pass, String salt){
        String hash = "";
        StringBuilder sb = new StringBuilder();
        MessageDigest md = null;

        try {
            md = MessageDigest.getInstance(hashAlgorithmName);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        for(int j = 0; j < hashRounds; j++){
            md.update(hash.concat(pass).concat(salt).getBytes());
            byte[] byteData = md.digest();

            for(int i = 0; i < byteData.length; i++){
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            hash = sb.toString();
            sb.setLength(0);
        }
        return hash;
    }


    public static void main(String [] args) {
        Map<String, String> result = Sha256Util.sha256EncryptionMap("123456", null);
        System.out.println(result.get("salt"));
        System.out.println(result.get("value"));
    }
}
