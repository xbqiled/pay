package com.pay.common.core.constant;

/**
 * @author kafe
 * @date 2019/10/26 14:03
 */
public class AdminConstant {
    //超级管理员
    public static int SYS_USER_LEVEL_1  = 1;

    public static final String RESET_PASS = "重置密码";

    public static final String RESET_MAIL = "重置邮箱";

    /**
     * 用于IP定位转换
     */
    public static final String REGION = "内网IP|内网IP";

    /**
     * 常用接口
     */
    public static class Url{
        public static final String SM_MS_URL = "https://sm.ms/api/upload";
    }
}
