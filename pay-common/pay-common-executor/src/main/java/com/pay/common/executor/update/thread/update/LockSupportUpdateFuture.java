package com.pay.common.executor.update.thread.update;


import com.pay.common.executor.future.AbstractFuture;
import com.pay.common.executor.future.ITaskFuture;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;

/**
 */
public class LockSupportUpdateFuture extends AbstractFuture<IUpdate> {

    private DispatchThread dispatchThread;

    public LockSupportUpdateFuture(DispatchThread dispatchThread) {
        this.dispatchThread = dispatchThread;
    }

    @Override
    public ITaskFuture<IUpdate> setSuccess(Object result) {
        return super.setSuccess(result);
    }

    @Override
    public ITaskFuture<IUpdate> setFailure(Throwable cause) {
        return super.setFailure(cause);
    }

    public DispatchThread getDispatchThread() {
        return dispatchThread;
    }

    public void setDispatchThread(DispatchThread dispatchThread) {
        this.dispatchThread = dispatchThread;
    }
}