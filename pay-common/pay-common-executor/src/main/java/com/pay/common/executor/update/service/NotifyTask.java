package com.pay.common.executor.update.service;

import java.util.TimerTask;

/**
 * 任务通知
 */
public class NotifyTask extends TimerTask {

    private final UpdateService updateService;

    public NotifyTask(UpdateService updateService) {
        this.updateService = updateService;
    }

    @Override
    public void run() {
        updateService.notifyRun();
    }
}
