package com.pay.common.executor.event;


import com.pay.common.executor.event.common.IEvent;
import java.io.Serializable;

/**
 */
public abstract  class AbstractEvent<ID extends Serializable> implements IEvent {

    private EventType eventType;
    private EventParam[] eventParamps;
    private ID id;

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public EventParam[] getParams() {
        return this.eventParamps;
    }

    public void setParams(EventParam... eventParams) {
        this.eventParamps = eventParams;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
