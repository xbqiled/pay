package com.pay.common.executor.event.impl.listener;



import com.pay.common.executor.event.AbstractEventListener;
import com.pay.common.executor.utils.Constants;

/**
 */
public class ReadyCreateEventListener extends AbstractEventListener {
    @Override
    public void initEventType() {
        register(Constants.EventTypeConstans.readyCreateEventType);
    }
}
