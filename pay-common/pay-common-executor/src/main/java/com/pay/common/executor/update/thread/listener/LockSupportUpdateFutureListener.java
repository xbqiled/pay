package com.pay.common.executor.update.thread.listener;



import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.impl.event.UpdateEvent;
import com.pay.common.executor.future.ITaskFuture;
import com.pay.common.executor.future.ITaskFutureListener;
import com.pay.common.executor.update.cache.UpdateEventCacheService;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.thread.update.LockSupportUpdateFuture;
import com.pay.common.executor.utils.Constants;
import com.pay.common.executor.utils.Loggers;

import java.util.concurrent.locks.LockSupport;

/**
 */
public class LockSupportUpdateFutureListener implements ITaskFutureListener {

    @Override
    public void operationComplete(ITaskFuture iTaskFuture) throws Exception {
        if(Loggers.gameExecutorUtil.isDebugEnabled()){
            IUpdate iUpdate = (IUpdate) iTaskFuture.get();
            Loggers.gameExecutorUtil.debug("update complete event id " + iUpdate.getUpdateId());
        }
        LockSupportUpdateFuture lockSupportUpdateFuture = (LockSupportUpdateFuture) iTaskFuture;
        IUpdate iUpdate = (IUpdate) iTaskFuture.get();
        //事件总线增加更新完成通知
        EventParam<IUpdate> params = new EventParam<IUpdate>(iUpdate);
//        UpdateEvent event = new UpdateEvent(Constants.EventTypeConstans.updateEventType, iUpdate.getUpdateId(), params);
        UpdateEvent updateEvent = UpdateEventCacheService.createUpdateEvent();
        updateEvent.setEventType(Constants.EventTypeConstans.updateEventType);
        updateEvent.setId(iUpdate.getUpdateId());
        updateEvent.setParams(params);
        updateEvent.setUpdateAliveFlag(iUpdate.isActive());
        lockSupportUpdateFuture.getDispatchThread().addUpdateEvent(updateEvent);
        //解锁
        LockSupport.unpark(lockSupportUpdateFuture.getDispatchThread());
    }
}
