package com.pay.common.executor.event.constant;

/**
 * 事件类型
 */
public enum EventTypeEnum {
    /**
     * 创建
     */
    CREATE,

    /**
     * 更新
     */
    UPDATE,

    /**
     * 完成
     */
    FINISH,


    /**
     * 准备创建
     */
    REDAY_CREATE,

    /**
     * 准备结束
     */
    REDAY_FINISH,

    /**
     * 结束
     */
    FINISHED,
    ;
}
