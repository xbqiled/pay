package com.pay.common.executor.update.entity;

/**
 * 空的weakup对象
 */
public class NullWeakUpUpdate extends AbstractUpdate{

    private static final long serialVersionUID = 8323340750934621540L;

    @Override
    public void update() {

    }
}
