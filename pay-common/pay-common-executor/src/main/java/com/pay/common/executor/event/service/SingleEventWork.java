package com.pay.common.executor.event.service;



import com.pay.common.executor.event.EventBus;
import com.pay.common.executor.event.SingleEvent;
import com.pay.common.executor.thread.worker.AbstractWork;
import com.pay.common.executor.utils.Loggers;

/**
 * 单事件worker
 */
public class SingleEventWork extends AbstractWork {

    private final EventBus eventBus;
    private final SingleEvent singleEvent;

    public SingleEventWork(EventBus eventBus, SingleEvent singleEvent) {
        this.eventBus = eventBus;
        this.singleEvent = singleEvent;
    }

    @Override
    public void run() {
        try {
            eventBus.handleSingleEvent(singleEvent);
        }catch (Exception e){
            Loggers.gameExecutorError.error(e.toString(), e);
        }

    }
}
