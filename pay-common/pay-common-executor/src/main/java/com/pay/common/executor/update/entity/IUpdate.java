package com.pay.common.executor.update.entity;

import java.io.Serializable;

/**
 * 基础循环接口
 */
public interface IUpdate <ID extends Serializable> extends Serializable {
    public void update();
    public ID getUpdateId();
    public boolean isActive();
    public void setActive(boolean activeFlag);
}
