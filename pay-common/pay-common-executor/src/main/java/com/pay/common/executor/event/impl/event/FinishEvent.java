package com.pay.common.executor.event.impl.event;



import com.pay.common.executor.event.CycleEvent;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.EventType;

import java.io.Serializable;

/**
 * dispatch 使用
 */
public class FinishEvent <ID extends Serializable> extends CycleEvent {
    public FinishEvent(EventType eventType, ID eventId, EventParam... parms){
//        setEventType(eventType);
//        setParams(parms);
        super(eventType, eventId, parms);
    }

    public void call() {
//        EventParam[] eventParams = getParams();
//        System.out.println(eventParams[0].getT() + "float"+ eventParams[1].getT());
    }
}
