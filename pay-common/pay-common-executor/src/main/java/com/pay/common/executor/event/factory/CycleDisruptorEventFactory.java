package com.pay.common.executor.event.factory;


import com.pay.common.executor.event.CycleEvent;
import com.lmax.disruptor.EventFactory;

/**
 */
public class CycleDisruptorEventFactory implements EventFactory<CycleEvent> {

    @Override
    public CycleEvent newInstance() {
        return new CycleEvent();
    }

}
