package com.pay.common.executor.update.thread.dispatch;


import com.pay.common.executor.event.EventBus;
import com.pay.common.executor.update.pool.UpdateBindExecutorService;

/**
 */
public class BindNotifyDisptachThread extends LockSupportDisptachThread{

    public BindNotifyDisptachThread(EventBus eventBus, UpdateBindExecutorService updateBindExcutorService, int cycleSleepTime, long minCycleTime) {
        super(eventBus, updateBindExcutorService, cycleSleepTime, minCycleTime);
    }
}
