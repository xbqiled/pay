package com.pay.common.executor.event.impl.listener;


import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.common.IEvent;
import com.pay.common.executor.event.impl.event.FinishEvent;
import com.pay.common.executor.event.impl.event.UpdateEvent;
import com.pay.common.executor.update.cache.UpdateEventCacheService;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.service.UpdateService;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;
import com.pay.common.executor.utils.Constants;

/**
 */
public class DispatchCreateEventListener extends CreateEventListener {

    private final DispatchThread dispatchThread;

    private final UpdateService updateService;
    public DispatchCreateEventListener(DispatchThread dispatchThread, UpdateService updateService) {
        super();
        this.dispatchThread = dispatchThread;
        this.updateService = updateService;
    }

    public void fireEvent(IEvent event) {
        super.fireEvent(event);
        EventParam[] eventParams = event.getParams();
        IUpdate iUpdate = (IUpdate) eventParams[0].getT();
        if(iUpdate.isActive()) {
//            UpdateEvent updateEvent = new UpdateEvent(Constants.EventTypeConstans.updateEventType, iUpdate.getUpdateId(), event.getParams());
            UpdateEvent updateEvent = UpdateEventCacheService.createUpdateEvent();
            updateEvent.setEventType(Constants.EventTypeConstans.updateEventType);
            updateEvent.setId(iUpdate.getUpdateId());
            updateEvent.setParams(eventParams);
            updateEvent.setInitFlag(true);
            updateEvent.setUpdateAliveFlag(true);
            this.dispatchThread.addUpdateEvent(updateEvent);
        }else{
            FinishEvent finishEvent = new FinishEvent(Constants.EventTypeConstans.finishEventType, iUpdate.getUpdateId(), eventParams);
            dispatchThread.addFinishEvent(finishEvent);
        }
    }
}
