package com.pay.common.executor.update.pool;

import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;

/**
 * 执行一个update
 */
public interface IUpdateExecutor {
    public void executorUpdate(DispatchThread dispatchThread, IUpdate iUpdate, boolean firstFlag, int updateExcutorIndex);
    public void startup();
    public void shutdown();
}
