package com.pay.common.executor.update.cache;

import com.pay.common.executor.event.impl.event.UpdateEvent;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 */
public class UpdateEventPoolFactory implements PooledObjectFactory<UpdateEvent> {
    @Override
    public PooledObject<UpdateEvent> makeObject() throws Exception {
        UpdateEvent updateEvent = new UpdateEvent();
        return new DefaultPooledObject<>(updateEvent);
    }

    @Override
    public void destroyObject(PooledObject<UpdateEvent> p) throws Exception {
        UpdateEvent updateEvent = p.getObject();
    }

    @Override
    public boolean validateObject(PooledObject<UpdateEvent> p) {
        return false;
    }

    @Override
    public void activateObject(PooledObject<UpdateEvent> p) throws Exception {

    }

    @Override
    public void passivateObject(PooledObject<UpdateEvent> p) throws Exception {
    }
}
