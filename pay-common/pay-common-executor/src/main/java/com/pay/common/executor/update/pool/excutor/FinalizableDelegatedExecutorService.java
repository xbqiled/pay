package com.pay.common.executor.update.pool.excutor;

import java.util.concurrent.ExecutorService;

/**
 */
class FinalizableDelegatedExecutorService extends DelegatedExecutorService {
    FinalizableDelegatedExecutorService(ExecutorService executor) {
        super(executor);
    }

    protected void finalize() {
        shutdown();
    }
}