package com.pay.common.executor.event.impl.listener;


import com.pay.common.executor.event.AbstractEventListener;
import com.pay.common.executor.utils.Constants;

/**
 * 更新监听器
 */
public class UpdateEventListener extends AbstractEventListener {

    @Override
    public void initEventType() {
        register(Constants.EventTypeConstans.updateEventType);
    }
}
