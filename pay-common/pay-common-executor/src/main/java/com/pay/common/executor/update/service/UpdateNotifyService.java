package com.pay.common.executor.update.service;


import com.pay.common.executor.thread.ThreadNameFactory;
import com.pay.common.executor.utils.Constants;
import com.pay.common.executor.utils.ExecutorUtil;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * updateservice的唤醒服务
 */
public class UpdateNotifyService {

    private ScheduledExecutorService scheduledExecutorService;


    private final UpdateService updateService;

    /**
     * 单位毫秒
     */
    private final int notifyTime;

    public UpdateNotifyService(UpdateService updateService, int notifyTime) {
        this.updateService = updateService;
        this.notifyTime = notifyTime;
    }

    public void startup() throws Exception {
        ThreadNameFactory threadNameFactory = new ThreadNameFactory(Constants.Thread.UpdateNotifyService);
        scheduledExecutorService = Executors.newScheduledThreadPool(1, threadNameFactory);
        scheduledExecutorService.scheduleAtFixedRate(new NotifyTask(updateService), notifyTime, 1, TimeUnit.MICROSECONDS);
    }

    public void shutdown() throws Exception {
        ExecutorUtil.shutdownAndAwaitTermination(scheduledExecutorService, 60L, TimeUnit.MILLISECONDS);
    }
}
