package com.pay.common.executor.event.impl.listener;


import com.pay.common.executor.event.CycleEvent;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.common.IEvent;
import com.pay.common.executor.event.impl.event.FinishEvent;
import com.pay.common.executor.event.impl.event.UpdateEvent;
import com.pay.common.executor.update.cache.UpdateEventCacheService;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.pool.IUpdateExecutor;
import com.pay.common.executor.update.service.UpdateService;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;
import com.pay.common.executor.utils.Constants;

/**
 */
public class DispatchUpdateEventListener extends UpdateEventListener {
    private final DispatchThread dispatchThread;
    private final UpdateService updateService;
    public DispatchUpdateEventListener(DispatchThread dispatchThread, UpdateService updateService) {
        this.dispatchThread = dispatchThread;
        this.updateService = updateService;
    }


    public void fireEvent(IEvent event) {
//        if(Loggers.gameExecutorUtil.isDebugEnabled()){
//            Loggers.gameExecutorUtil.debug("处理update");
//        }
        super.fireEvent(event);

        //提交执行线程
        CycleEvent cycleEvent = (CycleEvent) event;
        EventParam[] eventParams = event.getParams();
        for(EventParam eventParam: eventParams) {
            IUpdate iUpdate = (IUpdate) eventParam.getT();
            boolean aliveFlag = cycleEvent.isUpdateAliveFlag();
            if (aliveFlag) {
                IUpdateExecutor iUpdateExecutor = dispatchThread.getiUpdateExecutor();
                iUpdateExecutor.executorUpdate(dispatchThread, iUpdate, cycleEvent.isInitFlag(), cycleEvent.getUpdateExcutorIndex());
            } else {
                FinishEvent finishEvent = new FinishEvent(Constants.EventTypeConstans.finishEventType, iUpdate.getUpdateId(), eventParams);
                dispatchThread.addFinishEvent(finishEvent);
            }
        }

        //如果是update，需要释放cache
        if(cycleEvent instanceof UpdateEvent){
            UpdateEvent updateEvent = (UpdateEvent) cycleEvent;
            UpdateEventCacheService.releaseUpdateEvent(updateEvent);
        }
    }
}
