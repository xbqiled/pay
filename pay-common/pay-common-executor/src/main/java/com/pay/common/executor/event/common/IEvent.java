package com.pay.common.executor.event.common;

import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.EventType;

/**
 * 事件定义
 */
public interface IEvent{
    public void setEventType(EventType eventType);
    public EventType getEventType();
    public EventParam[] getParams();
    public void setParams(EventParam... eventParams);
    public void call();
}
