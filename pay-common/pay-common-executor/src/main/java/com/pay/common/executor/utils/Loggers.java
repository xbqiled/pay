package com.pay.common.executor.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
public final class Loggers {
    public static final Logger threadLogger = LoggerFactory.getLogger("thread");

    /** Server相关的日志 */
    public static final Logger gameExecutorError = (Logger) LoggerFactory.getLogger("gameExecutorError");
    /** Server相关的日志 */
    public static final Logger gameExecutorUtil = (Logger) LoggerFactory.getLogger("gameExecutorUtil");
    /** Server相关的日志 */
    public static final Logger gameExecutorEvent = (Logger) LoggerFactory.getLogger("gameExecutorEvent");

    private Loggers() {
    }
}

