package com.pay.common.executor.event.common;


import com.pay.common.executor.event.AbstractEventListener;

/**
 * ⌚
 */
public interface IEventBus {
    public void addEventListener(AbstractEventListener listene);
    public void removeEventListener(AbstractEventListener listene);
    public void clearEventListener();
    public void addEvent(IEvent event);
    public void handleEvent();
    public void handleSingleEvent(IEvent event) throws Exception;
    public void clearEvent();
    public void clear();
}
