package com.pay.common.executor.event.common;


import com.pay.common.executor.event.EventType;

/**
 * 事件监听器
 */
public interface IEventListener {

    public void register(EventType eventType);
    public boolean containEventType(EventType eventType);
    public void fireEvent(IEvent event);
}
