package com.pay.common.executor.event.impl.event;



import com.pay.common.executor.event.CycleEvent;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.EventType;

import java.io.Serializable;

/**
 * updateService使用
 */
public class FinishedEvent<ID extends Serializable> extends CycleEvent {

    public FinishedEvent(EventType eventType, ID eventId, EventParam... parms){
        super(eventType, eventId, parms);
    }

    public void call() {
//        EventParam[] eventParams = getParams();
//        System.out.println(eventParams[0].getT() + "float"+ eventParams[1].getT());
    }
}
