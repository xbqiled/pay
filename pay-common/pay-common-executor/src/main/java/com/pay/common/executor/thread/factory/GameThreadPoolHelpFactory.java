package com.pay.common.executor.thread.factory;



import com.pay.common.executor.common.BlockingQueueType;
import com.pay.common.executor.thread.policy.*;

import java.util.concurrent.*;

/**
 * 游戏线程池辅助工厂
 */
public class GameThreadPoolHelpFactory {

    public RejectedExecutionHandler createPolicy(RejectedPolicyType rejectedPolicyType) {

        switch (rejectedPolicyType) {
            case BLOCKING_POLICY:
                return new BlockingPolicy();
            case CALLER_RUNS_POLICY:
                return new ThreadPoolExecutor.CallerRunsPolicy();
            case ABORT_POLICY:
                return new ThreadPoolExecutor.AbortPolicy();
            case DISCARD_POLICY:
                return new ThreadPoolExecutor.DiscardPolicy();
            case DISCARD_OLDEST_POLICY:
                return new ThreadPoolExecutor.DiscardOldestPolicy();
        }

        return null;
    }

    public RejectedExecutionHandler createPolicy(RejectedPolicyType rejectedPolicyType, String threadName) {

        switch (rejectedPolicyType) {
            case BLOCKING_POLICY:
                return new BlockingPolicy(threadName);
            case CALLER_RUNS_POLICY:
                return new CallerRunsPolicy(threadName);
            case ABORT_POLICY:
                return new AbortPolicy(threadName);
            case DISCARD_POLICY:
                return new DiscardPolicy(threadName);
            case DISCARD_OLDEST_POLICY:
                return new DiscardOldestPolicy(threadName);
        }

        return null;
    }

    public BlockingQueue<Runnable> createBlockingQueue(BlockingQueueType blockingQueueType, int queues) {

        switch (blockingQueueType) {
            case LINKED_BLOCKING_QUEUE:
                return new LinkedBlockingQueue<Runnable>();
            case ARRAY_BLOCKING_QUEUE:
                return new ArrayBlockingQueue<Runnable>(queues);
            case SYNCHRONOUS_QUEUE:
                return new SynchronousQueue<Runnable>();
        }

        return null;
    }
}

