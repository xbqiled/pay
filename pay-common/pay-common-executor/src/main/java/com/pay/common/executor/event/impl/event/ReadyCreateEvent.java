package com.pay.common.executor.event.impl.event;


import com.pay.common.executor.event.CycleEvent;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.EventType;

/**
 * updateService使用
 */
public class ReadyCreateEvent extends CycleEvent {

    public ReadyCreateEvent(EventType eventType, long eventId, EventParam... parms){
        super(eventType, eventId, parms);
    }

    public void call() {

    }
}
