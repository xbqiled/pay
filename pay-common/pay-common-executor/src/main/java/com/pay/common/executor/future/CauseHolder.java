package com.pay.common.executor.future;

/**
 * 错误原因
 */
public class CauseHolder {
    public Throwable cause;

    public CauseHolder(Throwable cause) {
        this.cause = cause;
    }

    public CauseHolder() {

    }
}
