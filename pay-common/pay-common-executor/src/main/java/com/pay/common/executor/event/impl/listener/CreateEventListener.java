package com.pay.common.executor.event.impl.listener;



import com.pay.common.executor.event.AbstractEventListener;
import com.pay.common.executor.utils.Constants;

/**
 * 创建监听器
 */
public class CreateEventListener extends AbstractEventListener {

    @Override
    public void initEventType() {
        register(Constants.EventTypeConstans.createEventType);
    }
}
