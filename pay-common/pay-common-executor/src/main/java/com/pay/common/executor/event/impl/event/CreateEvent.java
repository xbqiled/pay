package com.pay.common.executor.event.impl.event;




import com.pay.common.executor.event.CycleEvent;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.EventType;
import com.pay.common.executor.utils.Loggers;

import java.io.Serializable;

/**
 *  dispatch thread使用
 */
public class CreateEvent<ID extends Serializable> extends CycleEvent {

    public CreateEvent(EventType eventType, ID eventId, EventParam... parms){
//        setEventType(eventType);
//        setParams(parms);
        super(eventType, eventId, parms);
    }

    public void call() {
        if(Loggers.gameExecutorUtil.isDebugEnabled()){
            EventParam[] eventParams = getParams();
            Loggers.gameExecutorUtil.debug("create event " + eventParams[0].getT());
        }

    }
}
