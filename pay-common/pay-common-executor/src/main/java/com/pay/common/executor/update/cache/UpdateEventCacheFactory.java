package com.pay.common.executor.update.cache;


import com.pay.common.executor.event.impl.event.UpdateEvent;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 * updateevent因为使用太频繁，使用commonpool2缓存
 */
public class UpdateEventCacheFactory extends GenericObjectPool<UpdateEvent> {

    public UpdateEventCacheFactory(PooledObjectFactory<UpdateEvent> factory, GenericObjectPoolConfig config) {
        super(factory, config);
    }
}
