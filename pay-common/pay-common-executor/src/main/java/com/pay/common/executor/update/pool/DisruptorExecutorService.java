package com.pay.common.executor.update.pool;


import com.pay.common.executor.event.EventBus;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.handler.CycleEventHandler;
import com.pay.common.executor.event.impl.event.UpdateEvent;
import com.pay.common.executor.thread.executor.NonOrderedQueuePoolExecutor;
import com.pay.common.executor.update.cache.UpdateEventCacheService;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;
import com.pay.common.executor.update.thread.dispatch.DisruptorDispatchThread;
import com.lmax.disruptor.FatalExceptionHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.WorkerPool;
import com.pay.common.executor.utils.Constants;

import java.util.concurrent.ExecutorService;

/**
 */
public class DisruptorExecutorService implements IUpdateExecutor {

    private WorkerPool workerPool;

    private final int excutorSize;

    private CycleEventHandler[] cycleEventHandler;

    private DisruptorDispatchThread disruptorDispatchThread ;

    private ExecutorService executorService;

    private final String poolName;
    public DisruptorExecutorService(String poolName, int excutorSize) {
        this.excutorSize = excutorSize;
        this.poolName = poolName;
    }

    @Override
    public void executorUpdate(DispatchThread dispatchThread, IUpdate iUpdate, boolean firstFlag, int updateExcutorIndex) {
        iUpdate.update();

        //事件总线增加更新完成通知
        EventParam<IUpdate> params = new EventParam<IUpdate>(iUpdate);
        UpdateEvent updateEvent = UpdateEventCacheService.createUpdateEvent();
        updateEvent.setEventType(Constants.EventTypeConstans.updateEventType);
        updateEvent.setId(iUpdate.getUpdateId());
        updateEvent.setParams(params);
//        UpdateEvent event = new UpdateEvent(Constants.EventTypeConstans.updateEventType, iUpdate.getUpdateId(), params);
        updateEvent.setUpdateAliveFlag(iUpdate.isActive());
        disruptorDispatchThread.addUpdateEvent(updateEvent);
    }

    @Override
    public void startup() {
        EventBus eventBus = disruptorDispatchThread.getEventBus();
        executorService = new NonOrderedQueuePoolExecutor(poolName, excutorSize);
        cycleEventHandler = new CycleEventHandler[excutorSize];
        for(int i = 0; i < excutorSize; i++){
            cycleEventHandler[i] = new CycleEventHandler(eventBus);
        }

        RingBuffer ringBuffer = disruptorDispatchThread.getRingBuffer();
        workerPool = new WorkerPool(ringBuffer, ringBuffer.newBarrier(), new FatalExceptionHandler(), cycleEventHandler);
        ringBuffer.addGatingSequences(workerPool.getWorkerSequences());

        workerPool.start(executorService);

//        BatchEventProcessor<CycleEvent>[] batchEventProcessors = new BatchEventProcessor[excutorSize];
//        for(int i = 0; i < excutorSize; i++){
//            batchEventProcessors[i] = new BatchEventProcessor<>(ringBuffer, sequenceBarrier, cycleEventHandler[i]);
//            ringBuffer.addGatingSequences(batchEventProcessors[i].getSequence());
////            executorService.submit(batchEventProcessors[i]);
//        }
    }

    @Override
    public void shutdown() {
        workerPool.drainAndHalt();
    }

    public DisruptorDispatchThread getDisruptorDispatchThread() {
        return disruptorDispatchThread;
    }

    public void setDisruptorDispatchThread(DisruptorDispatchThread disruptorDispatchThread) {
        this.disruptorDispatchThread = disruptorDispatchThread;
    }

    public WorkerPool getWorkerPool() {
        return workerPool;
    }

    public void setWorkerPool(WorkerPool workerPool) {
        this.workerPool = workerPool;
    }

    public RingBuffer getDispatchRingBuffer(){
        return disruptorDispatchThread.getRingBuffer();
    }
}
