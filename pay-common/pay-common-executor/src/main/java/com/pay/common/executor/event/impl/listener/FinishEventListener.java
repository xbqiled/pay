package com.pay.common.executor.event.impl.listener;



import com.pay.common.executor.event.AbstractEventListener;
import com.pay.common.executor.utils.Constants;

/**
 * 完成监听器
 */
public class FinishEventListener extends AbstractEventListener {

    @Override
    public void initEventType() {
        register(Constants.EventTypeConstans.finishEventType);
    }

}
