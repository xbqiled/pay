package com.pay.common.executor.future;

/**
 */
public interface ITaskFutureListener<V extends ITaskFuture<?>> extends EventListener {

    /**
     *  完成
     * @param
     * @throws Exception
     */
    void operationComplete(ITaskFuture<V> future) throws Exception;
}
