package com.pay.common.executor.event.impl.event;


import com.pay.common.executor.event.CycleEvent;
import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.EventType;

/**
 * updateService使用
 */
public class ReadFinishEvent extends CycleEvent {

    //是否是内部销毁 内部销毁才会销毁缓存
    private boolean innerUpdateFlag;

    public ReadFinishEvent(EventType eventType, long eventId, EventParam... parms){
        super(eventType, eventId, parms);
    }

    public void call() {

    }

    public boolean isInnerUpdateFlag() {
        return innerUpdateFlag;
    }

    public void setInnerUpdateFlag(boolean innerUpdateFlag) {
        this.innerUpdateFlag = innerUpdateFlag;
    }
}
