package com.pay.common.executor.update.thread.dispatch;


import com.pay.common.executor.event.EventBus;
import com.pay.common.executor.event.common.IEvent;
import com.pay.common.executor.update.pool.IUpdateExecutor;

/**
 * ⌚事件分配器
 */
public abstract class DispatchThread extends Thread{

    private EventBus eventBus;

    public DispatchThread(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void notifyRun(){
        eventBus.handleEvent();
    }

    public EventBus getEventBus() {
        return eventBus;
    }

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void shutDown(){
        eventBus.clear();
    }

    public void addUpdateEvent(IEvent event){
        getEventBus().addEvent(event);
    }
    public void addCreateEvent(IEvent event){
        getEventBus().addEvent(event);
    }

    public void addFinishEvent(IEvent event){
        getEventBus().addEvent(event);
    }
    public abstract void unpark();
    abstract void park();
    public abstract IUpdateExecutor getiUpdateExecutor();
    public abstract void startup();

}
