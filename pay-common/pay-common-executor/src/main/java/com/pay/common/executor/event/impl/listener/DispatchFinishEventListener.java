package com.pay.common.executor.event.impl.listener;


import com.pay.common.executor.event.EventParam;
import com.pay.common.executor.event.common.IEvent;
import com.pay.common.executor.event.impl.event.FinishedEvent;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.service.UpdateService;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;
import com.pay.common.executor.utils.Constants;

/**
 */
public class DispatchFinishEventListener extends FinishEventListener {

    private final UpdateService updateService;
    private final DispatchThread dispatchThread;

    public DispatchFinishEventListener(DispatchThread dispatchThread, UpdateService updateService) {
        this.dispatchThread = dispatchThread;
        this.updateService = updateService;
    }

    public void fireEvent(IEvent event) {
        super.fireEvent(event);
        //提交更新服务器 执行完成调度
        EventParam[] eventParams = event.getParams();
        IUpdate iUpdate = (IUpdate) eventParams[0].getT();
        FinishedEvent finishedEvent = new FinishedEvent(Constants.EventTypeConstans.finishedEventType, iUpdate.getUpdateId(), event.getParams());
        this.updateService.addFinishedEvent(finishedEvent);
    }

}
