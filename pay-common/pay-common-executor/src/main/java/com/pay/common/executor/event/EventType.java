package com.pay.common.executor.event;

/**
 * 事件类型
 */
public class EventType {

    private int index;

    public EventType(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
