package com.pay.common.executor.update.pool;



import com.pay.common.executor.thread.executor.NonOrderedQueuePoolExecutor;
import com.pay.common.executor.thread.factory.GameThreadPoolHelpFactory;
import com.pay.common.executor.thread.policy.RejectedPolicyType;
import com.pay.common.executor.update.entity.IUpdate;
import com.pay.common.executor.update.thread.dispatch.DispatchThread;
import com.pay.common.executor.update.thread.listener.LockSupportUpdateFutureListener;
import com.pay.common.executor.update.thread.update.LockSupportUpdateFuture;
import com.pay.common.executor.update.thread.update.LockSupportUpdateFutureThread;
import com.pay.common.executor.utils.Constants;
import com.pay.common.executor.utils.ExecutorUtil;

import java.util.concurrent.TimeUnit;

/**
 * 更新执行器
 */
public class UpdateExecutorService implements IUpdateExecutor {

    private final NonOrderedQueuePoolExecutor nonOrderedQueuePoolExecutor;


    public UpdateExecutorService(int corePoolSize, int maxSize, RejectedPolicyType rejectedPolicyType) {
        String name = Constants.Thread.UpdateExecutorService;
        GameThreadPoolHelpFactory gameThreadPoolHelpFactory = new GameThreadPoolHelpFactory();
        nonOrderedQueuePoolExecutor = new NonOrderedQueuePoolExecutor(Constants.Thread.UpdateExecutorService, corePoolSize, maxSize, gameThreadPoolHelpFactory.createPolicy(rejectedPolicyType, name));
    }

    @Override
    public void executorUpdate(DispatchThread dispatchThread, IUpdate iUpdate, boolean firstFlag, int updateExcutorIndex) {
        LockSupportUpdateFuture lockSupportUpdateFuture = new LockSupportUpdateFuture(dispatchThread);
        lockSupportUpdateFuture.addListener(new LockSupportUpdateFutureListener());
        LockSupportUpdateFutureThread lockSupportUpdateFutureThread = new LockSupportUpdateFutureThread(dispatchThread, iUpdate, lockSupportUpdateFuture);
        nonOrderedQueuePoolExecutor.execute(lockSupportUpdateFutureThread);
    }

    @Override
    public void startup() {

    }

    @Override
    public void shutdown() {
        ExecutorUtil.shutdownAndAwaitTermination(nonOrderedQueuePoolExecutor, 60,
                TimeUnit.MILLISECONDS);
    }
}
