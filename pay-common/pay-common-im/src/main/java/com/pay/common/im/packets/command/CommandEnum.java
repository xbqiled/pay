package com.pay.common.im.packets.command;


public class CommandEnum {

    public enum Command {

        COMMAND_UNKNOW(0, "缺省命令"),

        COMMAND_HANDSHAKE_REQ(1, "握手请求"),

        COMMAND_HANDSHAKE_RESP(2, "握手响应"),

        COMMAND_LOGIN_REQ(3, "登录请求"),

        COMMAND_LOGIN_RESP(4, "登录响应"),

        COMMAND_CHAT_REQ(5, "聊天请求"),

        COMMAND_CHAT_RESP(6, "聊天响应"),

        COMMAND_HEARTBEAT_REQ(7, "心跳请求"),

        COMMAND_LOGOUT_REQ(8, "退出请求"),

        COMMAND_LOGOUT_RESP(9, "退出响应"),

        COMMAND_HEARTBEAT_RESP(10, "心跳reps"),

        COMMAND_FORCE_DISCONN(11, "强制下线");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        Command(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum MsgType {

        MSG_TYPE_TEXT(0, "文本"),

        MSG_TYPE_IMG(1, "图片"),

        MSG_TYPE_VOICE(2, "语音"),

        MSG_TYPE_VIDEO(3, "视频"),

        MSG_TYPE_MUSIC(4, "音乐"),

        MSG_TYPE_PAY(5, "支付类型");

        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        MsgType(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum SessionCloseReason {

        NORMAL(0, "超时退出"),

        OVER_TIME(1, "其他原因");

        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        SessionCloseReason(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }

}

