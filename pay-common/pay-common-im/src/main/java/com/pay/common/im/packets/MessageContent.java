package com.pay.common.im.packets;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessageContent implements Serializable {
    String text;
    Integer width;
    Integer higth;
    Integer duration;
    String coverurl;
}
