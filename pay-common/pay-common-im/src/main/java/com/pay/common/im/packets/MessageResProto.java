// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: MessageRes.proto

package com.pay.common.im.packets;

public final class MessageResProto {
  private MessageResProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface MessageResOrBuilder extends
      // @@protoc_insertion_point(interface_extends:im.MessageRes)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <pre>
     *返回信息 0成功1失败
     * </pre>
     *
     * <code>optional uint32 code = 1;</code>
     */
    int getCode();

    /**
     * <pre>
     *消息
     * </pre>
     *
     * <code>optional string msg = 2;</code>
     */
    String getMsg();
    /**
     * <pre>
     *消息
     * </pre>
     *
     * <code>optional string msg = 2;</code>
     */
    com.google.protobuf.ByteString
        getMsgBytes();

    /**
     * <pre>
     *消息编号
     * </pre>
     *
     * <code>optional string messageId = 3;</code>
     */
    String getMessageId();
    /**
     * <pre>
     *消息编号
     * </pre>
     *
     * <code>optional string messageId = 3;</code>
     */
    com.google.protobuf.ByteString
        getMessageIdBytes();

    /**
     * <pre>
     *服务端接受时间
     * </pre>
     *
     * <code>optional int64 acceptTime = 4;</code>
     */
    long getAcceptTime();
  }
  /**
   * Protobuf type {@code im.MessageRes}
   */
  public  static final class MessageRes extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:im.MessageRes)
      MessageResOrBuilder {
    // Use MessageRes.newBuilder() to construct.
    private MessageRes(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private MessageRes() {
      code_ = 0;
      msg_ = "";
      messageId_ = "";
      acceptTime_ = 0L;
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private MessageRes(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 8: {

              code_ = input.readUInt32();
              break;
            }
            case 18: {
              String s = input.readStringRequireUtf8();

              msg_ = s;
              break;
            }
            case 26: {
              String s = input.readStringRequireUtf8();

              messageId_ = s;
              break;
            }
            case 32: {

              acceptTime_ = input.readInt64();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return MessageResProto.internal_static_im_MessageRes_descriptor;
    }

    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return MessageResProto.internal_static_im_MessageRes_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              MessageRes.class, Builder.class);
    }

    public static final int CODE_FIELD_NUMBER = 1;
    private int code_;
    /**
     * <pre>
     *返回信息 0成功1失败
     * </pre>
     *
     * <code>optional uint32 code = 1;</code>
     */
    public int getCode() {
      return code_;
    }

    public static final int MSG_FIELD_NUMBER = 2;
    private volatile Object msg_;
    /**
     * <pre>
     *消息
     * </pre>
     *
     * <code>optional string msg = 2;</code>
     */
    public String getMsg() {
      Object ref = msg_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        msg_ = s;
        return s;
      }
    }
    /**
     * <pre>
     *消息
     * </pre>
     *
     * <code>optional string msg = 2;</code>
     */
    public com.google.protobuf.ByteString
        getMsgBytes() {
      Object ref = msg_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        msg_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int MESSAGEID_FIELD_NUMBER = 3;
    private volatile Object messageId_;
    /**
     * <pre>
     *消息编号
     * </pre>
     *
     * <code>optional string messageId = 3;</code>
     */
    public String getMessageId() {
      Object ref = messageId_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        messageId_ = s;
        return s;
      }
    }
    /**
     * <pre>
     *消息编号
     * </pre>
     *
     * <code>optional string messageId = 3;</code>
     */
    public com.google.protobuf.ByteString
        getMessageIdBytes() {
      Object ref = messageId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        messageId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int ACCEPTTIME_FIELD_NUMBER = 4;
    private long acceptTime_;
    /**
     * <pre>
     *服务端接受时间
     * </pre>
     *
     * <code>optional int64 acceptTime = 4;</code>
     */
    public long getAcceptTime() {
      return acceptTime_;
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (code_ != 0) {
        output.writeUInt32(1, code_);
      }
      if (!getMsgBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 2, msg_);
      }
      if (!getMessageIdBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 3, messageId_);
      }
      if (acceptTime_ != 0L) {
        output.writeInt64(4, acceptTime_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (code_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(1, code_);
      }
      if (!getMsgBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, msg_);
      }
      if (!getMessageIdBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, messageId_);
      }
      if (acceptTime_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(4, acceptTime_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof MessageRes)) {
        return super.equals(obj);
      }
      MessageRes other = (MessageRes) obj;

      boolean result = true;
      result = result && (getCode()
          == other.getCode());
      result = result && getMsg()
          .equals(other.getMsg());
      result = result && getMessageId()
          .equals(other.getMessageId());
      result = result && (getAcceptTime()
          == other.getAcceptTime());
      return result;
    }

    @Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptorForType().hashCode();
      hash = (37 * hash) + CODE_FIELD_NUMBER;
      hash = (53 * hash) + getCode();
      hash = (37 * hash) + MSG_FIELD_NUMBER;
      hash = (53 * hash) + getMsg().hashCode();
      hash = (37 * hash) + MESSAGEID_FIELD_NUMBER;
      hash = (53 * hash) + getMessageId().hashCode();
      hash = (37 * hash) + ACCEPTTIME_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getAcceptTime());
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static MessageRes parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static MessageRes parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static MessageRes parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static MessageRes parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static MessageRes parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static MessageRes parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static MessageRes parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static MessageRes parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static MessageRes parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static MessageRes parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(MessageRes prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code im.MessageRes}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:im.MessageRes)
        MessageResOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return MessageResProto.internal_static_im_MessageRes_descriptor;
      }

      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return MessageResProto.internal_static_im_MessageRes_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                MessageRes.class, Builder.class);
      }

      // Construct using com.pay.common.im.packets.MessageResProto.MessageRes.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        code_ = 0;

        msg_ = "";

        messageId_ = "";

        acceptTime_ = 0L;

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return MessageResProto.internal_static_im_MessageRes_descriptor;
      }

      public MessageRes getDefaultInstanceForType() {
        return MessageRes.getDefaultInstance();
      }

      public MessageRes build() {
        MessageRes result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public MessageRes buildPartial() {
        MessageRes result = new MessageRes(this);
        result.code_ = code_;
        result.msg_ = msg_;
        result.messageId_ = messageId_;
        result.acceptTime_ = acceptTime_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.setField(field, value);
      }
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof MessageRes) {
          return mergeFrom((MessageRes)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(MessageRes other) {
        if (other == MessageRes.getDefaultInstance()) return this;
        if (other.getCode() != 0) {
          setCode(other.getCode());
        }
        if (!other.getMsg().isEmpty()) {
          msg_ = other.msg_;
          onChanged();
        }
        if (!other.getMessageId().isEmpty()) {
          messageId_ = other.messageId_;
          onChanged();
        }
        if (other.getAcceptTime() != 0L) {
          setAcceptTime(other.getAcceptTime());
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        MessageRes parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (MessageRes) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int code_ ;
      /**
       * <pre>
       *返回信息 0成功1失败
       * </pre>
       *
       * <code>optional uint32 code = 1;</code>
       */
      public int getCode() {
        return code_;
      }
      /**
       * <pre>
       *返回信息 0成功1失败
       * </pre>
       *
       * <code>optional uint32 code = 1;</code>
       */
      public Builder setCode(int value) {
        
        code_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *返回信息 0成功1失败
       * </pre>
       *
       * <code>optional uint32 code = 1;</code>
       */
      public Builder clearCode() {
        
        code_ = 0;
        onChanged();
        return this;
      }

      private Object msg_ = "";
      /**
       * <pre>
       *消息
       * </pre>
       *
       * <code>optional string msg = 2;</code>
       */
      public String getMsg() {
        Object ref = msg_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          msg_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <pre>
       *消息
       * </pre>
       *
       * <code>optional string msg = 2;</code>
       */
      public com.google.protobuf.ByteString
          getMsgBytes() {
        Object ref = msg_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          msg_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <pre>
       *消息
       * </pre>
       *
       * <code>optional string msg = 2;</code>
       */
      public Builder setMsg(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        msg_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *消息
       * </pre>
       *
       * <code>optional string msg = 2;</code>
       */
      public Builder clearMsg() {
        
        msg_ = getDefaultInstance().getMsg();
        onChanged();
        return this;
      }
      /**
       * <pre>
       *消息
       * </pre>
       *
       * <code>optional string msg = 2;</code>
       */
      public Builder setMsgBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        msg_ = value;
        onChanged();
        return this;
      }

      private Object messageId_ = "";
      /**
       * <pre>
       *消息编号
       * </pre>
       *
       * <code>optional string messageId = 3;</code>
       */
      public String getMessageId() {
        Object ref = messageId_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          messageId_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <pre>
       *消息编号
       * </pre>
       *
       * <code>optional string messageId = 3;</code>
       */
      public com.google.protobuf.ByteString
          getMessageIdBytes() {
        Object ref = messageId_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          messageId_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <pre>
       *消息编号
       * </pre>
       *
       * <code>optional string messageId = 3;</code>
       */
      public Builder setMessageId(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        messageId_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *消息编号
       * </pre>
       *
       * <code>optional string messageId = 3;</code>
       */
      public Builder clearMessageId() {
        
        messageId_ = getDefaultInstance().getMessageId();
        onChanged();
        return this;
      }
      /**
       * <pre>
       *消息编号
       * </pre>
       *
       * <code>optional string messageId = 3;</code>
       */
      public Builder setMessageIdBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        messageId_ = value;
        onChanged();
        return this;
      }

      private long acceptTime_ ;
      /**
       * <pre>
       *服务端接受时间
       * </pre>
       *
       * <code>optional int64 acceptTime = 4;</code>
       */
      public long getAcceptTime() {
        return acceptTime_;
      }
      /**
       * <pre>
       *服务端接受时间
       * </pre>
       *
       * <code>optional int64 acceptTime = 4;</code>
       */
      public Builder setAcceptTime(long value) {
        
        acceptTime_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *服务端接受时间
       * </pre>
       *
       * <code>optional int64 acceptTime = 4;</code>
       */
      public Builder clearAcceptTime() {
        
        acceptTime_ = 0L;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:im.MessageRes)
    }

    // @@protoc_insertion_point(class_scope:im.MessageRes)
    private static final MessageRes DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new MessageRes();
    }

    public static MessageRes getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<MessageRes>
        PARSER = new com.google.protobuf.AbstractParser<MessageRes>() {
      public MessageRes parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new MessageRes(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<MessageRes> parser() {
      return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<MessageRes> getParserForType() {
      return PARSER;
    }

    public MessageRes getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_im_MessageRes_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_im_MessageRes_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\020MessageRes.proto\022\002im\"N\n\nMessageRes\022\014\n\004" +
      "code\030\001 \001(\r\022\013\n\003msg\030\002 \001(\t\022\021\n\tmessageId\030\003 \001" +
      "(\t\022\022\n\nacceptTime\030\004 \001(\003B.\n\031com.pay.common" +
      ".im.packetsB\017MessageResProtoH\001b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_im_MessageRes_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_im_MessageRes_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_im_MessageRes_descriptor,
        new String[] { "Code", "Msg", "MessageId", "AcceptTime", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
