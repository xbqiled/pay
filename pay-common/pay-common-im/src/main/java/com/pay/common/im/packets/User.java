package com.pay.common.im.packets;

import lombok.Data;

import java.io.Serializable;

/**
 * 版本: [1.0]
 * 功能说明: 
 * 作者: Lion
 */
@Data
public class User implements Serializable{
	private static final long serialVersionUID = 1L;

	Integer userId;
	String nickName;
	String avatar;
	Long orgId;
}
