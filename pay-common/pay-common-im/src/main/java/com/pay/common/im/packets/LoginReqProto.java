// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: LoginReq.proto

package com.pay.common.im.packets;

public final class LoginReqProto {
  private LoginReqProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface LoginReqOrBuilder extends
      // @@protoc_insertion_point(interface_extends:im.LoginReq)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <pre>
     *组织机构编码
     * </pre>
     *
     * <code>optional string orgId = 1;</code>
     */
    String getOrgId();
    /**
     * <pre>
     *组织机构编码
     * </pre>
     *
     * <code>optional string orgId = 1;</code>
     */
    com.google.protobuf.ByteString
        getOrgIdBytes();

    /**
     * <pre>
     *用户账号
     * </pre>
     *
     * <code>optional string userName = 3;</code>
     */
    String getUserName();
    /**
     * <pre>
     *用户账号
     * </pre>
     *
     * <code>optional string userName = 3;</code>
     */
    com.google.protobuf.ByteString
        getUserNameBytes();

    /**
     * <pre>
     *密码
     * </pre>
     *
     * <code>optional string password = 4;</code>
     */
    String getPassword();
    /**
     * <pre>
     *密码
     * </pre>
     *
     * <code>optional string password = 4;</code>
     */
    com.google.protobuf.ByteString
        getPasswordBytes();

    /**
     * <pre>
     *设备类型
     * </pre>
     *
     * <code>optional int32 deviceType = 5;</code>
     */
    int getDeviceType();

    /**
     * <pre>
     *设备唯一标志
     * </pre>
     *
     * <code>optional string deviceId = 6;</code>
     */
    String getDeviceId();
    /**
     * <pre>
     *设备唯一标志
     * </pre>
     *
     * <code>optional string deviceId = 6;</code>
     */
    com.google.protobuf.ByteString
        getDeviceIdBytes();
  }
  /**
   * Protobuf type {@code im.LoginReq}
   */
  public  static final class LoginReq extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:im.LoginReq)
      LoginReqOrBuilder {
    // Use LoginReq.newBuilder() to construct.
    private LoginReq(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private LoginReq() {
      orgId_ = "";
      userName_ = "";
      password_ = "";
      deviceType_ = 0;
      deviceId_ = "";
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private LoginReq(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 10: {
              String s = input.readStringRequireUtf8();

              orgId_ = s;
              break;
            }
            case 26: {
              String s = input.readStringRequireUtf8();

              userName_ = s;
              break;
            }
            case 34: {
              String s = input.readStringRequireUtf8();

              password_ = s;
              break;
            }
            case 40: {

              deviceType_ = input.readInt32();
              break;
            }
            case 50: {
              String s = input.readStringRequireUtf8();

              deviceId_ = s;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return LoginReqProto.internal_static_im_LoginReq_descriptor;
    }

    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return LoginReqProto.internal_static_im_LoginReq_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              LoginReq.class, Builder.class);
    }

    public static final int ORGID_FIELD_NUMBER = 1;
    private volatile Object orgId_;
    /**
     * <pre>
     *组织机构编码
     * </pre>
     *
     * <code>optional string orgId = 1;</code>
     */
    public String getOrgId() {
      Object ref = orgId_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        orgId_ = s;
        return s;
      }
    }
    /**
     * <pre>
     *组织机构编码
     * </pre>
     *
     * <code>optional string orgId = 1;</code>
     */
    public com.google.protobuf.ByteString
        getOrgIdBytes() {
      Object ref = orgId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        orgId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int USERNAME_FIELD_NUMBER = 3;
    private volatile Object userName_;
    /**
     * <pre>
     *用户账号
     * </pre>
     *
     * <code>optional string userName = 3;</code>
     */
    public String getUserName() {
      Object ref = userName_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        userName_ = s;
        return s;
      }
    }
    /**
     * <pre>
     *用户账号
     * </pre>
     *
     * <code>optional string userName = 3;</code>
     */
    public com.google.protobuf.ByteString
        getUserNameBytes() {
      Object ref = userName_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        userName_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int PASSWORD_FIELD_NUMBER = 4;
    private volatile Object password_;
    /**
     * <pre>
     *密码
     * </pre>
     *
     * <code>optional string password = 4;</code>
     */
    public String getPassword() {
      Object ref = password_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        password_ = s;
        return s;
      }
    }
    /**
     * <pre>
     *密码
     * </pre>
     *
     * <code>optional string password = 4;</code>
     */
    public com.google.protobuf.ByteString
        getPasswordBytes() {
      Object ref = password_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        password_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int DEVICETYPE_FIELD_NUMBER = 5;
    private int deviceType_;
    /**
     * <pre>
     *设备类型
     * </pre>
     *
     * <code>optional int32 deviceType = 5;</code>
     */
    public int getDeviceType() {
      return deviceType_;
    }

    public static final int DEVICEID_FIELD_NUMBER = 6;
    private volatile Object deviceId_;
    /**
     * <pre>
     *设备唯一标志
     * </pre>
     *
     * <code>optional string deviceId = 6;</code>
     */
    public String getDeviceId() {
      Object ref = deviceId_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        deviceId_ = s;
        return s;
      }
    }
    /**
     * <pre>
     *设备唯一标志
     * </pre>
     *
     * <code>optional string deviceId = 6;</code>
     */
    public com.google.protobuf.ByteString
        getDeviceIdBytes() {
      Object ref = deviceId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        deviceId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getOrgIdBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 1, orgId_);
      }
      if (!getUserNameBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 3, userName_);
      }
      if (!getPasswordBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 4, password_);
      }
      if (deviceType_ != 0) {
        output.writeInt32(5, deviceType_);
      }
      if (!getDeviceIdBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 6, deviceId_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getOrgIdBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, orgId_);
      }
      if (!getUserNameBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, userName_);
      }
      if (!getPasswordBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(4, password_);
      }
      if (deviceType_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(5, deviceType_);
      }
      if (!getDeviceIdBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(6, deviceId_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof LoginReq)) {
        return super.equals(obj);
      }
      LoginReq other = (LoginReq) obj;

      boolean result = true;
      result = result && getOrgId()
          .equals(other.getOrgId());
      result = result && getUserName()
          .equals(other.getUserName());
      result = result && getPassword()
          .equals(other.getPassword());
      result = result && (getDeviceType()
          == other.getDeviceType());
      result = result && getDeviceId()
          .equals(other.getDeviceId());
      return result;
    }

    @Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptorForType().hashCode();
      hash = (37 * hash) + ORGID_FIELD_NUMBER;
      hash = (53 * hash) + getOrgId().hashCode();
      hash = (37 * hash) + USERNAME_FIELD_NUMBER;
      hash = (53 * hash) + getUserName().hashCode();
      hash = (37 * hash) + PASSWORD_FIELD_NUMBER;
      hash = (53 * hash) + getPassword().hashCode();
      hash = (37 * hash) + DEVICETYPE_FIELD_NUMBER;
      hash = (53 * hash) + getDeviceType();
      hash = (37 * hash) + DEVICEID_FIELD_NUMBER;
      hash = (53 * hash) + getDeviceId().hashCode();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static LoginReq parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static LoginReq parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static LoginReq parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static LoginReq parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static LoginReq parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static LoginReq parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static LoginReq parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static LoginReq parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static LoginReq parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static LoginReq parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(LoginReq prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code im.LoginReq}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:im.LoginReq)
        LoginReqOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return LoginReqProto.internal_static_im_LoginReq_descriptor;
      }

      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return LoginReqProto.internal_static_im_LoginReq_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                LoginReq.class, Builder.class);
      }

      // Construct using com.pay.common.im.packets.LoginReqProto.LoginReq.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        orgId_ = "";

        userName_ = "";

        password_ = "";

        deviceType_ = 0;

        deviceId_ = "";

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return LoginReqProto.internal_static_im_LoginReq_descriptor;
      }

      public LoginReq getDefaultInstanceForType() {
        return LoginReq.getDefaultInstance();
      }

      public LoginReq build() {
        LoginReq result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public LoginReq buildPartial() {
        LoginReq result = new LoginReq(this);
        result.orgId_ = orgId_;
        result.userName_ = userName_;
        result.password_ = password_;
        result.deviceType_ = deviceType_;
        result.deviceId_ = deviceId_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.setField(field, value);
      }
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof LoginReq) {
          return mergeFrom((LoginReq)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(LoginReq other) {
        if (other == LoginReq.getDefaultInstance()) return this;
        if (!other.getOrgId().isEmpty()) {
          orgId_ = other.orgId_;
          onChanged();
        }
        if (!other.getUserName().isEmpty()) {
          userName_ = other.userName_;
          onChanged();
        }
        if (!other.getPassword().isEmpty()) {
          password_ = other.password_;
          onChanged();
        }
        if (other.getDeviceType() != 0) {
          setDeviceType(other.getDeviceType());
        }
        if (!other.getDeviceId().isEmpty()) {
          deviceId_ = other.deviceId_;
          onChanged();
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        LoginReq parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (LoginReq) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private Object orgId_ = "";
      /**
       * <pre>
       *组织机构编码
       * </pre>
       *
       * <code>optional string orgId = 1;</code>
       */
      public String getOrgId() {
        Object ref = orgId_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          orgId_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <pre>
       *组织机构编码
       * </pre>
       *
       * <code>optional string orgId = 1;</code>
       */
      public com.google.protobuf.ByteString
          getOrgIdBytes() {
        Object ref = orgId_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          orgId_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <pre>
       *组织机构编码
       * </pre>
       *
       * <code>optional string orgId = 1;</code>
       */
      public Builder setOrgId(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        orgId_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *组织机构编码
       * </pre>
       *
       * <code>optional string orgId = 1;</code>
       */
      public Builder clearOrgId() {
        
        orgId_ = getDefaultInstance().getOrgId();
        onChanged();
        return this;
      }
      /**
       * <pre>
       *组织机构编码
       * </pre>
       *
       * <code>optional string orgId = 1;</code>
       */
      public Builder setOrgIdBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        orgId_ = value;
        onChanged();
        return this;
      }

      private Object userName_ = "";
      /**
       * <pre>
       *用户账号
       * </pre>
       *
       * <code>optional string userName = 3;</code>
       */
      public String getUserName() {
        Object ref = userName_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          userName_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <pre>
       *用户账号
       * </pre>
       *
       * <code>optional string userName = 3;</code>
       */
      public com.google.protobuf.ByteString
          getUserNameBytes() {
        Object ref = userName_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          userName_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <pre>
       *用户账号
       * </pre>
       *
       * <code>optional string userName = 3;</code>
       */
      public Builder setUserName(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        userName_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *用户账号
       * </pre>
       *
       * <code>optional string userName = 3;</code>
       */
      public Builder clearUserName() {
        
        userName_ = getDefaultInstance().getUserName();
        onChanged();
        return this;
      }
      /**
       * <pre>
       *用户账号
       * </pre>
       *
       * <code>optional string userName = 3;</code>
       */
      public Builder setUserNameBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        userName_ = value;
        onChanged();
        return this;
      }

      private Object password_ = "";
      /**
       * <pre>
       *密码
       * </pre>
       *
       * <code>optional string password = 4;</code>
       */
      public String getPassword() {
        Object ref = password_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          password_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <pre>
       *密码
       * </pre>
       *
       * <code>optional string password = 4;</code>
       */
      public com.google.protobuf.ByteString
          getPasswordBytes() {
        Object ref = password_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          password_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <pre>
       *密码
       * </pre>
       *
       * <code>optional string password = 4;</code>
       */
      public Builder setPassword(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        password_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *密码
       * </pre>
       *
       * <code>optional string password = 4;</code>
       */
      public Builder clearPassword() {
        
        password_ = getDefaultInstance().getPassword();
        onChanged();
        return this;
      }
      /**
       * <pre>
       *密码
       * </pre>
       *
       * <code>optional string password = 4;</code>
       */
      public Builder setPasswordBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        password_ = value;
        onChanged();
        return this;
      }

      private int deviceType_ ;
      /**
       * <pre>
       *设备类型
       * </pre>
       *
       * <code>optional int32 deviceType = 5;</code>
       */
      public int getDeviceType() {
        return deviceType_;
      }
      /**
       * <pre>
       *设备类型
       * </pre>
       *
       * <code>optional int32 deviceType = 5;</code>
       */
      public Builder setDeviceType(int value) {
        
        deviceType_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *设备类型
       * </pre>
       *
       * <code>optional int32 deviceType = 5;</code>
       */
      public Builder clearDeviceType() {
        
        deviceType_ = 0;
        onChanged();
        return this;
      }

      private Object deviceId_ = "";
      /**
       * <pre>
       *设备唯一标志
       * </pre>
       *
       * <code>optional string deviceId = 6;</code>
       */
      public String getDeviceId() {
        Object ref = deviceId_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          deviceId_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <pre>
       *设备唯一标志
       * </pre>
       *
       * <code>optional string deviceId = 6;</code>
       */
      public com.google.protobuf.ByteString
          getDeviceIdBytes() {
        Object ref = deviceId_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          deviceId_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <pre>
       *设备唯一标志
       * </pre>
       *
       * <code>optional string deviceId = 6;</code>
       */
      public Builder setDeviceId(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        deviceId_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *设备唯一标志
       * </pre>
       *
       * <code>optional string deviceId = 6;</code>
       */
      public Builder clearDeviceId() {
        
        deviceId_ = getDefaultInstance().getDeviceId();
        onChanged();
        return this;
      }
      /**
       * <pre>
       *设备唯一标志
       * </pre>
       *
       * <code>optional string deviceId = 6;</code>
       */
      public Builder setDeviceIdBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        deviceId_ = value;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:im.LoginReq)
    }

    // @@protoc_insertion_point(class_scope:im.LoginReq)
    private static final LoginReq DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new LoginReq();
    }

    public static LoginReq getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<LoginReq>
        PARSER = new com.google.protobuf.AbstractParser<LoginReq>() {
      public LoginReq parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new LoginReq(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<LoginReq> parser() {
      return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<LoginReq> getParserForType() {
      return PARSER;
    }

    public LoginReq getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_im_LoginReq_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_im_LoginReq_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\016LoginReq.proto\022\002im\"c\n\010LoginReq\022\r\n\005orgI" +
      "d\030\001 \001(\t\022\020\n\010userName\030\003 \001(\t\022\020\n\010password\030\004 " +
      "\001(\t\022\022\n\ndeviceType\030\005 \001(\005\022\020\n\010deviceId\030\006 \001(" +
      "\tB,\n\031com.pay.common.im.packetsB\rLoginReq" +
      "ProtoH\001b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_im_LoginReq_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_im_LoginReq_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_im_LoginReq_descriptor,
        new String[] { "OrgId", "UserName", "Password", "DeviceType", "DeviceId", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
