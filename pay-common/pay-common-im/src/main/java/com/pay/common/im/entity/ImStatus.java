package com.pay.common.im.entity;

import com.pay.common.im.constant.Status;

/**
 * 版本: [1.0]
 * 功能说明: 
 * 作者: Lion
 */
public enum ImStatus implements Status {
    //公用部分
	ERROR_SEQ(100, "errorSeq", "同步序列号不能为空"),
	ERROR_COMMAND(101,"unsupported cmd command","不支持的cmd命令!"),
	ERROR_PARAMETER(102, "errorParameter",  "参数错误"),
	ERROR_VERSION(103,"Protocol version number does not match","协议版本号不匹配!"),

	//心跳回复
	HEARTBEAT_OK(0,"心跳回复","ok"),

	//握手回复
	HANDSHAKE_OK(0,"握手回复","ok"),
	HANDSHAKE_ERROR(1,"握手回复错误","ok"),

	//登录部分
	LOGIN_OK(0, "ok", "登录成功"),
	LOGIN_PASSWD_REEOR(1, "passwdError", "用户名或密码错误"),
	LOGIN_ACCOUNT_FREEZE(2, "accountFreeze", "当前账号失效或被冻结"),
	LOGIN_FAIL(3, "fail", "登录失败"),

	//添加好友部分
	FRIEND_SEND_OK(0, "ok", "申请已发送,请等待"),
	FRIEND_ADD_OK(0, "ok", "添加好友成功"),
	FRIEND_EFUSE_OK(0, "ok", "拒绝好友成功"),
	FRIEND_IS_FRIEND(1, "isfriend", "已经是好友了"),

	//创建群组部分
	GROUP_CREATE_OK(0, "ok", "创建群组成功"),
	GROUP_CREATE_REPEAT(1,  "create group name repeat", "群组名称已经被占用"),
	GROUP_EXIT_OK(0, "ok", "退出群組成功"),
	GROUP_EXIT_FAIL(1, "fail", "退出群組失败"),
	GROUP_EXIT_ADMIN(2, "you group manage", "你是群组管理员不能退出"),

	//群组踢人
	GROUP_NOT_PERMISSION(3, "you Permission denied", "你没有对应的权限"),
	GROUP_DELMEMBER_OK(0, "OK", "群组踢人成功"),
	GROUP_INVITATION_OK(0, "Invitation success", "邀请用户加入群组成功"),

	//发送消息部分
	MESSAGE_SEND_OK(0, "ok", "发送消息成功"),
	MESSAGE_SEND_FAIL(1,  "send message fail", "发送消息失败"),

	//撤销消息部分
	MESSAGE_CANCEL_OK(0, "ok", "撤销消息成功"),
	MESSAGE_CANCEL_FAIL(0, "ok", "撤销消息成功");

	private int status;
	private String description;
	private String text;

	private ImStatus(int status, String description, String text) {
		this.status = status;
		this.description = description;
		this.text = text;
	}
	
	public int getStatus() {
		return status;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public int getCode() {
		return this.status;
	}

	@Override
	public String getMsg() {
		return this.getText();
	}
}
