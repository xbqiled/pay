package com.pay.common.im.utils;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;

import java.util.Date;

/**
 * 生成seq
 */
public class SynSeqUtil {

    /**
     * <B>生成规则9 + HHMMSS + 自定义</B>
     * @return
     */
    public static String createSynSeq() {
        String timeIndex = DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN);
        String seq = "1" + timeIndex + RandomUtil.randomInt(0, 999);
        return seq;
    }


    public static String createMessageId() {
        String timeIndex = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
        String seq = "2" + timeIndex + RandomUtil.randomInt(0, 999);
        return seq;
    }

    public static String createTimeMessageId() {
        Long timeIndex = DateUtil.current(Boolean.TRUE);
        return timeIndex + "";
    }

    public static void main(String [] args) {
        System.out.println(SynSeqUtil.createTimeMessageId());
    }
}
