package com.pay.common.im.constant;

public class CodeEnumConstant {

    public enum MSG_TYPE {

        MSG_TYPE_TEXT(0, "文本!"),

        MSG_TYPE_IMG(1, "图片!"),

        MSG_TYPE_VOICE(2, "语音!"),

        MSG_TYPE_VIDEO(3, "视频!"),

        MSG_TYPE_MUSIC(4, "音乐!"),

        MSG_TYPE_NEWS(5, "图文!"),

        MSG_TYPE_PAY(6, "入款请求!"),

        MSG_TYPE_PAY_DESC(7, "入款详情!");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        MSG_TYPE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum IS_FRIEND_CODE {

        YOUR_FRIEND_CODE(1, "他是你好友!"),

        MY_FRIEND_CODE(2, "你是他好友!"),

        UNKNOWN_CODE(3, "未知!");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        IS_FRIEND_CODE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }



    public enum REQUEST_RES_CODE {

        REQUEST_RES_AGREE(0, "同意!"),

        REQUEST_RES_REFUSE(1, "拒绝!"),

        REQUEST_RES_UNKNOWN(2, "未知!");

        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        REQUEST_RES_CODE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum REQUEST_MODE {

        REQUEST_MODE_USER(1, "用户请求!"),

        REQUEST_MODE_GROUP(2, "群组请求!");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        REQUEST_MODE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum REQUEST_STATUS {

        REQUEST_STATUS_WAIT(0, "等待处理!"),

        REQUEST_STATUS_AGREE(1, "已同意!"),

        REQUEST_STATUS_REFUSE(2, "已拒绝!"),

        REQUEST_STATUS_EXPIRE(3, "已过期!");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        REQUEST_STATUS(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }
}
